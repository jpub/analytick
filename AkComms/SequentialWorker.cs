﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AkBase;
using AkMessages;

namespace AkComms
{
    public class SequentialWorker
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger( System.Reflection.MethodBase.GetCurrentMethod().DeclaringType );
        
        private BaseEventHandlerRegistry eventHandlerRegistry = null;
        private MessageQueue<BaseMessage> incomingQueue = null;

        private BackgroundWorker bwSequentialProcessor = new BackgroundWorker();

        public SequentialWorker( BaseEventHandlerRegistry eventHandlerRegistry, MessageQueue<BaseMessage> incomingQueue )
        {
            this.eventHandlerRegistry = eventHandlerRegistry;
            this.incomingQueue = incomingQueue;

            this.bwSequentialProcessor.WorkerReportsProgress = true;
            this.bwSequentialProcessor.WorkerSupportsCancellation = true;
            this.bwSequentialProcessor.DoWork += new DoWorkEventHandler( bwSequentialProcessor_DoWork );
            this.bwSequentialProcessor.ProgressChanged += new ProgressChangedEventHandler( bwSequentialProcessor_ProgressChanged );
            this.bwSequentialProcessor.RunWorkerCompleted += new RunWorkerCompletedEventHandler( bwSequentialProcessor_RunWorkerCompleted );

            if ( this.bwSequentialProcessor.IsBusy != true )
            {
                this.bwSequentialProcessor.RunWorkerAsync();
            }
        }

        #region Sequential Processing Thread
        void bwSequentialProcessor_RunWorkerCompleted( object sender, RunWorkerCompletedEventArgs e )
        {
        }

        void bwSequentialProcessor_ProgressChanged( object sender, ProgressChangedEventArgs e )
        {
            if ( e.ProgressPercentage < 0 )
                return;

            this.eventHandlerRegistry.handle( (BaseMessage)e.UserState );
        }

        void bwSequentialProcessor_DoWork( object sender, DoWorkEventArgs e )
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            while ( true )
            {
                if ( ( worker.CancellationPending == true ) )
                {
                    e.Cancel = true;
                    break;
                }

                BaseMessage message = null;
                this.incomingQueue.get( ref message );
                if ( message == null )
                {
                    log.Fatal( "null message received in sequential processing queue" );
                    break;
                }

                worker.ReportProgress( 0, message );                
            }
        }
        #endregion
    }
}
