﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using System.Net;
using System.IO;

using AkMessages;
using AkBase;

namespace AkOanda
{
    public class OandaStreamApi
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger( System.Reflection.MethodBase.GetCurrentMethod().DeclaringType );

        // Account details
        private string API_SERVER = "";
        private int ACCOUNT_ID = 0;
        private string ACCESS_TOKEN = "";

        private MessageQueue<BaseMessage> incomingQueue = null;
        private BackgroundWorker bwRateFetcher = new BackgroundWorker();
        private BackgroundWorker bwEventFetcher = new BackgroundWorker();

        private FactoryMessage factoryMessage = new FactoryMessage();

        public OandaStreamApi( MessageQueue<BaseMessage> incomingQueue, string apiServer, int accountId, string accessToken )
        {
            this.API_SERVER = apiServer;
            this.ACCOUNT_ID = accountId;
            this.ACCESS_TOKEN = accessToken;

            this.incomingQueue = incomingQueue;
        }

        public void startStreamingRates( string instrument )
        {
            this.bwRateFetcher.WorkerReportsProgress = true;
            this.bwRateFetcher.WorkerSupportsCancellation = true;
            this.bwRateFetcher.DoWork += new DoWorkEventHandler( bwRateFetcher_DoWork );
            this.bwRateFetcher.ProgressChanged += new ProgressChangedEventHandler( bwRateFetcher_ProgressChanged );
            this.bwRateFetcher.RunWorkerCompleted += new RunWorkerCompletedEventHandler( bwRateFetcher_RunWorkerCompleted );

            if ( this.bwRateFetcher.IsBusy != true )
            {
                this.bwRateFetcher.RunWorkerAsync( instrument );
            }
        }        

        public void startStreamingEvents()
        {
            this.bwEventFetcher.WorkerReportsProgress = true;
            this.bwEventFetcher.WorkerSupportsCancellation = true;
            this.bwEventFetcher.DoWork += new DoWorkEventHandler( bwEventFetcher_DoWork );
            this.bwEventFetcher.ProgressChanged += new ProgressChangedEventHandler( bwEventFetcher_ProgressChanged );
            this.bwEventFetcher.RunWorkerCompleted += new RunWorkerCompletedEventHandler( bwEventFetcher_RunWorkerCompleted );

            if ( this.bwEventFetcher.IsBusy != true )
            {
                this.bwEventFetcher.RunWorkerAsync();
            }
        }

        public void abortAPI()
        {
            this.bwRateFetcher.CancelAsync();
            this.bwEventFetcher.CancelAsync();
        }

        #region Rates Streaming
        void bwRateFetcher_DoWork( object sender, DoWorkEventArgs e )
        {
            string instrument = (string)e.Argument;
            BackgroundWorker bw = (BackgroundWorker)sender;
            string progressStr = "";
            string requestString = "https://stream-fxtrade.oanda.com/v1/prices?accountId=" + this.ACCOUNT_ID + "&instruments=" + instrument;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create( requestString );
            request.KeepAlive = true;

            // for non-sandbox requests
            request.Headers.Add( "Authorization", "Bearer " + this.ACCESS_TOKEN );
            request.Method = "GET";

            try
            {
                using ( var response = request.GetResponse() )
                {
                    using ( var reader = new StreamReader( response.GetResponseStream() ) )
                    {
                        JavaScriptSerializer serializer = new JavaScriptSerializer();

                        string tmp = "";
                        while ( ( tmp = reader.ReadLine() ) != null )
                        {
                            BaseMessage message = null;
                            StreamResponse streamResponse = serializer.Deserialize<StreamResponse>( tmp );
                            if ( streamResponse.tick != null )
                            {
                                message = this.factoryMessage.getMessage( BaseMessage.ID_TICK, tmp );
                                this.incomingQueue.add( message );
                            }
                            else if ( streamResponse.heartbeat != null )
                            {
                                message = this.factoryMessage.getMessage( BaseMessage.ID_HEARTBEAT_TICK, tmp );
                                this.incomingQueue.add( message );
                            }
                            else if ( streamResponse.disconnect != null )
                            {
                                e.Cancel = true;
                                message = this.factoryMessage.getMessage( BaseMessage.ID_DISCONNECTED_TICK, tmp );
                                this.incomingQueue.add( message );
                                return;
                            }
                        }
                    }
                }
            }
            catch ( WebException ex )
            {
                e.Cancel = true;
                e.Result = ex.Message;
                progressStr = ex.Message;
                bw.ReportProgress( -1, progressStr );
            }
        }

        void bwRateFetcher_ProgressChanged( object sender, ProgressChangedEventArgs e )
        {
            if ( e.ProgressPercentage == 0 )
            {
                MessageProgressFeedback mpf = new MessageProgressFeedback();
                mpf.stopProgram = true;
                mpf.information = (string)e.UserState;
                this.incomingQueue.add( mpf );
            }
        }

        void bwRateFetcher_RunWorkerCompleted( object sender, RunWorkerCompletedEventArgs e )
        {
            log.Error( "Oanda Stream Rate API stopped." );
        }
        #endregion

        #region Events Streaming
        void bwEventFetcher_DoWork( object sender, DoWorkEventArgs e )
        {
            BackgroundWorker bw = (BackgroundWorker)sender;
            string progressStr = "";
            string requestString = "https://stream-fxtrade.oanda.com/v1/events?accountIds=" + this.ACCOUNT_ID;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create( requestString );
            request.KeepAlive = true;

            // for non-sandbox requests
            request.Headers.Add( "Authorization", "Bearer " + this.ACCESS_TOKEN );
            request.Method = "GET";

            try
            {
                using ( var response = request.GetResponse() )
                {
                    using ( var reader = new StreamReader( response.GetResponseStream() ) )
                    {
                        JavaScriptSerializer serializer = new JavaScriptSerializer();

                        string tmp = "";
                        while ( ( tmp = reader.ReadLine() ) != null )
                        {
                            if ( bw.CancellationPending == true )
                            {
                                e.Cancel = true;
                                break;
                            }

                            BaseMessage message = null;
                            StreamResponse streamResponse = serializer.Deserialize<StreamResponse>( tmp );
                            if ( streamResponse.transaction != null )
                            {
                                message = this.factoryMessage.getMessage( BaseMessage.ID_EVENT, tmp );
                                this.incomingQueue.add( message );
                            }
                            else if ( streamResponse.heartbeat != null )
                            {
                                message = this.factoryMessage.getMessage( BaseMessage.ID_HEARTBEAT_EVENT, tmp );
                                this.incomingQueue.add( message );
                            }
                            else if ( streamResponse.disconnect != null )
                            {
                                e.Cancel = true;
                                message = this.factoryMessage.getMessage( BaseMessage.ID_DISCONNECTED_EVENT, tmp );
                                this.incomingQueue.add( message );
                                return;
                            }
                        }
                    }
                }
            }
            catch ( WebException ex )
            {
                e.Cancel = true;
                e.Result = ex.Message;
                progressStr = ex.Message;
                bw.ReportProgress( -1, progressStr );
            }
        }

        void bwEventFetcher_ProgressChanged( object sender, ProgressChangedEventArgs e )
        {
            if ( e.ProgressPercentage < 0 )
            {
                MessageProgressFeedback mpf = new MessageProgressFeedback();
                mpf.stopProgram = true;
                mpf.information = (string)e.UserState;
                this.incomingQueue.add( mpf );
            }
        }

        void bwEventFetcher_RunWorkerCompleted( object sender, RunWorkerCompletedEventArgs e )
        {
            log.Error( "Oanda Stream Event API stopped." );
        }
        #endregion
    }
}
