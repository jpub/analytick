﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using System.Net;
using System.IO;

using AkBase;
using AkMessages;

namespace AkOanda
{
    public class OandaStaticApi
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger( System.Reflection.MethodBase.GetCurrentMethod().DeclaringType );

        // Account details
        private string API_SERVER = "";
        private int ACCOUNT_ID = 0;
        private string ACCESS_TOKEN = "";

        protected MessageQueue<BaseMessage> incomingQueue = null;
        private MessageQueue<BaseOandaMessage> internalQueue = new MessageQueue<BaseOandaMessage>();
        private FactoryMessage factoryMessage = new FactoryMessage();

        private BackgroundWorker bwStaticRequest = new BackgroundWorker();

        public OandaStaticApi( MessageQueue<BaseMessage> incomingQueue, string apiServer, int accountId, string accessToken )
        {
            this.API_SERVER = apiServer;
            this.ACCOUNT_ID = accountId;
            this.ACCESS_TOKEN = accessToken;

            this.incomingQueue = incomingQueue;

            this.bwStaticRequest.WorkerReportsProgress = true;
            this.bwStaticRequest.WorkerSupportsCancellation = true;
            this.bwStaticRequest.DoWork += new DoWorkEventHandler( bwStaticRequest_DoWork );
            this.bwStaticRequest.ProgressChanged += new ProgressChangedEventHandler( bwStaticRequest_ProgressChanged );
            this.bwStaticRequest.RunWorkerCompleted += new RunWorkerCompletedEventHandler( bwStaticRequest_RunWorkerCompleted );

            if ( this.bwStaticRequest.IsBusy != true )
            {
                this.bwStaticRequest.RunWorkerAsync();
            }
        }

        public void abortAPI()
        {
            this.bwStaticRequest.CancelAsync();
        }

        public void getAccountDetails()
        {
            log.Debug( "Live getAccountDetails" );
            MessageStaticAccountDetails message = new MessageStaticAccountDetails();
            message.RequestString = this.API_SERVER + "v1/accounts/" + this.ACCOUNT_ID;
            message.Method = "GET";
            message.PostData = null;
            this.internalQueue.add( message );
        }

        public void getListOfOpenTrades()
        {
            log.Debug( "Live getListOfOpenTrades" );
            MessageStaticOpenTradeList message = new MessageStaticOpenTradeList();
            message.RequestString = this.API_SERVER + "v1/accounts/" + this.ACCOUNT_ID + "/trades";
            message.Method = "GET";
            message.PostData = null;
            this.internalQueue.add( message );
        }

        public void getCurrentPrices( string instrument )
        {
            log.Debug( "Live getCurrentPrices" );
            MessageStaticCurrentPrices message = new MessageStaticCurrentPrices();
            message.RequestString = this.API_SERVER + "v1/prices?instruments=" + instrument;
            message.Method = "GET";
            message.PostData = null;
            this.internalQueue.add( message );
        }

        public void getHistoricalCandles( string instrument, int count, DateTime startTime, string granularity )
        {
            log.Debug( "Live getCandleList" );
            string requestString = string.Format( "{0}v1/candles?instrument={1}&count={2}&start={3}-{4}-{5}T{6}%3A{7}%3A{8}Z&candleFormat=midpoint&granularity={9}&dailyAlignment=0&alignmentTimezone=Singapore",
                this.API_SERVER,
                instrument,
                count.ToString(),
                startTime.ToString( "yyyy" ),
                startTime.ToString( "MM" ),
                startTime.ToString( "dd" ),
                startTime.ToString( "HH" ),
                startTime.ToString( "mm" ),
                startTime.ToString( "ss" ),
                granularity );
            
            MessageStaticCandleList message = new MessageStaticCandleList();
            message.RequestString = requestString;
            message.Method = "GET";
            message.PostData = null;
            this.internalQueue.add( message );
        }

        public void closeAnExistingPosition( string instrument )
        {
            log.Debug( "Live closeAnExistingPosition" );
            MessageStaticClosePosition message = new MessageStaticClosePosition();
            message.RequestString = this.API_SERVER + "/v1/accounts/" + this.ACCOUNT_ID + "/positions/" + instrument;
            message.Method = "DELETE";
            message.PostData = null;
            this.internalQueue.add( message );
        }

        public void createNewOrder( string instrument, uint tradeUnits, string side, double stopLossPrice, double takeProfitPrice, double price )
        {
            log.Debug( "Live createNewOrder attempPrice:" + price );
            MessageStaticCreateNewOrder message = new MessageStaticCreateNewOrder();
            message.RequestString = this.API_SERVER + "v1/accounts/" + this.ACCOUNT_ID + "/orders";
            message.Method = "POST";
            if ( stopLossPrice == 0 && takeProfitPrice == 0 )
                message.PostData = "instrument=" + instrument + "&units=" + tradeUnits + "&side=" + side + "&type=market";
            else
                message.PostData = "instrument=" + instrument + "&units=" + tradeUnits + "&side=" + side + "&type=market" + "&stopLoss=" + stopLossPrice + "&takeProfit=" + takeProfitPrice;
            this.internalQueue.add( message );
        }

        public void modifyAnExistingTrade( long tradeId, double stopLossPrice, double takeProfitPrice )
        {
            log.Debug( "modifyAnExistingTrade" );
            MessageStaticModifyExistingTrade message = new MessageStaticModifyExistingTrade();
            message.RequestString = this.API_SERVER + "v1/accounts/" + this.ACCOUNT_ID + "/trades/" + tradeId;
            message.Method = "PATCH";
            message.PostData = "stopLoss=" + stopLossPrice + "&takeProfit=" + takeProfitPrice;
            this.internalQueue.add( message );
        }

        #region Threaded static request
        void bwStaticRequest_DoWork( object sender, DoWorkEventArgs e )
        {
            BackgroundWorker bw = (BackgroundWorker)sender;
            while ( true )
            {
                BaseOandaMessage outgoingOandaMessage = null;
                this.internalQueue.get( ref outgoingOandaMessage );
                if ( outgoingOandaMessage == null )
                {
                    bw.ReportProgress( -1, "Invalid internal Oanda message" );
                    break;
                }

                if ( bw.CancellationPending == true )
                {
                    e.Cancel = true;
                    break;
                }

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create( outgoingOandaMessage.RequestString );
                request.KeepAlive = true;


                // for non-sandbox requests
                request.Headers.Add( "Authorization", "Bearer " + this.ACCESS_TOKEN );

                request.Method = outgoingOandaMessage.Method;
                try
                { 
                    if ( outgoingOandaMessage.Method == "POST" || outgoingOandaMessage.Method == "PATCH" )
                    {
                        var data = Encoding.UTF8.GetBytes( outgoingOandaMessage.PostData );
                        //this is needed for the limit order expiry time
                        //http://developer.oanda.com/rest-live/development-guide/
                        request.Headers.Add( "X-Accept-Datetime-Format: UNIX" );
                        request.ContentType = "application/x-www-form-urlencoded";
                        request.ContentLength = data.Length;

                        using ( var stream = request.GetRequestStream() )
                        {
                            stream.Write( data, 0, data.Length );
                        }
                    }

                    using ( var response = request.GetResponse() )
                    {
                        using ( var reader = new StreamReader( response.GetResponseStream() ) )
                        {                            
                            string responseString = reader.ReadToEnd().Trim();
                            BaseMessage incomingOandaMessage = this.factoryMessage.getMessage( outgoingOandaMessage.MessageType, responseString );  
                            if ( incomingOandaMessage != null )
                                this.incomingQueue.add( incomingOandaMessage );
                        }
                    }
                }
                catch ( WebException ex )
                {
                    bw.ReportProgress( -1, ex.Message );
                    break;
                }
            }
        }

        void bwStaticRequest_ProgressChanged( object sender, ProgressChangedEventArgs e )
        {
            if ( e.ProgressPercentage < 0 )
            {
                MessageProgressFeedback mpf = new MessageProgressFeedback();
                mpf.stopProgram = true;
                mpf.information = (string)e.UserState;
                this.incomingQueue.add( mpf );
            }
        }

        void bwStaticRequest_RunWorkerCompleted( object sender, RunWorkerCompletedEventArgs e )
        {
            log.Error( "Oanda Static API stopped." );
        }
        #endregion
    }
}
