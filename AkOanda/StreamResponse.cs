﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AkMessages;

namespace AkOanda
{
    public class StreamResponse
    {
        public DataTick tick { get; set; }
        public DataHeartbeat heartbeat { get; set; }
        public DataDisconnect disconnect { get; set; }
        public DataTransaction transaction { get; set; }
    }
}
