﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkAlgo
{
    public class AlgoRsi
    {
        public static double UNDEFINE_VALUE = double.MaxValue;

        private int period = int.MaxValue;
        private int totalDataPoint = 0;
        private double prevDataPoint = UNDEFINE_VALUE;
        private double averageGain = 0;
        private double averageLoss = 0;

        public AlgoRsi( int period )
        {
            this.period = period;
            this.totalDataPoint = 0;            
        }

        public bool addDataPoint( double dataPoint, out double weightedPoint )
        {
            weightedPoint = UNDEFINE_VALUE;

            if ( this.prevDataPoint == UNDEFINE_VALUE )
            {
                this.prevDataPoint = dataPoint;                
                return false;
            }

            if ( this.totalDataPoint < this.period - 1 )
            {
                if ( this.prevDataPoint < dataPoint )
                    this.averageGain += ( dataPoint - this.prevDataPoint );
                else
                    this.averageLoss += ( this.prevDataPoint - dataPoint );

                ++this.totalDataPoint;
                this.prevDataPoint = dataPoint;
                return false;
            }

            if ( this.totalDataPoint == this.period - 1 )
            {
                if ( this.prevDataPoint < dataPoint )
                    this.averageGain += ( dataPoint - this.prevDataPoint );
                else
                    this.averageLoss += ( this.prevDataPoint - dataPoint );

                ++this.totalDataPoint;
                this.averageGain /= this.period;
                this.averageLoss /= this.period;
            }
            else
            {
                ++this.totalDataPoint;
                if ( this.prevDataPoint < dataPoint )
                {
                    this.averageGain = ( this.averageGain * ( this.period - 1 ) + ( dataPoint - this.prevDataPoint ) ) / this.period;
                    this.averageLoss = ( this.averageLoss * ( this.period - 1 ) ) / this.period;
                }
                else
                {
                    this.averageGain = ( this.averageGain * ( this.period - 1 ) ) / this.period;
                    this.averageLoss = ( this.averageLoss * ( this.period - 1 ) + ( this.prevDataPoint - dataPoint ) ) / this.period;                    
                }
            }

            if ( this.averageLoss != 0 )
            {
                weightedPoint = 100 - ( 100 / ( 1 + ( this.averageGain / this.averageLoss ) ) );
            }
            else
            {
                weightedPoint = 100;
            }

            this.prevDataPoint = dataPoint;
            return true;
        }
    }
}
