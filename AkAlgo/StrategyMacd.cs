﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AkBase;
using AkMessages;
using AkCommon;
using AkModel;
using AkDatabase;

namespace AkAlgo
{
    public class StrategyMacd : Strategy
    {        
        private Dictionary<int, ModelLineGraph.LineData> ldMacd = null;
        private Dictionary<int, ModelLineGraph.LineData> ldSignal = null;        

        private bool crossOverDetected = false;
        private bool isMacdAboveSignal = false;

        public double MacdThreshold { get; private set; }

        public StrategyMacd( bool isLive, MessageQueue<BaseMessage> messageQueue, double pipTakeProfit, double pipCutLoss, double macdThreshold )
            : base( isLive, messageQueue, pipTakeProfit, pipCutLoss )
        {
            this.MacdThreshold = macdThreshold;            
            
            ModelLineGraph model = null;
            DbLineGraph.getInstance().get( GlobalConstant.GRAPH_NAME_LINE_MACD, out model );
            this.ldMacd = model.getCopyListLineData().ToDictionary( v => v.index, v => v );

            DbLineGraph.getInstance().get( GlobalConstant.GRAPH_NAME_LINE_MACD_SIGNAL, out model );
            this.ldSignal = model.getCopyListLineData().ToDictionary( v => v.index, v => v );
        }

        public override string getStrategyRemarks()
        {
            return string.Format( "MACD: {0} {1} {2}", this.pipTakeProfit, this.pipStopLoss, this.MacdThreshold );
        }

        public override void preprocessing( ModelCandleGraph.CandleData cd )
        {
            int candleIndex = cd.index;
            double macdValue = this.ldMacd[candleIndex].yValue;
            double signalValue = this.ldSignal[candleIndex].yValue;

            if ( macdValue > signalValue )
            {
                if ( !this.isMacdAboveSignal )
                {
                    if ( this.transactionState == EnumTransactionState.WAITING_FOR_ENTRY )
                        this.crossOverDetected = true;
                }
                this.isMacdAboveSignal = true;
            }
            else
            {
                if ( this.isMacdAboveSignal )
                {
                    if ( this.transactionState == EnumTransactionState.WAITING_FOR_ENTRY )
                        this.crossOverDetected = true;
                }
                this.isMacdAboveSignal = false;
            }
        }

        public override bool checkForEntry( ModelCandleGraph.CandleData cd, out ModelTransaction.EnumTransactType transactType )
        {
            int candleIndex = cd.index;
            double macdValue = this.ldMacd[candleIndex].yValue;
            double signalValue = this.ldSignal[candleIndex].yValue;
            double tmd = macdValue - signalValue;

            transactType = ModelTransaction.EnumTransactType.UNDEFINE;
            if ( signalValue == AlgoMacd.UNDEFINE_VALUE )
                return false;

            if ( this.crossOverDetected )
            {
                if ( ( macdValue - signalValue ) > this.MacdThreshold )
                {
                    transactType = ModelTransaction.EnumTransactType.LONG;
                    this.crossOverDetected = false;
                    return true;
                }
                else if ( ( macdValue - signalValue ) < ( -this.MacdThreshold ) )
                {
                    transactType = ModelTransaction.EnumTransactType.SHORT;
                    this.crossOverDetected = false;
                    return true;
                }
            }
            return false;
        }        
    }
}

