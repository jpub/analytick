﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AkBase;
using AkMessages;
using AkCommon;
using AkModel;
using AkDatabase;

namespace AkAlgo
{
    public class StrategyEmaMacd : Strategy
    {
        private StrategyEma stEma = null;
        private StrategyMacd stMacd = null;

        public StrategyEmaMacd( bool isLive, MessageQueue<BaseMessage> messageQueue, double pipTakeProfit, double pipCutLoss, double emaThresholdFastMedium, double emaThresholdMediumSlow, double macdThreshold )
            : base( isLive, messageQueue, pipTakeProfit, pipCutLoss )
        {
            this.stEma = new StrategyEma( isLive, messageQueue, pipTakeProfit, pipCutLoss, emaThresholdFastMedium, emaThresholdMediumSlow );
            this.stMacd = new StrategyMacd( isLive, messageQueue, pipTakeProfit, pipCutLoss, macdThreshold );
        }

        public override string getStrategyRemarks()
        {
            return string.Format( "EMA+MACD: {0} {1} {2} {3} {4}", this.pipTakeProfit, this.pipStopLoss, stEma.EmaThresholdFastMedium, stEma.EmaThresholdMediumSlow, stMacd.MacdThreshold );
        }

        public override void preprocessing( ModelCandleGraph.CandleData cd )
        {
            this.stEma.preprocessing( cd );
            this.stMacd.preprocessing( cd );
        }

        public override bool checkForEntry( ModelCandleGraph.CandleData cd, out ModelTransaction.EnumTransactType transactType )
        {
            transactType = ModelTransaction.EnumTransactType.UNDEFINE;
            ModelTransaction.EnumTransactType emaTransactType = ModelTransaction.EnumTransactType.UNDEFINE;
            ModelTransaction.EnumTransactType macdTransactType = ModelTransaction.EnumTransactType.UNDEFINE;

            if ( this.stEma.checkForEntry( cd, out emaTransactType ) && this.stMacd.checkForEntry( cd, out macdTransactType ) )
            {
                // this should not happen as both indicator should point to the same direction
                if ( emaTransactType != macdTransactType )
                    return false;

                transactType = emaTransactType;
                return true;
            }            
            return false;
        }        
    }
}

