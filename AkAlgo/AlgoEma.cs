﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkAlgo
{
    public class AlgoEma
    {        
        /*
         * EMA(n) = P(n) * 2/(T+1)   +   EMA(n-1) * ( 1 - 2/(T+1) )
         */
        public static double UNDEFINE_VALUE = double.MaxValue;

        private int period = int.MaxValue;
        private double weightMultiplier = UNDEFINE_VALUE;
        private double previousEma = UNDEFINE_VALUE;

        private List<double> firstAverageList = new List<double>();

        public AlgoEma( int period )
        {
            this.period = period;
            this.weightMultiplier = 2.0 / ( this.period + 1 );                        
        }

        public bool addDataPoint( double dataPoint, out double weightedPoint )
        {
            if ( this.previousEma == UNDEFINE_VALUE )
            {
                this.firstAverageList.Add( dataPoint );
                if ( this.firstAverageList.Count == this.period )
                {
                    double total = 0;
                    foreach ( double dp in this.firstAverageList )
                        total += dp;

                    this.previousEma = total / this.period;
                    weightedPoint = this.previousEma;
                    return true;
                }

                weightedPoint = UNDEFINE_VALUE;
                return false;
            }

            weightedPoint = ( ( dataPoint - this.previousEma ) * this.weightMultiplier ) + this.previousEma;
            this.previousEma = weightedPoint;
            return true;
        }
    }
}
