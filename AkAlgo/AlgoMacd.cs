﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkAlgo
{
    public class AlgoMacd
    {
        public static double UNDEFINE_VALUE = double.MaxValue;
        private AlgoEma emaFast = null;
        private AlgoEma emaSlow = null;
        private AlgoEma emaSignal = null;

        public AlgoMacd( int periodEmaFast, int periodEmaSlow, int periodEmaSignal )
        {
            this.emaFast = new AlgoEma( periodEmaFast );
            this.emaSlow = new AlgoEma( periodEmaSlow );
            this.emaSignal = new AlgoEma( periodEmaSignal );
        }

        public bool addDataPoint( double dataPoint, out double macdPoint, out double signalPoint )
        {
            double emaFastPoint;
            double emaSlowPoint;            

            this.emaFast.addDataPoint( dataPoint, out emaFastPoint );
            this.emaSlow.addDataPoint( dataPoint, out emaSlowPoint );
            if ( emaSlowPoint == AlgoEma.UNDEFINE_VALUE )
            {
                macdPoint = UNDEFINE_VALUE;
                signalPoint = UNDEFINE_VALUE;
                return false;
            }
            else
            {
                macdPoint = emaFastPoint - emaSlowPoint;
                this.emaSignal.addDataPoint( macdPoint, out signalPoint );
                return true;
            }
        }
    }
}
