﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AkBase;
using AkModel;
using AkCommon;
using AkDatabase;
using AkMessages;

namespace AkAlgo
{
    abstract public class Strategy
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger( System.Reflection.MethodBase.GetCurrentMethod().DeclaringType );

        public enum EnumTransactionState
        {
            WAITING_FOR_ENTRY = 0,
            READY_FOR_ENTRY = 1,
            WAITING_FOR_EXIT = 2
        }

        protected bool isLive = false;        
        protected MessageQueue<BaseMessage> messageQueue = null;
        protected EnumTransactionState transactionState;
        protected ModelTransaction modelTransaction = null;
        protected ModelTransaction.TransactionData fullTransaction;
        protected double pipTakeProfit;
        protected double pipStopLoss;

        protected Strategy( bool isLive, MessageQueue<BaseMessage> messageQueue, double pipTakeProfit, double pipStopLoss )
        {
            this.isLive = isLive;
            this.messageQueue = messageQueue;
            this.pipTakeProfit = pipTakeProfit;
            this.pipStopLoss = pipStopLoss;
            this.transactionState = EnumTransactionState.WAITING_FOR_ENTRY;
            this.modelTransaction = new ModelTransaction();
            this.fullTransaction = new ModelTransaction.TransactionData();            
        }

        abstract public string getStrategyRemarks();
        abstract public bool checkForEntry( ModelCandleGraph.CandleData cd, out ModelTransaction.EnumTransactType transactType );
        abstract public void preprocessing( ModelCandleGraph.CandleData cd );        

        public ModelTransaction getCompletedTransaction()
        {
            this.modelTransaction.Remarks = this.getStrategyRemarks();
            return this.modelTransaction;
        }

        #region MgrOanda notification updates
        public bool incomingTick( MessageStreamTick tickMessage )
        {
            if ( this.transactionState == EnumTransactionState.READY_FOR_ENTRY )
            {
                // change the state imeediately..rather not trade then keep sending message while waiting of the callback confirmation
                this.transactionState = EnumTransactionState.WAITING_FOR_EXIT;

                MessageStrategyCreateNewOrder message = new MessageStrategyCreateNewOrder();
                message.instrument = GlobalConstant.INSTRUMENT;
                message.side = this.fullTransaction.transactType == ModelTransaction.EnumTransactType.LONG ? "buy" : "sell";
                message.price = 0;
                message.units = GlobalConstant.STRATEGY_TRADE_UNITS;
                this.messageQueue.add( message );
                return true;
            }
            return false;
        }

        public bool incomingCandle( ModelCandleGraph.CandleData cd )
        {
            this.preprocessing( cd );

            if ( this.transactionState == EnumTransactionState.WAITING_FOR_ENTRY )
            {
                ModelTransaction.EnumTransactType transactType = ModelTransaction.EnumTransactType.UNDEFINE;
                if ( this.checkForEntry( cd, out transactType ) )
                {
                    this.transactionState = EnumTransactionState.READY_FOR_ENTRY;
                    this.fullTransaction.transactType = transactType;
                }
            }
            else if ( this.transactionState == EnumTransactionState.READY_FOR_ENTRY )
            {
                if ( !this.isLive )
                    return this.attemptEntry( cd );
            }
            else if ( this.transactionState == EnumTransactionState.WAITING_FOR_EXIT )
            {
                if ( !this.isLive )
                    return this.checkForExit( cd );
            }
            return false;
        }

        public void incomingTradeUpdate( MessageStaticCreateNewOrder message )
        {
            this.fullTransaction.transactType = message.tradeOpened.side == "buy" ? ModelTransaction.EnumTransactType.LONG : ModelTransaction.EnumTransactType.SHORT;
            this.fullTransaction.transactOutcome = ModelTransaction.EnumTransactOutcome.UNDEFINE;
            this.fullTransaction.entryCandleIndex = 0;
            this.fullTransaction.exitCandleIndex = 0;
            this.fullTransaction.entryPrice = message.price;
            this.fullTransaction.exitPrice = 0;
            this.fullTransaction.takeProfitPrice = message.tradeOpened.takeProfit;
            this.fullTransaction.stopLossPrice = message.tradeOpened.stopLoss;

            // not using Oanda message time..apparently that field is not according to their REST api
            this.fullTransaction.dtStartSecSince1970 = (int)AkUtil.getSecondsSince1970( DateTime.Now );
            this.fullTransaction.dtEndSecSince1970 = 0;
            this.fullTransaction.units = message.tradeOpened.units;
            this.fullTransaction.pl = 0;
        }
        
        public void incomingTradeUpdate( MessageStaticModifyExistingTrade message )
        {
            this.fullTransaction.takeProfitPrice = message.takeProfit;
            this.fullTransaction.stopLossPrice = message.stopLoss;
        }

        public void incomingTradeClose( MessageStreamEvent message )
        {
            this.fullTransaction.transactOutcome = message.transaction.pl >= 0 ? ModelTransaction.EnumTransactOutcome.PROFIT : ModelTransaction.EnumTransactOutcome.LOSS;
            this.fullTransaction.exitPrice = message.transaction.price;
            this.fullTransaction.dtEndSecSince1970 = (int)AkUtil.getSecondsSince1970( message.transaction.time );
            this.fullTransaction.pl = message.transaction.pl;            

            this.modelTransaction.add( this.fullTransaction );
            this.checkForCompletedTransaction( true );
        }

        public void incomingTakeProfit( MessageStreamEvent message )
        {
            this.fullTransaction.transactOutcome = ModelTransaction.EnumTransactOutcome.PROFIT;
            this.fullTransaction.exitPrice = message.transaction.price;
            this.fullTransaction.dtEndSecSince1970 = (int)AkUtil.getSecondsSince1970( message.transaction.time );
            this.fullTransaction.pl = message.transaction.pl;

            this.modelTransaction.add( this.fullTransaction );
            this.checkForCompletedTransaction( true );
        }

        public void incomingStopLoss( MessageStreamEvent message )
        {
            this.fullTransaction.transactOutcome = ModelTransaction.EnumTransactOutcome.LOSS;
            this.fullTransaction.exitPrice = message.transaction.price;
            this.fullTransaction.dtEndSecSince1970 = (int)AkUtil.getSecondsSince1970( message.transaction.time );
            this.fullTransaction.pl = message.transaction.pl;

            this.modelTransaction.add( this.fullTransaction );
            this.checkForCompletedTransaction( true );
        }
        #endregion

        private bool attemptEntry( ModelCandleGraph.CandleData cd )
        {
            bool status = false;
            this.fullTransaction.entryCandleIndex = cd.index;
            this.fullTransaction.exitCandleIndex = cd.index;

            if ( this.fullTransaction.transactType == ModelTransaction.EnumTransactType.LONG )
            {
                this.fullTransaction.entryPrice = cd.openMid;
                this.fullTransaction.takeProfitPrice = this.fullTransaction.entryPrice + GlobalConstant.ONE_PIP_PRICE * this.pipTakeProfit;
                this.fullTransaction.stopLossPrice = this.fullTransaction.entryPrice - GlobalConstant.ONE_PIP_PRICE * this.pipStopLoss;

                status = this.checkForExitLongTrade( cd );
            }
            else if ( this.fullTransaction.transactType == ModelTransaction.EnumTransactType.SHORT )
            {
                this.fullTransaction.entryPrice = cd.openMid;
                this.fullTransaction.takeProfitPrice = this.fullTransaction.entryPrice - GlobalConstant.ONE_PIP_PRICE * this.pipTakeProfit;
                this.fullTransaction.stopLossPrice = this.fullTransaction.entryPrice + GlobalConstant.ONE_PIP_PRICE * this.pipStopLoss;

                status = this.checkForExitShortTrade( cd );
            }

            return this.checkForCompletedTransaction( status );
        }

        private bool checkForExit( ModelCandleGraph.CandleData cd )
        {
            bool status = false;
            this.fullTransaction.exitCandleIndex = cd.index;
            if ( this.fullTransaction.transactType == ModelTransaction.EnumTransactType.LONG )
                status = this.checkForExitLongTrade( cd );
            else if ( this.fullTransaction.transactType == ModelTransaction.EnumTransactType.SHORT )
                status = this.checkForExitShortTrade( cd );

            return this.checkForCompletedTransaction( status );
        }

        private bool checkForExitLongTrade( ModelCandleGraph.CandleData cd )
        {
            // ambiguos..treat it as loss
            if ( cd.highMid >= this.fullTransaction.takeProfitPrice && cd.lowMid <= this.fullTransaction.stopLossPrice )
            {
                return this.solveLongUsingMinuteCandle( cd );
            }
            else if ( cd.highMid >= this.fullTransaction.takeProfitPrice )
            {
                this.fullTransaction.transactOutcome = ModelTransaction.EnumTransactOutcome.PROFIT;
                this.fullTransaction.exitPrice = this.fullTransaction.takeProfitPrice;
                this.modelTransaction.add( this.fullTransaction );
                return true;
            }
            else if ( cd.lowMid <= this.fullTransaction.stopLossPrice )
            {
                this.fullTransaction.transactOutcome = ModelTransaction.EnumTransactOutcome.LOSS;
                this.fullTransaction.exitPrice = this.fullTransaction.stopLossPrice;
                this.modelTransaction.add( this.fullTransaction );
                return true;
            }
            return false;
        }

        private bool checkForExitShortTrade( ModelCandleGraph.CandleData cd )
        {
            if ( cd.lowMid <= this.fullTransaction.takeProfitPrice && cd.highMid >= this.fullTransaction.stopLossPrice )
            {
                return this.solveShortUsingMinuteCandle( cd );
            }
            else if ( cd.lowMid <= this.fullTransaction.takeProfitPrice )
            {
                this.fullTransaction.transactOutcome = ModelTransaction.EnumTransactOutcome.PROFIT;
                this.fullTransaction.exitPrice = this.fullTransaction.takeProfitPrice;
                this.modelTransaction.add( this.fullTransaction );
                return true;
            }
            else if ( cd.highMid >= this.fullTransaction.stopLossPrice )
            {
                this.fullTransaction.transactOutcome = ModelTransaction.EnumTransactOutcome.LOSS;
                this.fullTransaction.exitPrice = this.fullTransaction.stopLossPrice;
                this.modelTransaction.add( this.fullTransaction );
                return true;
            }
            return false;
        }

        private bool solveLongUsingMinuteCandle( ModelCandleGraph.CandleData cd )
        {
            DateTime hourCandleDt = new DateTime( cd.xValue );
            List<DbOanda.HistoryCandle> historyMinuteCandleList = DbOanda.getInstance().getHistoryMinuteCandleSince( hourCandleDt, hourCandleDt.AddHours( 1 ) );

            foreach ( DbOanda.HistoryCandle hc in historyMinuteCandleList )
            {
                if ( hc.highMid >= this.fullTransaction.takeProfitPrice && hc.lowMid <= this.fullTransaction.stopLossPrice )
                {
                    this.fullTransaction.transactOutcome = ModelTransaction.EnumTransactOutcome.UNRESOLVE;
                    this.fullTransaction.exitPrice = this.fullTransaction.stopLossPrice;
                    this.modelTransaction.add( this.fullTransaction );
                    return true;
                }
                else if ( hc.highMid >= this.fullTransaction.takeProfitPrice )
                {
                    this.fullTransaction.transactOutcome = ModelTransaction.EnumTransactOutcome.PROFIT;
                    this.fullTransaction.exitPrice = this.fullTransaction.takeProfitPrice;
                    this.modelTransaction.add( this.fullTransaction );
                    return true;
                }
                else if ( hc.lowMid <= this.fullTransaction.stopLossPrice )
                {
                    this.fullTransaction.transactOutcome = ModelTransaction.EnumTransactOutcome.LOSS;
                    this.fullTransaction.exitPrice = this.fullTransaction.stopLossPrice;
                    this.modelTransaction.add( this.fullTransaction );
                    return true;
                }
            }

            // just in case we cannot find the minute candle
            this.fullTransaction.transactOutcome = ModelTransaction.EnumTransactOutcome.UNRESOLVE;
            this.fullTransaction.exitPrice = this.fullTransaction.stopLossPrice;
            this.modelTransaction.add( this.fullTransaction );
            return true;
        }

        private bool solveShortUsingMinuteCandle( ModelCandleGraph.CandleData cd )
        {
            DateTime hourCandleDt = new DateTime( cd.xValue );
            List<DbOanda.HistoryCandle> historyMinuteCandleList = DbOanda.getInstance().getHistoryMinuteCandleSince( hourCandleDt, hourCandleDt.AddHours( 1 ) );

            foreach ( DbOanda.HistoryCandle hc in historyMinuteCandleList )
            {
                if ( hc.lowMid <= this.fullTransaction.takeProfitPrice && hc.highMid >= this.fullTransaction.stopLossPrice )
                {
                    this.fullTransaction.transactOutcome = ModelTransaction.EnumTransactOutcome.UNRESOLVE;
                    this.fullTransaction.exitPrice = this.fullTransaction.stopLossPrice;
                    this.modelTransaction.add( this.fullTransaction );
                    return true;
                }
                else if ( hc.lowMid <= this.fullTransaction.takeProfitPrice )
                {
                    this.fullTransaction.transactOutcome = ModelTransaction.EnumTransactOutcome.PROFIT;
                    this.fullTransaction.exitPrice = this.fullTransaction.takeProfitPrice;
                    this.modelTransaction.add( this.fullTransaction );
                    return true;
                }
                else if ( hc.highMid >= this.fullTransaction.stopLossPrice )
                {
                    this.fullTransaction.transactOutcome = ModelTransaction.EnumTransactOutcome.LOSS;
                    this.fullTransaction.exitPrice = this.fullTransaction.stopLossPrice;
                    this.modelTransaction.add( this.fullTransaction );
                    return true;
                }
            }

            // just in case we cannot find the minute candle
            this.fullTransaction.transactOutcome = ModelTransaction.EnumTransactOutcome.UNRESOLVE;
            this.fullTransaction.exitPrice = this.fullTransaction.stopLossPrice;
            this.modelTransaction.add( this.fullTransaction );
            return true;
        }

        protected bool checkForCompletedTransaction( bool hasCompletedTransaction )
        {
            if ( hasCompletedTransaction )
            {
                if ( this.isLive )
                {
                    DbOanda.getInstance().insertTransaction(
                        this.fullTransaction.dtStartSecSince1970,
                        this.fullTransaction.dtEndSecSince1970,
                        (int)this.fullTransaction.transactType,
                        this.fullTransaction.entryPrice,
                        this.fullTransaction.exitPrice,
                        this.fullTransaction.takeProfitPrice,
                        this.fullTransaction.stopLossPrice,
                        this.fullTransaction.units,
                        this.fullTransaction.pl );
                }

                this.transactionState = EnumTransactionState.WAITING_FOR_ENTRY;
                this.fullTransaction = new ModelTransaction.TransactionData();
                return true;
            }

            this.transactionState = EnumTransactionState.WAITING_FOR_EXIT;
            return false;
        }

        protected void reportFeedback( bool stopProgram, string info )
        {
            MessageProgressFeedback message = new MessageProgressFeedback();
            message.stopProgram = stopProgram;
            message.information = info;
            this.messageQueue.add( message );
        }


        #region deprecated... only use this if use back raw ticks check instead of candle check
        /*
        virtual protected bool checkForExitLongTrade( ModelCandleGraph.CandleData cd )
        {
            for ( long i = cd.CtIndexStart; i <= cd.CtIndexEnd; ++i )
            {
                DbRawTick.ConciseTick ct = DbRawTick.getInstance().get( i );                
                if ( ct.bid >= this.fullTransaction.takeProfitPrice )
                {
                    this.fullTransaction.transactOutcome = ModelSimulatedTransaction.EnumTransactOutcome.PROFIT;
                    this.fullTransaction.exitPrice = ct.bid;
                    this.modelSimulatedTransaction.add( this.fullTransaction );
                    return true;
                }                
                else if ( ct.bid <= this.fullTransaction.cutLossPrice )
                {
                    this.fullTransaction.transactOutcome = ModelSimulatedTransaction.EnumTransactOutcome.LOSS;
                    this.fullTransaction.exitPrice = ct.bid;
                    this.modelSimulatedTransaction.add( this.fullTransaction );
                    return true;
                }
            }
            return false;
        }

        virtual protected bool checkForExitShortTrade( ModelCandleGraph.CandleData cd )
        {
            for ( long i = cd.CtIndexStart; i <= cd.CtIndexEnd; ++i )
            {
                DbRawTick.ConciseTick ct = DbRawTick.getInstance().get( i );                
                if ( ct.ask <= this.fullTransaction.takeProfitPrice )
                {
                    this.fullTransaction.transactOutcome = ModelSimulatedTransaction.EnumTransactOutcome.PROFIT;
                    this.fullTransaction.exitPrice = ct.ask;
                    this.modelSimulatedTransaction.add( this.fullTransaction );
                    return true;
                }
                // cut loss activated in the same opening candle
                else if ( ct.ask >= this.fullTransaction.cutLossPrice )
                {
                    this.fullTransaction.transactOutcome = ModelSimulatedTransaction.EnumTransactOutcome.LOSS;
                    this.fullTransaction.exitPrice = ct.ask;
                    this.modelSimulatedTransaction.add( this.fullTransaction );
                    return true;
                }
            }
            return false;
        }
        */
        #endregion
    }
}
