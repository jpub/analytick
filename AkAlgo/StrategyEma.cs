﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AkBase;
using AkMessages;
using AkCommon;
using AkModel;
using AkDatabase;

namespace AkAlgo
{
    public class StrategyEma : Strategy
    {
        private enum CrossOverStateEnum
        {
            UNINITIALISE = 0,
            HAS_CROSSOVER = 1,
            NOT_CROSSOVER = 2
        }

        private enum EmaStateEnum
        {
            UNINITIALISE = 0,
            FAST_ABOVE_MEDIUM = 1,
            MEDIUM_ABOVE_FAST = 2
        }

        private ModelLineGraph modelLineEmaFast = null;
        private ModelLineGraph modelLineEmaMedium = null;
        private ModelLineGraph modelLineEmaSlow = null;

        private CrossOverStateEnum crossOverState = CrossOverStateEnum.UNINITIALISE;
        private EmaStateEnum emaState = EmaStateEnum.UNINITIALISE;

        public double EmaThresholdFastMedium { get; private set; }
        public double EmaThresholdMediumSlow { get; private set; }

        public StrategyEma( bool isLive, MessageQueue<BaseMessage> messageQueue, double pipTakeProfit, double pipStopLoss, double emaThresholdFastMedium, double emaThresholdMediumSlow )
            : base( isLive, messageQueue, pipTakeProfit, pipStopLoss )
        {
            this.EmaThresholdFastMedium = emaThresholdFastMedium;
            this.EmaThresholdMediumSlow = emaThresholdMediumSlow;

            DbLineGraph.getInstance().get( GlobalConstant.GRAPH_NAME_LINE_EMA_T1, out this.modelLineEmaFast );
            DbLineGraph.getInstance().get( GlobalConstant.GRAPH_NAME_LINE_EMA_T2, out this.modelLineEmaMedium );
            DbLineGraph.getInstance().get( GlobalConstant.GRAPH_NAME_LINE_EMA_T3, out this.modelLineEmaSlow );
        }

        public override string getStrategyRemarks()
        {
            return string.Format( "EMA: {0} {1} {2} {3}", this.pipTakeProfit, this.pipStopLoss, this.EmaThresholdFastMedium, this.EmaThresholdMediumSlow );
        }

        public override void preprocessing( ModelCandleGraph.CandleData cd )
        {
            int candleIndex = cd.index;
            ModelLineGraph.LineData ld;
            double emaFast = this.modelLineEmaFast.getByIndex( candleIndex, out ld ) ? ld.yValue : 0;
            double emaMedium = this.modelLineEmaMedium.getByIndex( candleIndex, out ld ) ? ld.yValue : 0;
            double emaSlow = this.modelLineEmaSlow.getByIndex( candleIndex, out ld ) ? ld.yValue : 0;

            if ( emaFast > emaMedium )
            {
                if ( this.emaState == EmaStateEnum.MEDIUM_ABOVE_FAST || this.emaState == EmaStateEnum.UNINITIALISE )
                {
                    if ( this.transactionState == EnumTransactionState.WAITING_FOR_ENTRY )
                        this.crossOverState = CrossOverStateEnum.HAS_CROSSOVER;
                }
                this.emaState = EmaStateEnum.FAST_ABOVE_MEDIUM;
            }
            else if ( emaMedium > emaFast )
            {
                if ( this.emaState == EmaStateEnum.FAST_ABOVE_MEDIUM || this.emaState == EmaStateEnum.UNINITIALISE )
                {
                    if ( this.transactionState == EnumTransactionState.WAITING_FOR_ENTRY )
                        this.crossOverState = CrossOverStateEnum.HAS_CROSSOVER;
                }
                this.emaState = EmaStateEnum.MEDIUM_ABOVE_FAST;
            }
        }

        public override bool checkForEntry( ModelCandleGraph.CandleData cd, out ModelTransaction.EnumTransactType transactType )
        {
            int candleIndex = cd.index;
            ModelLineGraph.LineData ld;
            double emaFast = this.modelLineEmaFast.getByIndex( candleIndex, out ld ) ? ld.yValue : 0;
            double emaMedium = this.modelLineEmaMedium.getByIndex( candleIndex, out ld ) ? ld.yValue : 0;
            double emaSlow = this.modelLineEmaSlow.getByIndex( candleIndex, out ld ) ? ld.yValue : 0;

            transactType = ModelTransaction.EnumTransactType.UNDEFINE;
            if ( emaSlow == AlgoEma.UNDEFINE_VALUE )
                return false;

            if ( this.crossOverState == CrossOverStateEnum.HAS_CROSSOVER )
            {
                if ( ( emaFast - emaMedium ) > this.EmaThresholdFastMedium )
                {
                    if ( ( emaMedium - emaSlow ) > this.EmaThresholdMediumSlow )
                    {
                        transactType = ModelTransaction.EnumTransactType.LONG;                        
                        this.crossOverState = CrossOverStateEnum.NOT_CROSSOVER;
                        return true;
                    }
                }
                else if ( ( emaFast - emaMedium ) < ( -this.EmaThresholdFastMedium ) )
                {
                    if ( ( emaMedium - emaSlow ) < ( -this.EmaThresholdMediumSlow ) )
                    {
                        transactType = ModelTransaction.EnumTransactType.SHORT;                        
                        this.crossOverState = CrossOverStateEnum.NOT_CROSSOVER;
                        return true;
                    }
                }
            }
            return false;
        }        
    }
}

