﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NUnit.Framework;

using AkCommon;
using AkAlgo;

namespace UnitTest
{
    [TestFixture]
    class AlgoRsiTest
    {
        private List<double> dataPointList = null;
        private double averageGain = 0;
        private double averageLoss = 0;

        [OneTimeSetUp]
        public void InitOnce()
        {
            // 20 data points
            this.dataPointList = new List<double>() {
                6, 7, 6, 7, 6, 7, 6, 7, 6, 7, 6, 7, 20, 1, 15
            };

            // base on the first 12 data points
            this.averageGain = 6.0;
            this.averageLoss = 5.0;
        }

        [Test]
        public void TestRsiCalulation()
        {
            int period = 12;
            bool status = false;
            double expectedPoint = AlgoRsi.UNDEFINE_VALUE;
            double weightedPoint = AlgoRsi.UNDEFINE_VALUE;

            AlgoRsi algoRsi = new AlgoRsi( period );
            // the first 12 values will be used to calculate the first average..that is why no values are return
            for ( int i = 0; i <= 11; ++i )
            {
                weightedPoint = double.MaxValue;
                status = algoRsi.addDataPoint( this.dataPointList[i], out weightedPoint );
                Assert.IsFalse( status );
                Assert.AreEqual( AlgoEma.UNDEFINE_VALUE, weightedPoint );
            }

            // the 13th data point will start producing results
            double gainOn13thPoint = 20 - 7;
            this.averageGain += gainOn13thPoint;
            this.averageGain /= period;
            this.averageLoss /= period;
            expectedPoint = 100 - ( 100 / ( 1 + ( this.averageGain / this.averageLoss ) ) );
            status = algoRsi.addDataPoint( this.dataPointList[12], out weightedPoint );
            Assert.IsTrue( status );
            Assert.AreEqual( expectedPoint, weightedPoint );

            // check the 14th point
            double lossOn14thPoint = 20 - 1;
            this.averageGain = ( this.averageGain * ( period - 1 ) ) / period;
            this.averageLoss = ( this.averageLoss * ( period - 1 ) + lossOn14thPoint ) / period;  
            expectedPoint = 100 - ( 100 / ( 1 + ( this.averageGain / this.averageLoss ) ) );
            status = algoRsi.addDataPoint( this.dataPointList[13], out weightedPoint );
            Assert.IsTrue( status );
            Assert.AreEqual( expectedPoint, weightedPoint );

            // check the 15th point
            double gainOn15thPoint = 15 - 1;
            this.averageGain = ( this.averageGain * ( period - 1 ) + gainOn15thPoint ) / period;
            this.averageLoss = ( this.averageLoss * ( period - 1 ) ) / period;
            expectedPoint = 100 - ( 100 / ( 1 + ( this.averageGain / this.averageLoss ) ) );
            status = algoRsi.addDataPoint( this.dataPointList[14], out weightedPoint );
            Assert.IsTrue( status );
            Assert.AreEqual( expectedPoint, weightedPoint );            
        }
    }
}
