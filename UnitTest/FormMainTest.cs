﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Threading;

using NUnit.Framework;

using AkCommon;
using AkDatabase;
using AkModel;
using Analytick.Forms;

namespace UnitTest
{
    [TestFixture, SingleThreaded] // single threaded run
    class FormMainTest
    {        
        private string rootDir = "";
        private string dataPath = "";
        private string appendDataPath = "";
        private string appendOutdatedDataPath = "";
        private FormMain formMain = null;
        private DateTime dt = DateTime.Now;

        [OneTimeSetUp]
        public void InitOnce()
        {
            this.rootDir = ConfigurationManager.AppSettings["root_dir"];
            this.dataPath = this.rootDir + Path.DirectorySeparatorChar + "UnitTestData";
            this.appendDataPath = this.rootDir + Path.DirectorySeparatorChar + "UnitTestAppendData";
            this.appendOutdatedDataPath = this.rootDir + Path.DirectorySeparatorChar + "UnitTestAppendOutdatedData";
            GlobalConstant.DATABASE_FOLDER = this.rootDir + Path.DirectorySeparatorChar + "UnitTestDb";
            GlobalConstant.MINUTE_BLOCK = 10;

            this.formMain = new FormMain();
            object obj = HelperUnitTest.RunInstanceMethod( typeof( FormMain ), "startFileLoader", this.formMain, new object[1] { this.dataPath } );
            Thread.Sleep( 500 );
            DbCandleGraph.getInstance().serialize();
            DbRawTick.getInstance().serialize();            
        }

        [OneTimeTearDown]
        public void DestroyOnce()
        {
            this.formMain.Dispose();
            DbCandleGraph.getInstance().clear();
        }

        [Test, Order( 1 )]
        public void TestLoadingRawTicks()
        {
            bool status = false;
            Assert.AreEqual( 24, DbRawTick.getInstance().count() );
            Assert.AreEqual( 1.5020, DbRawTick.getInstance().get( 0 ).bid ); Assert.AreEqual( 1.5022, DbRawTick.getInstance().get( 0 ).ask );
            Assert.AreEqual( 1.5000, DbRawTick.getInstance().get( 1 ).bid ); Assert.AreEqual( 1.5002, DbRawTick.getInstance().get( 1 ).ask );
            Assert.AreEqual( 1.5030, DbRawTick.getInstance().get( 2 ).bid ); Assert.AreEqual( 1.5032, DbRawTick.getInstance().get( 2 ).ask );
            Assert.AreEqual( 1.6020, DbRawTick.getInstance().get( 3 ).bid ); Assert.AreEqual( 1.6022, DbRawTick.getInstance().get( 3 ).ask );
            Assert.AreEqual( 1.6000, DbRawTick.getInstance().get( 4 ).bid ); Assert.AreEqual( 1.6002, DbRawTick.getInstance().get( 4 ).ask );
            Assert.AreEqual( 1.6030, DbRawTick.getInstance().get( 5 ).bid ); Assert.AreEqual( 1.6032, DbRawTick.getInstance().get( 5 ).ask );
            Assert.AreEqual( 1.7020, DbRawTick.getInstance().get( 6 ).bid ); Assert.AreEqual( 1.7022, DbRawTick.getInstance().get( 6 ).ask );
            Assert.AreEqual( 1.7000, DbRawTick.getInstance().get( 7 ).bid ); Assert.AreEqual( 1.7002, DbRawTick.getInstance().get( 7 ).ask );
            Assert.AreEqual( 1.7030, DbRawTick.getInstance().get( 8 ).bid ); Assert.AreEqual( 1.7032, DbRawTick.getInstance().get( 8 ).ask );
            Assert.AreEqual( 1.4020, DbRawTick.getInstance().get( 9 ).bid ); Assert.AreEqual( 1.4022, DbRawTick.getInstance().get( 9 ).ask );
            Assert.AreEqual( 1.4000, DbRawTick.getInstance().get( 10 ).bid ); Assert.AreEqual( 1.4002, DbRawTick.getInstance().get( 10 ).ask );
            Assert.AreEqual( 1.4030, DbRawTick.getInstance().get( 11 ).bid ); Assert.AreEqual( 1.4032, DbRawTick.getInstance().get( 11 ).ask );
            
            Assert.AreEqual( 2.5020, DbRawTick.getInstance().get( 12 ).bid ); Assert.AreEqual( 2.5022, DbRawTick.getInstance().get( 12 ).ask );
            Assert.AreEqual( 2.5000, DbRawTick.getInstance().get( 13 ).bid ); Assert.AreEqual( 2.5002, DbRawTick.getInstance().get( 13 ).ask );
            Assert.AreEqual( 2.5030, DbRawTick.getInstance().get( 14 ).bid ); Assert.AreEqual( 2.5032, DbRawTick.getInstance().get( 14 ).ask );
            Assert.AreEqual( 2.6020, DbRawTick.getInstance().get( 15 ).bid ); Assert.AreEqual( 2.6022, DbRawTick.getInstance().get( 15 ).ask );
            Assert.AreEqual( 2.6000, DbRawTick.getInstance().get( 16 ).bid ); Assert.AreEqual( 2.6002, DbRawTick.getInstance().get( 16 ).ask );
            Assert.AreEqual( 2.6030, DbRawTick.getInstance().get( 17 ).bid ); Assert.AreEqual( 2.6032, DbRawTick.getInstance().get( 17 ).ask );
            Assert.AreEqual( 2.7020, DbRawTick.getInstance().get( 18 ).bid ); Assert.AreEqual( 2.7022, DbRawTick.getInstance().get( 18 ).ask );
            Assert.AreEqual( 2.7000, DbRawTick.getInstance().get( 19 ).bid ); Assert.AreEqual( 2.7002, DbRawTick.getInstance().get( 19 ).ask );
            Assert.AreEqual( 2.7030, DbRawTick.getInstance().get( 20 ).bid ); Assert.AreEqual( 2.7032, DbRawTick.getInstance().get( 20 ).ask );
            Assert.AreEqual( 2.4020, DbRawTick.getInstance().get( 21 ).bid ); Assert.AreEqual( 2.4022, DbRawTick.getInstance().get( 21 ).ask );
            Assert.AreEqual( 2.4000, DbRawTick.getInstance().get( 22 ).bid ); Assert.AreEqual( 2.4002, DbRawTick.getInstance().get( 22 ).ask );
            Assert.AreEqual( 2.4030, DbRawTick.getInstance().get( 23 ).bid ); Assert.AreEqual( 2.4032, DbRawTick.getInstance().get( 23 ).ask );
        }

        [Test, Order( 2 )]
        public void TestLoadingMinuteCandleGraph()
        {
            bool status = false;
            ModelCandleGraph modelCandle = null;
            status = DbCandleGraph.getInstance().get( GlobalConstant.GRAPH_NAME_CANDLE_MINUTE, out modelCandle );
            Assert.IsTrue( status );

            List<ModelCandleGraph.CandleData> listCandleData = modelCandle.getCopyListCandleData();
            Assert.AreEqual( 8, listCandleData.Count ); // ensure 8 candles

            this.checkMinuteCandle1( listCandleData[0] );
            this.checkMinuteCandle2( listCandleData[1] );
            this.checkMinuteCandle3( listCandleData[2] );
            this.checkMinuteCandle4( listCandleData[3] );
            this.checkMinuteCandle5( listCandleData[4] );
            this.checkMinuteCandle6( listCandleData[5] );
            this.checkMinuteCandle7( listCandleData[6] );
            this.checkMinuteCandle8( listCandleData[7] );
        }

        [Test, Order( 3 )]
        public void TestLoadingHourCandleGraph()
        {
            bool status = false;
            ModelCandleGraph modelCandle = null;
            status = DbCandleGraph.getInstance().get( GlobalConstant.GRAPH_NAME_CANDLE_HOUR, out modelCandle );
            Assert.IsTrue( status );

            List<ModelCandleGraph.CandleData> listCandleData = modelCandle.getCopyListCandleData();
            Assert.AreEqual( 6, listCandleData.Count ); // ensure 6 candles

            this.checkHourCandle1( listCandleData[0] );
            this.checkHourCandle2( listCandleData[1] );
            this.checkHourCandle3( listCandleData[2] );
            this.checkHourCandle4( listCandleData[3] );
            this.checkHourCandle5( listCandleData[4] );
            this.checkHourCandle6( listCandleData[5] );            
        }

        [Test, Order( 4 )]
        public void TestLoadingDayCandleGraph()
        {
            bool status = false;
            ModelCandleGraph modelCandle = null;
            status = DbCandleGraph.getInstance().get( GlobalConstant.GRAPH_NAME_CANDLE_DAY, out modelCandle );
            Assert.IsTrue( status );

            List<ModelCandleGraph.CandleData> listCandleData = modelCandle.getCopyListCandleData();
            Assert.AreEqual( 2, listCandleData.Count ); // ensure 2 candles

            this.checkDayCandle1( listCandleData[0] );
            this.checkDayCandle2( listCandleData[1] );            
        }

        [Test, Order( 5 )]
        public void TestAppendOutdatedData()
        {
            // somehow I cannot use this.formMain..it keeps complaining the backgroundworker is busy
            FormMain tempFormMain = new FormMain();
            object obj = HelperUnitTest.RunInstanceMethod( typeof( FormMain ), "startFileLoader", tempFormMain, new object[1] { this.appendOutdatedDataPath } );
            Thread.Sleep( 500 );

            // no new candles should be added
            this.TestLoadingRawTicks();
            this.TestLoadingMinuteCandleGraph();
            this.TestLoadingHourCandleGraph();
            this.TestLoadingDayCandleGraph();            
        }

        [Test, Order( 6 )]
        public void TestAppendDatabase()
        {
            // somehow I cannot use this.formMain..it keeps complaining the backgroundworker is busy
            FormMain tempFormMain = new FormMain();
            object obj = HelperUnitTest.RunInstanceMethod( typeof( FormMain ), "startFileLoader", tempFormMain, new object[1] { this.appendDataPath } );
            Thread.Sleep( 500 );

            bool status = false;
            List<ModelCandleGraph.CandleData> listCandleData = null;
            ModelCandleGraph modelCandle = null;

            status = DbCandleGraph.getInstance().get( GlobalConstant.GRAPH_NAME_CANDLE_MINUTE, out modelCandle );
            Assert.IsTrue( status );
            listCandleData = modelCandle.getCopyListCandleData();
            Assert.AreEqual( 12, listCandleData.Count ); // ensure 12 candles
            Assert.AreEqual( 36, DbRawTick.getInstance().count() );

            this.checkMinuteCandle1( listCandleData[0] );
            this.checkMinuteCandle2( listCandleData[1] );
            this.checkMinuteCandle3( listCandleData[2] );
            this.checkMinuteCandle4( listCandleData[3] );
            this.checkMinuteCandle5( listCandleData[4] );
            this.checkMinuteCandle6( listCandleData[5] );
            this.checkMinuteCandle7( listCandleData[6] );
            this.checkMinuteCandle8( listCandleData[7] );
            this.checkMinuteCandle9( listCandleData[8] );
            this.checkMinuteCandle10( listCandleData[9] );
            this.checkMinuteCandle11( listCandleData[10] );
            this.checkMinuteCandle12( listCandleData[11] );


            status = DbCandleGraph.getInstance().get( GlobalConstant.GRAPH_NAME_CANDLE_HOUR, out modelCandle );
            Assert.IsTrue( status );
            listCandleData = modelCandle.getCopyListCandleData();
            Assert.AreEqual( 9, listCandleData.Count ); // ensure 9 candles
            this.checkHourCandle1( listCandleData[0] );
            this.checkHourCandle2( listCandleData[1] );
            this.checkHourCandle3( listCandleData[2] );
            this.checkHourCandle4( listCandleData[3] );
            this.checkHourCandle5( listCandleData[4] );
            this.checkHourCandle6( listCandleData[5] );
            this.checkHourCandle7( listCandleData[6] );
            this.checkHourCandle8( listCandleData[7] );
            this.checkHourCandle9( listCandleData[8] );


            status = DbCandleGraph.getInstance().get( GlobalConstant.GRAPH_NAME_CANDLE_DAY, out modelCandle );
            Assert.IsTrue( status );
            listCandleData = modelCandle.getCopyListCandleData();
            Assert.AreEqual( 3, listCandleData.Count ); // ensure 3 candles
            this.checkDayCandle1( listCandleData[0] );
            this.checkDayCandle2( listCandleData[1] );
            this.checkDayCandle3( listCandleData[2] );

            // ensure 36 ticks
            Assert.AreEqual( 36, DbRawTick.getInstance().count() );
            Assert.AreEqual( 1.5020, DbRawTick.getInstance().get( 0 ).bid ); Assert.AreEqual( 1.5022, DbRawTick.getInstance().get( 0 ).ask );
            Assert.AreEqual( 1.5000, DbRawTick.getInstance().get( 1 ).bid ); Assert.AreEqual( 1.5002, DbRawTick.getInstance().get( 1 ).ask );
            Assert.AreEqual( 1.5030, DbRawTick.getInstance().get( 2 ).bid ); Assert.AreEqual( 1.5032, DbRawTick.getInstance().get( 2 ).ask );
            Assert.AreEqual( 1.6020, DbRawTick.getInstance().get( 3 ).bid ); Assert.AreEqual( 1.6022, DbRawTick.getInstance().get( 3 ).ask );
            Assert.AreEqual( 1.6000, DbRawTick.getInstance().get( 4 ).bid ); Assert.AreEqual( 1.6002, DbRawTick.getInstance().get( 4 ).ask );
            Assert.AreEqual( 1.6030, DbRawTick.getInstance().get( 5 ).bid ); Assert.AreEqual( 1.6032, DbRawTick.getInstance().get( 5 ).ask );
            Assert.AreEqual( 1.7020, DbRawTick.getInstance().get( 6 ).bid ); Assert.AreEqual( 1.7022, DbRawTick.getInstance().get( 6 ).ask );
            Assert.AreEqual( 1.7000, DbRawTick.getInstance().get( 7 ).bid ); Assert.AreEqual( 1.7002, DbRawTick.getInstance().get( 7 ).ask );
            Assert.AreEqual( 1.7030, DbRawTick.getInstance().get( 8 ).bid ); Assert.AreEqual( 1.7032, DbRawTick.getInstance().get( 8 ).ask );
            Assert.AreEqual( 1.4020, DbRawTick.getInstance().get( 9 ).bid ); Assert.AreEqual( 1.4022, DbRawTick.getInstance().get( 9 ).ask );
            Assert.AreEqual( 1.4000, DbRawTick.getInstance().get( 10 ).bid ); Assert.AreEqual( 1.4002, DbRawTick.getInstance().get( 10 ).ask );
            Assert.AreEqual( 1.4030, DbRawTick.getInstance().get( 11 ).bid ); Assert.AreEqual( 1.4032, DbRawTick.getInstance().get( 11 ).ask );

            Assert.AreEqual( 2.5020, DbRawTick.getInstance().get( 12 ).bid ); Assert.AreEqual( 2.5022, DbRawTick.getInstance().get( 12 ).ask );
            Assert.AreEqual( 2.5000, DbRawTick.getInstance().get( 13 ).bid ); Assert.AreEqual( 2.5002, DbRawTick.getInstance().get( 13 ).ask );
            Assert.AreEqual( 2.5030, DbRawTick.getInstance().get( 14 ).bid ); Assert.AreEqual( 2.5032, DbRawTick.getInstance().get( 14 ).ask );
            Assert.AreEqual( 2.6020, DbRawTick.getInstance().get( 15 ).bid ); Assert.AreEqual( 2.6022, DbRawTick.getInstance().get( 15 ).ask );
            Assert.AreEqual( 2.6000, DbRawTick.getInstance().get( 16 ).bid ); Assert.AreEqual( 2.6002, DbRawTick.getInstance().get( 16 ).ask );
            Assert.AreEqual( 2.6030, DbRawTick.getInstance().get( 17 ).bid ); Assert.AreEqual( 2.6032, DbRawTick.getInstance().get( 17 ).ask );
            Assert.AreEqual( 2.7020, DbRawTick.getInstance().get( 18 ).bid ); Assert.AreEqual( 2.7022, DbRawTick.getInstance().get( 18 ).ask );
            Assert.AreEqual( 2.7000, DbRawTick.getInstance().get( 19 ).bid ); Assert.AreEqual( 2.7002, DbRawTick.getInstance().get( 19 ).ask );
            Assert.AreEqual( 2.7030, DbRawTick.getInstance().get( 20 ).bid ); Assert.AreEqual( 2.7032, DbRawTick.getInstance().get( 20 ).ask );
            Assert.AreEqual( 2.4020, DbRawTick.getInstance().get( 21 ).bid ); Assert.AreEqual( 2.4022, DbRawTick.getInstance().get( 21 ).ask );
            Assert.AreEqual( 2.4000, DbRawTick.getInstance().get( 22 ).bid ); Assert.AreEqual( 2.4002, DbRawTick.getInstance().get( 22 ).ask );
            Assert.AreEqual( 2.4030, DbRawTick.getInstance().get( 23 ).bid ); Assert.AreEqual( 2.4032, DbRawTick.getInstance().get( 23 ).ask );

            Assert.AreEqual( 3.5020, DbRawTick.getInstance().get( 24 ).bid ); Assert.AreEqual( 3.5022, DbRawTick.getInstance().get( 24 ).ask );
            Assert.AreEqual( 3.5000, DbRawTick.getInstance().get( 25 ).bid ); Assert.AreEqual( 3.5002, DbRawTick.getInstance().get( 25 ).ask );
            Assert.AreEqual( 3.5030, DbRawTick.getInstance().get( 26 ).bid ); Assert.AreEqual( 3.5032, DbRawTick.getInstance().get( 26 ).ask );
            Assert.AreEqual( 3.6020, DbRawTick.getInstance().get( 27 ).bid ); Assert.AreEqual( 3.6022, DbRawTick.getInstance().get( 27 ).ask );
            Assert.AreEqual( 3.6000, DbRawTick.getInstance().get( 28 ).bid ); Assert.AreEqual( 3.6002, DbRawTick.getInstance().get( 28 ).ask );
            Assert.AreEqual( 3.6030, DbRawTick.getInstance().get( 29 ).bid ); Assert.AreEqual( 3.6032, DbRawTick.getInstance().get( 29 ).ask );
            Assert.AreEqual( 3.7020, DbRawTick.getInstance().get( 30 ).bid ); Assert.AreEqual( 3.7022, DbRawTick.getInstance().get( 30 ).ask );
            Assert.AreEqual( 3.7000, DbRawTick.getInstance().get( 31 ).bid ); Assert.AreEqual( 3.7002, DbRawTick.getInstance().get( 31 ).ask );
            Assert.AreEqual( 3.7030, DbRawTick.getInstance().get( 32 ).bid ); Assert.AreEqual( 3.7032, DbRawTick.getInstance().get( 32 ).ask );
            Assert.AreEqual( 3.4020, DbRawTick.getInstance().get( 33 ).bid ); Assert.AreEqual( 3.4022, DbRawTick.getInstance().get( 33 ).ask );
            Assert.AreEqual( 3.4000, DbRawTick.getInstance().get( 34 ).bid ); Assert.AreEqual( 3.4002, DbRawTick.getInstance().get( 34 ).ask );
            Assert.AreEqual( 3.4030, DbRawTick.getInstance().get( 35 ).bid ); Assert.AreEqual( 3.4032, DbRawTick.getInstance().get( 35 ).ask );
        }

        [Test, Order( 7 )]
        public void TestDeserializeDatabase()
        {
            DbCandleGraph.getInstance().clear();
            Assert.AreEqual( DbCandleGraph.getInstance().count(), 0 );
            DbRawTick.getInstance().deserialize();
            DbCandleGraph.getInstance().deserialize();

            this.TestLoadingRawTicks();
            this.TestLoadingMinuteCandleGraph();
            this.TestLoadingHourCandleGraph();
            this.TestLoadingDayCandleGraph();            
        }

        [Test, Order( 8 )]
        public void TestFindingNearestMatchMinuteCandle()
        {
            bool status = false;
            ModelCandleGraph modelCandle = null;
            status = DbCandleGraph.getInstance().get( GlobalConstant.GRAPH_NAME_CANDLE_MINUTE, out modelCandle );
            Assert.IsTrue( status );

            List<ModelCandleGraph.CandleData> listCandleData = modelCandle.getCopyListCandleData();
            Assert.AreEqual( 8, listCandleData.Count ); // ensure 8 candles

            dt = new DateTime( 2017, 1, 15, 9, 16, 0, DateTimeKind.Utc );
            int candleIndex = DbCandleGraph.getInstance().getCandleIndexNearestMatch( GlobalConstant.GRAPH_NAME_CANDLE_MINUTE, dt.Ticks );
            Assert.AreEqual( 1, candleIndex );            
        }

        [Test, Order( 9 )]
        public void TestFindingNearestMatchHourCandle()
        {
            bool status = false;
            ModelCandleGraph modelCandle = null;
            status = DbCandleGraph.getInstance().get( GlobalConstant.GRAPH_NAME_CANDLE_HOUR, out modelCandle );
            Assert.IsTrue( status );

            List<ModelCandleGraph.CandleData> listCandleData = modelCandle.getCopyListCandleData();
            Assert.AreEqual( 6, listCandleData.Count ); // ensure 6 candles

            dt = new DateTime( 2017, 1, 16, 9, 16, 0, DateTimeKind.Utc );
            int candleIndex = DbCandleGraph.getInstance().getCandleIndexNearestMatch( GlobalConstant.GRAPH_NAME_CANDLE_HOUR, dt.Ticks );
            Assert.AreEqual( 3, candleIndex );
        }

        [Test, Order( 10 )]
        public void TestFindingNearestMatchDayCandle()
        {
            bool status = false;
            ModelCandleGraph modelCandle = null;
            status = DbCandleGraph.getInstance().get( GlobalConstant.GRAPH_NAME_CANDLE_DAY, out modelCandle );
            Assert.IsTrue( status );

            List<ModelCandleGraph.CandleData> listCandleData = modelCandle.getCopyListCandleData();
            Assert.AreEqual( 2, listCandleData.Count ); // ensure 2 candles

            dt = new DateTime( 2017, 1, 16, 9, 16, 0, DateTimeKind.Utc );
            int candleIndex = DbCandleGraph.getInstance().getCandleIndexNearestMatch( GlobalConstant.GRAPH_NAME_CANDLE_DAY, dt.Ticks );
            Assert.AreEqual( 1, candleIndex );
        }

        #region Minute Candles
        private void checkMinuteCandle1( ModelCandleGraph.CandleData cd )
        {
            dt = new DateTime( 2017, 1, 15, 9, 0, 0, DateTimeKind.Utc );
            Assert.AreEqual( cd.xValue, dt.Ticks );
            Assert.AreEqual( cd.yBotValue, 1.5000 ); // lowest of bid
            Assert.AreEqual( cd.yTopValue, 1.5032 ); // highest of ask
            Assert.AreEqual( cd.highMid, 1.5030 );
            Assert.AreEqual( cd.lowMid, 1.5000 );
            Assert.AreEqual( cd.openMid, 1.5020 );
            Assert.AreEqual( cd.closeMid, 1.5030 );
        }

        private void checkMinuteCandle2( ModelCandleGraph.CandleData cd )
        {
            dt = new DateTime( 2017, 1, 15, 9, 10, 0, DateTimeKind.Utc );
            Assert.AreEqual( cd.xValue, dt.Ticks );
            Assert.AreEqual( cd.yBotValue, 1.6000 ); // lowest of bid
            Assert.AreEqual( cd.yTopValue, 1.6032 ); // highest of ask
            Assert.AreEqual( cd.bidHigh, 1.6030 );
            Assert.AreEqual( cd.bidLow, 1.6000 );
            Assert.AreEqual( cd.bidOpen, 1.6020 );
            Assert.AreEqual( cd.bidClose, 1.6030 );
            Assert.AreEqual( cd.askHigh, 1.6032 );
            Assert.AreEqual( cd.askLow, 1.6002 );
            Assert.AreEqual( cd.askOpen, 1.6022 );
            Assert.AreEqual( cd.askClose, 1.6032 );

            Assert.AreEqual( 3, cd.CtIndexStart );
            Assert.AreEqual( 5, cd.CtIndexEnd );            
        }

        private void checkMinuteCandle3( ModelCandleGraph.CandleData cd )
        {
            dt = new DateTime( 2017, 1, 15, 10, 20, 0, DateTimeKind.Utc );
            Assert.AreEqual( cd.xValue, dt.Ticks );
            Assert.AreEqual( cd.yBotValue, 1.7000 ); // lowest of bid
            Assert.AreEqual( cd.yTopValue, 1.7032 ); // highest of ask
            Assert.AreEqual( cd.bidHigh, 1.7030 );
            Assert.AreEqual( cd.bidLow, 1.7000 );
            Assert.AreEqual( cd.bidOpen, 1.7020 );
            Assert.AreEqual( cd.bidClose, 1.7030 );
            Assert.AreEqual( cd.askHigh, 1.7032 );
            Assert.AreEqual( cd.askLow, 1.7002 );
            Assert.AreEqual( cd.askOpen, 1.7022 );
            Assert.AreEqual( cd.askClose, 1.7032 );

            Assert.AreEqual( 6, cd.CtIndexStart );
            Assert.AreEqual( 8, cd.CtIndexEnd );            
        }

        private void checkMinuteCandle4( ModelCandleGraph.CandleData cd )
        {
            dt = new DateTime( 2017, 1, 15, 11, 30, 0, DateTimeKind.Utc );
            Assert.AreEqual( cd.xValue, dt.Ticks );
            Assert.AreEqual( cd.yBotValue, 1.4000 ); // lowest of bid
            Assert.AreEqual( cd.yTopValue, 1.4032 ); // highest of ask
            Assert.AreEqual( cd.bidHigh, 1.4030 );
            Assert.AreEqual( cd.bidLow, 1.4000 );
            Assert.AreEqual( cd.bidOpen, 1.4020 );
            Assert.AreEqual( cd.bidClose, 1.4030 );
            Assert.AreEqual( cd.askHigh, 1.4032 );
            Assert.AreEqual( cd.askLow, 1.4002 );
            Assert.AreEqual( cd.askOpen, 1.4022 );
            Assert.AreEqual( cd.askClose, 1.4032 );

            Assert.AreEqual( 9, cd.CtIndexStart );
            Assert.AreEqual( 11, cd.CtIndexEnd );   
        }

        private void checkMinuteCandle5( ModelCandleGraph.CandleData cd )
        {
            dt = new DateTime( 2017, 1, 16, 9, 0, 0, DateTimeKind.Utc );
            Assert.AreEqual( cd.xValue, dt.Ticks );
            Assert.AreEqual( cd.yBotValue, 2.5000 ); // lowest of bid
            Assert.AreEqual( cd.yTopValue, 2.5032 ); // highest of ask
            Assert.AreEqual( cd.bidHigh, 2.5030 );
            Assert.AreEqual( cd.bidLow, 2.5000 );
            Assert.AreEqual( cd.bidOpen, 2.5020 );
            Assert.AreEqual( cd.bidClose, 2.5030 );
            Assert.AreEqual( cd.askHigh, 2.5032 );
            Assert.AreEqual( cd.askLow, 2.5002 );
            Assert.AreEqual( cd.askOpen, 2.5022 );
            Assert.AreEqual( cd.askClose, 2.5032 );

            Assert.AreEqual( 12, cd.CtIndexStart );
            Assert.AreEqual( 14, cd.CtIndexEnd );   
        }

        private void checkMinuteCandle6( ModelCandleGraph.CandleData cd )
        {
            dt = new DateTime( 2017, 1, 16, 9, 10, 0, DateTimeKind.Utc );
            Assert.AreEqual( cd.xValue, dt.Ticks );
            Assert.AreEqual( cd.yBotValue, 2.6000 ); // lowest of bid
            Assert.AreEqual( cd.yTopValue, 2.6032 ); // highest of ask
            Assert.AreEqual( cd.bidHigh, 2.6030 );
            Assert.AreEqual( cd.bidLow, 2.6000 );
            Assert.AreEqual( cd.bidOpen, 2.6020 );
            Assert.AreEqual( cd.bidClose, 2.6030 );
            Assert.AreEqual( cd.askHigh, 2.6032 );
            Assert.AreEqual( cd.askLow, 2.6002 );
            Assert.AreEqual( cd.askOpen, 2.6022 );
            Assert.AreEqual( cd.askClose, 2.6032 );

            Assert.AreEqual( 15, cd.CtIndexStart );
            Assert.AreEqual( 17, cd.CtIndexEnd );   
        }

        private void checkMinuteCandle7( ModelCandleGraph.CandleData cd )
        {
            dt = new DateTime( 2017, 1, 16, 10, 20, 0, DateTimeKind.Utc );
            Assert.AreEqual( cd.xValue, dt.Ticks );
            Assert.AreEqual( cd.yBotValue, 2.7000 ); // lowest of bid
            Assert.AreEqual( cd.yTopValue, 2.7032 ); // highest of ask
            Assert.AreEqual( cd.bidHigh, 2.7030 );
            Assert.AreEqual( cd.bidLow, 2.7000 );
            Assert.AreEqual( cd.bidOpen, 2.7020 );
            Assert.AreEqual( cd.bidClose, 2.7030 );
            Assert.AreEqual( cd.askHigh, 2.7032 );
            Assert.AreEqual( cd.askLow, 2.7002 );
            Assert.AreEqual( cd.askOpen, 2.7022 );
            Assert.AreEqual( cd.askClose, 2.7032 );

            Assert.AreEqual( 18, cd.CtIndexStart );
            Assert.AreEqual( 20, cd.CtIndexEnd );   
        }

        private void checkMinuteCandle8( ModelCandleGraph.CandleData cd )
        {
            dt = new DateTime( 2017, 1, 16, 11, 30, 0, DateTimeKind.Utc );
            Assert.AreEqual( cd.xValue, dt.Ticks );
            Assert.AreEqual( cd.yBotValue, 2.4000 ); // lowest of bid
            Assert.AreEqual( cd.yTopValue, 2.4032 ); // highest of ask
            Assert.AreEqual( cd.bidHigh, 2.4030 );
            Assert.AreEqual( cd.bidLow, 2.4000 );
            Assert.AreEqual( cd.bidOpen, 2.4020 );
            Assert.AreEqual( cd.bidClose, 2.4030 );
            Assert.AreEqual( cd.askHigh, 2.4032 );
            Assert.AreEqual( cd.askLow, 2.4002 );
            Assert.AreEqual( cd.askOpen, 2.4022 );
            Assert.AreEqual( cd.askClose, 2.4032 );

            Assert.AreEqual( 21, cd.CtIndexStart );
            Assert.AreEqual( 23, cd.CtIndexEnd );   
        }

        private void checkMinuteCandle9( ModelCandleGraph.CandleData cd )
        {
            dt = new DateTime( 2017, 1, 17, 9, 0, 0, DateTimeKind.Utc );
            Assert.AreEqual( cd.xValue, dt.Ticks );
            Assert.AreEqual( cd.yBotValue, 3.5000 ); // lowest of bid
            Assert.AreEqual( cd.yTopValue, 3.5032 ); // highest of ask
            Assert.AreEqual( cd.bidHigh, 3.5030 );
            Assert.AreEqual( cd.bidLow, 3.5000 );
            Assert.AreEqual( cd.bidOpen, 3.5020 );
            Assert.AreEqual( cd.bidClose, 3.5030 );
            Assert.AreEqual( cd.askHigh, 3.5032 );
            Assert.AreEqual( cd.askLow, 3.5002 );
            Assert.AreEqual( cd.askOpen, 3.5022 );
            Assert.AreEqual( cd.askClose, 3.5032 );

            Assert.AreEqual( 24, cd.CtIndexStart );
            Assert.AreEqual( 26, cd.CtIndexEnd );   
        }

        private void checkMinuteCandle10( ModelCandleGraph.CandleData cd )
        {
            dt = new DateTime( 2017, 1, 17, 9, 10, 0, DateTimeKind.Utc );
            Assert.AreEqual( cd.xValue, dt.Ticks );
            Assert.AreEqual( cd.yBotValue, 3.6000 ); // lowest of bid
            Assert.AreEqual( cd.yTopValue, 3.6032 ); // highest of ask
            Assert.AreEqual( cd.bidHigh, 3.6030 );
            Assert.AreEqual( cd.bidLow, 3.6000 );
            Assert.AreEqual( cd.bidOpen, 3.6020 );
            Assert.AreEqual( cd.bidClose, 3.6030 );
            Assert.AreEqual( cd.askHigh, 3.6032 );
            Assert.AreEqual( cd.askLow, 3.6002 );
            Assert.AreEqual( cd.askOpen, 3.6022 );
            Assert.AreEqual( cd.askClose, 3.6032 );

            Assert.AreEqual( 27, cd.CtIndexStart );
            Assert.AreEqual( 29, cd.CtIndexEnd );   
        }

        private void checkMinuteCandle11( ModelCandleGraph.CandleData cd )
        {
            dt = new DateTime( 2017, 1, 17, 10, 20, 0, DateTimeKind.Utc );
            Assert.AreEqual( cd.xValue, dt.Ticks );
            Assert.AreEqual( cd.yBotValue, 3.7000 ); // lowest of bid
            Assert.AreEqual( cd.yTopValue, 3.7032 ); // highest of ask
            Assert.AreEqual( cd.bidHigh, 3.7030 );
            Assert.AreEqual( cd.bidLow, 3.7000 );
            Assert.AreEqual( cd.bidOpen, 3.7020 );
            Assert.AreEqual( cd.bidClose, 3.7030 );
            Assert.AreEqual( cd.askHigh, 3.7032 );
            Assert.AreEqual( cd.askLow, 3.7002 );
            Assert.AreEqual( cd.askOpen, 3.7022 );
            Assert.AreEqual( cd.askClose, 3.7032 );

            Assert.AreEqual( 30, cd.CtIndexStart );
            Assert.AreEqual( 32, cd.CtIndexEnd );   
        }

        private void checkMinuteCandle12( ModelCandleGraph.CandleData cd )
        {
            dt = new DateTime( 2017, 1, 17, 11, 30, 0, DateTimeKind.Utc );
            Assert.AreEqual( cd.xValue, dt.Ticks );
            Assert.AreEqual( cd.yBotValue, 3.4000 ); // lowest of bid
            Assert.AreEqual( cd.yTopValue, 3.4032 ); // highest of ask
            Assert.AreEqual( cd.bidHigh, 3.4030 );
            Assert.AreEqual( cd.bidLow, 3.4000 );
            Assert.AreEqual( cd.bidOpen, 3.4020 );
            Assert.AreEqual( cd.bidClose, 3.4030 );
            Assert.AreEqual( cd.askHigh, 3.4032 );
            Assert.AreEqual( cd.askLow, 3.4002 );
            Assert.AreEqual( cd.askOpen, 3.4022 );
            Assert.AreEqual( cd.askClose, 3.4032 );

            Assert.AreEqual( 33, cd.CtIndexStart );
            Assert.AreEqual( 35, cd.CtIndexEnd );   
        }
        #endregion

        #region Hour Candles
        private void checkHourCandle1( ModelCandleGraph.CandleData cd )
        {
            dt = new DateTime( 2017, 1, 15, 9, 0, 0, DateTimeKind.Utc );
            Assert.AreEqual( cd.xValue, dt.Ticks );
            Assert.AreEqual( cd.yBotValue, 1.5000 ); // lowest of bid
            Assert.AreEqual( cd.yTopValue, 1.6032 ); // highest of ask
            Assert.AreEqual( cd.bidHigh, 1.6030 );
            Assert.AreEqual( cd.bidLow, 1.5000 );
            Assert.AreEqual( cd.bidOpen, 1.5020 );
            Assert.AreEqual( cd.bidClose, 1.6030 );
            Assert.AreEqual( cd.askHigh, 1.6032 );
            Assert.AreEqual( cd.askLow, 1.5002 );
            Assert.AreEqual( cd.askOpen, 1.5022 );
            Assert.AreEqual( cd.askClose, 1.6032 );

            Assert.AreEqual( 0, cd.CtIndexStart );
            Assert.AreEqual( 5, cd.CtIndexEnd );   
        }

        private void checkHourCandle2( ModelCandleGraph.CandleData cd )
        {
            dt = new DateTime( 2017, 1, 15, 10, 0, 0, DateTimeKind.Utc );
            Assert.AreEqual( cd.xValue, dt.Ticks );
            Assert.AreEqual( cd.yBotValue, 1.7000 ); // lowest of bid
            Assert.AreEqual( cd.yTopValue, 1.7032 ); // highest of ask
            Assert.AreEqual( cd.bidHigh, 1.7030 );
            Assert.AreEqual( cd.bidLow, 1.7000 );
            Assert.AreEqual( cd.bidOpen, 1.7020 );
            Assert.AreEqual( cd.bidClose, 1.7030 );
            Assert.AreEqual( cd.askHigh, 1.7032 );
            Assert.AreEqual( cd.askLow, 1.7002 );
            Assert.AreEqual( cd.askOpen, 1.7022 );
            Assert.AreEqual( cd.askClose, 1.7032 );

            Assert.AreEqual( 6, cd.CtIndexStart );
            Assert.AreEqual( 8, cd.CtIndexEnd );   
        }

        private void checkHourCandle3( ModelCandleGraph.CandleData cd )
        {
            dt = new DateTime( 2017, 1, 15, 11, 0, 0, DateTimeKind.Utc );
            Assert.AreEqual( cd.xValue, dt.Ticks );
            Assert.AreEqual( cd.yBotValue, 1.4000 ); // lowest of bid
            Assert.AreEqual( cd.yTopValue, 1.4032 ); // highest of ask
            Assert.AreEqual( cd.bidHigh, 1.4030 );
            Assert.AreEqual( cd.bidLow, 1.4000 );
            Assert.AreEqual( cd.bidOpen, 1.4020 );
            Assert.AreEqual( cd.bidClose, 1.4030 );
            Assert.AreEqual( cd.askHigh, 1.4032 );
            Assert.AreEqual( cd.askLow, 1.4002 );
            Assert.AreEqual( cd.askOpen, 1.4022 );
            Assert.AreEqual( cd.askClose, 1.4032 );

            Assert.AreEqual( 9, cd.CtIndexStart );
            Assert.AreEqual( 11, cd.CtIndexEnd );   
        }

        private void checkHourCandle4( ModelCandleGraph.CandleData cd )
        {
            dt = new DateTime( 2017, 1, 16, 9, 0, 0, DateTimeKind.Utc );
            Assert.AreEqual( cd.xValue, dt.Ticks );
            Assert.AreEqual( cd.yBotValue, 2.5000 ); // lowest of bid
            Assert.AreEqual( cd.yTopValue, 2.6032 ); // highest of ask
            Assert.AreEqual( cd.bidHigh, 2.6030 );
            Assert.AreEqual( cd.bidLow, 2.5000 );
            Assert.AreEqual( cd.bidOpen, 2.5020 );
            Assert.AreEqual( cd.bidClose, 2.6030 );
            Assert.AreEqual( cd.askHigh, 2.6032 );
            Assert.AreEqual( cd.askLow, 2.5002 );
            Assert.AreEqual( cd.askOpen, 2.5022 );
            Assert.AreEqual( cd.askClose, 2.6032 );

            Assert.AreEqual( 12, cd.CtIndexStart );
            Assert.AreEqual( 17, cd.CtIndexEnd );   
        }

        private void checkHourCandle5( ModelCandleGraph.CandleData cd )
        {
            dt = new DateTime( 2017, 1, 16, 10, 0, 0, DateTimeKind.Utc );
            Assert.AreEqual( cd.xValue, dt.Ticks );
            Assert.AreEqual( cd.yBotValue, 2.7000 ); // lowest of bid
            Assert.AreEqual( cd.yTopValue, 2.7032 ); // highest of ask
            Assert.AreEqual( cd.bidHigh, 2.7030 );
            Assert.AreEqual( cd.bidLow, 2.7000 );
            Assert.AreEqual( cd.bidOpen, 2.7020 );
            Assert.AreEqual( cd.bidClose, 2.7030 );
            Assert.AreEqual( cd.askHigh, 2.7032 );
            Assert.AreEqual( cd.askLow, 2.7002 );
            Assert.AreEqual( cd.askOpen, 2.7022 );
            Assert.AreEqual( cd.askClose, 2.7032 );

            Assert.AreEqual( 18, cd.CtIndexStart );
            Assert.AreEqual( 20, cd.CtIndexEnd );   
        }

        private void checkHourCandle6( ModelCandleGraph.CandleData cd )
        {
            dt = new DateTime( 2017, 1, 16, 11, 0, 0, DateTimeKind.Utc );
            Assert.AreEqual( cd.xValue, dt.Ticks );
            Assert.AreEqual( cd.yBotValue, 2.4000 ); // lowest of bid
            Assert.AreEqual( cd.yTopValue, 2.4032 ); // highest of ask
            Assert.AreEqual( cd.bidHigh, 2.4030 );
            Assert.AreEqual( cd.bidLow, 2.4000 );
            Assert.AreEqual( cd.bidOpen, 2.4020 );
            Assert.AreEqual( cd.bidClose, 2.4030 );
            Assert.AreEqual( cd.askHigh, 2.4032 );
            Assert.AreEqual( cd.askLow, 2.4002 );
            Assert.AreEqual( cd.askOpen, 2.4022 );
            Assert.AreEqual( cd.askClose, 2.4032 );

            Assert.AreEqual( 21, cd.CtIndexStart );
            Assert.AreEqual( 23, cd.CtIndexEnd );   
        }

        private void checkHourCandle7( ModelCandleGraph.CandleData cd )
        {
            dt = new DateTime( 2017, 1, 17, 9, 0, 0, DateTimeKind.Utc );
            Assert.AreEqual( cd.xValue, dt.Ticks );
            Assert.AreEqual( cd.yBotValue, 3.5000 ); // lowest of bid
            Assert.AreEqual( cd.yTopValue, 3.6032 ); // highest of ask
            Assert.AreEqual( cd.bidHigh, 3.6030 );
            Assert.AreEqual( cd.bidLow, 3.5000 );
            Assert.AreEqual( cd.bidOpen, 3.5020 );
            Assert.AreEqual( cd.bidClose, 3.6030 );
            Assert.AreEqual( cd.askHigh, 3.6032 );
            Assert.AreEqual( cd.askLow, 3.5002 );
            Assert.AreEqual( cd.askOpen, 3.5022 );
            Assert.AreEqual( cd.askClose, 3.6032 );

            Assert.AreEqual( 24, cd.CtIndexStart );
            Assert.AreEqual( 29, cd.CtIndexEnd );   
        }

        private void checkHourCandle8( ModelCandleGraph.CandleData cd )
        {
            dt = new DateTime( 2017, 1, 17, 10, 0, 0, DateTimeKind.Utc );
            Assert.AreEqual( cd.xValue, dt.Ticks );
            Assert.AreEqual( cd.yBotValue, 3.7000 ); // lowest of bid
            Assert.AreEqual( cd.yTopValue, 3.7032 ); // highest of ask
            Assert.AreEqual( cd.bidHigh, 3.7030 );
            Assert.AreEqual( cd.bidLow, 3.7000 );
            Assert.AreEqual( cd.bidOpen, 3.7020 );
            Assert.AreEqual( cd.bidClose, 3.7030 );
            Assert.AreEqual( cd.askHigh, 3.7032 );
            Assert.AreEqual( cd.askLow, 3.7002 );
            Assert.AreEqual( cd.askOpen, 3.7022 );
            Assert.AreEqual( cd.askClose, 3.7032 );

            Assert.AreEqual( 30, cd.CtIndexStart );
            Assert.AreEqual( 32, cd.CtIndexEnd );   
        }

        private void checkHourCandle9( ModelCandleGraph.CandleData cd )
        {
            dt = new DateTime( 2017, 1, 17, 11, 0, 0, DateTimeKind.Utc );
            Assert.AreEqual( cd.xValue, dt.Ticks );
            Assert.AreEqual( cd.yBotValue, 3.4000 ); // lowest of bid
            Assert.AreEqual( cd.yTopValue, 3.4032 ); // highest of ask
            Assert.AreEqual( cd.bidHigh, 3.4030 );
            Assert.AreEqual( cd.bidLow, 3.4000 );
            Assert.AreEqual( cd.bidOpen, 3.4020 );
            Assert.AreEqual( cd.bidClose, 3.4030 );
            Assert.AreEqual( cd.askHigh, 3.4032 );
            Assert.AreEqual( cd.askLow, 3.4002 );
            Assert.AreEqual( cd.askOpen, 3.4022 );
            Assert.AreEqual( cd.askClose, 3.4032 );

            Assert.AreEqual( 33, cd.CtIndexStart );
            Assert.AreEqual( 35, cd.CtIndexEnd );   
        }
        #endregion

        #region Day Candles
        private void checkDayCandle1( ModelCandleGraph.CandleData cd )
        {
            dt = new DateTime( 2017, 1, 15, 0, 0, 0, DateTimeKind.Utc );
            Assert.AreEqual( cd.xValue, dt.Ticks );
            Assert.AreEqual( cd.yBotValue, 1.4000 ); // lowest of bid
            Assert.AreEqual( cd.yTopValue, 1.7032 ); // highest of ask
            Assert.AreEqual( cd.bidHigh, 1.7030 );
            Assert.AreEqual( cd.bidLow, 1.4000 );
            Assert.AreEqual( cd.bidOpen, 1.5020 );
            Assert.AreEqual( cd.bidClose, 1.4030 );
            Assert.AreEqual( cd.askHigh, 1.7032 );
            Assert.AreEqual( cd.askLow, 1.4002 );
            Assert.AreEqual( cd.askOpen, 1.5022 );
            Assert.AreEqual( cd.askClose, 1.4032 );

            Assert.AreEqual( 0, cd.CtIndexStart );
            Assert.AreEqual( 11, cd.CtIndexEnd );   
        }

        private void checkDayCandle2( ModelCandleGraph.CandleData cd )
        {
            dt = new DateTime( 2017, 1, 16, 0, 0, 0, DateTimeKind.Utc );
            Assert.AreEqual( cd.xValue, dt.Ticks );
            Assert.AreEqual( cd.yBotValue, 2.4000 ); // lowest of bid
            Assert.AreEqual( cd.yTopValue, 2.7032 ); // highest of ask
            Assert.AreEqual( cd.bidHigh, 2.7030 );
            Assert.AreEqual( cd.bidLow, 2.4000 );
            Assert.AreEqual( cd.bidOpen, 2.5020 );
            Assert.AreEqual( cd.bidClose, 2.4030 );
            Assert.AreEqual( cd.askHigh, 2.7032 );
            Assert.AreEqual( cd.askLow, 2.4002 );
            Assert.AreEqual( cd.askOpen, 2.5022 );
            Assert.AreEqual( cd.askClose, 2.4032 );

            Assert.AreEqual( 12, cd.CtIndexStart );
            Assert.AreEqual( 23, cd.CtIndexEnd );   
        }

        private void checkDayCandle3( ModelCandleGraph.CandleData cd )
        {
            dt = new DateTime( 2017, 1, 17, 0, 0, 0, DateTimeKind.Utc );
            Assert.AreEqual( cd.xValue, dt.Ticks );
            Assert.AreEqual( cd.yBotValue, 3.4000 ); // lowest of bid
            Assert.AreEqual( cd.yTopValue, 3.7032 ); // highest of ask
            Assert.AreEqual( cd.bidHigh, 3.7030 );
            Assert.AreEqual( cd.bidLow, 3.4000 );
            Assert.AreEqual( cd.bidOpen, 3.5020 );
            Assert.AreEqual( cd.bidClose, 3.4030 );
            Assert.AreEqual( cd.askHigh, 3.7032 );
            Assert.AreEqual( cd.askLow, 3.4002 );
            Assert.AreEqual( cd.askOpen, 3.5022 );
            Assert.AreEqual( cd.askClose, 3.4032 );

            Assert.AreEqual( 24, cd.CtIndexStart );
            Assert.AreEqual( 35, cd.CtIndexEnd );   
        }
        #endregion
    }
}
