﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using NUnit.Framework;

namespace UnitTest
{
    [TestFixture, Explicit]
    //[TestFixture, SingleThreaded] // this will force all the test in this fixture to run single threadedly
    class CheatSheet
    {
        // call once only
        [OneTimeSetUp]
        public void InitOnce()
        {
        }

        [OneTimeTearDown]
        public void DestroyOnce()
        {
        }

        // call before every test
        [SetUp]
        public void Init()
        {
        }

        [TearDown]
        public void Destroy()
        {
        }

        [Test, Description( "Description of your test" )]
        public void TestWithDescription()
        {
            Assert.IsTrue( true );
        }

        [Test]
        public void TestWithoutDescription()
        {
            Assert.IsTrue( true );
        }

        // can group test together base on same category name
        [Test]
        [Category( "My categorised test" )]
        public void TestWithCategory1()
        {
            Assert.IsTrue( true );
        }

        [Test]
        [Category( "My categorised test" )]
        public void TestWithCategory2()
        {
            Assert.IsTrue( true );
        }

        [Test]
        public void TestWithAllPossibleCombination( [Values( 1, 2, 3 )] int x, [Values( "a", "b" )] string y )
        {
            Assert.IsTrue( true );
        }

        [Test]
        [Ignore( "Work in progress" )]
        public void TestBeingIgnored()
        {
            Assert.IsTrue( true );
        }

        // test will run finish and check against the max time
        [Test, MaxTime( 2000 )]
        public void TestWithTimeLimit()
        {
            //Thread.Sleep( 3000 );
            Assert.IsTrue( true );
        }

        // test will terminate if greater than max time and will be reported as fail
        [Test, Timeout( 2000 )]
        public void TestWithTimeOut()
        {
            //Thread.Sleep( 3000 );
            Assert.IsTrue( true );
        }

        [Test]
        public void TestWithRangeOfValues( [Range( 10, 20, 5 )] int x )
        {
            Assert.IsTrue( true );
        }

        [Test, Sequential]
        public void TestWithSequentialValues( [Values( 1, 2, 3 )] int x, [Values( "a", "b", "c" )] string y )
        {
            Assert.IsTrue( true );
        }

        [TestCase( 1, "a" )]
        [TestCase( 2, "b" )]
        public void TestWithSpecificTestCase( int x, string y )
        {
            Assert.IsTrue( true );
        }

        [Test, Order( 1 )]
        public void TestWithOrderOne()
        {
            Assert.IsTrue( true );
        }

        [Test, Order( 2 )]
        public void TestWithOrderTwo()
        {
            Assert.IsTrue( true );
        }
    }
}
