﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NUnit.Framework;

using AkCommon;
using AkAlgo;

namespace UnitTest
{
    [TestFixture]
    class AlgoEmaTest
    {
        private List<double> dataPointList = null;

        [OneTimeSetUp]
        public void InitOnce()
        {
            this.dataPointList = new List<double>() {
                1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20
            };
        }

        [Test]
        public void TestEmaCalulation()
        {
            int period = 12;
            bool status = false;
            double weightedPoint = AlgoEma.UNDEFINE_VALUE;
            double weightMultiplier = 2.0 / ( period + 1 );

            AlgoEma algoEma = new AlgoEma( period );
            for ( int i = 0; i <= 10; ++i )
            {
                weightedPoint = double.MaxValue;
                status = algoEma.addDataPoint( this.dataPointList[i], out weightedPoint );
                Assert.IsFalse( status );
                Assert.AreEqual( AlgoEma.UNDEFINE_VALUE, weightedPoint );
            }
            
            double total = 0;
            for ( int i = 0; i <= 11; ++i )
            {
                total += this.dataPointList[i];
            }
            double prevEma = total / period;

            status = algoEma.addDataPoint( this.dataPointList[11], out weightedPoint );
            Assert.IsTrue( status );
            Assert.AreEqual( prevEma, weightedPoint );
            
            for ( int i = 12; i < this.dataPointList.Count; ++i )
            {
                double expectedPoint = ( ( this.dataPointList[i] - prevEma ) * weightMultiplier ) + prevEma;
                status = algoEma.addDataPoint( this.dataPointList[i], out weightedPoint );
                prevEma = weightedPoint;
                Assert.IsTrue( status );
                Assert.AreEqual( expectedPoint, weightedPoint );
            }            
        }
    }
}
