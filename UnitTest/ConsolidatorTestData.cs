﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AkCommon;
using AkOanda;

namespace UnitTest
{
    class ConsolidatorTestData
    {
        static public object[] DataSingleTick = {
            new object[] {
                new DataTick("", new DateTime( 2016, 1, 1, 11, 22, 33 ), 1.5000, 1.5002)
            }
        };

        static public object[] DataMinuteTickList = {
            new object[] {
                new List<DataTick>() {
                    new DataTick("", new DateTime( 2016, 1, 1, 11, 22, 33 ), 1.5020, 1.5022),
                    new DataTick("", new DateTime( 2016, 1, 1, 11, 23, 33 ), 1.5000, 1.5002),
                    new DataTick("", new DateTime( 2016, 1, 1, 11, 24, 33 ), 1.5030, 1.5032),
                    new DataTick("", new DateTime( 2016, 1, 1, 11, 30, 33 ), 1.6020, 1.6022),
                    new DataTick("", new DateTime( 2016, 1, 1, 11, 31, 33 ), 1.6000, 1.6002)
                }
            }
        };

        static public object[] DataHourTickList = {
            new object[] {
                new List<DataTick>() {
                    new DataTick("", new DateTime( 2016, 1, 1, 11, 22, 33 ), 1.5020, 1.5022),
                    new DataTick("", new DateTime( 2016, 1, 1, 11, 23, 33 ), 1.5000, 1.5002),
                    new DataTick("", new DateTime( 2016, 1, 1, 11, 24, 33 ), 1.5030, 1.5032),
                    new DataTick("", new DateTime( 2016, 1, 1, 12, 30, 33 ), 1.6020, 1.6022),
                    new DataTick("", new DateTime( 2016, 1, 1, 12, 31, 33 ), 1.6000, 1.6002)
                }
            }
        };

        static public object[] DataDayTickList = {
            new object[] {
                new List<DataTick>() {
                    new DataTick("", new DateTime( 2016, 1, 1, 11, 22, 33 ), 1.5020, 1.5022),
                    new DataTick("", new DateTime( 2016, 1, 1, 11, 23, 33 ), 1.5000, 1.5002),
                    new DataTick("", new DateTime( 2016, 1, 1, 11, 24, 33 ), 1.5030, 1.5032),
                    new DataTick("", new DateTime( 2016, 1, 2, 11, 30, 33 ), 1.6020, 1.6022),
                    new DataTick("", new DateTime( 2016, 1, 2, 11, 31, 33 ), 1.6000, 1.6002)
                }
            }
        };
    }
}
