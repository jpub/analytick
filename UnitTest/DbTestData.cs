﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnitTest
{
    class DbTestData
    {
        static public object[] DataList = {
            new object[] {
                new List<DbTest.ModelData>() {
                    new DbTest.ModelData("A"),
                    new DbTest.ModelData("B"),
                    new DbTest.ModelData("C"),
                    new DbTest.ModelData("D"),
                    new DbTest.ModelData("E"),
                }
            }
        };
    }
}
