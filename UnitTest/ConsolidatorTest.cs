﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;

using AkCommon;
using AkMessages;
using AkOanda;

namespace UnitTest
{
    [TestFixture]
    class ConsolidatorTest
    {
        [OneTimeSetUp]
        public void InitOnce()
        {
            GlobalConstant.MINUTE_BLOCK = 10;
        }

        [SetUp]
        public void SetUp()
        {
        }

        #region Test Retrieve ConsoTime
        [TestCaseSource( typeof( ConsolidatorTestData ), "DataSingleTick" )]
        public void TestMinuteRetrieveConsoTime( DataTick dataTick )
        {
            ConsolidatorMinute consoMinute = new ConsolidatorMinute();
            ConsoInfo consoInfo = new ConsoInfo();

            consoMinute.group( 0, dataTick, ref consoInfo );

            DateTime orgDt = dataTick.time;
            DateTime modDt = consoMinute.getConsoTime( dataTick.time );

            Assert.AreEqual( modDt.Year, orgDt.Year );
            Assert.AreEqual( modDt.Month, orgDt.Month );
            Assert.AreEqual( modDt.Day, orgDt.Day );
            Assert.AreEqual( modDt.Hour, orgDt.Hour );
            Assert.AreEqual( modDt.Minute, orgDt.Minute / GlobalConstant.MINUTE_BLOCK * GlobalConstant.MINUTE_BLOCK );
            Assert.AreEqual( modDt.Second, 0 );
        }

        [TestCaseSource( typeof( ConsolidatorTestData ), "DataSingleTick" )]
        public void TestHourRetrieveConsoTime( DataTick dataTick )
        {
            ConsolidatorHour consoHour = new ConsolidatorHour();
            ConsoInfo consoInfo = new ConsoInfo();

            consoHour.group( 0, dataTick, ref consoInfo );

            DateTime orgDt = dataTick.time;
            DateTime modDt = consoHour.getConsoTime( dataTick.time );

            Assert.AreEqual( modDt.Year, orgDt.Year );
            Assert.AreEqual( modDt.Month, orgDt.Month );
            Assert.AreEqual( modDt.Day, orgDt.Day );
            Assert.AreEqual( modDt.Hour, orgDt.Hour );
            Assert.AreEqual( modDt.Minute, 0 );
            Assert.AreEqual( modDt.Second, 0 );
        }

        [TestCaseSource( typeof( ConsolidatorTestData ), "DataSingleTick" )]
        public void TestDayRetrieveConsoTime( DataTick dataTick )
        {
            ConsolidatorDay consoDay = new ConsolidatorDay();
            ConsoInfo consoInfo = new ConsoInfo();

            consoDay.group( 0, dataTick, ref consoInfo );

            DateTime orgDt = dataTick.time;
            DateTime modDt = consoDay.getConsoTime( dataTick.time );

            Assert.AreEqual( modDt.Year, orgDt.Year );
            Assert.AreEqual( modDt.Month, orgDt.Month );
            Assert.AreEqual( modDt.Day, orgDt.Day );
            Assert.AreEqual( modDt.Hour, 0 );
            Assert.AreEqual( modDt.Minute, 0 );
            Assert.AreEqual( modDt.Second, 0 );
        }
        #endregion

        #region Test Grouping Single Tick Data
        [TestCaseSource( typeof( ConsolidatorTestData ), "DataSingleTick" )]
        public void TestMinuteGroupingSingleTick( DataTick dataTick )
        {
            bool status = true;
            ConsolidatorMinute consoMinute = new ConsolidatorMinute();
            ConsoInfo consoInfo = new ConsoInfo();

            consoMinute.group( 0, dataTick, ref consoInfo );
            status = consoMinute.close( ref consoInfo );
            Assert.IsTrue( status );

            double expectedBid = dataTick.bid;
            Assert.AreEqual( consoInfo.BidOpen, expectedBid );
            Assert.AreEqual( consoInfo.BidClose, expectedBid );
            Assert.AreEqual( consoInfo.BidHigh, expectedBid );
            Assert.AreEqual( consoInfo.BidLow, expectedBid );

            double expectedAsk = dataTick.ask;
            Assert.AreEqual( consoInfo.AskOpen, expectedAsk );
            Assert.AreEqual( consoInfo.AskClose, expectedAsk );
            Assert.AreEqual( consoInfo.AskHigh, expectedAsk );
            Assert.AreEqual( consoInfo.AskLow, expectedAsk );

            Assert.AreEqual( consoInfo.Time.Year, dataTick.time.Year );
            Assert.AreEqual( consoInfo.Time.Month, dataTick.time.Month );
            Assert.AreEqual( consoInfo.Time.Day, dataTick.time.Day );
            Assert.AreEqual( consoInfo.Time.Hour, dataTick.time.Hour );
            Assert.AreEqual( consoInfo.Time.Minute, dataTick.time.Minute / GlobalConstant.MINUTE_BLOCK * GlobalConstant.MINUTE_BLOCK );
            Assert.AreEqual( consoInfo.Time.Second, 0 );
        }

        [TestCaseSource( typeof( ConsolidatorTestData ), "DataSingleTick" )]
        public void TestHourGroupingSingleTick( DataTick dataTick )
        {
            bool status = true;
            ConsolidatorHour consoHour = new ConsolidatorHour();
            ConsoInfo consoInfo = new ConsoInfo();

            consoHour.group( 0, dataTick, ref consoInfo );
            status = consoHour.close( ref consoInfo );
            Assert.IsTrue( status );

            double expectedBid = dataTick.bid;
            Assert.AreEqual( consoInfo.BidOpen, expectedBid );
            Assert.AreEqual( consoInfo.BidClose, expectedBid );
            Assert.AreEqual( consoInfo.BidHigh, expectedBid );
            Assert.AreEqual( consoInfo.BidLow, expectedBid );

            double expectedAsk = dataTick.ask;
            Assert.AreEqual( consoInfo.AskOpen, expectedAsk );
            Assert.AreEqual( consoInfo.AskClose, expectedAsk );
            Assert.AreEqual( consoInfo.AskHigh, expectedAsk );
            Assert.AreEqual( consoInfo.AskLow, expectedAsk );

            Assert.AreEqual( consoInfo.Time.Year, dataTick.time.Year );
            Assert.AreEqual( consoInfo.Time.Month, dataTick.time.Month );
            Assert.AreEqual( consoInfo.Time.Day, dataTick.time.Day );
            Assert.AreEqual( consoInfo.Time.Hour, dataTick.time.Hour );
            Assert.AreEqual( consoInfo.Time.Minute, 0 );
            Assert.AreEqual( consoInfo.Time.Second, 0 );
        }

        [TestCaseSource( typeof( ConsolidatorTestData ), "DataSingleTick" )]
        public void TestDayGroupingSingleTick( DataTick dataTick )
        {
            bool status = true;
            ConsolidatorDay consoDay = new ConsolidatorDay();
            ConsoInfo consoInfo = new ConsoInfo();

            consoDay.group( 0, dataTick, ref consoInfo );
            status = consoDay.close( ref consoInfo );
            Assert.IsTrue( status );

            double expectedBid = dataTick.bid;
            Assert.AreEqual( consoInfo.BidOpen, expectedBid );
            Assert.AreEqual( consoInfo.BidClose, expectedBid );
            Assert.AreEqual( consoInfo.BidHigh, expectedBid );
            Assert.AreEqual( consoInfo.BidLow, expectedBid );

            double expectedAsk = dataTick.ask;
            Assert.AreEqual( consoInfo.AskOpen, expectedAsk );
            Assert.AreEqual( consoInfo.AskClose, expectedAsk );
            Assert.AreEqual( consoInfo.AskHigh, expectedAsk );
            Assert.AreEqual( consoInfo.AskLow, expectedAsk );

            Assert.AreEqual( consoInfo.Time.Year, dataTick.time.Year );
            Assert.AreEqual( consoInfo.Time.Month, dataTick.time.Month );
            Assert.AreEqual( consoInfo.Time.Day, dataTick.time.Day );
            Assert.AreEqual( consoInfo.Time.Hour, 0 );
            Assert.AreEqual( consoInfo.Time.Minute, 0 );
            Assert.AreEqual( consoInfo.Time.Second, 0 );
        }
        #endregion

        #region Test Grouping Multiple Tick Data
        [TestCaseSource( typeof( ConsolidatorTestData ), "DataMinuteTickList" )]
        public void TestMinuteGroupingMultipleTick( List<DataTick> dataTickList )
        {
            bool status = true;
            ConsolidatorMinute consoMinute = new ConsolidatorMinute();
            ConsoInfo consoInfo = new ConsoInfo();

            int ctIndex = 0;
            foreach ( DataTick tick in dataTickList )
            {
                status = consoMinute.group( ctIndex, tick, ref consoInfo );
                ++ctIndex;
            }            
            Assert.IsFalse( status );

            double bidOpen = dataTickList[0].bid;
            double askOpen = dataTickList[0].ask;
            double bidClose = dataTickList[2].bid;
            double askClose = dataTickList[2].ask;
            double bidLow = dataTickList[1].bid;
            double askLow = dataTickList[1].ask;
            double bidHigh = dataTickList[2].bid;
            double askHigh = dataTickList[2].ask;

            Assert.AreEqual( consoInfo.BidOpen, bidOpen );
            Assert.AreEqual( consoInfo.BidClose, bidClose );
            Assert.AreEqual( consoInfo.BidHigh, bidHigh );
            Assert.AreEqual( consoInfo.BidLow, bidLow );

            Assert.AreEqual( consoInfo.AskOpen, askOpen );
            Assert.AreEqual( consoInfo.AskClose, askClose );
            Assert.AreEqual( consoInfo.AskHigh, askHigh );
            Assert.AreEqual( consoInfo.AskLow, askLow );

            DateTime dt = dataTickList[0].time;
            Assert.AreEqual( consoInfo.Time.Year, dt.Year );
            Assert.AreEqual( consoInfo.Time.Month, dt.Month );
            Assert.AreEqual( consoInfo.Time.Day, dt.Day );
            Assert.AreEqual( consoInfo.Time.Hour, dt.Hour );
            Assert.AreEqual( consoInfo.Time.Minute, dt.Minute / GlobalConstant.MINUTE_BLOCK * GlobalConstant.MINUTE_BLOCK );
            Assert.AreEqual( consoInfo.Time.Second, 0 );
        }

        [TestCaseSource( typeof( ConsolidatorTestData ), "DataHourTickList" )]
        public void TestHourGroupingMultipleTick( List<DataTick> dataTickList )
        {
            bool status = true;
            ConsolidatorHour consoHour = new ConsolidatorHour();
            ConsoInfo consoInfo = new ConsoInfo();

            int ctIndex = 0;
            foreach ( DataTick tick in dataTickList )
            {
                status = consoHour.group( ctIndex, tick, ref consoInfo );
                ++ctIndex;
            }
            Assert.IsFalse( status );

            double bidOpen = dataTickList[0].bid;
            double askOpen = dataTickList[0].ask;
            double bidClose = dataTickList[2].bid;
            double askClose = dataTickList[2].ask;
            double bidLow = dataTickList[1].bid;
            double askLow = dataTickList[1].ask;
            double bidHigh = dataTickList[2].bid;
            double askHigh = dataTickList[2].ask;

            Assert.AreEqual( consoInfo.BidOpen, bidOpen );
            Assert.AreEqual( consoInfo.BidClose, bidClose );
            Assert.AreEqual( consoInfo.BidHigh, bidHigh );
            Assert.AreEqual( consoInfo.BidLow, bidLow );

            Assert.AreEqual( consoInfo.AskOpen, askOpen );
            Assert.AreEqual( consoInfo.AskClose, askClose );
            Assert.AreEqual( consoInfo.AskHigh, askHigh );
            Assert.AreEqual( consoInfo.AskLow, askLow );

            DateTime dt = dataTickList[0].time;
            Assert.AreEqual( consoInfo.Time.Year, dt.Year );
            Assert.AreEqual( consoInfo.Time.Month, dt.Month );
            Assert.AreEqual( consoInfo.Time.Day, dt.Day );
            Assert.AreEqual( consoInfo.Time.Hour, dt.Hour );
            Assert.AreEqual( consoInfo.Time.Minute, 0 );
            Assert.AreEqual( consoInfo.Time.Second, 0 );
        }

        [TestCaseSource( typeof( ConsolidatorTestData ), "DataDayTickList" )]
        public void TestDayGroupingMultipleTick( List<DataTick> dataTickList )
        {
            bool status = true;
            ConsolidatorDay consoDay = new ConsolidatorDay();
            ConsoInfo consoInfo = new ConsoInfo();

            int ctIndex = 0;
            foreach ( DataTick tick in dataTickList )
            {
                status = consoDay.group( ctIndex, tick, ref consoInfo );
                ++ctIndex;
            }
            Assert.IsFalse( status );

            double bidOpen = dataTickList[0].bid;
            double askOpen = dataTickList[0].ask;
            double bidClose = dataTickList[2].bid;
            double askClose = dataTickList[2].ask;
            double bidLow = dataTickList[1].bid;
            double askLow = dataTickList[1].ask;
            double bidHigh = dataTickList[2].bid;
            double askHigh = dataTickList[2].ask;

            Assert.AreEqual( consoInfo.BidOpen, bidOpen );
            Assert.AreEqual( consoInfo.BidClose, bidClose );
            Assert.AreEqual( consoInfo.BidHigh, bidHigh );
            Assert.AreEqual( consoInfo.BidLow, bidLow );

            Assert.AreEqual( consoInfo.AskOpen, askOpen );
            Assert.AreEqual( consoInfo.AskClose, askClose );
            Assert.AreEqual( consoInfo.AskHigh, askHigh );
            Assert.AreEqual( consoInfo.AskLow, askLow );

            DateTime dt = dataTickList[0].time;
            Assert.AreEqual( consoInfo.Time.Year, dt.Year );
            Assert.AreEqual( consoInfo.Time.Month, dt.Month );
            Assert.AreEqual( consoInfo.Time.Day, dt.Day );
            Assert.AreEqual( consoInfo.Time.Hour, 0 );
            Assert.AreEqual( consoInfo.Time.Minute, 0 );
            Assert.AreEqual( consoInfo.Time.Second, 0 );
        }
        #endregion

        #region Test Grouping with Partial Close
        [TestCaseSource( typeof( ConsolidatorTestData ), "DataMinuteTickList" )]
        public void TestMinuteGroupingPartialClose( List<DataTick> dataTickList )
        {
            bool status = true;
            ConsolidatorMinute consoMinute = new ConsolidatorMinute();
            ConsoInfo consoInfo = new ConsoInfo();

            int ctIndex = 0;
            foreach ( DataTick tick in dataTickList )
            {
                status = consoMinute.group( ctIndex, tick, ref consoInfo );
                ++ctIndex;
            }
            status = consoMinute.close( ref consoInfo );
            Assert.IsTrue( status );

            double bidOpen = dataTickList[3].bid;
            double askOpen = dataTickList[3].ask;
            double bidClose = dataTickList[4].bid;
            double askClose = dataTickList[4].ask;
            double bidLow = dataTickList[4].bid;
            double askLow = dataTickList[4].ask;
            double bidHigh = dataTickList[3].bid;
            double askHigh = dataTickList[3].ask;

            Assert.AreEqual( consoInfo.BidOpen, bidOpen );
            Assert.AreEqual( consoInfo.BidClose, bidClose );
            Assert.AreEqual( consoInfo.BidHigh, bidHigh );
            Assert.AreEqual( consoInfo.BidLow, bidLow );

            Assert.AreEqual( consoInfo.AskOpen, askOpen );
            Assert.AreEqual( consoInfo.AskClose, askClose );
            Assert.AreEqual( consoInfo.AskHigh, askHigh );
            Assert.AreEqual( consoInfo.AskLow, askLow );

            DateTime dt = dataTickList[3].time;
            Assert.AreEqual( consoInfo.Time.Year, dt.Year );
            Assert.AreEqual( consoInfo.Time.Month, dt.Month );
            Assert.AreEqual( consoInfo.Time.Day, dt.Day );
            Assert.AreEqual( consoInfo.Time.Hour, dt.Hour );
            Assert.AreEqual( consoInfo.Time.Minute, dt.Minute / GlobalConstant.MINUTE_BLOCK * GlobalConstant.MINUTE_BLOCK );
            Assert.AreEqual( consoInfo.Time.Second, 0 );
        }

        [TestCaseSource( typeof( ConsolidatorTestData ), "DataHourTickList" )]
        public void TestHourGroupingPartialClose( List<DataTick> dataTickList )
        {
            bool status = true;
            ConsolidatorHour consoHour = new ConsolidatorHour();
            ConsoInfo consoInfo = new ConsoInfo();

            int ctIndex = 0;
            foreach ( DataTick tick in dataTickList )
            {
                status = consoHour.group( ctIndex, tick, ref consoInfo );
                ++ctIndex;
            }
            status = consoHour.close( ref consoInfo );
            Assert.IsTrue( status );

            double bidOpen = dataTickList[3].bid;
            double askOpen = dataTickList[3].ask;
            double bidClose = dataTickList[4].bid;
            double askClose = dataTickList[4].ask;
            double bidLow = dataTickList[4].bid;
            double askLow = dataTickList[4].ask;
            double bidHigh = dataTickList[3].bid;
            double askHigh = dataTickList[3].ask;

            Assert.AreEqual( consoInfo.BidOpen, bidOpen );
            Assert.AreEqual( consoInfo.BidClose, bidClose );
            Assert.AreEqual( consoInfo.BidHigh, bidHigh );
            Assert.AreEqual( consoInfo.BidLow, bidLow );

            Assert.AreEqual( consoInfo.AskOpen, askOpen );
            Assert.AreEqual( consoInfo.AskClose, askClose );
            Assert.AreEqual( consoInfo.AskHigh, askHigh );
            Assert.AreEqual( consoInfo.AskLow, askLow );

            DateTime dt = dataTickList[3].time;
            Assert.AreEqual( consoInfo.Time.Year, dt.Year );
            Assert.AreEqual( consoInfo.Time.Month, dt.Month );
            Assert.AreEqual( consoInfo.Time.Day, dt.Day );
            Assert.AreEqual( consoInfo.Time.Hour, dt.Hour );
            Assert.AreEqual( consoInfo.Time.Minute, 0 );
            Assert.AreEqual( consoInfo.Time.Second, 0 );
        }

        [TestCaseSource( typeof( ConsolidatorTestData ), "DataDayTickList" )]
        public void TestDayGroupingPartialClose( List<DataTick> dataTickList )
        {
            bool status = true;
            ConsolidatorDay consoDay = new ConsolidatorDay();
            ConsoInfo consoInfo = new ConsoInfo();

            int ctIndex = 0;
            foreach ( DataTick tick in dataTickList )
            {
                status = consoDay.group( ctIndex, tick, ref consoInfo );
                ++ctIndex;
            }
            status = consoDay.close( ref consoInfo );
            Assert.IsTrue( status );

            double bidOpen = dataTickList[3].bid;
            double askOpen = dataTickList[3].ask;
            double bidClose = dataTickList[4].bid;
            double askClose = dataTickList[4].ask;
            double bidLow = dataTickList[4].bid;
            double askLow = dataTickList[4].ask;
            double bidHigh = dataTickList[3].bid;
            double askHigh = dataTickList[3].ask;

            Assert.AreEqual( consoInfo.BidOpen, bidOpen );
            Assert.AreEqual( consoInfo.BidClose, bidClose );
            Assert.AreEqual( consoInfo.BidHigh, bidHigh );
            Assert.AreEqual( consoInfo.BidLow, bidLow );

            Assert.AreEqual( consoInfo.AskOpen, askOpen );
            Assert.AreEqual( consoInfo.AskClose, askClose );
            Assert.AreEqual( consoInfo.AskHigh, askHigh );
            Assert.AreEqual( consoInfo.AskLow, askLow );

            DateTime dt = dataTickList[3].time;
            Assert.AreEqual( consoInfo.Time.Year, dt.Year );
            Assert.AreEqual( consoInfo.Time.Month, dt.Month );
            Assert.AreEqual( consoInfo.Time.Day, dt.Day );
            Assert.AreEqual( consoInfo.Time.Hour, 0 );
            Assert.AreEqual( consoInfo.Time.Minute, 0 );
            Assert.AreEqual( consoInfo.Time.Second, 0 );
        }
        #endregion

    }
}
