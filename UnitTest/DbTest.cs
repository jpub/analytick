﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NUnit.Framework;

using AkBase;

namespace UnitTest
{
    [TestFixture]
    class DbTest
    {
        [Serializable]
        public class ModelData
        {
            static public string DB_ACTION_UPDATE = "DB_ACTION_UPDATE";
            public string Id { get; set; }

            public ModelData( string id )
            {
                this.Id = id;
            }
        }

        class DbData : BaseDb<ModelData>
        {
        }

        class ObserverData : IBaseObserver
        {
            public bool WasNotified { get; set; }

            public ObserverData()
            {
                this.WasNotified = false;
            }

            public void update( string action, object subject )
            {
                if ( action == ModelData.DB_ACTION_UPDATE )
                    this.WasNotified = true;
            }
        }

        [TestCaseSource( typeof( DbTestData ), "DataList" )]
        public void TestUpdate( List<ModelData> dataList )
        {
            DbData db = new DbData();
            foreach ( ModelData data in dataList )
            {
                db.update( data.Id, data );
            }
            Assert.AreEqual( db.count(), dataList.Count );
        }

        [TestCaseSource( typeof( DbTestData ), "DataList" )]
        public void TestUpdateWithNotification( List<ModelData> dataList )
        {
            DbData db = new DbData();
            ObserverData observerData1 = new ObserverData();
            ObserverData observerData2 = new ObserverData();
            db.attach( observerData1 );
            db.attach( observerData2 );
            Assert.IsFalse( observerData1.WasNotified );
            Assert.IsFalse( observerData2.WasNotified );

            db.update( ModelData.DB_ACTION_UPDATE, dataList[0].Id, dataList[0] );
            Assert.AreEqual( db.count(), 1 );
            Assert.IsTrue( observerData1.WasNotified );
            Assert.IsTrue( observerData2.WasNotified );
        }

        [TestCaseSource( typeof( DbTestData ), "DataList" )]
        public void TestRetrieve( List<ModelData> dataList )
        {
            DbData db = new DbData();
            foreach ( ModelData data in dataList )
            {
                db.update( data.Id, data );
            }

            ModelData outData = null;
            string id = dataList[0].Id;
            db.get( id, out outData );
            Assert.AreEqual( dataList[0].Id, outData.Id );
        }

        [TestCaseSource( typeof( DbTestData ), "DataList" )]
        public void TestContain( List<ModelData> dataList )
        {
            DbData db = new DbData();
            foreach ( ModelData data in dataList )
            {
                db.update( data.Id, data );
            }
            
            bool status = db.contain( dataList[0].Id );
            Assert.IsTrue( status );
        }

        [TestCaseSource( typeof( DbTestData ), "DataList" )]
        public void TestDelete( List<ModelData> dataList )
        {
            DbData db = new DbData();
            foreach ( ModelData data in dataList )
            {
                db.update( data.Id, data );
            }

            string id = dataList[0].Id;
            bool status = db.contain( id );
            Assert.IsTrue( status );

            ModelData temp = null;
            db.delete( id, out temp );
            Assert.AreEqual( id, temp.Id );
            status = db.contain( id );
            Assert.IsFalse( status );
        }

        [TestCaseSource( typeof( DbTestData ), "DataList" )]
        public void TestDeleteList( List<ModelData> dataList )
        {
            DbData db = new DbData();
            foreach ( ModelData data in dataList )
            {
                db.update( data.Id, data );
            }

            string id1 = dataList[0].Id;
            string id2 = dataList[2].Id;
            bool status = db.contain( id1 );
            Assert.IsTrue( status );
            status = db.contain( id2 );
            Assert.IsTrue( status );

            List<string> idList = new List<string>() { id1, id2 };
            db.deleteList( idList );
            status = db.contain( id1 );
            Assert.IsFalse( status );
            status = db.contain( id2 );
            Assert.IsFalse( status );
        }

        [TestCaseSource( typeof( DbTestData ), "DataList" )]
        public void TestRetrieveCopyList( List<ModelData> dataList )
        {
            DbData db = new DbData();
            foreach ( ModelData data in dataList )
            {
                db.update( data.Id, data );
            }

            List<ModelData> copyList = db.getCopyListValues();
            foreach ( ModelData copyData in copyList )
            {
                bool found = false;
                foreach ( ModelData orgData in dataList )
                {
                    if ( copyData.Id == orgData.Id )
                    {
                        found = true;
                        break;
                    }
                }
                Assert.IsTrue( found );
            }            
        }
    }
}
