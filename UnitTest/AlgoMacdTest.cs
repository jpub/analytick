﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NUnit.Framework;

using AkCommon;
using AkAlgo;

namespace UnitTest
{
    [TestFixture]
    class AlgoMacdTest
    {
        private List<double> dataPointList = null;
        private int periodFast;
        private int periodSlow;
        private int periodSignal;

        [OneTimeSetUp]
        public void InitOnce()
        {
            this.dataPointList = new List<double>() {
                1, 3, 5, 10, 13, 9, 5, 1, 17, 20
            };

            this.periodFast = 2;
            this.periodSlow = 4;
            this.periodSignal = 3;
        }

        [Test]
        public void TestMacdCalulation()
        {
            List<double> dpFastList = new List<double>();
            List<double> dpSlowList = new List<double>();
            List<double> dpMacdList = new List<double>();

            AlgoEma emaFast = new AlgoEma( this.periodFast );
            AlgoEma emaSlow = new AlgoEma( this.periodSlow );
            AlgoEma emaSignal = new AlgoEma( this.periodSignal );
            AlgoMacd algoMacd = new AlgoMacd( this.periodFast, this.periodSlow, this.periodSignal );

            bool status = false;

            double dpFast;
            double dpSlow;
            double dpMacd;
            double dpSignal;
            double dpExpectedSignal;

            for ( int i = 0; i <= 2; ++i )
            {
                emaFast.addDataPoint( this.dataPointList[i], out dpFast );
                emaSlow.addDataPoint( this.dataPointList[i], out dpSlow );
                status = algoMacd.addDataPoint( this.dataPointList[i], out dpMacd, out dpSignal );
                Assert.IsFalse( status );
            }

            for ( int i = 3; i <= 4; ++i )
            {
                emaFast.addDataPoint( this.dataPointList[i], out dpFast );
                emaSlow.addDataPoint( this.dataPointList[i], out dpSlow );
                status = algoMacd.addDataPoint( this.dataPointList[i], out dpMacd, out dpSignal );
                emaSignal.addDataPoint( dpMacd, out dpExpectedSignal );
                Assert.IsTrue( status );
                Assert.AreEqual( ( dpFast - dpSlow ), dpMacd );
                Assert.AreEqual( AlgoMacd.UNDEFINE_VALUE, dpSignal );
            }

            for ( int i = 5; i <= 9; ++i )
            {
                emaFast.addDataPoint( this.dataPointList[5], out dpFast );
                emaSlow.addDataPoint( this.dataPointList[5], out dpSlow );
                status = algoMacd.addDataPoint( this.dataPointList[5], out dpMacd, out dpSignal );
                emaSignal.addDataPoint( dpMacd, out dpExpectedSignal );
                Assert.IsTrue( status );
                Assert.AreEqual( ( dpFast - dpSlow ), dpMacd );
                Assert.AreEqual( dpExpectedSignal, dpSignal );
            }
        }
    }
}
