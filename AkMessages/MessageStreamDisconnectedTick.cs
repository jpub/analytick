﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AkBase;

namespace AkMessages
{
    public class MessageStreamDisconnectedTick : BaseOandaMessage
    {
        public DataDisconnect disconnect { get; set; }

        public MessageStreamDisconnectedTick()
        {
            this.MessageType = BaseMessage.ID_DISCONNECTED_TICK;
        }
    }
}
