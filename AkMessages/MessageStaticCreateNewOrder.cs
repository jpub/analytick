﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AkBase;

namespace AkMessages
{
    public class MessageStaticCreateNewOrder : BaseOandaMessage
    {
        public string instrument { get; set; }        
        public double price { get; set; }
        public DataTrade tradeOpened { get; set; }

        public MessageStaticCreateNewOrder()
        {
            this.MessageType = BaseMessage.ID_CREATE_NEW_ORDER;
        }
    }
}
