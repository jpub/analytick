﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AkBase;

namespace AkMessages
{
    public class MessageStrategyCreateNewOrder : BaseOandaMessage
    {
        //uint tradeUnits, string side, double stopLossPrice, double takeProfitPrice, double price
        public string instrument { get; set; }
        public string side { get; set; }
        public double price { get; set; }
        public uint units { get; set; }

        public MessageStrategyCreateNewOrder()
        {
            this.MessageType = BaseMessage.ID_STRATEGY_CREATE_NEW_ORDER;
        }
    }
}
