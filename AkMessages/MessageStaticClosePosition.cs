﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AkBase;

namespace AkMessages
{
    public class MessageStaticClosePosition : BaseOandaMessage
    {
        public List<long> ids { get; set; }
        public string instrument { get; set; }
        public int totalUnits { get; set; }
        public double price { get; set; }

        public MessageStaticClosePosition()
        {
            this.MessageType = BaseMessage.ID_CLOSE_POSITION;
        }
    }
}
