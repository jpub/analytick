﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AkBase;

namespace AkMessages
{
    public class MessageStaticOpenTradeList : BaseOandaMessage
    {
        public List<DataTrade> trades { get; set; }
        public string nextPage { get; set; }

        public MessageStaticOpenTradeList()
        {
            this.MessageType = BaseMessage.ID_OPEN_TRADE_LIST;
        }
    }
}
