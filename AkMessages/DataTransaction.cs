﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AkMessages
{
    public class DataTransaction
    {
        public long id { get; set; }
        public int accountId { get; set; }
        public DateTime time { get; set; }
        public string type { get; set; }
        public string instrument { get; set; }
        public string side { get; set; }
        public int units { get; set; }
        public double price { get; set; }
        public double lowerBound { get; set; }
        public double upperBound { get; set; }
        public double takeProfitPrice { get; set; }
        public double stopLossPrice { get; set; }
        public double trailingStopLossDistance { get; set; }
        public double pl { get; set; }
        public double interest { get; set; }
        public double accountBalance { get; set; }
        public long tradeId { get; set; }
        public long orderId { get; set; }
    }
}
