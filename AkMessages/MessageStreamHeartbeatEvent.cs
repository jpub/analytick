﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AkBase;

namespace AkMessages
{
    public class MessageStreamHeartbeatEvent : BaseOandaMessage
    {
        public MessageStreamHeartbeatEvent()
        {
            this.MessageType = BaseMessage.ID_HEARTBEAT_EVENT;
        }
    }
}
