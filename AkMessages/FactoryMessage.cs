﻿using System;
using System.Web.Script.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

using AkBase;

namespace AkMessages
{
    public class FactoryMessage
    {
        private JavaScriptSerializer serializer = new JavaScriptSerializer();

        public BaseMessage getMessage( string messageType, string jsonString )
        {
            if ( jsonString == "" )
                return null;

            BaseMessage message = null;
            if ( messageType == BaseMessage.ID_TICK )
            {
                message = serializer.Deserialize<MessageStreamTick>( jsonString );
            }
            else if ( messageType == BaseMessage.ID_HEARTBEAT_TICK )
            {
                message = serializer.Deserialize<MessageStreamHeartbeatTick>( jsonString );
            }
            else if ( messageType == BaseMessage.ID_HEARTBEAT_EVENT )
            {
                message = serializer.Deserialize<MessageStreamHeartbeatEvent>( jsonString );
            }
            else if ( messageType == BaseMessage.ID_DISCONNECTED_TICK )
            {
                message = serializer.Deserialize<MessageStreamDisconnectedTick>( jsonString );
            }
            else if ( messageType == BaseMessage.ID_DISCONNECTED_EVENT )
            {
                message = serializer.Deserialize<MessageStreamDisconnectedEvent>( jsonString );
            }
            else if ( messageType == BaseMessage.ID_EVENT )
            {
                message = serializer.Deserialize<MessageStreamEvent>( jsonString );
            }
            else if ( messageType == BaseMessage.ID_ACCOUNT_DETAILS )
            {
                message = serializer.Deserialize<MessageStaticAccountDetails>( jsonString );
            }
            else if ( messageType == BaseMessage.ID_OPEN_TRADE_LIST )
            {
                message = serializer.Deserialize<MessageStaticOpenTradeList>( jsonString );
            }
            else if ( messageType == BaseMessage.ID_CURRENT_PRICES )
            {
                message = serializer.Deserialize<MessageStaticCurrentPrices>( jsonString );
            }
            else if ( messageType == BaseMessage.ID_CANDLE_LIST )
            {               
                message = serializer.Deserialize<MessageStaticCandleList>( jsonString );
            }
            else if ( messageType == BaseMessage.ID_CLOSE_POSITION )
            {
                message = serializer.Deserialize<MessageStaticClosePosition>( jsonString );
            }
            else if ( messageType == BaseMessage.ID_CREATE_NEW_ORDER )
            {
                message = serializer.Deserialize<MessageStaticCreateNewOrder>( jsonString );
            }
            else if ( messageType == BaseMessage.ID_MODIFY_EXISTING_TRADE )
            {
                message = serializer.Deserialize<MessageStaticModifyExistingTrade>( jsonString );
            }

            return message;
        }
    }
}
