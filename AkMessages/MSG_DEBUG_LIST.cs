﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkMessages
{
    public class MSG_DEBUG_LIST
    {
        private List<MSG_DEBUG> messageList = new List<MSG_DEBUG>();

        public void sort()
        {
            lock ( this )
            {
                this.messageList = this.messageList.OrderBy( o => o.MessageTime ).ToList();
            }
        }

        public void clear()
        {
            lock ( this )
            {
                this.messageList.Clear();
            }
        }

        public void add( MSG_DEBUG message )
        {
            lock ( this )
            {
                this.messageList.Add( message );
            }
        }

        public MSG_DEBUG get( int index )
        {
            lock ( this )
            {
                return this.messageList[index];
            }
        }

        public List<MSG_DEBUG> getTillTime( int startIndex, DateTime tillTime, out int stopIndex )
        {
            lock ( this )
            {
                List<MSG_DEBUG> intervalList = new List<MSG_DEBUG>();

                stopIndex = startIndex;
                for ( int i = startIndex; i < this.messageList.Count; ++i )
                {
                    stopIndex = i;
                    if ( this.messageList[i].MessageTime <= tillTime )
                    {
                        intervalList.Add( this.messageList[i] );
                        continue;
                    }
                    else
                    {
                        break;
                    }
                }
                return intervalList;
            }
        }

        public int count()
        {
            lock ( this )
            {
                return this.messageList.Count;
            }
        }

        public List<MSG_DEBUG> getShallowList()
        {
            lock ( this )
            {
                return new List<MSG_DEBUG>( this.messageList );
            }
        }
    }
}
