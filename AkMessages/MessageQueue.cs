﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace AkMessages
{
    public class MessageQueue<T>
    {
        private object LockThis = new object();

        private Queue<T> unsafeQueue = new Queue<T>();

        public virtual bool get( ref T t )
        {
            lock ( LockThis )
            {
                while ( this.unsafeQueue.Count == 0 )
                {
                    Monitor.Wait( LockThis );

                    // could be waken up from pulseAll
                    if ( this.unsafeQueue.Count == 0 )
                        return false;
                }

                t = this.unsafeQueue.Dequeue();
                return true;
            }
        }

        public virtual void add( T message )
        {
            lock ( LockThis )
            {
                this.unsafeQueue.Enqueue( message );
                Monitor.Pulse( LockThis );
            }
        }

        public virtual void pulseAllThread()
        {
            lock ( LockThis )
            {
                Monitor.PulseAll( LockThis );
            }
        }
    }
}
