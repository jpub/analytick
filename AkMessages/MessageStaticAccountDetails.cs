﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AkBase;

namespace AkMessages
{
    public class MessageStaticAccountDetails : BaseOandaMessage
    {
        public int accountId { get; set; }
        public string accountName;
        public double balance { get; set; }
        public double unrealizedPl { get; set; }
        public double realizedPl { get; set; }
        public double marginUsed { get; set; }
        public double marginAvail { get; set; }
        public int openTrades;
        public int openOrders;
        public double marginRate;
        public string accountCurrency;

        public MessageStaticAccountDetails()
        {
            this.MessageType = BaseMessage.ID_ACCOUNT_DETAILS;
        }
    }
}
