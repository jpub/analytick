﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AkMessages
{
    public class DataTick
    {
        public DataTick( string instrument, DateTime time, double bid, double ask )
        {
            this.instrument = instrument;
            this.time = time;
            this.bid = bid;
            this.ask = ask;
        }

        public DataTick()
        {
        }

        public string instrument { get; set; }
        public DateTime time { get; set; }
        public double bid { get; set; }
        public double ask { get; set; }
    }
}
