﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AkBase;

namespace AkMessages
{
    public class MessageStaticCandleList : BaseOandaMessage
    {
        public string instrument;
        public string granularity;
        public List<DataCandle> candles { get; set; }

        public MessageStaticCandleList()
        {
            this.MessageType = BaseMessage.ID_CANDLE_LIST;
        }
    }
}
