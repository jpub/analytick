﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AkMessages
{
    public class DataCandle
    {
        public DateTime time;
        public double openMid;
        public double highMid;
        public double lowMid;
        public double closeMid;
        public int volume;
        public bool complete;
    }
}
