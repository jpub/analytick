﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AkBase;

namespace AkMessages
{
    public class MessageStaticModifyExistingTrade : BaseOandaMessage
    {
        public long id { get; set; }
        public int units { get; set; }
        public string side { get; set; }
        public string instrument { get; set; }
        public double price { get; set; }
        public double takeProfit { get; set; }
        public double stopLoss { get; set; }
        public int trailingStop { get; set; }
        public int trailingAmount { get; set; }

        public MessageStaticModifyExistingTrade()
        {
            this.MessageType = BaseMessage.ID_MODIFY_EXISTING_TRADE;
        }
    }
}
