﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AkMessages
{
    public class DataDisconnect
    {
        public int code { get; set; }
        public string message { get; set; }
        public string moreInfo { get; set; }
    }
}
