﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AkBase;

namespace AkMessages
{
    public class MessageStreamHeartbeatTick : BaseOandaMessage
    {
        public DateTime heartbeat { get; set; }

        public MessageStreamHeartbeatTick()
        {
            this.MessageType = BaseMessage.ID_HEARTBEAT_TICK;
        }
    }
}
