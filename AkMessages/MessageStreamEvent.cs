﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AkBase;

namespace AkMessages
{
    public class MessageStreamEvent : BaseOandaMessage
    {
        public DataTransaction transaction { get; set; }

        public MessageStreamEvent()
        {
            this.MessageType = BaseMessage.ID_EVENT;
        }
    }
}
