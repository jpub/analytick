﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AkBase;

namespace AkMessages
{
    public class MessageStaticCurrentPrices : BaseOandaMessage
    {
        public long time { get; set; }
        public List<DataPrice> prices { get; set; }

        public MessageStaticCurrentPrices()
        {
            this.MessageType = BaseMessage.ID_CURRENT_PRICES;
        }
    }
}
