﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AkMessages
{
    public class DataPrice
    {
        public string instrument { get; set; }
        public DateTime time;
        public double bid { get; set; }
        public double ask { get; set; }
    }
}
