﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AkBase;

namespace AkMessages
{
    public class MessageStreamTick : BaseOandaMessage
    {
        public DataTick tick { get; set; }

        public MessageStreamTick()
        {
            this.MessageType = BaseMessage.ID_TICK;
        }
    }
}
