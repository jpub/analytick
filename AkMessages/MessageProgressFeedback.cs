﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AkBase;

namespace AkMessages
{
    public class MessageProgressFeedback : BaseMessage
    {
        public MessageProgressFeedback()
        {
            this.MessageType = BaseMessage.ID_PROGRESS_FEEDBACK;
        }

        public bool stopProgram { get; set; }
        public string information { get; set; }
    }
}
