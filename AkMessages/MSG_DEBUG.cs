﻿using System;
using System.Web.Script.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

using AkBase;

namespace AkMessages
{
    public class Record
    {
        private string name;
        private object jsonValue;
        private bool isArray;
        private List<Record> recordList = new List<Record>();

        public List<Record> RecordList
        {
            get { return this.recordList; }
            set { this.recordList = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public bool IsArray
        {
            get { return isArray; }
            set { isArray = value; }
        }

        public object RawValue
        {
            get { return jsonValue; }
            set { jsonValue = value; }
        }

        public string Value
        {
            get
            {
                if ( jsonValue.GetType() == typeof( string ) )
                {
                    return (string)jsonValue;
                }
                else if ( jsonValue.GetType() == typeof( long ) )
                {
                    return ( (long)jsonValue ).ToString();
                }
                else if ( jsonValue.GetType() == typeof( decimal ) )
                {
                    return ( (decimal)jsonValue ).ToString();
                }
                else if ( jsonValue.GetType() == typeof( double ) )
                {
                    return ( (double)jsonValue ).ToString();
                }
                else if ( jsonValue.GetType() == typeof( int ) )
                {
                    return ( (int)jsonValue ).ToString();
                }
                else
                {
                    string tmd = jsonValue.GetType().ToString();
                    return "unknown type";
                }
            }

            set
            {
                try
                {
                    if ( jsonValue.GetType() == typeof( string ) )
                    {
                        jsonValue = value;
                    }
                    else if ( jsonValue.GetType() == typeof( long ) )
                    {
                        jsonValue = long.Parse( value );
                    }
                    else if ( jsonValue.GetType() == typeof( decimal ) )
                    {
                        jsonValue = decimal.Parse( value );
                    }
                    else if ( jsonValue.GetType() == typeof( double ) )
                    {
                        jsonValue = double.Parse( value );
                    }
                    else if ( jsonValue.GetType() == typeof( int ) )
                    {
                        jsonValue = int.Parse( value );
                    }
                    else
                    {
                        jsonValue = "not set";
                    }
                }
                catch ( Exception )
                {
                }
            }
        }

        public Record( string name, object jsonValue, bool isArray )
        {
            this.name = name;
            this.jsonValue = jsonValue;
            this.isArray = isArray;
        }
    }

    public class MSG_DEBUG
    {
        private JavaScriptSerializer serializer = new JavaScriptSerializer();
        private List<Record> jsonRecordList = new List<Record>();

        private uint index;
        private string name;
        private string messageValue;
        private DateTime messageTime;        

        public List<Record> RecordList
        {
            get { return this.jsonRecordList; }
            set { this.jsonRecordList = value; }
        }

        public uint Index
        {
            get { return index; }
            set { index = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Value
        {
            get { return messageValue; }
            set { messageValue = value; }
        }

        public DateTime MessageTime
        {
            get { return messageTime; }
            set { messageTime = value; }
        }

        public MSG_DEBUG( Dictionary<string, object> jsonGraph )
        {
            foreach ( KeyValuePair<string, object> kvp in jsonGraph )
            {
                if ( kvp.Value.GetType() == typeof( Dictionary<string, object> ) )
                {
                    Dictionary<string, object> dictData = (Dictionary<string, object>)kvp.Value;
                    if ( dictData.ContainsKey( "MessageType" ) )
                        this.name = (string)dictData["MessageType"];

                    Record dictRecord = new Record( kvp.Key, "", false );
                    this.jsonRecordList.Add( dictRecord );
                    this.deserializeDict( dictRecord.RecordList, dictData );
                }
                else if ( kvp.Value.GetType() == typeof( ArrayList ) )
                {
                    ArrayList arrayData = (ArrayList)kvp.Value;
                    Record arrayRecord = new Record( kvp.Key, "", true );
                    this.jsonRecordList.Add( arrayRecord );
                    this.deserializeArray( arrayRecord.RecordList, arrayData, kvp.Key );
                }
                else
                {
                    if ( kvp.Key == "timeReceived" )
                        this.messageTime = Convert.ToDateTime( (string)kvp.Value );
                    else if ( kvp.Key == "MessageType" )
                        this.name = (string)kvp.Value;

                    this.jsonRecordList.Add( new Record( kvp.Key, kvp.Value, false ) );
                }
            }
        }

        public Dictionary<string, object> toJsonGraph()
        {
            Dictionary<string, object> jsonGraph = new Dictionary<string, object>();
            foreach ( Record record in this.jsonRecordList )
                this.serializeDict( record, null, jsonGraph );

            return jsonGraph;
        }

        public Dictionary<string, object> toJsonGraphDataOnly()
        {
            Dictionary<string, object> jsonGraph = new Dictionary<string, object>();
            foreach ( Record record in this.jsonRecordList )
                this.serializeDict( record, null, jsonGraph );

            if ( ( (Dictionary<string, object>)jsonGraph ).ContainsKey( "data" ) )
                return (Dictionary<string, object>)jsonGraph["data"];
            else
                return jsonGraph;
        }

        public Dictionary<string, object> toFamJsonGraph()
        {
            Dictionary<string, object> jsonGraph = new Dictionary<string, object>();
            foreach ( Record record in this.jsonRecordList )
                this.serializeDict( record, null, jsonGraph );

            Dictionary<string, object> timeDataDic = new Dictionary<string, object>();
            timeDataDic.Add( "data", jsonGraph );
            timeDataDic.Add( "timeReceived", this.messageTime.ToUniversalTime().ToString( "O" ) );
            return timeDataDic;
        }

        #region deserialization
        private void deserializeDict( List<Record> recordList, Dictionary<string, object> jsonData )
        {
            foreach ( KeyValuePair<string, object> kvp in jsonData )
            {
                if ( kvp.Value.GetType() == typeof( ArrayList ) )
                {
                    ArrayList arrayData = (ArrayList)kvp.Value;
                    Record arrayRecord = new Record( kvp.Key, "", true );
                    recordList.Add( arrayRecord );
                    this.deserializeArray( arrayRecord.RecordList, arrayData, kvp.Key );
                }
                else if ( kvp.Value.GetType() == typeof( Dictionary<string, object> ) )
                {
                    Dictionary<string, object> dictData = (Dictionary<string, object>)kvp.Value;
                    Record dictRecord = new Record( kvp.Key, "", false );
                    recordList.Add( dictRecord );
                    this.deserializeDict( dictRecord.RecordList, dictData );
                }
                else
                {
                    recordList.Add( new Record( kvp.Key, kvp.Value, false ) );
                }
            }
        }

        private void deserializeArray( List<Record> recordList, ArrayList arrayData, string arrayName )
        {
            int counter = 0;
            foreach ( object obj in arrayData )
            {                
                if ( obj.GetType() == typeof( Dictionary<string, object> ) )
                {
                    Dictionary<string, object> dictData = (Dictionary<string, object>)obj;
                    Record dictRecord = new Record( "[" + counter.ToString() + "]", "", false );
                    recordList.Add( dictRecord );
                    this.deserializeDict( dictRecord.RecordList, dictData );                    
                }
                else
                {
                    //recordList.Add( new Record( "problem key", "problem value", false ) );
                    recordList.Add( new Record( "[" + counter.ToString() + "]", obj, false ) );
                }
                ++counter;
            }
        }
        #endregion

        #region serialization
        private void serializeDict( Record record, ArrayList arrayData, Dictionary<string, object> jsonGraph )
        {
            if ( record.RecordList.Count > 0 )
            {
                if ( record.IsArray )
                {
                    ArrayList arrayList = new ArrayList();
                    jsonGraph.Add( record.Name, arrayList );
                    this.serializeArray( record.RecordList, arrayList, jsonGraph );
                }
                else
                {
                    Dictionary<string, object> dictGraph = new Dictionary<string, object>();
                    if ( arrayData != null )
                        arrayData.Add( dictGraph );
                    else if ( jsonGraph != null )
                        jsonGraph.Add( record.Name, dictGraph );

                    foreach ( Record subRecord in record.RecordList )
                        this.serializeDict( subRecord, null, dictGraph );
                }
            }
            else
            {
                if ( record.IsArray )
                    jsonGraph.Add( record.Name, new List<object>() );
                else
                    jsonGraph.Add( record.Name, record.RawValue );
            }
        }

        private void serializeArray( List<Record> recordList, ArrayList arrayData, Dictionary<string, object> jsonGraph )
        {
            foreach ( Record record in recordList )
            {
                if ( (string)record.RawValue == "" )
                    this.serializeDict( record, arrayData, null );
                else
                    arrayData.Add( record.RawValue );
                /*
                if ( record.RawValue.GetType() == typeof( Dictionary<string, object> ) )
                    this.serializeDict( record, arrayData, null );
                else
                    arrayData.Add( record.RawValue );
                */
            }
        }
        #endregion
    }
}
