﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;

using AkBase;
using AkCommon;
using AkModel;
using AkWidget;
using AkDatabase;

namespace AkView
{
    public class ViewActiveTrade : BaseView
    {
        public bool IsShowValue { get; set; }

        public ViewActiveTrade()
        {
            this.IsShowValue = true;
        }

        override public void draw( GraphPanel graphPanel, Graphics g )
        {
            if ( !this.Visible )
                return;

            if ( graphPanel.MinX == graphPanel.MaxX )
                return;

            ModelActiveTrade model = null;
            if ( !DbActiveTrade.getInstance().get( GlobalConstant.DB_NAME_ACTIVE_TRADE, out model ) )
                return;

            if ( model.Bid == 0 )
                return;

            /*
            Point pBid = graphPanel.logicalToPixel( 0, model.Bid );
            Point pAsk = graphPanel.logicalToPixel( 0, model.Ask );            

            Point horiBidP1 = new Point( 0, pBid.Y );
            Point horiBidP2 = new Point( graphPanel.Width, pBid.Y );

            Point horiAskP1 = new Point( 0, pAsk.Y );
            Point horiAskP2 = new Point( graphPanel.Width, pAsk.Y );

            g.DrawLine( GlobalConstant.PEN_CURRENT_TICK_LINE, horiBidP1, horiBidP2 );
            g.DrawLine( GlobalConstant.PEN_CURRENT_TICK_LINE, horiAskP1, horiAskP2 );
            g.FillRectangle( GlobalConstant.BRUSH_CURRENT_TICK_FILL, 0, horiAskP1.Y, graphPanel.Width, horiBidP1.Y - horiAskP1.Y );
            */
            Point pBidAskMid = graphPanel.logicalToPixel( 0, ( model.Bid + model.Ask ) / 2 );
            Point horiBidAskMidP1 = new Point( 0, pBidAskMid.Y );
            Point horiBidAskMidP2 = new Point( graphPanel.Width, pBidAskMid.Y );
            g.DrawLine( GlobalConstant.PEN_CURRENT_TICK_LINE, horiBidAskMidP1, horiBidAskMidP2 );

            if ( model.EntryPrice != 0 )
            {
                double dollarPnl = 0;
                double pipPnl = 0;
                this.getPnl( model, out dollarPnl, out pipPnl );

                Point pEntryPrice = graphPanel.logicalToPixel( 0, model.EntryPrice );
                Point pTakeProfit = graphPanel.logicalToPixel( 0, model.TakeProfit );
                Point pStopLoss = graphPanel.logicalToPixel( 0, model.StopLoss );

                Point horiEntryPriceP1 = new Point( 0, pEntryPrice.Y );
                Point horiEntryPriceP2 = new Point( graphPanel.Width, pEntryPrice.Y );

                Point horiTakeProfitP1 = new Point( 0, pTakeProfit.Y );
                Point horiTakeProfitP2 = new Point( graphPanel.Width, pTakeProfit.Y );

                Point horiStopLossP1 = new Point( 0, pStopLoss.Y );
                Point horiStopLossP2 = new Point( graphPanel.Width, pStopLoss.Y );

                //g.DrawLine( GlobalConstant.PEN_LINE_EMA_T2, horiEntryPriceP1, horiEntryPriceP2 );
                g.DrawLine( GlobalConstant.PEN_TAKE_PROFIT, horiTakeProfitP1, horiTakeProfitP2 );
                g.DrawLine( GlobalConstant.PEN_STOP_LOSS, horiStopLossP1, horiStopLossP2 );

                //Point pPnl = horiAskP2;
                Point pPnl = horiBidAskMidP2;
                pPnl.X = pPnl.X - 120;
                pPnl.Y = pPnl.Y - 26;
                string pnlStr = string.Format( "{0:0.0}  ${1:0.0000}", pipPnl, dollarPnl );
                float strWidth = g.MeasureString( pnlStr, GlobalConstant.COEF_FONT_MEDIUM ).Width;
                g.FillRectangle( GlobalConstant.BRUSH_SIMULATION_NAME_BACKGROUND, pPnl.X - 2, pPnl.Y - 2, strWidth + 2, 22 );
                g.DrawRectangle( GlobalConstant.PEN_SIMULATION_NAME, pPnl.X - 2, pPnl.Y - 2, strWidth + 2, 22 );

                if ( pipPnl >= 0 )
                    g.DrawString( pnlStr, GlobalConstant.COEF_FONT_MEDIUM, GlobalConstant.BRUSH_RISING_CANDLE, pPnl );
                else
                    g.DrawString( pnlStr, GlobalConstant.COEF_FONT_MEDIUM, GlobalConstant.BRUSH_DROPPING_CANDLE, pPnl );
            }
        }

        private void getPnl( ModelActiveTrade model, out double dollarPnl, out double pipPnl )
        {
            double bid = model.Bid;
            double ask = model.Ask;

            string tradeSide = model.Side;
            int units = model.Units;
            double entryPrice = model.EntryPrice;
            double stopLossPrice = model.StopLoss;
            double takeProfitPrice = model.TakeProfit;            

            if ( tradeSide == "buy" )
            {
                dollarPnl = ( bid - entryPrice ) * units;
                pipPnl = ( bid - entryPrice ) / GlobalConstant.ONE_PIP_PRICE;
            }
            else
            {
                dollarPnl = ( entryPrice - ask ) * units;
                pipPnl = ( entryPrice - ask ) / GlobalConstant.ONE_PIP_PRICE;
            }            
        }
    }
}
