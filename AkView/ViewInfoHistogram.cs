﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

using AkBase;
using AkCommon;
using AkWidget;

namespace AkView
{
    public class ViewInfoHistogram : BaseView
    {        
        public Point CurrentMousePoint { get; set; }

        public ViewInfoHistogram()
        {
            this.CurrentMousePoint = new Point( -1, -1 );            
        }

        override public void draw( GraphPanel graphPanel, Graphics g )
        {
            if ( !this.Visible )
                return;

            if ( this.CurrentMousePoint.X != -1 && this.CurrentMousePoint.Y != -1 )
            {
                
                PointD p = graphPanel.pixelToLogical( this.CurrentMousePoint.X, this.CurrentMousePoint.Y );
                Point pixelPoint = graphPanel.logicalToPixel( (int)p.X, (int)p.Y );
                int snapX = pixelPoint.X;
                int snapY = pixelPoint.Y;

                if ( snapX < 0 )
                    return;

                Point vertP1 = new Point( snapX, 0 );
                Point vertP2 = new Point( snapX, graphPanel.Height );
                g.DrawLine( GlobalConstant.PEN_CROSS_HAIR, vertP1, vertP2 );

                Point horiP1 = new Point( 0, snapY );
                Point horiP2 = new Point( graphPanel.Width, snapY );
                g.DrawLine( GlobalConstant.PEN_CROSS_HAIR, horiP1, horiP2 );

                Point displayPoint = new Point( snapX, snapY - 30 );
                PointD logicalPoint = graphPanel.pixelToLogical( snapX, snapY );                
                g.DrawString( string.Format( "pip:{0}  freq:{1:0}", (int)p.X, (int)p.Y ), GlobalConstant.COEF_FONT_MEDIUM, GlobalConstant.BRUSH_CROSS_HAIR_VALUE, displayPoint );                
            }
        }
    }
}
