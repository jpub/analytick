﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

using AkBase;
using AkCommon;
using AkModel;
using AkWidget;
using AkDatabase;

namespace AkView
{
    public class ViewMarker : BaseView
    {        
        public bool IsShowValue { get; set; }

        public ViewMarker()
        {
            this.IsShowValue = true;
        }

        override public void draw( GraphPanel graphPanel, Graphics g )
        {
            if ( !this.Visible )
                return;

            List<ModelMarker> allMarkerList = DbMarker.getInstance().getCopyListValues();

            foreach ( ModelMarker model in allMarkerList )
            {
                if ( model.IsHorizontal )
                {
                    Point p = graphPanel.logicalToPixel( 0, model.Value );
                    Point horiP1 = new Point( 0, p.Y );
                    Point horiP2 = new Point( graphPanel.Width, p.Y );
                    if ( model.IsMarkerA )
                        g.DrawLine( GlobalConstant.PEN_HORIZONTAL_MARKER_A, horiP1, horiP2 );
                    else
                        g.DrawLine( GlobalConstant.PEN_HORIZONTAL_MARKER_B, horiP1, horiP2 );

                    if ( this.IsShowValue )
                    {
                        Point displayPoint = new Point( horiP1.X + 32, horiP1.Y - 20 );
                        if ( model.IsMarkerA )
                            g.DrawString( string.Format( "{0}  {1}", AkUtil.GetPriceStringNoPippete( model.Value ), model.Remarks ), GlobalConstant.COEF_FONT_MEDIUM, GlobalConstant.BRUSH_HORIZONTAL_MARKER_A_VALUE, displayPoint );
                        else
                            g.DrawString( string.Format( "{0}  {1}", AkUtil.GetPriceStringNoPippete( model.Value ), model.Remarks ), GlobalConstant.COEF_FONT_MEDIUM, GlobalConstant.BRUSH_HORIZONTAL_MARKER_B_VALUE, displayPoint );
                    }
                }
                else
                {                    
                    int candleIndex = DbCandleGraph.getInstance().getCandleIndexNearestMatch( this.CurrentGraphName, (long)model.Value );
                    if ( candleIndex < 0 )
                        continue;

                    Point p = graphPanel.logicalToPixel( candleIndex, 0 );
                    Point vertP1 = new Point( p.X, 0 );
                    Point vertP2 = new Point( p.X, graphPanel.Height );
                    g.DrawLine( GlobalConstant.PEN_VERTIAL_MARKER, vertP1, vertP2 );                    
                }
            }
        }
    }
}
