﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

using AkBase;
using AkCommon;
using AkModel;
using AkWidget;
using AkDatabase;
using AkAlgo;

namespace AkView
{
    public class ViewEmaGraph : BaseView
    {
        public ViewEmaGraph()
        {           
        }

        override public void draw( GraphPanel graphPanel, Graphics g )
        {
            if ( !this.Visible )
                return;

            int lineCounter = 0;
            List<string> copyList = DbLineGraph.getInstance().getCopyListKeys();
            copyList.Sort();
            Point legendDisplayPoint = new Point( 50, 25 );

            foreach( string emaName in copyList )
            {
                ModelLineGraph model = null;
                DbLineGraph.getInstance().get( emaName, out model );
                if ( model.LineGraphType != ModelLineGraph.EnumLineGraphType.EMA )
                    continue;

                List<Point> pixelPointList = new List<Point>();
                List<ModelLineGraph.LineData> listLineDataCopy = model.getCopyListLineData();                                
                int candleIndex = -1;
                foreach ( ModelLineGraph.LineData ld in listLineDataCopy )
                {
                    ++candleIndex;
                    if ( ld.yValue == AlgoEma.UNDEFINE_VALUE )
                        continue;

                    if ( candleIndex < graphPanel.MinX || candleIndex > graphPanel.MaxX )
                        continue;

                    pixelPointList.Add( graphPanel.logicalToPixel( candleIndex, ld.yValue ) );
                }

                if ( pixelPointList.Count >= 2 )
                {
                    g.DrawLines( this.getPen( lineCounter ), pixelPointList.ToArray() );
                }

                g.DrawString( string.Format( "{0} ({1})", "EMA", this.getT( lineCounter ) ), GlobalConstant.COEF_FONT_SMALL, this.getBrush( lineCounter ), legendDisplayPoint );
                legendDisplayPoint.Y += 16;
                ++lineCounter;
            }              
        }

        private Pen getPen( int lineCounter )
        {
            if ( lineCounter == 0 )
                return GlobalConstant.PEN_LINE_EMA_T1;
            else if ( lineCounter == 1 )
                return GlobalConstant.PEN_LINE_EMA_T2;
            else
                return GlobalConstant.PEN_LINE_EMA_T3;
        }

        private Brush getBrush( int lineCounter )
        {
            if ( lineCounter == 0 )
                return GlobalConstant.BRUSH_LINE_EMA_T1;
            else if ( lineCounter == 1 )
                return GlobalConstant.BRUSH_LINE_EMA_T2;
            else
                return GlobalConstant.BRUSH_LINE_EMA_T3;
        }

        private int getT( int lineCounter )
        {
            if ( lineCounter == 0 )
                return GlobalConstant.DEFAULT_EMA_T1;
            else if ( lineCounter == 1 )
                return GlobalConstant.DEFAULT_EMA_T2;
            else
                return GlobalConstant.DEFAULT_EMA_T3;
        }
    }
}
