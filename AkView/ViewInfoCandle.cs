﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;


using AkBase;
using AkCommon;
using AkWidget;
using AkDatabase;
using AkModel;

namespace AkView
{
    public class ViewInfoCandle : BaseView
    {        
        public Point CurrentMousePoint { get; set; }
        public Point StartMiddleButtonMousePoint { get; set; }

        private DateTime currentDateTime = DateTime.MinValue;
        private double currentPrice = 0;        
       
        public ViewInfoCandle()
        {
            this.CurrentMousePoint = new Point( -1, -1 );
            this.StartMiddleButtonMousePoint = new Point( -1, -1 );
        }

        public void resetMiddleButtonMousePoint()
        {
            this.StartMiddleButtonMousePoint = new Point( -1, -1 );
        }

        public DateTime getCurrentDateTime()
        {
            return this.currentDateTime;
        }

        public double getCurrentPrice()
        {
            return this.currentPrice;
        }

        override public void draw( GraphPanel graphPanel, Graphics g )
        {
            if ( !this.Visible )
                return;

            if ( this.CurrentMousePoint.X != -1 && this.CurrentMousePoint.Y != -1 )
            {
                int snapCurrentX = 0;
                int snapStartX = 0;
                int candleIndexCurrent = (int)( graphPanel.pixelToLogical( this.CurrentMousePoint.X, 0 ).X );
                int candleIndexStart = (int)( graphPanel.pixelToLogical( this.StartMiddleButtonMousePoint.X, 0 ).X );

                ModelCandleGraph model = null;
                ModelCandleGraph.CandleData candleDataCurrent;
                ModelCandleGraph.CandleData candleDataStart;

                if ( DbCandleGraph.getInstance().get( this.CurrentGraphName, out model ) )
                {
                    if ( !model.getByIndex( candleIndexCurrent, out candleDataCurrent ) )
                    {
                        return;    
                    }
                    snapCurrentX = graphPanel.logicalToPixel( candleIndexCurrent, 0 ).X;

                    if ( model.getByIndex( candleIndexStart, out candleDataStart ) )
                    {
                        snapStartX = graphPanel.logicalToPixel( candleIndexStart, 0 ).X;     
                    }

                    DateTime candleDtCurrent = new DateTime( candleDataCurrent.xValue );
                    DateTime candleDtStart = new DateTime( candleDataStart.xValue );

                    Point vertP1 = new Point( snapCurrentX, 0 );
                    Point vertP2 = new Point( snapCurrentX, graphPanel.Height );
                    g.DrawLine( GlobalConstant.PEN_CROSS_HAIR, vertP1, vertP2 );

                    Point horiP1 = new Point( 0, this.CurrentMousePoint.Y );
                    Point horiP2 = new Point( graphPanel.Width, this.CurrentMousePoint.Y );
                    g.DrawLine( GlobalConstant.PEN_CROSS_HAIR, horiP1, horiP2 );

                    Point displayPoint = new Point( snapCurrentX, this.CurrentMousePoint.Y - 20 );
                    PointD logicalPoint = graphPanel.pixelToLogical( snapCurrentX, this.CurrentMousePoint.Y );
                    this.currentDateTime = candleDtCurrent;
                    this.currentPrice = logicalPoint.Y;
                    if ( graphPanel.ViewScale == GraphPanel.EnumViewScale.DAY )
                        //g.DrawString( string.Format( "{0}  {1}", AkUtil.GetPriceStringNoPippete( this.currentPrice ), Formatter.ToGridTimeDate( this.currentDateTime ) ), GlobalConstant.COEF_FONT_MEDIUM, GlobalConstant.BRUSH_CROSS_HAIR_VALUE, displayPoint );
                        g.DrawString( string.Format( "{0}  {1}", AkUtil.GetPriceStringNoPippete( this.currentPrice ), AkUtil.ToGridTimeDateHourMinute( this.currentDateTime ) ), GlobalConstant.COEF_FONT_MEDIUM, GlobalConstant.BRUSH_CROSS_HAIR_VALUE, displayPoint );
                    else
                        g.DrawString( string.Format( "{0}  {1}", AkUtil.GetPriceStringNoPippete( this.currentPrice ), AkUtil.ToGridTimeDateHourMinute( this.currentDateTime ) ), GlobalConstant.COEF_FONT_MEDIUM, GlobalConstant.BRUSH_CROSS_HAIR_VALUE, displayPoint );

                    if ( this.StartMiddleButtonMousePoint.X != -1 && this.StartMiddleButtonMousePoint.Y != -1 )
                    {
                        if ( snapCurrentX < 0 || snapStartX < 0 )
                            return;

                        double timePeriod = 0;
                        string timePeriodStr = "";
                        timePeriod = ( candleDtCurrent - candleDtStart ).TotalDays;
                        if ( timePeriod < 1 )
                        {
                            timePeriod = ( candleDtCurrent - candleDtStart ).TotalHours;
                            if ( timePeriod < 1 )
                            {
                                timePeriod = ( candleDtCurrent - candleDtStart ).TotalMinutes;
                                timePeriodStr = String.Format( "{0} {1}", (int)timePeriod, "Minutes" );
                            }
                            else
                            {
                                timePeriodStr = String.Format( "{0:0.0} {1}", timePeriod, "Hour" );
                            }
                        }
                        else
                        {
                            timePeriodStr = String.Format( "{0:0.0} {1}", timePeriod, "Day" );
                        }

                        g.DrawLine( GlobalConstant.PEN_PIP_ARROW, this.StartMiddleButtonMousePoint, this.CurrentMousePoint );
                        int yPixelDiff = Math.Abs( this.CurrentMousePoint.Y - this.StartMiddleButtonMousePoint.Y );
                        double yLogicalDiff = ( (double)yPixelDiff / graphPanel.Height ) * ( graphPanel.MaxY - graphPanel.MinY );
                        double pipDiff = yLogicalDiff / GlobalConstant.ONE_PIP_PRICE;                        
                        displayPoint = new Point( snapCurrentX, this.CurrentMousePoint.Y - 40 );
                        g.DrawString( string.Format( "{0} pips {1}", AkUtil.GetPipDisplay( pipDiff ), timePeriodStr ), GlobalConstant.COEF_FONT_MEDIUM, GlobalConstant.BRUSH_PIP_ARROW_VALUE, displayPoint );
                    }
                }                                
            }
        }        
    }
}
