﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

using AkBase;
using AkCommon;
using AkModel;
using AkWidget;
using AkDatabase;

namespace AkView
{
    public class ViewSimulatedTransaction : BaseView
    {                
        override public void draw( GraphPanel graphPanel, Graphics g )
        {
            if ( !this.Visible )
                return;

            if ( DbCandleGraph.getInstance().count() <= 0 )
                return;

            int yBotOffset = graphPanel.Height - 16;

            List<string> keyList = DbTransaction.getInstance().getCopyListKeys();
            keyList.Sort();
            Point legendDisplayPoint = new Point( 42, yBotOffset - 8 );

            foreach ( string name in keyList )
            {
                ModelTransaction model = null;
                DbTransaction.getInstance().get( name, out model );

                double totalWinPip = 0;
                double totalLosePip = 0;
                List<ModelTransaction.TransactionData> copyList = model.getCopyListTransactionData();
                foreach ( ModelTransaction.TransactionData td in copyList )
                {
                    Point p1 = graphPanel.logicalToPixel( td.entryCandleIndex, 0 );
                    Point p2 = graphPanel.logicalToPixel( td.exitCandleIndex, 0 );
                    if ( p1.X == p2.X )
                        p2.X += 5;
                    p1.Y = yBotOffset;
                    p2.Y = yBotOffset;

                    if ( td.transactOutcome == ModelTransaction.EnumTransactOutcome.PROFIT )
                    {                        
                        g.DrawLine( GlobalConstant.PEN_TRANSACTION_PROFIT, p1, p2 );
                        totalWinPip += ( Math.Abs( td.exitPrice - td.entryPrice ) / GlobalConstant.ONE_PIP_PRICE );
                    }
                    else if ( td.transactOutcome == ModelTransaction.EnumTransactOutcome.LOSS )
                    {                        
                        g.DrawLine( GlobalConstant.PEN_TRANSACTION_LOSS, p1, p2 );
                        totalLosePip += ( Math.Abs( td.exitPrice - td.entryPrice ) / GlobalConstant.ONE_PIP_PRICE );
                    }
                    else
                    {
                        g.DrawLine( GlobalConstant.PEN_TRANSACTION_UNRESOLVE, p1, p2 );
                    }

                    if ( td.transactType == ModelTransaction.EnumTransactType.LONG )
                    {
                        g.FillRectangle( GlobalConstant.BRUSH_TRANSACTION_LONG, p1.X, p1.Y - 4, 2, 8 );                        
                    }
                    else if ( td.transactType == ModelTransaction.EnumTransactType.SHORT )
                    {
                        g.FillRectangle( GlobalConstant.BRUSH_TRANSACTION_SHORT, p1.X, p1.Y - 4, 2, 8 );
                    }
                }

                double nettPip = totalWinPip - totalLosePip;
                string nettPipStr = nettPip.ToString( "0.0" );
                string shorterName = ( name.Split( '^' ) )[1];
                float nameWidth = g.MeasureString( shorterName, GlobalConstant.COEF_FONT_SMALL ).Width;
                float nettPipWidth = g.MeasureString( nettPipStr, GlobalConstant.COEF_FONT_SMALL ).Width;
                g.FillRectangle( GlobalConstant.BRUSH_SIMULATION_NAME_BACKGROUND, legendDisplayPoint.X - 2, legendDisplayPoint.Y - 2, nameWidth + nettPipWidth + 2, 16 );
                g.DrawRectangle( GlobalConstant.PEN_SIMULATION_NAME, legendDisplayPoint.X - 2, legendDisplayPoint.Y - 2, nameWidth + nettPipWidth + 2, 16 );

                g.DrawString( shorterName, GlobalConstant.COEF_FONT_SMALL, GlobalConstant.BRUSH_SIMULATION_NAME, legendDisplayPoint );
                if ( nettPip >= 0 )
                    g.DrawString( nettPipStr, GlobalConstant.COEF_FONT_SMALL, GlobalConstant.BRUSH_RISING_CANDLE, legendDisplayPoint.X + nameWidth, legendDisplayPoint.Y );
                else
                    g.DrawString( nettPipStr, GlobalConstant.COEF_FONT_SMALL, GlobalConstant.BRUSH_DROPPING_CANDLE, legendDisplayPoint.X + nameWidth, legendDisplayPoint.Y );
                legendDisplayPoint.Y -= 22;
                yBotOffset -= 22;
            }
        }
    }
}
