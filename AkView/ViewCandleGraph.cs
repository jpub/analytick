﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

using AkBase;
using AkCommon;
using AkModel;
using AkWidget;
using AkDatabase;

namespace AkView
{
    public class ViewCandleGraph : BaseView
    {        
        public int BarWidth { get; set; }
        public bool IsShowCandleWick { get; set; }

        public ViewCandleGraph()
        {
            this.CurrentGraphName = "";
            this.BarWidth = GlobalConstant.BAR_WIDTH;
            this.IsShowCandleWick = true;
        }

        override public void draw( GraphPanel graphPanel, Graphics g )
        {
            if ( !this.Visible )
                return;

            ModelCandleGraph model = null;
            if ( DbCandleGraph.getInstance().get( this.CurrentGraphName, out model ) )
            {
                List<Point> pixelPointList = new List<Point>();
                List<ModelCandleGraph.CandleData> listCandleDataCopy = model.getCopyListCandleData();
                if ( listCandleDataCopy.Count == 0 )
                    return;

                double yOpen, yClose, yHigh, yLow, yCandleTop, yCandleBot;
                int candleIndex = -1;
                foreach ( ModelCandleGraph.CandleData bd in listCandleDataCopy )
                {
                    ++candleIndex;
                    if ( candleIndex < graphPanel.MinX || candleIndex > graphPanel.MaxX )
                        continue;

                    yOpen = bd.openMid;
                    yClose = bd.closeMid;
                    yHigh = bd.highMid;
                    yLow = bd.lowMid;

                    Brush candleBrush = null;
                    Pen candlePen = null;

                    if ( yClose >= yOpen )
                    {
                        candleBrush = GlobalConstant.BRUSH_RISING_CANDLE;
                        candlePen = GlobalConstant.PEN_RISING_CANDLE;
                        yCandleTop = yClose;
                        yCandleBot = yOpen;
                    }
                    else
                    {
                        candleBrush = GlobalConstant.BRUSH_DROPPING_CANDLE;
                        candlePen = GlobalConstant.PEN_DROPPING_CANDLE;
                        yCandleTop = yOpen;
                        yCandleBot = yClose;
                    }

                    if ( this.IsShowCandleWick )
                    {
                        Point pointWick1a = graphPanel.logicalToPixel( candleIndex, yHigh );
                        Point pointWick1b = graphPanel.logicalToPixel( candleIndex, yCandleTop );
                        g.DrawLine( candlePen, pointWick1a, pointWick1b );

                        Point pointWick2a = graphPanel.logicalToPixel( candleIndex, yLow );
                        Point pointWick2b = graphPanel.logicalToPixel( candleIndex, yCandleBot );
                        g.DrawLine( candlePen, pointWick2a, pointWick2b );
                    }

                    Point topLeftPoint = graphPanel.logicalToPixel( candleIndex, yCandleTop );
                    topLeftPoint.X -= ( this.BarWidth / 2 );
                    int pixelHeight = graphPanel.logicalToPixelHeight( yCandleTop, yCandleBot );
                    if ( yCandleTop == yCandleBot )
                    {
                        Point topRightPoint = new Point( topLeftPoint.X + this.BarWidth, topLeftPoint.Y );
                        g.DrawLine( candlePen, topLeftPoint, topRightPoint );
                    }
                    else
                        g.FillRectangle( candleBrush, topLeftPoint.X, topLeftPoint.Y, this.BarWidth, pixelHeight );
                }

                //HelperView.drawCandleMainXaxis( graphPanel, g, listCandleDataCopy );
                //HelperView.drawCandleMainYaxis( graphPanel, g );
                HelperView.drawCandleXaxis( graphPanel, g, listCandleDataCopy );
                HelperView.drawCandleYaxis( graphPanel, g );
            }
        }
    }
}
