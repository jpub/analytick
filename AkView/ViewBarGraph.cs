﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

using AkBase;
using AkCommon;
using AkModel;
using AkWidget;
using AkDatabase;

namespace AkView
{
    public class ViewBarGraph : BaseView
    {        
        public int BarWidth { get; set; }

        public ViewBarGraph()
        {
            this.CurrentGraphName = "";
            this.BarWidth = GlobalConstant.BAR_WIDTH;
        }

        override public void draw( GraphPanel graphPanel, Graphics g )
        {
            if ( !this.Visible )
                return;

            ModelCandleGraph model = null;
            if ( DbCandleGraph.getInstance().get( this.CurrentGraphName, out model ) )
            {                
                List<ModelCandleGraph.CandleData> listCandleDataCopy = model.getCopyListCandleData();
                int candleIndex = -1;
                foreach ( ModelCandleGraph.CandleData cd in listCandleDataCopy )
                {
                    ++candleIndex;
                    Point topLeftPoint = graphPanel.logicalToPixel( candleIndex, cd.highMid );
                    topLeftPoint.X -= ( this.BarWidth / 2 );
                    int pixelHeight = graphPanel.logicalToPixelHeight( cd.highMid, cd.lowMid );
                    //g.DrawRectangle( GlobalConstant.PEN_LINE_GRAPH, topLeftPoint.X, topLeftPoint.Y, GlobalConstant.BAR_WIDTH, pixelHeight );
                    g.FillRectangle( GlobalConstant.BRUSH_BAR, topLeftPoint.X, topLeftPoint.Y, this.BarWidth, pixelHeight );
                }

                HelperView.drawCandleXaxis( graphPanel, g, listCandleDataCopy );
                HelperView.drawCandleYaxis( graphPanel, g );
            }            
        }
    }
}
