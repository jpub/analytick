﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

using AkBase;
using AkCommon;
using AkModel;
using AkWidget;
using AkDatabase;

namespace AkView
{
    public class ViewHistogramGraph : BaseView
    {        
        public int BarWidth { get; set; }

        public ViewHistogramGraph()
        {
            this.CurrentGraphName = "";
            this.BarWidth = GlobalConstant.BAR_HISTOGRAM_WIDTH;
        }

        override public void draw( GraphPanel graphPanel, Graphics g )
        {
            if ( !this.Visible )
                return;

            // X-axis is pip
            // Y-axis is frequency

            SortedDictionary<long, double> sortedPipAxis = new SortedDictionary<long, double>();
            ModelCandleGraph model = null;
            if ( DbCandleGraph.getInstance().get( this.CurrentGraphName, out model ) )
            {
                List<Point> pixelPointList = new List<Point>();
                List<ModelCandleGraph.CandleData> listCandleDataCopy = model.getCopyListCandleData();                

                double totalFreq = 0;
                double peakValueY = -1;
                double peakValueX = -1;

                if ( listCandleDataCopy.Count == 0 )
                    return;

                foreach ( ModelCandleGraph.CandleData bd in listCandleDataCopy )
                {
                    sortedPipAxis.Add( bd.xValue, bd.highMid );
                    totalFreq += bd.highMid;
                    if ( bd.highMid > peakValueY )
                    {
                        peakValueY = bd.highMid;
                        peakValueX = bd.xValue;
                    }

                    Point topLeftPoint = graphPanel.logicalToPixel( bd.xValue, bd.highMid );
                    topLeftPoint.X -= ( this.BarWidth / 2 );
                    int pixelHeight = graphPanel.logicalToPixelHeight( bd.highMid, bd.lowMid );                    
                    g.FillRectangle( GlobalConstant.BRUSH_BAR, topLeftPoint.X, topLeftPoint.Y, this.BarWidth, pixelHeight );
                }

                double percentileFreq50 = totalFreq * 0.5;
                double percentileFreq75 = totalFreq * 0.75;
                double percentileFreq90 = totalFreq * 0.9;
                double percentileX50 = 0;
                double percentileX75 = 0;
                double percentileX90 = 0;
                double accumulationFreq = 0;
                foreach ( KeyValuePair<long, double> kvp in sortedPipAxis )
                {
                    accumulationFreq += kvp.Value;
                    if ( accumulationFreq <= percentileFreq50 )
                    {
                        percentileX50 = kvp.Key;
                    }
                    else if ( accumulationFreq <= percentileFreq75 )
                    {
                        percentileX75 = kvp.Key;
                    }
                    else if ( accumulationFreq <= percentileFreq90 )
                    {
                        percentileX90 = kvp.Key;
                    }
                }

                Point infoPoint = new Point( graphPanel.Width - 200, 30 );
                g.DrawString( "Peak: " + peakValueX.ToString( "0" ) + " pips", GlobalConstant.COEF_FONT_MEDIUM, GlobalConstant.BRUSH_DISTRIBUTION_HISTOGRAM_INFO, infoPoint );
                infoPoint.Y += 20;
                g.DrawString( "occur: " + peakValueY.ToString( "0" ), GlobalConstant.COEF_FONT_MEDIUM, GlobalConstant.BRUSH_DISTRIBUTION_HISTOGRAM_INFO, infoPoint );
                infoPoint.Y += 40;
                g.DrawString( "total: " + totalFreq.ToString( "0" ), GlobalConstant.COEF_FONT_MEDIUM, GlobalConstant.BRUSH_DISTRIBUTION_HISTOGRAM_INFO, infoPoint );
                infoPoint.Y += 20;
                g.DrawString( "50%: " + percentileX50.ToString( "0" ), GlobalConstant.COEF_FONT_MEDIUM, GlobalConstant.BRUSH_DISTRIBUTION_HISTOGRAM_INFO, infoPoint );
                infoPoint.Y += 20;
                g.DrawString( "75%: " + percentileX75.ToString( "0" ), GlobalConstant.COEF_FONT_MEDIUM, GlobalConstant.BRUSH_DISTRIBUTION_HISTOGRAM_INFO, infoPoint );
                infoPoint.Y += 20;
                g.DrawString( "90%: " + percentileX90.ToString( "0" ), GlobalConstant.COEF_FONT_MEDIUM, GlobalConstant.BRUSH_DISTRIBUTION_HISTOGRAM_INFO, infoPoint );

                HelperView.drawDistributionHistogramAxis( graphPanel, g );                
            }            
        }
    }
}
