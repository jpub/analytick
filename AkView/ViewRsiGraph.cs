﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

using AkBase;
using AkCommon;
using AkModel;
using AkWidget;
using AkDatabase;
using AkAlgo;

namespace AkView
{
    public class ViewRsiGraph : BaseView
    {
        public Point CurrentMousePoint { get; set; }

        public ViewRsiGraph()
        {
            this.CurrentMousePoint = new Point( -1, -1 );
        }

        override public void draw( GraphPanel graphPanel, Graphics g )
        {
            if ( !this.Visible )
                return;

            int lineCounter = 0;
            List<string> copyList = DbLineGraph.getInstance().getCopyListKeys();
            copyList.Sort();
            Point legendDisplayPoint = new Point( 50, 10 );

            foreach ( string emaName in copyList )
            {
                ModelLineGraph model = null;
                DbLineGraph.getInstance().get( emaName, out model );
                if ( model.LineGraphType != ModelLineGraph.EnumLineGraphType.RSI )
                    continue;

                List<Point> pixelPointList = new List<Point>();
                List<ModelLineGraph.LineData> listLineDataCopy = model.getCopyListLineData();                
                int candleIndex = -1;
                foreach ( ModelLineGraph.LineData ld in listLineDataCopy )
                {
                    ++candleIndex;
                    if ( ld.yValue == AlgoRsi.UNDEFINE_VALUE )
                        continue;

                    if ( candleIndex < graphPanel.MinX || candleIndex > graphPanel.MaxX )
                        continue;

                    pixelPointList.Add( graphPanel.logicalToPixel( candleIndex, ld.yValue ) );
                }

                if ( pixelPointList.Count >= 2 )
                {
                    g.DrawLines( GlobalConstant.PEN_LINE_RSI, pixelPointList.ToArray() );
                }

                int pixelUpperY = graphPanel.logicalToPixel( 0, GlobalConstant.DEFAULT_RSI_UPPER ).Y;
                int pixelLowerY = graphPanel.logicalToPixel( 0, GlobalConstant.DEFAULT_RSI_LOWER ).Y;

                Point horiUpperP1 = new Point( 0, pixelUpperY );
                Point horiUpperP2 = new Point( graphPanel.Width, pixelUpperY );
                Point labelUpper = new Point( 0, pixelUpperY - 15 );

                Point horiLowerP1 = new Point( 0, pixelLowerY );
                Point horiLowerP2 = new Point( graphPanel.Width, pixelLowerY );
                Point labelLower = new Point( 0, pixelLowerY - 15 );

                g.DrawLine( GlobalConstant.PEN_LINE_RSI_UPPER_LOWER, horiUpperP1, horiUpperP2 );
                g.DrawLine( GlobalConstant.PEN_LINE_RSI_UPPER_LOWER, horiLowerP1, horiLowerP2 );

                g.DrawString( string.Format( "{0} ({1})", "RSI", GlobalConstant.DEFAULT_RSI_PERIOD ), GlobalConstant.COEF_FONT_SMALL, GlobalConstant.BRUSH_LINE_RSI, legendDisplayPoint );
                g.DrawString( string.Format( "{0}", GlobalConstant.DEFAULT_RSI_UPPER ), GlobalConstant.COEF_FONT_SMALL, GlobalConstant.BRUSH_LINE_RSI_UPPER_LOWER, labelUpper );
                g.DrawString( string.Format( "{0}", GlobalConstant.DEFAULT_RSI_LOWER ), GlobalConstant.COEF_FONT_SMALL, GlobalConstant.BRUSH_LINE_RSI_UPPER_LOWER, labelLower );
                legendDisplayPoint.Y += 16;
                ++lineCounter;
            }
            this.drawVerticalCrossHair( graphPanel, g );
        }

        private void drawVerticalCrossHair( GraphPanel graphPanel, Graphics g )
        {
            if ( this.CurrentMousePoint.X != -1 )
            {
                int candleIndexCurrent = (int)( graphPanel.pixelToLogical( this.CurrentMousePoint.X, 0 ).X );
                if ( candleIndexCurrent <= 0 )
                    return;

                int snapCurrentX = graphPanel.logicalToPixel( candleIndexCurrent, 0 ).X;

                Point vertP1 = new Point( snapCurrentX, 0 );
                Point vertP2 = new Point( snapCurrentX, graphPanel.Height );
                g.DrawLine( GlobalConstant.PEN_CROSS_HAIR, vertP1, vertP2 );
            }
        }
    }
}
