﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

using AkBase;
using AkCommon;
using AkModel;
using AkWidget;
using AkDatabase;

namespace AkView
{
    public class ViewLineGraph : BaseView
    {        
        public ViewLineGraph()
        {
            this.CurrentGraphName = "";
        }

        override public void draw( GraphPanel graphPanel, Graphics g )
        {
            if ( !this.Visible )
                return;

            ModelLineGraph model = null;
            if ( DbLineGraph.getInstance().get( this.CurrentGraphName, out model ) )
            {
                List<Point> pixelPointList = new List<Point>();
                List<ModelLineGraph.LineData> listLineDataCopy = model.getCopyListLineData();

                foreach ( ModelLineGraph.LineData ld in listLineDataCopy )
                {
                    pixelPointList.Add( graphPanel.logicalToPixel( ld.xValue, ld.yValue ) );
                }

                if ( pixelPointList.Count >= 2 )
                {
                    g.DrawLines( GlobalConstant.PEN_LINE_GRAPH, pixelPointList.ToArray() );
                }       
            }            
        }
    }
}
