﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

using AkBase;
using AkCommon;
using AkWidget;

namespace AkView
{
    public class ViewOverviewRegion : BaseView
    {
        public double LeftX = 0;
        public double RightX = 0;        

        public ViewOverviewRegion()
        {            
        }

        override public void draw( GraphPanel graphPanel, Graphics g )
        {
            if ( !this.Visible )
                return;

            if ( this.LeftX != 0 && this.RightX != 0 )
            {
                int snapX1 = graphPanel.logicalToPixel( this.LeftX, 0 ).X;
                Point vert1P1 = new Point( snapX1, 0 );
                Point vert1P2 = new Point( snapX1, graphPanel.Height );

                int snapX2 = graphPanel.logicalToPixel( this.RightX, 0 ).X;
                Point vert2P1 = new Point( snapX2, 0 );
                Point vert2P2 = new Point( snapX2, graphPanel.Height );

                g.DrawLine( GlobalConstant.PEN_CROSS_HAIR, vert1P1, vert1P2 );
                g.DrawLine( GlobalConstant.PEN_CROSS_HAIR, vert2P1, vert2P2 );

                g.FillRectangle( GlobalConstant.BRUSH_OVERVIEW_REGION, snapX1, 0, snapX2 - snapX1, graphPanel.Height );                
            }
        }
    }
}
