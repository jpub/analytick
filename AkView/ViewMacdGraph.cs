﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

using AkBase;
using AkCommon;
using AkModel;
using AkWidget;
using AkDatabase;
using AkAlgo;

namespace AkView
{
    public class ViewMacdGraph : BaseView
    {
        public Point CurrentMousePoint { get; set; }
        public int BarWidth { get; set; }

        public ViewMacdGraph()
        {
            this.CurrentMousePoint = new Point( -1, -1 );
            this.BarWidth = GlobalConstant.BAR_WIDTH;
        }

        override public void draw( GraphPanel graphPanel, Graphics g )
        {
            if ( !this.Visible )
                return;
            
            Point legendDisplayPoint = new Point( 50, 10 );
            ModelLineGraph modelMacd = null;
            ModelLineGraph modelSignal = null;
            if ( !DbLineGraph.getInstance().get( GlobalConstant.GRAPH_NAME_LINE_MACD, out modelMacd ) )
                return;
            DbLineGraph.getInstance().get( GlobalConstant.GRAPH_NAME_LINE_MACD_SIGNAL, out modelSignal );

            List<ModelLineGraph.LineData> macdListLd = modelMacd.getCopyListLineData();            
            this.drawLineGraph( graphPanel, g, macdListLd, GlobalConstant.PEN_LINE_MACD, GlobalConstant.BRUSH_LINE_MACD, "MACD", legendDisplayPoint );

            legendDisplayPoint.Y += 16;  
            List<ModelLineGraph.LineData> signalListLd = modelSignal.getCopyListLineData();            
            this.drawLineGraph( graphPanel, g, signalListLd, GlobalConstant.PEN_LINE_MACD_SIGNAL, GlobalConstant.BRUSH_LINE_MACD_SIGNAL, "Signal", legendDisplayPoint );

            this.drawHistogram( graphPanel, g, macdListLd, signalListLd );
            HelperView.drawMacdYaxis( graphPanel, g );
            this.drawVertialCrossHair( graphPanel, g );
        }

        private void drawLineGraph( GraphPanel graphPanel, Graphics g, List<ModelLineGraph.LineData> listLineDataCopy, Pen pen, Brush brush, string legend, Point legendDisplayPoint )
        {
            try
            {
                int candleIndex = -1;
                List<Point> pixelPointList = new List<Point>();

                foreach ( ModelLineGraph.LineData ld in listLineDataCopy )
                {
                    ++candleIndex;
                    if ( ld.yValue == AlgoMacd.UNDEFINE_VALUE )
                        continue;

                    if ( candleIndex < graphPanel.MinX || candleIndex > graphPanel.MaxX )
                        continue;

                    pixelPointList.Add( graphPanel.logicalToPixel( candleIndex, ld.yValue ) );
                }

                if ( pixelPointList.Count >= 2 )
                {
                    g.DrawLines( pen, pixelPointList.ToArray() );
                }

                g.DrawString( string.Format( "{0}", legend ), GlobalConstant.COEF_FONT_SMALL, brush, legendDisplayPoint );
            }
            catch(Exception)
            {
            }
        }

        private void drawHistogram( GraphPanel graphPanel, Graphics g, List<ModelLineGraph.LineData> macdListLd, List<ModelLineGraph.LineData> signalListLd )
        {
            try
            {
                int totalPoint = macdListLd.Count;
                double prevDiff = double.MinValue;

                for ( int i = 0; i < totalPoint; ++i )
                {
                    if ( signalListLd[i].yValue == AlgoMacd.UNDEFINE_VALUE )
                        continue;

                    Point topLeftPoint;
                    double diff = macdListLd[i].yValue - signalListLd[i].yValue;
                    if ( diff >= 0 )
                        topLeftPoint = graphPanel.logicalToPixel( i, diff );
                    else
                        topLeftPoint = graphPanel.logicalToPixel( i, 0 );

                    topLeftPoint.X -= ( this.BarWidth / 2 );
                    int pixelHeight = graphPanel.logicalToPixelHeight( Math.Abs( diff ), 0 );
                    if ( diff > prevDiff )
                        g.FillRectangle( GlobalConstant.BRUSH_RISING_CANDLE, topLeftPoint.X, topLeftPoint.Y, this.BarWidth, pixelHeight );
                    else
                        g.FillRectangle( GlobalConstant.BRUSH_DROPPING_CANDLE, topLeftPoint.X, topLeftPoint.Y, this.BarWidth, pixelHeight );

                    prevDiff = diff;
                }
            }
            catch ( Exception )
            {
            }
        }

        private void drawVertialCrossHair( GraphPanel graphPanel, Graphics g )
        {
            if ( this.CurrentMousePoint.X != -1 )
            {
                int candleIndexCurrent = (int)( graphPanel.pixelToLogical( this.CurrentMousePoint.X, 0 ).X );
                if ( candleIndexCurrent <= 0 )
                    return;

                int snapCurrentX = graphPanel.logicalToPixel( candleIndexCurrent, 0 ).X;

                Point vertP1 = new Point( snapCurrentX, 0 );
                Point vertP2 = new Point( snapCurrentX, graphPanel.Height );
                g.DrawLine( GlobalConstant.PEN_CROSS_HAIR, vertP1, vertP2 );
            }
        }
    }
}
