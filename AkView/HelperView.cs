﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;

using AkCommon;
using AkWidget;
using AkModel;

namespace AkView
{
    public class HelperView
    {
        private enum EnumScaleSeparator
        {
            DAY = 0,
            MONTH = 1,
            YEAR = 2
        }

        #region Candle
        static public void drawCandleXaxis( GraphPanel graphPanel, Graphics g, List<ModelCandleGraph.CandleData> listCandleDataCopy )
        {
            if ( listCandleDataCopy.Count == 0 )
                return;

            if ( !graphPanel.ShowXaxisLabel )
                return;

            EnumScaleSeparator deduceScaleX = getDeduceScaleSeparatorX( graphPanel );

            int startIndex = listCandleDataCopy[0].index;
            int endIndex = 0;
            int colorIndex = 0;
            DateTime prevDt = new DateTime( listCandleDataCopy[0].xValue );

            foreach ( ModelCandleGraph.CandleData data in listCandleDataCopy )
            {
                DateTime currentDt = new DateTime( data.xValue );
                endIndex = data.index;
                if ( checkComparison( deduceScaleX, prevDt, currentDt ) )
                {
                    Point left = graphPanel.logicalToPixel( startIndex, 0 );
                    Point right = graphPanel.logicalToPixel( endIndex, 0 );
                    left.Y = 0;

                    if ( graphPanel.ShowYGrid )
                    {
                        Point verP1 = new Point( right.X, 0 );
                        Point verP2 = new Point( right.X, graphPanel.Height );
                        g.DrawLine( GlobalConstant.PEN_GRID, verP1, verP2 );
                    }

                    g.DrawRectangle( GlobalConstant.PEN_GRID_BOX_BORDER, left.X, left.Y, ( right.X - left.X ), GlobalConstant.X_GRID_BOX_HEIGHT );
                    if ( colorIndex % 2 == 0 )
                        g.FillRectangle( GlobalConstant.BRUSH_X_GRID_BOX, left.X, left.Y, ( right.X - left.X ), GlobalConstant.X_GRID_BOX_HEIGHT );
                    else
                        g.FillRectangle( GlobalConstant.BRUSH_X_GRID_BOX_ALT, left.X, left.Y, ( right.X - left.X ), GlobalConstant.X_GRID_BOX_HEIGHT );

                    g.DrawString( string.Format( "{0}", getXLabelString( deduceScaleX, prevDt ) ), GlobalConstant.COEF_FONT_SMALL, GlobalConstant.BRUSH_X_GRID_VALUE, left );

                    ++colorIndex;
                    startIndex = endIndex;
                    prevDt = currentDt;
                }
            }

            // draw the last grid box
            if ( startIndex != endIndex )
            {
                ++colorIndex;
                Point left = graphPanel.logicalToPixel( startIndex, 0 );
                Point right = graphPanel.logicalToPixel( endIndex, 0 );
                left.Y = 0;

                g.DrawRectangle( GlobalConstant.PEN_GRID_BOX_BORDER, left.X, left.Y, ( right.X - left.X ), GlobalConstant.X_GRID_BOX_HEIGHT );
                if ( colorIndex % 2 == 0 )
                    g.FillRectangle( GlobalConstant.BRUSH_X_GRID_BOX, left.X, left.Y, ( right.X - left.X ), GlobalConstant.X_GRID_BOX_HEIGHT );
                else
                    g.FillRectangle( GlobalConstant.BRUSH_X_GRID_BOX_ALT, left.X, left.Y, ( right.X - left.X ), GlobalConstant.X_GRID_BOX_HEIGHT );

                g.DrawString( string.Format( "{0}", getXLabelString( deduceScaleX, prevDt ) ), GlobalConstant.COEF_FONT_SMALL, GlobalConstant.BRUSH_X_GRID_VALUE, left );
            }
        }

        static public void drawCandleYaxis( GraphPanel graphPanel, Graphics g )
        {
            if ( !graphPanel.ShowYaxisLabel )
                return;

            double gridY = ( (int)( Math.Ceiling( graphPanel.MaxY / GlobalConstant.ONE_PIP_PRICE / 10 ) * 10 ) ) * GlobalConstant.ONE_PIP_PRICE;
            int deduceScaleY = getDeduceScaleSeparatorY( graphPanel );
            Point scalePoint = new Point( graphPanel.Width - 32, 20 );

            Point p;
            do
            {
                p = graphPanel.logicalToPixel( 0, gridY );
                Point horP1 = new Point( 0, p.Y );
                Point horP2 = new Point( graphPanel.Width, p.Y );
                if ( graphPanel.ShowYGrid )
                    g.DrawLine( GlobalConstant.PEN_GRID, horP1, horP2 );

                g.DrawString( AkUtil.GetPriceStringNoPippete( gridY ), GlobalConstant.COEF_FONT_SMALL, GlobalConstant.BRUSH_Y_GRID_VALUE, horP1 );
                horP2.X -= 40;
                g.DrawString( AkUtil.GetPriceStringNoPippete( gridY ), GlobalConstant.COEF_FONT_SMALL, GlobalConstant.BRUSH_Y_GRID_VALUE, horP2 );

                if ( graphPanel.ViewScale == GraphPanel.EnumViewScale.DAY )
                    gridY -= ( deduceScaleY * GlobalConstant.ONE_PIP_PRICE );
                else
                    gridY -= ( deduceScaleY * GlobalConstant.ONE_PIP_PRICE );
            } while ( p.Y < graphPanel.Height );

            g.DrawString( deduceScaleY.ToString(), GlobalConstant.COEF_FONT_SMALL_BOLD, GlobalConstant.BRUSH_CROSS_HAIR_VALUE, scalePoint );
        }

        static private EnumScaleSeparator getDeduceScaleSeparatorX( GraphPanel graphPanel )
        {
            double totalViewableCandles = graphPanel.MaxX - graphPanel.MinX;
            GraphPanel.EnumViewScale currentViewScale = graphPanel.ViewScale;

            EnumScaleSeparator deduceScale = EnumScaleSeparator.DAY;
            if ( currentViewScale == GraphPanel.EnumViewScale.DAY )
            {
                if ( totalViewableCandles > 365 )
                    deduceScale = EnumScaleSeparator.YEAR;
                else
                    deduceScale = EnumScaleSeparator.MONTH;
            }
            else if ( currentViewScale == GraphPanel.EnumViewScale.HOUR )
            {
                if ( totalViewableCandles > ( 365 * 24 ) )
                    deduceScale = EnumScaleSeparator.YEAR;
                else if ( totalViewableCandles > ( 30 * 24 ) )
                    deduceScale = EnumScaleSeparator.MONTH;
            }
            return deduceScale;
        }

        static private int getDeduceScaleSeparatorY( GraphPanel graphPanel )
        {
            double totalPipDiff = ( graphPanel.MaxY - graphPanel.MinY ) / GlobalConstant.ONE_PIP_PRICE;
            int deduceInterval = 20;

            if ( totalPipDiff > 5000 )
                deduceInterval = 1000;
            else if ( totalPipDiff > 1000 )
                deduceInterval = 100;
            else if ( totalPipDiff > 300 )
                deduceInterval = 50;

            return deduceInterval;
        }

        static private bool checkComparison( EnumScaleSeparator scaleSeparator, DateTime prevDt, DateTime currentDt )
        {
            if ( scaleSeparator == EnumScaleSeparator.YEAR )
            {
                if ( prevDt.Year != currentDt.Year )
                    return true;
            }
            else if ( scaleSeparator == EnumScaleSeparator.MONTH )
            {
                if ( prevDt.Month != currentDt.Month )
                    return true;
            }
            else if ( scaleSeparator == EnumScaleSeparator.DAY )
            {
                if ( prevDt.Day != currentDt.Day )
                    return true;
            }

            return false;
        }

        static private string getXLabelString( EnumScaleSeparator scaleSeparator, DateTime prevDt )
        {
            if ( scaleSeparator == EnumScaleSeparator.YEAR )
            {
                return AkUtil.ToGridTimeYear( prevDt );
            }
            else if ( scaleSeparator == EnumScaleSeparator.MONTH )
            {
                return AkUtil.ToGridTimeMonth( prevDt );
            }
            else if ( scaleSeparator == EnumScaleSeparator.DAY )
            {
                return AkUtil.ToGridTimeDayMonth( prevDt );
            }
            return "unknown";
        }
        #endregion

        #region MACD
        static public void drawMacdYaxis( GraphPanel graphPanel, Graphics g )
        {
            try
            {
                if ( graphPanel.MaxY == double.MinValue )
                    return;

                // deduce a reasonable X interval            
                double intervalLogicalY = ( graphPanel.MaxY - graphPanel.MinY ) / 10;
                if ( intervalLogicalY >= 0.001 )
                    intervalLogicalY = Math.Round( intervalLogicalY, 3 );
                else
                    intervalLogicalY = Math.Round( intervalLogicalY, 4 );

                double sepearation = intervalLogicalY == 0 ? 0.001 : intervalLogicalY;
                double gridY = Math.Ceiling( graphPanel.MaxY / sepearation ) * sepearation;
                Point p;
                do
                {
                    p = graphPanel.logicalToPixel( 0, gridY );
                    Point horP1 = new Point( 0, p.Y );
                    Point horP2 = new Point( graphPanel.Width, p.Y );
                    if ( graphPanel.ShowYGrid )
                        g.DrawLine( GlobalConstant.PEN_GRID, horP1, horP2 );

                    if ( graphPanel.ShowYaxisLabel )
                    {
                        g.DrawString( AkUtil.GetPriceStringNoPippete( gridY ), GlobalConstant.COEF_FONT_SMALL, GlobalConstant.BRUSH_Y_GRID_VALUE, horP1 );
                        horP2.X -= 40;
                        g.DrawString( AkUtil.GetPriceStringNoPippete( gridY ), GlobalConstant.COEF_FONT_SMALL, GlobalConstant.BRUSH_Y_GRID_VALUE, horP2 );
                    }

                    gridY -= sepearation;
                } while ( p.Y < graphPanel.Height );
            }
            catch ( Exception )
            {
            }
        }
        #endregion        

        #region Distribution Histogram
        static public void drawDistributionHistogramAxis( GraphPanel graphPanel, Graphics g )        
        {
            // deduce a reasonable X interval
            int intervalLogicalX = (int)( ( graphPanel.MaxX - graphPanel.MinX ) / 10 );
            int intervalLogicalY = (int)( ( graphPanel.MaxY - graphPanel.MinY ) / 10 );
            Point p;

            int gridX = (int)( Math.Ceiling( graphPanel.MaxX / 10 ) * 10 );

            do
            {
                p = graphPanel.logicalToPixel( gridX, 0 );
                Point vertP1 = new Point( p.X, 0 );
                Point vertP2 = new Point( p.X, graphPanel.Height );
                if ( graphPanel.ShowXGrid )
                    g.DrawLine( GlobalConstant.PEN_GRID, vertP1, vertP2 );

                if ( graphPanel.ShowXaxisLabel )
                {
                    g.DrawString( gridX.ToString(), GlobalConstant.COEF_FONT_SMALL, GlobalConstant.BRUSH_Y_GRID_VALUE, vertP1 );
                    vertP2.Y -= 14;
                    g.DrawString( gridX.ToString(), GlobalConstant.COEF_FONT_SMALL, GlobalConstant.BRUSH_Y_GRID_VALUE, vertP2 );
                }

                gridX -= intervalLogicalX;
            } while ( p.X > 0 );

            int gridY = (int)( Math.Ceiling( graphPanel.MaxY / 10 ) * 10 );
            do
            {
                p = graphPanel.logicalToPixel( 0, gridY );
                Point horP1 = new Point( 0, p.Y );
                Point horP2 = new Point( graphPanel.Width, p.Y );
                if ( graphPanel.ShowYGrid )
                    g.DrawLine( GlobalConstant.PEN_GRID, horP1, horP2 );

                if ( graphPanel.ShowYaxisLabel )
                {
                    g.DrawString( gridY.ToString(), GlobalConstant.COEF_FONT_SMALL, GlobalConstant.BRUSH_Y_GRID_VALUE, horP1 );
                    horP2.X -= 30;
                    g.DrawString( gridY.ToString(), GlobalConstant.COEF_FONT_SMALL, GlobalConstant.BRUSH_Y_GRID_VALUE, horP2 );
                }

                gridY -= intervalLogicalY;
            } while ( p.Y < graphPanel.Height );
        }
        #endregion        
    }
}
