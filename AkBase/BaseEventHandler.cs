﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkBase
{
    abstract public class BaseEventHandler
    {
        abstract public void handle( BaseMessage message );
    }
}
