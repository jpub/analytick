﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

using AkWidget;

namespace AkBase
{
    abstract public class BaseView
    {
        public bool Visible { get; set; }
        public string CurrentGraphName { get; set; }
        abstract public void draw( GraphPanel graphPanel, Graphics g );

        protected BaseView()
        {
            this.Visible = true;
        }
    }
}
