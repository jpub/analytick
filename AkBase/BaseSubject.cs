﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkBase
{
    [Serializable]
    public class BaseSubject
    {
        private List<IBaseObserver> observerList = new List<IBaseObserver>();

        public void attach( IBaseObserver observer )
        {
            this.observerList.Add( observer );
        }

        public void detach( IBaseObserver observer )
        {
            this.observerList.Remove( observer );
        }

        public void notify( string action, object subject )
        {
            foreach ( IBaseObserver obs in this.observerList )
                obs.update( action, subject );
        }
    }
}
