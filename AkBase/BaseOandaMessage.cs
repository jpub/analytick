﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkBase
{
    abstract public class BaseOandaMessage : BaseMessage
    {        
        public string RequestString { get; set; }
        public string Method { get; set; }
        public string PostData { get; set; }
    }
}
