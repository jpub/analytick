﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkBase
{
    abstract public class BaseMessage
    {
        static public readonly string ID_PROGRESS_FEEDBACK = "FEEDBACK";
        static public readonly string ID_STRATEGY_CREATE_NEW_ORDER = "STRATEGY_CREATE_NEW_ORDER";

        static public readonly string ID_HEARTBEAT_TICK = "HEARTBEAT_TICK";
        static public readonly string ID_HEARTBEAT_EVENT = "HEARTBEAT_EVENT";
        static public readonly string ID_DISCONNECTED_TICK = "DISCONNECTED_TICK";
        static public readonly string ID_DISCONNECTED_EVENT = "DISCONNECTED_EVENT";
        static public readonly string ID_TICK = "TICK";
        static public readonly string ID_EVENT = "EVENT";

        static public readonly string ID_ACCOUNT_DETAILS = "ACCOUNT_DETAILS";
        static public readonly string ID_OPEN_TRADE_LIST = "OPEN_TRADE_LIST";
        static public readonly string ID_CURRENT_PRICES = "CURRENT_PRICES";
        static public readonly string ID_CANDLE_LIST = "CANDLE_LIST";
        static public readonly string ID_CLOSE_POSITION = "CLOSE_POSITION";
        static public readonly string ID_CREATE_NEW_ORDER = "CREATE_NEW_ORDER";
        static public readonly string ID_MODIFY_EXISTING_TRADE = "MODIFY_EXISTING_TRADE";

        public string MessageType { get; set; }
    }
}
