﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace AkBase
{    
    abstract public class BaseDb<T> : BaseSubject
    {
        protected object lockMe = new object();
        protected Dictionary<string, T> dbMap = new Dictionary<string, T>();
        protected string binFilename = "";

        protected BaseDb()
        {
        }

        virtual public void clear()
        {
            lock ( lockMe )
            {
                this.dbMap.Clear();
            }
        }

        virtual public int count()
        {
            lock ( lockMe )
            {
                return this.dbMap.Count;
            }
        }

        virtual public void update( string id, T item )
        {
            lock ( lockMe )
            {
                if ( this.dbMap.ContainsKey( id ) )
                    this.dbMap[id] = item;
                else
                    this.dbMap.Add( id, item );
            }
        }

        virtual public void update( string action, string id, T item )
        {
            lock ( lockMe )
            {
                if ( this.dbMap.ContainsKey( id ) )
                    this.dbMap[id] = item;
                else
                    this.dbMap.Add( id, item );
            }
            this.notify( action, item );
        }

        virtual public List<T> getCopyListValues()
        {
            lock ( lockMe )
            {
                List<T> copyList = new List<T>( this.dbMap.Values );
                return copyList;
            }
        }

        virtual public List<string> getCopyListKeys()
        {
            lock ( lockMe )
            {
                List<string> copyList = new List<string>( this.dbMap.Keys );
                return copyList;
            }
        }

        virtual public bool get( string id, out T item )
        {
            lock ( lockMe )
            {                
                if ( !this.dbMap.ContainsKey( id ) )
                {
                    item = default( T );
                    return false;
                }

                item = this.dbMap[id];
                return true;
            }
        }

        virtual public bool contain( string id )
        {
            lock ( lockMe )
            {
                return this.dbMap.ContainsKey( id );
            }
        }

        virtual public bool delete( string id, out T deletedItem )
        {
            bool status = false;
            lock ( lockMe )
            {
                status = false;
                deletedItem = default( T );

                if ( this.dbMap.ContainsKey( id ) )
                {
                    deletedItem = this.dbMap[id];
                    this.dbMap.Remove( id );
                    status = true;
                }
            }

            return status;
        }

        virtual public bool delete( string action, string id, out T deletedItem )
        {
            bool status = false;
            lock ( lockMe )
            {
                status = false;
                deletedItem = default( T );

                if ( this.dbMap.ContainsKey( id ) )
                {
                    deletedItem = this.dbMap[id];
                    this.dbMap.Remove( id );
                    status = true;
                }
            }

            this.notify( action, deletedItem );
            return status;
        }

        virtual public void deleteList( List<string> idList )
        {
            lock ( lockMe )
            {
                foreach ( string id in idList )
                {
                    if ( this.dbMap.ContainsKey( id ) )
                    {
                        this.dbMap.Remove( id );
                    }
                }
            }
        }

        virtual public void serialize()
        {
            lock ( lockMe )
            {
                FileStream fs = new FileStream( this.binFilename, FileMode.Create );
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize( fs, this.dbMap );
                fs.Close();
            }
        }

        virtual public void deserialize()
        {
            lock ( lockMe )
            {
                this.dbMap.Clear();
                
                if ( File.Exists( this.binFilename ) )
                {
                    FileStream fs = new FileStream( this.binFilename, FileMode.Open );
                    BinaryFormatter bf = new BinaryFormatter();
                    this.dbMap = (Dictionary<string, T>)bf.Deserialize( fs, null );
                    fs.Close();
                }
            }
        }
    }
}
