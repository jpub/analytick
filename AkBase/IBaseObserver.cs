﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkBase
{
    public interface IBaseObserver
    {
        void update( string action, object subject );
    }
}
