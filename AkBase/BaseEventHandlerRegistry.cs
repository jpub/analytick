﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkBase
{
    abstract public class BaseEventHandlerRegistry
    {
        protected Dictionary<string, BaseEventHandler> registryMap = null;

        abstract public bool handle( BaseMessage message );
    }
}
