﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using AkBase;
using AkCommon;
using AkModel;

namespace AkDatabase
{    
    public class DbEventDate : BaseDb<ModelEventDate>
    {
        private static DbEventDate msInstance = null;

        private DbEventDate()
        {
            this.binFilename = GlobalConstant.DATABASE_FOLDER + Path.DirectorySeparatorChar + GlobalConstant.BIN_FILENAME_EVENT_DATE;
        }

        static public DbEventDate getInstance()
        {
            if ( msInstance == null )
            {
                msInstance = new DbEventDate();
                return msInstance;
            }
            return msInstance;
        }
    }
}