﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using AkBase;
using AkCommon;
using AkModel;

namespace AkDatabase
{
    // custom serialization becuase default serialization is too slow and large as alot of meta data and checking is done by .Net
    public class DbRawTick
    {        
        public struct ConciseTick
        {
            public double bid;
            public double ask;

            public ConciseTick( double bid, double ask )
            {
                this.bid = bid;
                this.ask = ask;
            }
        }

        private static DbRawTick msInstance = null;
        private string binFilename = "";
        private Dictionary<long, ConciseTick> allTickMap = new Dictionary<long, ConciseTick>();

        private DbRawTick()
        {
            this.binFilename = GlobalConstant.DATABASE_FOLDER + Path.DirectorySeparatorChar + GlobalConstant.BIN_FILENAME_RAW_TICK;
        }

        static public DbRawTick getInstance()
        {
            if ( msInstance == null )
            {
                msInstance = new DbRawTick();
                return msInstance;
            }
            return msInstance;
        }

        public int count()
        {
            return this.allTickMap.Count;
        }

        public void add( long index, ConciseTick tick )
        {
            this.allTickMap.Add( index, tick );
        }

        public ConciseTick get( long index )
        {
            return this.allTickMap[index];
        }

        public void serialize()
        {
            using ( MemoryStream ms = new MemoryStream() )
            {
                BinaryWriter bw = new BinaryWriter( ms );
                foreach ( KeyValuePair<long, ConciseTick> kvp in this.allTickMap )
                {
                    bw.Write( kvp.Key );
                    bw.Write( kvp.Value.bid );
                    bw.Write( kvp.Value.ask );
                }
                bw.Flush();
                ms.Position = 0;

                using ( FileStream fs = new FileStream( this.binFilename, FileMode.Create ) )
                {
                    ms.CopyTo( fs );
                    fs.Close();
                }
            }
        }

        public void deserialize()
        {
            this.allTickMap.Clear();

            if ( File.Exists( this.binFilename ) )
            {
                BinaryReader br = new BinaryReader( File.OpenRead( this.binFilename ) );
                long totalLength = br.BaseStream.Length;
                while ( br.BaseStream.Position < totalLength )
                {
                    ConciseTick ct = new ConciseTick();
                    long index = br.ReadInt64();
                    ct.bid = br.ReadDouble();
                    ct.ask = br.ReadDouble();
                    this.allTickMap.Add( index, ct );
                }
                br.Close();
            }
        }
    }
}