﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using AkBase;
using AkCommon;
using AkModel;

namespace AkDatabase
{    
    public class DbCandleGraph : BaseDb<ModelCandleGraph>
    {
        private static DbCandleGraph msInstance = null;

        private DbCandleGraph()
        {
            this.binFilename = GlobalConstant.DATABASE_FOLDER + Path.DirectorySeparatorChar + GlobalConstant.BIN_FILENAME_CANDLE_GRAPH;
        }

        static public DbCandleGraph getInstance()
        {
            if ( msInstance == null )
            {
                msInstance = new DbCandleGraph();
                return msInstance;
            }
            return msInstance;
        }

        public int getCandleIndexNearestMatch( string id, long ticks )
        {
            lock( lockMe )
            {
                if ( id == null || !this.dbMap.ContainsKey( id ) )
                {
                    return -1;
                }

                ModelCandleGraph model = this.dbMap[id];
                return model.getCandleIndexNearestMatch( ticks );                
            }
        }
    }
}
