﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AkBase;
using AkCommon;
using AkModel;

namespace AkDatabase
{
    public class DbUserInterfaceParam : BaseDb<ModelUserInterfaceParam>
    {
        private static DbUserInterfaceParam msInstance = null;

        private DbUserInterfaceParam()
        {            
        }

        static public DbUserInterfaceParam getInstance()
        {
            if ( msInstance == null )
            {
                msInstance = new DbUserInterfaceParam();
                return msInstance;
            }
            return msInstance;
        }
    }
}