﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using AkBase;
using AkCommon;
using AkModel;

namespace AkDatabase
{    
    public class DbLineGraph : BaseDb<ModelLineGraph>
    {
        private static DbLineGraph msInstance = null;

        private DbLineGraph()
        {
            this.binFilename = GlobalConstant.DATABASE_FOLDER + Path.DirectorySeparatorChar + GlobalConstant.BIN_FILENAME_LINE_GRAPH;
        }

        static public DbLineGraph getInstance()
        {
            if ( msInstance == null )
            {
                msInstance = new DbLineGraph();
                return msInstance;
            }
            return msInstance;
        }
    }
}
