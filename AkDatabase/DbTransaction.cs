﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AkBase;
using AkCommon;
using AkModel;

namespace AkDatabase
{
    public class DbTransaction : BaseDb<ModelTransaction>
    {
        private static DbTransaction msInstance = null;

        private DbTransaction()
        {            
        }

        static public DbTransaction getInstance()
        {
            if ( msInstance == null )
            {
                msInstance = new DbTransaction();
                return msInstance;
            }
            return msInstance;
        }
    }
}