﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

using AkCommon;

namespace AkDatabase
{
    public class DbOanda
    {
        protected object lockMe = new object();

        public struct HistoryCandle
        {
            public int timeSecSince1970;
            public double openMid;
            public double highMid;
            public double lowMid;
            public double closeMid;
        }

        public struct HistoryTransaction
        {
            public int timeStartSecSince1970;
            public int timeEndSecSince1970;
            public int transactType;
            public double entryPrice;
            public double exitPice;
            public double takeProfitPrice;
            public double stopLossPrice;
            public int units;
            public double pnl;
        }

        private static DbOanda msInstance = null;
        private SQLiteConnection conn = null;
        
        private SQLiteCommand cmdInsertDayCandle = null;
        private SQLiteCommand cmdGetDayCandleLatestTime = null;
        private SQLiteCommand cmdGetDayCandleSinceGivenDate = null;

        private SQLiteCommand cmdInsertHourCandle = null;
        private SQLiteCommand cmdGetHourCandleLatestTime = null;
        private SQLiteCommand cmdGetHourCandleSinceGivenDate = null;

        private SQLiteCommand cmdInsertMinuteCandle = null;
        private SQLiteCommand cmdGetMinuteCandleLatestTime = null;
        private SQLiteCommand cmdGetMinuteCandleSinceGivenDate = null;

        private SQLiteCommand cmdInsertTransaction = null;        
        private SQLiteCommand cmdGetTransactionSinceGivenDate = null;
        

        private DbOanda()
        {
            this.conn = new SQLiteConnection( GlobalConstant.SQLITE_CONNECTION_STRING );
            this.conn.Open();

            // day candle
            this.cmdInsertDayCandle = new SQLiteCommand( this.conn );
            this.cmdInsertDayCandle.CommandText = "INSERT INTO candle_day(time, open_mid, high_mid, low_mid, close_mid) VALUES (@time, @open_mid, @high_mid, @low_mid, @close_mid)";
            this.cmdInsertDayCandle.Prepare();

            this.cmdGetDayCandleLatestTime = new SQLiteCommand( this.conn );
            this.cmdGetDayCandleLatestTime.CommandText = "SELECT MAX(time) FROM candle_day";

            this.cmdGetDayCandleSinceGivenDate = new SQLiteCommand( this.conn );
            this.cmdGetDayCandleSinceGivenDate.CommandText = "SELECT * FROM candle_day WHERE time >= @startTime AND time <= @endTime";
            this.cmdGetDayCandleSinceGivenDate.Prepare();

            // hour candle
            this.cmdInsertHourCandle = new SQLiteCommand( this.conn );
            this.cmdInsertHourCandle.CommandText = "INSERT INTO candle_hour(time, open_mid, high_mid, low_mid, close_mid) VALUES (@time, @open_mid, @high_mid, @low_mid, @close_mid)";
            this.cmdInsertHourCandle.Prepare();

            this.cmdGetHourCandleLatestTime = new SQLiteCommand( this.conn );
            this.cmdGetHourCandleLatestTime.CommandText = "SELECT MAX(time) FROM candle_hour";

            this.cmdGetHourCandleSinceGivenDate = new SQLiteCommand( this.conn );
            this.cmdGetHourCandleSinceGivenDate.CommandText = "SELECT * FROM candle_hour WHERE time >= @startTime AND time <= @endTime";
            this.cmdGetHourCandleSinceGivenDate.Prepare();

            // minute candle
            this.cmdInsertMinuteCandle = new SQLiteCommand( this.conn );
            this.cmdInsertMinuteCandle.CommandText = "INSERT INTO candle_minute(time, open_mid, high_mid, low_mid, close_mid) VALUES (@time, @open_mid, @high_mid, @low_mid, @close_mid)";
            this.cmdInsertMinuteCandle.Prepare();

            this.cmdGetMinuteCandleLatestTime = new SQLiteCommand( this.conn );
            this.cmdGetMinuteCandleLatestTime.CommandText = "SELECT MAX(time) FROM candle_minute";

            this.cmdGetMinuteCandleSinceGivenDate = new SQLiteCommand( this.conn );
            this.cmdGetMinuteCandleSinceGivenDate.CommandText = "SELECT * FROM candle_minute WHERE time >= @startTime AND time <= @endTime";
            this.cmdGetMinuteCandleSinceGivenDate.Prepare();

            // transaction
            this.cmdInsertTransaction = new SQLiteCommand( this.conn );
            this.cmdInsertTransaction.CommandText = "INSERT INTO completed_transaction(time_start, time_end, transact_type, entry_price, exit_price, take_profit_price, stop_loss_price, units, pnl) VALUES (@time_start, @time_end, @transact_type, @entry_price, @exit_price, @take_profit_price, @stop_loss_price, @units, @pnl)";
            this.cmdInsertTransaction.Prepare();

            this.cmdGetTransactionSinceGivenDate = new SQLiteCommand( this.conn );
            this.cmdGetTransactionSinceGivenDate.CommandText = "SELECT * FROM completed_transaction WHERE time_start >= @startTime AND time_end <= @endTime";
            this.cmdGetTransactionSinceGivenDate.Prepare();
        }

        static public DbOanda getInstance()
        {
            if ( msInstance == null )
            {
                msInstance = new DbOanda();
                return msInstance;
            }
            return msInstance;
        }

        #region day candle
        public List<HistoryCandle> getHistoryDayCandleSince( DateTime dtStart, DateTime dtEnd )
        {
            lock ( lockMe )
            {
                return this.getHistoryCandleSince( this.cmdGetDayCandleSinceGivenDate, dtStart, dtEnd );
            }
        }

        public bool insertDayCandle( int timeSecSince1970, double openMid, double highMid, double lowMid, double closeMid )
        {
            lock ( lockMe )
            {
                return this.insertCandle( this.cmdInsertDayCandle, timeSecSince1970, openMid, highMid, lowMid, closeMid );
            }
        }

        public int getLatestDayCandleTime()
        {
            lock ( lockMe )
            {
                return this.getLatestCandleTime( this.cmdGetDayCandleLatestTime );
            }
        }
        #endregion

        #region hour candle
        public List<HistoryCandle> getHistoryHourCandleSince( DateTime dtStart, DateTime dtEnd )
        {
            lock ( lockMe )
            {
                return this.getHistoryCandleSince( this.cmdGetHourCandleSinceGivenDate, dtStart, dtEnd );
            }
        }

        public bool insertHourCandle( int timeSecSince1970, double openMid, double highMid, double lowMid, double closeMid )
        {
            lock ( lockMe )
            {
                return this.insertCandle( this.cmdInsertHourCandle, timeSecSince1970, openMid, highMid, lowMid, closeMid );
            }
        }

        public int getLatestHourCandleTime()
        {
            lock ( lockMe )
            {
                return this.getLatestCandleTime( this.cmdGetHourCandleLatestTime );
            }
        }
        #endregion

        #region minute candle
        public List<HistoryCandle> getHistoryMinuteCandleSince( DateTime dtStart, DateTime dtEnd )
        {
            lock ( lockMe )
            {
                return this.getHistoryCandleSince( this.cmdGetMinuteCandleSinceGivenDate, dtStart, dtEnd );
            }
        }

        public bool insertMinuteCandle( int timeSecSince1970, double openMid, double highMid, double lowMid, double closeMid )
        {
            lock ( lockMe )
            {
                return this.insertCandle( this.cmdInsertMinuteCandle, timeSecSince1970, openMid, highMid, lowMid, closeMid );
            }
        }

        public int getLatestMinuteCandleTime()
        {
            lock ( lockMe )
            {
                return this.getLatestCandleTime( this.cmdGetMinuteCandleLatestTime );
            }
        }                
        #endregion

        #region transaction
        public List<HistoryTransaction> getHistoryTransactionSince( DateTime dtStart, DateTime dtEnd )
        {
            lock ( lockMe )
            {
                this.cmdGetTransactionSinceGivenDate.Parameters.Clear();
                this.cmdGetTransactionSinceGivenDate.Parameters.AddWithValue( "@startTime", AkUtil.getSecondsSince1970( dtStart ) );
                this.cmdGetTransactionSinceGivenDate.Parameters.AddWithValue( "@endTime", AkUtil.getSecondsSince1970( dtEnd ) );

                List<HistoryTransaction> historyTransactionList = new List<HistoryTransaction>();
                using ( SQLiteDataReader reader = this.cmdGetTransactionSinceGivenDate.ExecuteReader() )
                {
                    while ( reader.Read() )
                    {
                        historyTransactionList.Add( new HistoryTransaction()
                        {
                            timeStartSecSince1970 = int.Parse( reader["time_start"].ToString() ),
                            timeEndSecSince1970 = int.Parse( reader["time_end"].ToString() ),
                            transactType = int.Parse( reader["transact_type"].ToString() ),
                            entryPrice = double.Parse( reader["entry_price"].ToString() ),
                            exitPice = double.Parse( reader["exit_price"].ToString() ),
                            takeProfitPrice = double.Parse( reader["take_profit_price"].ToString() ),
                            stopLossPrice = double.Parse( reader["stop_loss_price"].ToString() ),
                            units = int.Parse( reader["units"].ToString() ),
                            pnl = double.Parse( reader["pnl"].ToString() )
                        } );
                    }
                }
                return historyTransactionList;
            }
        }

        public bool insertTransaction( int timeStartSecSince1970, int timeEndSecSince1970, int transactType, double entryPrice, double exitPice, double takeProfitPrice, double stopLossPrice, int units, double pnl )
        {
            lock ( lockMe )
            {
                this.cmdInsertTransaction.Parameters.Clear();
                this.cmdInsertTransaction.Parameters.AddWithValue( "@time_start", timeStartSecSince1970 );
                this.cmdInsertTransaction.Parameters.AddWithValue( "@time_end", timeEndSecSince1970 );
                this.cmdInsertTransaction.Parameters.AddWithValue( "@transact_type", transactType );
                this.cmdInsertTransaction.Parameters.AddWithValue( "@entry_price", entryPrice );
                this.cmdInsertTransaction.Parameters.AddWithValue( "@exit_price", exitPice );
                this.cmdInsertTransaction.Parameters.AddWithValue( "@take_profit_price", takeProfitPrice );
                this.cmdInsertTransaction.Parameters.AddWithValue( "@stop_loss_price", stopLossPrice );
                this.cmdInsertTransaction.Parameters.AddWithValue( "@units", units );
                this.cmdInsertTransaction.Parameters.AddWithValue( "@pnl", pnl );

                if ( this.cmdInsertTransaction.ExecuteNonQuery() == -1 )
                    return false;

                return true;
            }
        }
        #endregion

        #region helper methods
        private List<HistoryCandle> getHistoryCandleSince( SQLiteCommand cmd, DateTime dtStart, DateTime dtEnd )
        {
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue( "@startTime", AkUtil.getSecondsSince1970( dtStart ) );
            cmd.Parameters.AddWithValue( "@endTime", AkUtil.getSecondsSince1970( dtEnd ) );

            List<HistoryCandle> historyCandleList = new List<HistoryCandle>();
            using ( SQLiteDataReader reader = cmd.ExecuteReader() )
            {
                while ( reader.Read() )
                {
                    historyCandleList.Add( new HistoryCandle()
                    {
                        timeSecSince1970 = int.Parse( reader["time"].ToString() ),
                        openMid = double.Parse( reader["open_mid"].ToString() ),
                        highMid = double.Parse( reader["high_mid"].ToString() ),
                        lowMid = double.Parse( reader["low_mid"].ToString() ),
                        closeMid = double.Parse( reader["close_mid"].ToString() )
                    } );
                }
            }
            return historyCandleList;
        }

        private bool insertCandle( SQLiteCommand cmd, int timeSecSince1970, double openMid, double highMid, double lowMid, double closeMid )
        {
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue( "@time", timeSecSince1970 );
            cmd.Parameters.AddWithValue( "@open_mid", openMid );
            cmd.Parameters.AddWithValue( "@high_mid", highMid );
            cmd.Parameters.AddWithValue( "@low_mid", lowMid );
            cmd.Parameters.AddWithValue( "@close_mid", closeMid );

            if ( cmd.ExecuteNonQuery() == -1 )
                return false;

            return true;
        }

        private int getLatestCandleTime( SQLiteCommand cmd )
        {
            try
            {
                return Convert.ToInt32( cmd.ExecuteScalar() );
            }
            catch ( Exception )
            {
                return -1;
            }
        }
        #endregion
    }
}
