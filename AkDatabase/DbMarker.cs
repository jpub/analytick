﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using AkBase;
using AkCommon;
using AkModel;

namespace AkDatabase
{
    public class DbMarker : BaseDb<ModelMarker>
    {
        private static DbMarker msInstance = null;

        private DbMarker()
        {
            this.binFilename = GlobalConstant.DATABASE_FOLDER + Path.DirectorySeparatorChar + GlobalConstant.BIN_FILENAME_MARKER;
        }

        static public DbMarker getInstance()
        {
            if ( msInstance == null )
            {
                msInstance = new DbMarker();
                return msInstance;
            }
            return msInstance;
        }
    }
}