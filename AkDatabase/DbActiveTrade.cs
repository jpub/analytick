﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AkBase;
using AkCommon;
using AkModel;

namespace AkDatabase
{
    public class DbActiveTrade : BaseDb<ModelActiveTrade>
    {
        private static DbActiveTrade msInstance = null;

        private DbActiveTrade()
        {            
        }

        static public DbActiveTrade getInstance()
        {
            if ( msInstance == null )
            {
                msInstance = new DbActiveTrade();
                return msInstance;
            }
            return msInstance;
        }
    }
}
