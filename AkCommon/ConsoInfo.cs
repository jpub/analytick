﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AkCommon
{
    [Serializable]
    public class ConsoInfo
    {
        public long CtIndexStart { get; set; }
        public long CtIndexEnd { get; set; }
        public DateTime Time { get; set; }        
        public double BidHigh { get; set; }
        public double BidLow { get; set; }
        public double BidOpen { get; set; }
        public double BidClose { get; set; }

        public double AskHigh { get; set; }
        public double AskLow { get; set; }
        public double AskOpen { get; set; }
        public double AskClose { get; set; }
    }
}
