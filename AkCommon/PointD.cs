﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AkCommon
{
    public struct PointD
    {
        public double X;
        public double Y;

        public PointD( double X, double Y )
        {
            this.X = X;
            this.Y = Y;
        }
    }
}
