﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AkCommon
{
    public class ConsolidatorMinute : Consolidator
    {
        override public DateTime getConsoTime( DateTime inTime )
        {
            return new DateTime( inTime.Year, inTime.Month, inTime.Day, inTime.Hour, this.consoParam * GlobalConstant.MINUTE_BLOCK, 0, inTime.Kind );
        }

        override public int getComparingParam( DateTime inTime )
        {
            return ( inTime.Minute / GlobalConstant.MINUTE_BLOCK );

        }              
    }
}
