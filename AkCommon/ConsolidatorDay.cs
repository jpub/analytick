﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AkCommon
{
    public class ConsolidatorDay : Consolidator
    {
        override public DateTime getConsoTime( DateTime inTime )
        {
            return new DateTime( inTime.Year, inTime.Month, inTime.Day, 0, 0, 0, inTime.Kind );
        }

        override public int getComparingParam( DateTime inTime )
        {
            return inTime.Day;
        }        
    }
}
