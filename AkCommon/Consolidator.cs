﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AkCommon
{
    abstract public class Consolidator
    {
        protected bool partialBar = true;
        protected int consoParam = int.MinValue;
        protected long ctIndexStart = long.MinValue;
        protected long ctIndexEnd = long.MaxValue;
        protected DateTime dataTime = DateTime.MinValue;
        protected double bidHigh = double.MinValue;
        protected double bidLow = double.MaxValue;
        protected double bidOpen = double.MaxValue;
        protected double bidClose = double.MaxValue;
        protected double askHigh = double.MinValue;
        protected double askLow = double.MaxValue;
        protected double askOpen = double.MaxValue;
        protected double askClose = double.MaxValue;        

        abstract public DateTime getConsoTime( DateTime inTime );
        abstract public int getComparingParam( DateTime inTime );

        virtual public bool close( ref ConsoInfo consoInfo )
        {
            if ( partialBar )
            {
                this.partialBar = true;
                consoInfo = new ConsoInfo()
                {
                    CtIndexStart = this.ctIndexStart,
                    CtIndexEnd = this.ctIndexEnd,
                    Time = this.getConsoTime( this.dataTime ),
                    BidHigh = this.bidHigh,
                    BidLow = this.bidLow,
                    BidOpen = this.bidOpen,
                    BidClose = this.bidClose,
                    AskHigh = this.askHigh,
                    AskLow = this.askLow,
                    AskOpen = this.askOpen,
                    AskClose = this.askClose,
                };
                return true;
            }
            return false;
        }

        virtual public bool group( long ctIndex, DateTime dt, double bid, double ask, ref ConsoInfo consoInfo )
        {
            int currentComparingParam = this.getComparingParam( dt );

            if ( this.consoParam == int.MinValue )
            {
                this.partialBar = true;
                this.ctIndexStart = ctIndex;
                this.consoParam = currentComparingParam;
                this.dataTime = dt;
                this.bidHigh = bid;
                this.bidLow = bid;
                this.bidOpen = bid;
                this.bidClose = bid;

                this.askHigh = ask;
                this.askLow = ask;
                this.askOpen = ask;
                this.askClose = ask;
                return false;
            }
            else if ( this.consoParam == currentComparingParam )
            {
                this.partialBar = true;
                this.ctIndexEnd = ctIndex;

                this.bidHigh = bid > this.bidHigh ? bid : this.bidHigh;
                this.bidLow = bid < this.bidLow ? bid : this.bidLow;
                this.bidClose = bid;

                this.askHigh = ask > this.askHigh ? ask : this.askHigh;
                this.askLow = ask < this.askLow ? ask : this.askLow;
                this.askClose = ask;

                return false;
            }
            else
            {
                this.partialBar = false;
                consoInfo = new ConsoInfo()
                {
                    CtIndexStart = this.ctIndexStart,
                    CtIndexEnd = this.ctIndexEnd,
                    Time = this.getConsoTime( this.dataTime ),
                    BidHigh = this.bidHigh,
                    BidLow = this.bidLow,
                    BidOpen = this.bidOpen,
                    BidClose = this.bidClose,
                    AskHigh = this.askHigh,
                    AskLow = this.askLow,
                    AskOpen = this.askOpen,
                    AskClose = this.askClose,
                };

                this.ctIndexStart = ctIndex;
                this.ctIndexEnd = long.MaxValue;

                this.bidHigh = bid;
                this.bidLow = bid;
                this.bidOpen = bid;

                this.askHigh = ask;
                this.askLow = ask;
                this.askOpen = ask;

                this.consoParam = currentComparingParam;
                this.dataTime = dt;

                return true;
            }
        }
    }
}
