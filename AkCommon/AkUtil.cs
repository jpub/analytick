﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkCommon
{
    public class AkUtil
    {
        static private DateTime dateTimeOrigin = new DateTime( 1970, 1, 1, 0, 0, 0 );

        static public double getSecondsSince1970( DateTime dt )
        {
            return dt.Subtract( dateTimeOrigin ).TotalSeconds;
        }

        static public DateTime getDateTime( int secondsSince1970 )
        {
            return dateTimeOrigin.AddSeconds( secondsSince1970 );
        }

        static public string ToGridTimeHourMinute( DateTime dt )
        {
            return dt.ToString( "HHmm" );
        }

        static public string ToGridTimeHour( DateTime dt )
        {
            return dt.ToString( "HH" ) + "00";
        }

        static public string ToGridTimeDayMonth( DateTime dt )
        {
            return dt.ToString( "dd MMM" );
        }

        static public string ToGridTimeMonth( DateTime dt )
        {
            return dt.ToString( "MMM" );
        }

        static public string ToGridTimeDateHourMinute( DateTime dt )
        {
            return dt.ToString( "ddd-MMM-dd--HHmm" );
        }

        static public string ToGridTimeDate( DateTime dt )
        {
            return dt.ToString( "ddd-MMM-dd" );
        }

        static public string ToGridTimeYear( DateTime dt )
        {
            return dt.ToString( "yyyy" );
        }

        static public string GetPipDisplay( double pip )
        {
            return pip.ToString( "0.0" );
        }

        static public string getTradeSideWithArrow( string side )
        {
            if ( side == "buy" )
                return side.ToUpper() + " \x25B2";
            else if ( side == "sell" )
                return side.ToUpper() + " \x25BC";
            else
                return side.ToUpper();
        }

        static public string GetPriceStringNoPippete( double price )
        {
            if ( GlobalConstant.ONE_PIP_PRICE == 0.0001 )
            {
                return string.Format( "{0:0.0000}", price );
            }
            else
            {
                return string.Format( "{0:0.00}", price );
            }
        }

        static public string GetShortPriceStringSeparatePippete( double price )
        {
            string significantDigits = "";
            string lastDigit = "";

            if ( GlobalConstant.ONE_PIP_PRICE == 0.0001 )
            {
                string fullPrice = string.Format( "{0:0.00000}", price );
                char[] arr = fullPrice.ToCharArray();
                significantDigits = new string( arr, 4, 2 );
                lastDigit = new string( arr, 6, 1 );
                return string.Format( "{0}{1}", significantDigits, getPipette( lastDigit ) );
            }
            else
            {
                string fullPrice = string.Format( "{0:0.000}", price );
                char[] arr = fullPrice.ToCharArray();
                int totalLength = arr.Length;
                int indexOfDecimal = fullPrice.IndexOf( "." );
                significantDigits = new string( arr, totalLength - 3, 2 );
                lastDigit = new string( arr, totalLength - 1, 1 );
                return string.Format( "{0}{1}", significantDigits, getPipette( lastDigit ) );
            }
        }

        static public string GetPriceStringSeparatePippete( double price )
        {
            string lastDigit = "";

            if ( GlobalConstant.ONE_PIP_PRICE == 0.0001 )
            {
                string fullPrice = string.Format( "{0:0.00000}", price );
                char[] arr = fullPrice.ToCharArray();
                lastDigit = new string( arr, 6, 1 );
                return string.Format( "{0} : {1}", fullPrice.Substring( 0, fullPrice.Length - 1 ), lastDigit );
            }
            else
            {
                string fullPrice = string.Format( "{0:0.000}", price );
                char[] arr = fullPrice.ToCharArray();
                int totalLength = arr.Length;
                int indexOfDecimal = fullPrice.IndexOf( "." );
                lastDigit = new string( arr, totalLength - 1, 1 );
                return string.Format( "{0} : {1}", fullPrice.Substring( 0, fullPrice.Length - 1 ), lastDigit );
            }
        }

        static public string GetPriceStringWithPippete( double price )
        {
            string movingPip = "";
            string lastDigit = "";
            string pippete = "";

            if ( GlobalConstant.ONE_PIP_PRICE == 0.0001 )
            {
                string fullPrice = string.Format( "{0:0.00000}", price );
                char[] arr = fullPrice.ToCharArray();
                lastDigit = new string( arr, 6, 1 );
                pippete = movingPip + getPipette( lastDigit );
                return string.Format( "{0} {1}", fullPrice.Substring( 0, fullPrice.Length - 1 ), pippete );
            }
            else
            {
                string fullPrice = string.Format( "{0:0.000}", price );
                char[] arr = fullPrice.ToCharArray();
                int totalLength = arr.Length;
                int indexOfDecimal = fullPrice.IndexOf( "." );
                lastDigit = new string( arr, totalLength - 1, 1 );
                pippete = movingPip + getPipette( lastDigit );
                return string.Format( "{0} {1}", fullPrice.Substring( 0, fullPrice.Length - 1 ), pippete );
            }
        }

        static private string getPipette( string lastDigit )
        {
            if ( lastDigit == "0" )
                return "\x2070";
            else if ( lastDigit == "1" )
                return "\x00B9";
            else if ( lastDigit == "2" )
                return "\x00B2";
            else if ( lastDigit == "3" )
                return "\x00B3";
            else if ( lastDigit == "4" )
                return "\x2074";
            else if ( lastDigit == "5" )
                return "\x2075";
            else if ( lastDigit == "6" )
                return "\x2076";
            else if ( lastDigit == "7" )
                return "\x2077";
            else if ( lastDigit == "8" )
                return "\x2078";
            else if ( lastDigit == "9" )
                return "\x2079";
            else
                return "XX";
        }
    }
}
