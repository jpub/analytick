﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AkBase;
using AkCommon;

namespace AkModel
{
    [Serializable]
    public class ModelCandleGraph : BaseModel
    {
        [Serializable]
        public struct CandleData
        {
            public int index;

            public long xValue;

            public double highMid;
            public double lowMid;
            public double openMid;
            public double closeMid;
        }

        private static CandleData NULL_CANDLE_DATA = new CandleData();
        private List<CandleData> listCandleData = new List<CandleData>();
        private Dictionary<int, CandleData> dictCandleData = new Dictionary<int, CandleData>();      

        public void clear()
        {
            this.listCandleData.Clear();
            this.dictCandleData.Clear();
        }

        public int count()
        {
            return this.listCandleData.Count;
        }

        public void add( CandleData dataPoint )
        {
            dataPoint.index = this.listCandleData.Count;
            this.listCandleData.Add( dataPoint );
            this.dictCandleData.Add( dataPoint.index, dataPoint );
        }

        public bool getByIndex( int index, out CandleData outData )
        {
            if ( this.dictCandleData.ContainsKey( index ) )
            {
                outData = this.dictCandleData[index];
                return true;
            }

            outData = NULL_CANDLE_DATA;
            return false;
        }

        public bool getByDateTime( DateTime dt, out CandleData outCandle )
        {
            long dtTicks = dt.Ticks;
            foreach( CandleData data in this.listCandleData)
            {
                if ( data.xValue > dtTicks )
                {
                    break;
                }
                else if ( data.xValue == dtTicks )
                {
                    outCandle = data;
                    return true;
                }
            }

            outCandle = NULL_CANDLE_DATA;
            return false;
        }

        public int getCandleIndexExactMatch( long ticks )
        {
            int candleIndex = -1;
            foreach ( CandleData data in this.listCandleData )
            {
                ++candleIndex;
                if ( data.xValue > ticks )
                {
                    break;
                }
                else if ( data.xValue == ticks )
                {
                    return candleIndex;                    
                }
            }
            return -1;
        }

        public int getCandleIndexNearestMatch( long ticks )
        {
            bool found = false;
            int candleIndex = -1;
            foreach ( CandleData data in this.listCandleData )
            {
                ++candleIndex;
                if ( data.xValue > ticks )
                {
                    found = true;
                    break;
                }
            }

            // the previous candle should engulf the time
            if ( found )
                return ( candleIndex - 1 );
            else
                return candleIndex; // return the most recent candle
        }

        public List<CandleData> getCopyListCandleData()
        {
            List<CandleData> copyList = new List<CandleData>( this.listCandleData );
            return copyList;
        }

        public List<CandleData> getRefListCandleData()
        {
            return this.listCandleData;
        }
    }
}
