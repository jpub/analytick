﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AkBase;

namespace AkModel
{
    [Serializable]
    public class ModelTransaction : BaseModel
    {
        public enum EnumTransactType
        {
            UNDEFINE = 0,
            LONG = 1,
            SHORT = 2
        }

        public enum EnumTransactOutcome
        {
            UNDEFINE = 0,
            LOSS = 1,
            PROFIT = 2,
            UNRESOLVE = 3
        }

        public string Remarks { get; set; }

        [Serializable]
        public struct TransactionData
        {
            public EnumTransactType transactType;
            public EnumTransactOutcome transactOutcome;            
            public int entryCandleIndex;
            public int exitCandleIndex;
            public double entryPrice;
            public double exitPrice;
            public double takeProfitPrice;
            public double stopLossPrice;

            // only used in live data
            public int dtStartSecSince1970;
            public int dtEndSecSince1970;
            public int units;
            public double pl;
        }
        
        private List<TransactionData> listTransactionData = new List<TransactionData>();

        public void clear()
        {
            this.listTransactionData.Clear();
        }

        public int count()
        {
            return this.listTransactionData.Count;
        }

        public void add( TransactionData td )
        {
            this.listTransactionData.Add( td );
        }

        public List<TransactionData> getCopyListTransactionData()
        {
            List<TransactionData> copyList = new List<TransactionData>( this.listTransactionData );
            return copyList;
        }

        public List<TransactionData> getRefListTransactionData()
        {
            return this.listTransactionData;
        }
    }
}
