﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AkBase;

namespace AkModel
{
    [Serializable]
    public class ModelMarker : BaseModel
    {        
        public double Value { get; set; }
        public string Remarks { get; set; }
        public bool IsMarkerA { get; set; }
        public bool IsHorizontal { get; set; }

        public ModelMarker( double value, string remarks, bool isMarkerA, bool isHorizontal )
        {
            this.Value = value;
            this.Remarks = remarks;
            this.IsMarkerA = isMarkerA;
            this.IsHorizontal = isHorizontal;
        }
    }
}
