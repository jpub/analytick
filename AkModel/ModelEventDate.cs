﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AkBase;

namespace AkModel
{
    [Serializable]
    public class ModelEventDate : BaseModel
    {        
        public string Name { get; set; }
        public DateTime Time { get; set; }

        public ModelEventDate( string name, DateTime time )
        {
            this.Name = name;
            this.Time = time;
        }
    }
}
