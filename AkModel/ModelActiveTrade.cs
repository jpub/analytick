﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AkBase;

namespace AkModel
{
    public class ModelActiveTrade : BaseModel
    {
        public DateTime TickTime { get; set; }
        public double Bid { get; set; }
        public double Ask { get; set; }
        public string Side { get; set; }
        public int Units { get; set; }
        public double EntryPrice { get; set; }
        public double TakeProfit { get; set; }
        public double StopLoss { get; set; }

        public ModelActiveTrade()
        {
            this.TickTime = DateTime.Now;
            this.Bid = 0;
            this.Ask = 0;
            this.EntryPrice = 0;
            this.TakeProfit = 0;
            this.StopLoss = 0;
        }
    }
}
