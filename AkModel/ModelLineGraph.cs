﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AkBase;

namespace AkModel
{
    [Serializable]
    public class ModelLineGraph : BaseModel
    {
        public enum EnumLineGraphType
        {
            UNDEFINE = 0,
            EMA = 1,
            RSI = 2,
            MACD = 3
        }
        public EnumLineGraphType LineGraphType { get; set; }   

        [Serializable]
        public struct LineData
        {
            public int index;

            public long xValue;
            public double yValue;
        }

        private static LineData NULL_LINE_DATA = new LineData();
        private List<LineData> listLineData = new List<LineData>();
        private Dictionary<int, LineData> dictLineData = new Dictionary<int, LineData>();

        public ModelLineGraph( EnumLineGraphType lineGraphType )
        {
            this.LineGraphType = lineGraphType;
        }

        public void clear()
        {
            this.listLineData.Clear();
            this.dictLineData.Clear();
        }

        public void add( LineData dataPoint )
        {
            dataPoint.index = this.listLineData.Count;
            this.listLineData.Add( dataPoint );
            this.dictLineData.Add( dataPoint.index, dataPoint );
        }

        public bool getByIndex( int index, out LineData outData )
        {
            if ( this.dictLineData.ContainsKey( index ) )
            {
                outData = this.dictLineData[index];
                return true;
            }
            
            outData = NULL_LINE_DATA;
            return false;
        }

        public List<LineData> getCopyListLineData()
        {
            List<LineData> copyList = new List<LineData>( this.listLineData );
            return copyList;
        }

        public List<LineData> getRefListLineData()
        {
            return this.listLineData;
        }
    }
}
