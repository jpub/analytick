﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AkBase;

namespace AkModel
{
    [Serializable]
    public class ModelUserInterfaceParam : BaseModel
    {
        static public readonly string DB_ACTION_UPDATE_MAIN_CANDLE_MIN_MAX_X = "DB_ACTION_UPDATE_MAIN_CANDLE_MIN_MAX_X";
        static public readonly string DB_ACTION_UPDATE_OVERVIEW_CANDLE_VISUAL_X = "DB_ACTION_UPDATE_OVERVIEW_CANDLE_VISUAL_X";        

        public double MainCandleMinX { get; set; }
        public double MainCandleMaxX { get; set; }
        public double OverviewCandleMinX { get; set; }
        public double OverviewCandleMaxX { get; set; }
    }
}
