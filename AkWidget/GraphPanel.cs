﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

using AkCommon;

namespace AkWidget
{
    public class GraphPanel : Panel
    {
        public enum EnumViewScale
        {
            MINUTE = 0,
            HOUR = 1,
            DAY = 2,
            ORDINAL
        }

        #region Properties                
        public EnumViewScale ViewScale { get; set; }                        
        public double MinX { get; set; }
        public double MaxX { get; set; }
        public double MinY { get; set; }
        public double MaxY { get; set; }        
        public bool EnablePanning { get; set; }
        public bool EnableZooming { get; set; }        
        public bool ShowXaxisLabel { get; set; }
        public bool ShowYaxisLabel { get; set; }
        public bool ShowXGrid { get; set; }
        public bool ShowYGrid { get; set; }
        #endregion

        private bool isMouseDown = false;
        private int lastX = 0;
        private int lastY = 0;
        private float zoomFactor = 0.1F;        
        private DateTime dt = DateTime.Now;

        public GraphPanel()
        {
            this.ViewScale = EnumViewScale.HOUR;            
            this.MinX = this.MaxX = this.MinY = this.MaxY = 0;                        
            this.EnablePanning = this.EnableZooming = true;            
            this.ShowXaxisLabel = true;
            this.ShowYaxisLabel = true;
            this.ShowXGrid = true;
            this.ShowYGrid = true;

            this.SetStyle( ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.DoubleBuffer, true );
            this.Paint += new System.Windows.Forms.PaintEventHandler( this.OnPaint );
            this.MouseDown += new System.Windows.Forms.MouseEventHandler( this.OnMouseDown );
            this.MouseUp += new System.Windows.Forms.MouseEventHandler( this.OnMouseUp );
            this.MouseMove += new System.Windows.Forms.MouseEventHandler( this.OnMouseMove );
            this.MouseWheel += new MouseEventHandler( this.OnMouseWheel );

            
        }
        
        public Point logicalToPixel( double logicalX, double logicalY )
        {
            int pixelX = (int)( ( logicalX - this.MinX ) * this.Width / ( this.MaxX - this.MinX ) );
            int pixelY = (int)( this.Height - ( ( logicalY - this.MinY ) * this.Height / ( this.MaxY - this.MinY ) ) );

            return new Point( pixelX, pixelY );
        }

        public PointD pixelToLogical( int pixelX, int pixelY )
        {
            double newX = (double)( ( (double)( pixelX ) / this.Width * ( this.MaxX - this.MinX ) ) + this.MinX );
            double newY = (double)( this.MaxY - ( (double)( pixelY ) / this.Height * ( this.MaxY - this.MinY ) ) );

            return new PointD( newX, newY );
        }

        public int logicalToPixelHeight( double logicalTop, double logicalBot )
        {
            int pixelHeight = (int)( ( logicalTop - logicalBot ) * this.Height / ( this.MaxY - this.MinY ) );
            return pixelHeight;
        }

        #region Events
        private void OnPaint( object sender, PaintEventArgs e )
        {
        }
        
        private void OnMouseDown( object sender, MouseEventArgs e )
        {
            // needed to so that mouseWheel event can be captured
            this.Focus();
            bool altPress = ( Control.ModifierKeys & Keys.Alt ) == Keys.Alt;

            if ( e.Button == MouseButtons.Left )
            {                
                this.mouseDownPanning( e );
            }
        }

        private void OnMouseUp( object sender, MouseEventArgs e )
        {
            if ( e.Button == MouseButtons.Left )
            {
                this.mouseUpPanning( e );
            }
        }

        private void OnMouseMove( object sender, MouseEventArgs e )
        {
            if ( e.Button == MouseButtons.Left )
            {
                this.mouseMovePanning( e );
            }
        }

        private void OnMouseWheel( object sender, MouseEventArgs e )
        {
            if ( this.isCtrlKeyDown() )
            {
                if ( !this.EnableZooming )
                    return;

                if ( e.Delta > 0 )
                    this.zoomIn();
                else
                    this.zoomOut();
            }
            else
            {
                if ( !this.EnablePanning )
                    return;

                double panPip = (double)( GlobalConstant.PAN_DELTA / (double)this.Height * ( this.MaxY - this.MinY ) );
                if ( e.Delta > 0 )
                {
                    this.MinY += ( panPip );
                    this.MaxY += ( panPip );
                }
                else
                {
                    this.MinY -= ( panPip );
                    this.MaxY -= ( panPip );
                }
            }
        }
        #endregion

        #region Panning
        private void mouseDownPanning( MouseEventArgs e )
        {
            if ( !this.EnablePanning )
                return;

            this.isMouseDown = true;
            this.lastX = e.X;
            this.lastY = e.Y;
            this.Cursor = Cursors.Hand;
        }

        private void mouseUpPanning( MouseEventArgs e )
        {
            if ( !this.EnablePanning )
                return;

            this.isMouseDown = false;
            this.Cursor = Cursors.Arrow;
        }

        private void mouseMovePanning( MouseEventArgs e )
        {
            if ( !this.EnablePanning )
                return;

            if ( this.isMouseDown )
            {
                double xDelta = (double)( ( e.X - this.lastX ) / (double)this.Width * ( this.MaxX - this.MinX ) );
                double yDelta = (double)( ( e.Y - this.lastY ) / (double)this.Height * ( this.MaxY - this.MinY ) );

                this.MinX -= ( xDelta );
                this.MaxX -= ( xDelta );
                this.MinY += ( yDelta );
                this.MaxY += ( yDelta );

                this.lastX = e.X;
                this.lastY = e.Y;

                //this.Refresh();
            }
        }
        #endregion

        #region Zooming
        private void zoomIn()
        {
            //double additionalX = (double)( ( this.MaxX - this.MinX ) * this.zoomFactor );
            double additionalY = (double)( ( this.MaxY - this.MinY ) * this.zoomFactor );            

            //this.MinX += ( additionalX / 2 );
            //this.MaxX -= ( additionalX / 2 );

            this.MinY += ( additionalY / 2 );
            this.MaxY -= ( additionalY / 2 );

            //if ( this.MinX > this.MaxX )
            //{
            //    this.MinX -= ( additionalX / 2 );
            //    this.MaxX += ( additionalX / 2 );
            //}

            if ( this.MinY > this.MaxY )
            {
                this.MinY -= ( additionalY / 2 );
                this.MaxY += ( additionalY / 2 );
            }

            //this.Refresh();
        }

        private void zoomOut()
        {
            //double additionalX = (double)( ( this.MaxX - this.MinX ) * this.zoomFactor );
            double additionalY = (double)( ( this.MaxY - this.MinY ) * this.zoomFactor );            

            //this.MinX -= ( additionalX / 2 );
            //this.MaxX += ( additionalX / 2 );

            this.MinY -= ( additionalY / 2 );
            this.MaxY += ( additionalY / 2 );

            //this.Refresh();
        }
        #endregion

        #region Helper function
        private bool isCtrlKeyDown()
        {
            return ( ( Control.ModifierKeys & Keys.Control ) != Keys.None );
        }        
        #endregion
    }    
}
