﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Configuration;

using Analytick.Forms;

//Here is the once-per-application setup information
[assembly: log4net.Config.XmlConfigurator( ConfigFile = "log4net.config", Watch = true )]

namespace Analytick
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler( CurrentDomain_UnhandledException );
            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler( Application_ThreadException );

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault( false );
            FormMain formMain = new FormMain();
            formMain.Show();
            Application.Run();
        }

        static void CurrentDomain_UnhandledException( object sender, UnhandledExceptionEventArgs e )
        {
            try
            {
                Exception ex = (Exception)e.ExceptionObject;
                FormCrash formCrash = new FormCrash();
                formCrash.setExceptionMessage( ex.Message );
                formCrash.setExceptionStackTrace( ex.StackTrace );
                formCrash.ShowDialog();

                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
            finally
            {
                Application.Exit();
            }
        }

        static void Application_ThreadException( object sender, System.Threading.ThreadExceptionEventArgs e )
        {
            try
            {
                FormCrash formCrash = new FormCrash();
                formCrash.setExceptionMessage( e.Exception.Message );
                formCrash.setExceptionStackTrace( e.Exception.StackTrace );
                formCrash.ShowDialog();

                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
            finally
            {
                Application.Exit();
            }
        }
    }
}
