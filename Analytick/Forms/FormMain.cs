﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Text;
using System.Xml;
using System.Web.Script.Serialization;
using System.Net.NetworkInformation;
using System.Windows.Forms;
using System.Data.SQLite;

using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Diagnostics;

using Analytick.Observer;
using Analytick.EventHandlers;
using Analytick.Manager;
using Analytick.Properties;

using AkOanda;
using AkBase;
using AkCommon;
using AkModel;
using AkView;
using AkDatabase;
using AkWidget;
using AkAlgo;
using AkMessages;
using AkComms;

namespace Analytick.Forms
{
    public partial class FormMain : Form
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger( System.Reflection.MethodBase.GetCurrentMethod().DeclaringType );

        private FormDateSelection formDateSelection = null;
        private FormPlayback formPlayback = null;

        private JavaScriptSerializer serializer = new JavaScriptSerializer();   
        private List<ConsoInfo> listBarMinute = new List<ConsoInfo>();
        private List<ConsoInfo> listBarHour = new List<ConsoInfo>();
        private List<ConsoInfo> listBarDay = new List<ConsoInfo>();

        private List<BaseView> graphCandleViewList = new List<BaseView>();                
        private ViewCandleGraph viewCandleGraph = new ViewCandleGraph();
        private ViewMarker viewMarker = new ViewMarker();
        private ViewEventDate viewEventDate = new ViewEventDate();
        private ViewInfoCandle viewInfoCandle = new ViewInfoCandle();
        private ViewEmaGraph viewEmaGraph = new ViewEmaGraph();
        private ViewActiveTrade viewActiveTrade = new ViewActiveTrade();
        private ViewSimulatedTransaction viewSimulatedTransaction = new ViewSimulatedTransaction();

        private List<BaseView> graphOverviewViewList = new List<BaseView>();
        private ViewCandleGraph viewOverviewCandleGraph = new ViewCandleGraph();
        private ViewOverviewRegion viewOverviewRegion = new ViewOverviewRegion();
        private ViewMarker viewOverviewMarker = new ViewMarker();

        private List<BaseView> graphDistributionViewList = new List<BaseView>();
        private ViewHistogramGraph viewHistogramGraph = new ViewHistogramGraph();
        private ViewInfoHistogram viewInfoHistogram = new ViewInfoHistogram();

        private List<BaseView> graphRsiViewList = new List<BaseView>();
        private ViewRsiGraph viewRsiGraph = new ViewRsiGraph();

        private List<BaseView> graphMacdViewList = new List<BaseView>();
        private ViewMacdGraph viewMacdGraph = new ViewMacdGraph();        

        private ObserverGraphPanelCandle observerGraphPanelCandle = null;
        private ObserverOverviewRegion observerOverviewRegion = null;
        private ObserverGraphPanelRsi observerGraphPanelRsi = null;
        private ObserverGraphPanelMacd observerGraphPanelMacd = null;

        private List<string> candleGraphNameList = new List<string>();
        private List<string> distributionGraphNameList = new List<string>();

        private MessageQueue<BaseMessage> incomingQueue = new MessageQueue<BaseMessage>();
        private EventHandlerRegistry eventHandlerRegistry = new EventHandlerRegistry();
        private SequentialWorker sequentialWorker = null;

        private Strategy liveStrategy = null;

        public FormMain()
        {            
            InitializeComponent();            
            GlobalConstant.init();
            this.formDateSelection = new FormDateSelection( GlobalConstant.DEFAULT_START_DATE );
            this.toggleStrategyButton( false );
            this.labelBidAsk.Visible = false;
            this.folderBrowserDialog.SelectedPath = Application.StartupPath;
            this.Location = Screen.AllScreens[1].WorkingArea.Location;

            this.candleGraphNameList.Add( GlobalConstant.GRAPH_NAME_CANDLE_MINUTE );
            this.candleGraphNameList.Add( GlobalConstant.GRAPH_NAME_CANDLE_HOUR );
            this.candleGraphNameList.Add( GlobalConstant.GRAPH_NAME_CANDLE_DAY );

            this.distributionGraphNameList.Add( GlobalConstant.GRAPH_NAME_BAR_DISTRIBUTION_MINUTE );
            this.distributionGraphNameList.Add( GlobalConstant.GRAPH_NAME_BAR_DISTRIBUTION_HOUR );
            this.distributionGraphNameList.Add( GlobalConstant.GRAPH_NAME_BAR_DISTRIBUTION_DAY );
                        
            this.graphPanelCandle.MouseWheel += new MouseEventHandler( graphPanelCandle_MouseWheel );            

            this.graphPanelDistribution.ViewScale = GraphPanel.EnumViewScale.ORDINAL;

            this.graphCandleViewList.Add( this.viewCandleGraph );
            this.graphCandleViewList.Add( this.viewMarker );
            this.graphCandleViewList.Add( this.viewEventDate );
            this.graphCandleViewList.Add( this.viewInfoCandle );
            this.graphCandleViewList.Add( this.viewEmaGraph );
            this.graphCandleViewList.Add( this.viewActiveTrade );
            this.graphCandleViewList.Add( this.viewSimulatedTransaction );
            
            this.viewOverviewCandleGraph.BarWidth = GlobalConstant.BAR_SLIM_WIDTH;
            this.viewOverviewMarker.IsShowValue = false;            
            this.graphOverviewViewList.Add( this.viewOverviewCandleGraph );
            this.graphOverviewViewList.Add( this.viewOverviewRegion );
            this.graphOverviewViewList.Add( this.viewOverviewMarker );

            this.graphDistributionViewList.Add( this.viewHistogramGraph );
            this.graphDistributionViewList.Add( this.viewInfoHistogram );

            this.graphRsiViewList.Add( this.viewRsiGraph );
            this.graphMacdViewList.Add( this.viewMacdGraph );            

            this.observerGraphPanelCandle = new ObserverGraphPanelCandle( this.graphPanelCandle );
            this.observerOverviewRegion = new ObserverOverviewRegion( this.viewOverviewRegion );
            this.observerGraphPanelRsi = new ObserverGraphPanelRsi( this.graphPanelRsi );
            this.observerGraphPanelMacd = new ObserverGraphPanelMacd( this.graphPanelMacd );

            DbUserInterfaceParam.getInstance().attach( this.observerGraphPanelCandle );
            DbUserInterfaceParam.getInstance().attach( this.observerOverviewRegion );
            DbUserInterfaceParam.getInstance().attach( this.observerGraphPanelRsi );
            DbUserInterfaceParam.getInstance().attach( this.observerGraphPanelMacd );

            this.sequentialWorker = new SequentialWorker( this.eventHandlerRegistry, this.incomingQueue );            

            if ( GlobalConstant.DEFAULT_SCALE == 0 )
                this.minuteToolStripMenuItem_Click( null, null );
            else if ( GlobalConstant.DEFAULT_SCALE == 1 )
                this.hourToolStripMenuItem_Click( null, null );
            else if ( GlobalConstant.DEFAULT_SCALE == 2 )
                this.dayToolStripMenuItem_Click( null, null );

            DbUserInterfaceParam.getInstance().update( GlobalConstant.INTERNAL_NAME_USER_INTERFACE_PARAM, new ModelUserInterfaceParam() );
            DbActiveTrade.getInstance().update( GlobalConstant.DB_NAME_ACTIVE_TRADE, new ModelActiveTrade() );

            this.textBoxEmaThresholdFastMedium.Text = GlobalConstant.STRATEGY_EMA_THRESHOLD_FAST_MEDIUM.ToString();
            this.textBoxEmaThresholdMediumSlow.Text = GlobalConstant.STRATEGY_EMA_THRESHOLD_MEDIUM_SLOW.ToString();
            this.textBoxTakeProfit.Text = GlobalConstant.STRATEGY_TAKE_PROFIT.ToString();
            this.textBoxStopLoss.Text = GlobalConstant.STRATEGY_STOP_LOSS.ToString();

            ManagerOanda.getInstance().init( this.incomingQueue, this );      
      
            if ( GlobalConstant.IS_PLAYBACK_ON )
            {
                this.formPlayback = new FormPlayback( this.incomingQueue );
                this.formPlayback.Show();                
            }
        }

        #region Toolbar notification
        private void toolStripButtonLoadDatabase_Click( object sender, EventArgs e )
        {
            DialogResult result = this.formDateSelection.ShowDialog();
            if ( result != DialogResult.OK )
                return;            

            DbCandleGraph.getInstance().clear();
            DbLineGraph.getInstance().clear();
            DbMarker.getInstance().clear();
            DbTransaction.getInstance().clear();

            DateTime startDt = this.formDateSelection.SelectedDateTimeStart;
            DateTime endDt = this.formDateSelection.SelectedDateTimeEnd;
            List<DbOanda.HistoryCandle> historyDayCandleList = DbOanda.getInstance().getHistoryDayCandleSince( startDt, endDt );
            List<DbOanda.HistoryCandle> historyHourCandleList = DbOanda.getInstance().getHistoryHourCandleSince( startDt, endDt );
            this.createAllHistoryCandleGraph( GlobalConstant.GRAPH_NAME_CANDLE_DAY, historyDayCandleList );
            this.createAllHistoryCandleGraph( GlobalConstant.GRAPH_NAME_CANDLE_HOUR, historyHourCandleList );
            this.createMainDistribution( GlobalConstant.GRAPH_NAME_BAR_DISTRIBUTION_DAY, historyDayCandleList );
            this.createMainDistribution( GlobalConstant.GRAPH_NAME_BAR_DISTRIBUTION_HOUR, historyHourCandleList );            

            this.updateAllGraphValues();
            this.updateAllGraphMinMaxXY();           
        }

        private void toolStripButtonSaveState_Click( object sender, EventArgs e )
        {
            DialogResult dialogResult = MessageBox.Show( "Proceed to overwrite database?", "Save Database", MessageBoxButtons.OKCancel );

            if ( dialogResult == DialogResult.OK )
            {
                DbCandleGraph.getInstance().serialize();
                DbEventDate.getInstance().serialize();
                DbMarker.getInstance().serialize();
                MessageBox.Show( "Successfully save state", "Status", MessageBoxButtons.OK );
            }
        }

        private void toolStripButtonLoadState_Click( object sender, EventArgs e )
        {
            this.timerGraph.Stop();
            DbCandleGraph.getInstance().clear();   

            DbCandleGraph.getInstance().deserialize();
            DbEventDate.getInstance().deserialize();
            DbMarker.getInstance().deserialize();
            DbRawTick.getInstance().deserialize();            

            this.updateAllGraphValues();
            this.updateAllGraphMinMaxXY();

            this.timerGraph.Start();            
        }

        private void toolStripButtonClearSimulationRun_Click( object sender, EventArgs e )
        {
            DbTransaction.getInstance().clear();
            this.listViewResult.Items.Clear();
        }

        private void toolStripButtonCalculator_Click( object sender, EventArgs e )
        {
            FormCalculator formCalculator = new FormCalculator();
            formCalculator.Show();
        }

        private void toolStripButtonZoomIn_Click( object sender, EventArgs e )
        {
            this.viewCandleGraph.BarWidth += 2;
            this.viewMacdGraph.BarWidth += 2;
            this.updateAllGraphMinMaxXY();
        }

        private void toolStripButtonZoomOut_Click( object sender, EventArgs e )
        {
            if ( this.viewCandleGraph.BarWidth > GlobalConstant.BAR_WIDTH )
            {
                this.viewCandleGraph.BarWidth -= 2;
                this.viewMacdGraph.BarWidth -= 2;
                this.updateAllGraphMinMaxXY();
            }
        }

        private void toolStripButtonInformation_Click( object sender, EventArgs e )
        {
            FormInformation formInformation = new FormInformation();
            formInformation.Show();
        }

        private void toolStripButtonDownloadHistoryCandles_Click( object sender, EventArgs e )
        {
            FormHistoryCandleDownloader formDownloader = new FormHistoryCandleDownloader();
            formDownloader.ShowDialog();
        }

        private void toolStripButtonStartLiveStreaming_Click( object sender, EventArgs e )
        {
            this.toolStripButtonStartLiveStreaming.Enabled = false;

            ManagerOanda.getInstance().sendAccountDetails();
            ManagerOanda.getInstance().startLiveStreaming();
            ManagerOanda.getInstance().sendOpenTradeList();
            this.toggleStrategyButton( true );
        }

        private void toolStripButtonCloseAllPosition_Click( object sender, EventArgs e )
        {
            ManagerOanda.getInstance().sendClosePosition();
        }
        #endregion           

        #region Manager Oanda callback    
        public void updateProgressFeedback( MessageProgressFeedback message )
        {
            if ( message.stopProgram )
            {
                ManagerOanda.getInstance().abortAPI();                
                MessageBox.Show( message.information, "Critical Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation );
            }
            else
                this.toolStripStatusLabel.Text = message.information;
        }

        public void updateLabelBidAsk( MessageStreamTick message )
        {
            this.labelBidAsk.Visible = true;
            string bidDisplay = AkUtil.GetShortPriceStringSeparatePippete( message.tick.bid );
            string askDisplay = AkUtil.GetShortPriceStringSeparatePippete( message.tick.ask );
            this.labelBidAsk.Text = bidDisplay + " | " + askDisplay;
        }

        public void updateAccountDetails( MessageStaticAccountDetails message )
        {
            this.listViewAccount.Items[0].SubItems[1].Text = string.Format( "{0:0.00}", message.balance );
            this.listViewAccount.Items[1].SubItems[1].Text = string.Format( "{0:0.00}", message.unrealizedPl );
            this.listViewAccount.Items[2].SubItems[1].Text = string.Format( "{0:0.00}", message.realizedPl );
            this.listViewAccount.Items[3].SubItems[1].Text = string.Format( "{0}", message.openTrades );
            this.listViewAccount.Items[4].SubItems[1].Text = string.Format( "{0}", message.openOrders );
            this.listViewAccount.Items[5].SubItems[1].Text = string.Format( "{0:0.00}", message.marginUsed );
            this.listViewAccount.Items[6].SubItems[1].Text = string.Format( "{0:0.00}", message.marginAvail );

            this.changeForegroundPnL( this.listViewAccount.Items[1].SubItems[1], message.unrealizedPl );
            this.changeForegroundPnL( this.listViewAccount.Items[2].SubItems[1], message.realizedPl );
        }

        public void updateOpenTrade( MessageStaticCreateNewOrder message )
        {
            this.updateActiveTrade(
                message.instrument,
                AkUtil.getTradeSideWithArrow( message.tradeOpened.side ),
                AkUtil.GetPriceStringSeparatePippete( message.price ),
                message.tradeOpened.units.ToString(),
                AkUtil.GetPriceStringSeparatePippete( message.tradeOpened.takeProfit ),
                AkUtil.GetPriceStringSeparatePippete( message.tradeOpened.stopLoss ) );
        }

        public void updateOpenTrade( MessageStaticModifyExistingTrade message )
        {
            this.updateActiveTrade(
                message.instrument,
                AkUtil.getTradeSideWithArrow( message.side ),
                AkUtil.GetPriceStringSeparatePippete( message.price ),
                message.units.ToString(),
                AkUtil.GetPriceStringSeparatePippete( message.takeProfit ),
                AkUtil.GetPriceStringSeparatePippete( message.stopLoss ) );
        }

        public void updateOpenTrade( DataTrade dataTrade )
        {
            this.updateActiveTrade(
                dataTrade.instrument,
                AkUtil.getTradeSideWithArrow( dataTrade.side ),
                AkUtil.GetPriceStringSeparatePippete( dataTrade.price ),
                dataTrade.units.ToString(),
                AkUtil.GetPriceStringSeparatePippete( dataTrade.takeProfit ),
                AkUtil.GetPriceStringSeparatePippete( dataTrade.stopLoss ) );
        }

        public void updateClosePosition()
        {                        
            this.updateActiveTrade(
                "---",
                "---",
                "---",
                "---",
                "---",
                "---" );

            // todo..to be completed
            if ( this.liveStrategy != null )
            {
                ModelTransaction model = this.liveStrategy.getCompletedTransaction();
                List<ModelTransaction.TransactionData> transactionList = model.getCopyListTransactionData();
            }
        }

        public void toggleLabelRateHeartbeatColor( bool isConnected )
        {
            if ( isConnected )
            {
                if ( this.toolStripStatusLabelRateHeartbeat.BackColor == Color.Red || this.toolStripStatusLabelRateHeartbeat.BackColor == Color.DarkGreen )
                {
                    this.toolStripStatusLabelRateHeartbeat.BackColor = Color.Green;
                }
                else
                    this.toolStripStatusLabelRateHeartbeat.BackColor = Color.DarkGreen;
            }
            else
                this.toolStripStatusLabelRateHeartbeat.BackColor = Color.Red;
        }
        
        public void toggleLabelEventHeartbeatColor( bool isConnected )
        {
            if ( isConnected )
            {
                if ( this.toolStripStatusLabelEventHeartbeat.BackColor == Color.Red || this.toolStripStatusLabelRateHeartbeat.BackColor == Color.DarkGreen )
                {
                    this.toolStripStatusLabelEventHeartbeat.BackColor = Color.Green;
                }
                else
                    this.toolStripStatusLabelEventHeartbeat.BackColor = Color.DarkGreen;
            }
            else
                this.toolStripStatusLabelEventHeartbeat.BackColor = Color.Red;
        }

        private void changeForegroundPnL( ListViewItem.ListViewSubItem lvt, double value )
        {
            if ( value < 0 )
                lvt.ForeColor = Color.Red;
            else
                lvt.ForeColor = Color.DarkGreen;
        }

        private void updateActiveTrade( string intrument, string side, string entryPrice, string units, string takeProfit, string stopLoss )
        {
            this.listViewOpenTrade.Items[0].SubItems[1].Text = intrument;
            this.listViewOpenTrade.Items[1].SubItems[1].Text = side;
            this.listViewOpenTrade.Items[2].SubItems[1].Text = entryPrice;
            this.listViewOpenTrade.Items[3].SubItems[1].Text = units;
            this.listViewOpenTrade.Items[5].SubItems[1].Text = takeProfit;
            this.listViewOpenTrade.Items[6].SubItems[1].Text = stopLoss;
        }
        #endregion

        #region Graph creation
        private void createAllHistoryCandleGraph( string graphName, List<DbOanda.HistoryCandle> historyCandleList )
        {
            ModelCandleGraph model = null;
            if ( !DbCandleGraph.getInstance().get( graphName, out model ) )
                model = new ModelCandleGraph();
            model.clear();

            foreach ( DbOanda.HistoryCandle hc in historyCandleList )
            {
                // oanda used to give history candles on Sunday before year 2012
                // ignore those candles because it is all flat and will affect EMA calculation
                if ( hc.highMid == hc.lowMid && hc.openMid == hc.closeMid )
                    continue;

                ModelCandleGraph.CandleData cd = new ModelCandleGraph.CandleData();

                cd.xValue = AkUtil.getDateTime( hc.timeSecSince1970 ).Ticks;

                cd.highMid = hc.highMid;
                cd.lowMid = hc.lowMid;
                cd.openMid = hc.openMid;
                cd.closeMid = hc.closeMid;

                model.add( cd );
            }
            DbCandleGraph.getInstance().update( graphName, model );
        }

        private void createMainDistribution( string graphName, List<DbOanda.HistoryCandle> historyCandleList )
        {
            ModelCandleGraph model = null;
            if ( !DbCandleGraph.getInstance().get( graphName, out model ) )
                model = new ModelCandleGraph();

            Dictionary<int, int> pipDiffMap = new Dictionary<int, int>();

            List<ModelCandleGraph.CandleData> existingListCopy = model.getCopyListCandleData();
            foreach ( ModelCandleGraph.CandleData bd in existingListCopy )
            {
                if ( pipDiffMap.ContainsKey( (int)bd.xValue ) )
                    pipDiffMap[(int)bd.xValue] = (int)bd.highMid;
                else
                    pipDiffMap.Add( (int)bd.xValue, (int)bd.highMid );
            }

            foreach ( DbOanda.HistoryCandle hc in historyCandleList )
            {
                int pipDiff = (int)( ( hc.highMid - hc.lowMid ) / GlobalConstant.ONE_PIP_PRICE );
                // ignore 0 readings
                if ( pipDiff == 0 )
                    continue;

                if ( pipDiffMap.ContainsKey( pipDiff ) )
                    pipDiffMap[pipDiff] += 1;
                else
                    pipDiffMap.Add( pipDiff, 1 );                
            }

            ModelCandleGraph updatedModel = new ModelCandleGraph();
            foreach ( KeyValuePair<int, int> kvp in pipDiffMap )
            {
                ModelCandleGraph.CandleData bd = new ModelCandleGraph.CandleData();
                bd.xValue = kvp.Key;
                bd.lowMid = 0;
                bd.highMid = kvp.Value;
                updatedModel.add( bd );
            }

            DbCandleGraph.getInstance().update( graphName, updatedModel );
        }

        private void updateGraphCandleMinMaxXY()
        {
            if ( DbCandleGraph.getInstance().count() == 0 )
                return;

            ModelCandleGraph model = null;
            List<ModelCandleGraph.CandleData> listCandleData = null;

            int totalBar = this.graphPanelCandle.Width / ( this.viewCandleGraph.BarWidth + GlobalConstant.BAR_GAP );

            if ( this.graphPanelCandle.ViewScale == GraphPanel.EnumViewScale.MINUTE )
            {
                DbCandleGraph.getInstance().get( GlobalConstant.GRAPH_NAME_CANDLE_MINUTE, out model );                
            }
            else if ( this.graphPanelCandle.ViewScale == GraphPanel.EnumViewScale.HOUR )
            {
                DbCandleGraph.getInstance().get( GlobalConstant.GRAPH_NAME_CANDLE_HOUR, out model );                
            }
            else if ( this.graphPanelCandle.ViewScale == GraphPanel.EnumViewScale.DAY )
            {
                DbCandleGraph.getInstance().get( GlobalConstant.GRAPH_NAME_CANDLE_DAY, out model );                
            }

            listCandleData = model.getRefListCandleData();
            this.graphPanelCandle.MaxX = model.count() + 20;
            this.graphPanelCandle.MinX = model.count() + 20 - totalBar;

            double minY = double.MaxValue;
            double maxY = double.MinValue;
            int candleIndex = -1;
            foreach ( ModelCandleGraph.CandleData candleData in listCandleData )
            {
                ++candleIndex;
                if ( candleIndex < this.graphPanelCandle.MinX || candleIndex > this.graphPanelCandle.MaxX )
                    continue;

                minY = candleData.lowMid < minY ? candleData.lowMid : minY;
                maxY = candleData.highMid > maxY ? candleData.highMid : maxY;
            }

            double paddingY = ( maxY - minY ) * 0.2;
            this.graphPanelCandle.MinY = minY - paddingY;
            this.graphPanelCandle.MaxY = maxY + paddingY;

            ModelUserInterfaceParam modelUserInterfaceParam = null;
            DbUserInterfaceParam.getInstance().get( GlobalConstant.INTERNAL_NAME_USER_INTERFACE_PARAM, out modelUserInterfaceParam );                

            modelUserInterfaceParam.MainCandleMinX = this.graphPanelCandle.MinX;
            modelUserInterfaceParam.MainCandleMaxX = this.graphPanelCandle.MaxX;
            DbUserInterfaceParam.getInstance().update( ModelUserInterfaceParam.DB_ACTION_UPDATE_MAIN_CANDLE_MIN_MAX_X, GlobalConstant.INTERNAL_NAME_USER_INTERFACE_PARAM, modelUserInterfaceParam );
        }

        private void updateGraphOverviewMinMaxXY()
        {
            if ( DbCandleGraph.getInstance().count() == 0 )
                return;

            ModelCandleGraph model = null;
            List<ModelCandleGraph.CandleData> listCandleData = null;
            if ( this.graphPanelOverview.ViewScale == GraphPanel.EnumViewScale.MINUTE )
            {
                DbCandleGraph.getInstance().get( GlobalConstant.GRAPH_NAME_CANDLE_MINUTE, out model );
            }
            else if ( this.graphPanelOverview.ViewScale == GraphPanel.EnumViewScale.HOUR )
            {
                DbCandleGraph.getInstance().get( GlobalConstant.GRAPH_NAME_CANDLE_HOUR, out model );
            }
            else if ( this.graphPanelOverview.ViewScale == GraphPanel.EnumViewScale.DAY )
            {
                DbCandleGraph.getInstance().get( GlobalConstant.GRAPH_NAME_CANDLE_DAY, out model );
            }
            listCandleData = model.getRefListCandleData();
            this.graphPanelOverview.MaxX = listCandleData.Count + 5;
            this.graphPanelOverview.MinX = -5;

            // use day to reduce processing since we only interested in max high and low
            //DbCandleGraph.getInstance().get( GlobalConstant.GRAPH_NAME_CANDLE_DAY, out model );
            DbCandleGraph.getInstance().get( GlobalConstant.GRAPH_NAME_CANDLE_HOUR, out model );
            listCandleData = model.getRefListCandleData();

            double minY = double.MaxValue;
            double maxY = double.MinValue;
            foreach ( ModelCandleGraph.CandleData candleData in listCandleData )
            {
                minY = candleData.lowMid < minY ? candleData.lowMid : minY;
                maxY = candleData.highMid > maxY ? candleData.highMid : maxY;
            }

            List<ModelMarker> markerList = DbMarker.getInstance().getCopyListValues();
            foreach ( ModelMarker marker in markerList )
            {
                if ( marker.IsHorizontal )
                {
                    minY = marker.Value < minY ? marker.Value : minY;
                    maxY = marker.Value > maxY ? marker.Value : maxY;
                }
            }

            double paddingY = ( maxY - minY ) * 0.2;
            this.graphPanelOverview.MinY = minY - paddingY;
            this.graphPanelOverview.MaxY = maxY + paddingY;
        }
        
        private void updateGraphDistributionMinMaxXY()
        {
            string graphName = this.viewHistogramGraph.CurrentGraphName;
            ModelCandleGraph model = null;
            if ( !DbCandleGraph.getInstance().get( graphName, out model ) )
                return;

            List<ModelCandleGraph.CandleData> listCandleData = model.getCopyListCandleData();
            
            double minX = double.MaxValue;
            double maxX = double.MinValue;
            double minY = 0;         
            double maxY = double.MinValue;

            foreach ( ModelCandleGraph.CandleData candleData in listCandleData )
            {
                minX = candleData.xValue < minX ? candleData.xValue : minX;
                maxX = candleData.xValue > maxX ? candleData.xValue : maxX;

                maxY = candleData.highMid > maxY ? candleData.highMid : maxY;
            }


            this.graphPanelDistribution.MinX = minX;
            this.graphPanelDistribution.MaxX = maxX;

            double paddingY = ( maxY - minY ) * 0.05;
            this.graphPanelDistribution.MinY = minY - paddingY;
            this.graphPanelDistribution.MaxY = maxY + paddingY;
        }  

        private void updateGraphRsiMinMaxXY()
        {
            this.graphPanelRsi.MinX = this.graphPanelCandle.MinX;
            this.graphPanelRsi.MaxX = this.graphPanelCandle.MaxX;
            this.graphPanelRsi.MinY = -10;
            this.graphPanelRsi.MaxY = 110;
        }

        private void updateGraphMacdMinMaxXY()
        {
            ModelLineGraph model = null;
            if ( !DbLineGraph.getInstance().get( GlobalConstant.GRAPH_NAME_LINE_MACD, out model ) )
                return;

            List<ModelLineGraph.LineData> listLineData = model.getCopyListLineData();

            double minY = double.MaxValue;
            double maxY = double.MinValue;
            int candleIndex = -1;
            foreach ( ModelLineGraph.LineData ld in listLineData )
            {
                if ( ld.yValue == AlgoMacd.UNDEFINE_VALUE )
                    continue;

                ++candleIndex;
                if ( candleIndex < graphPanelCandle.MinX || candleIndex > graphPanelCandle.MaxX )
                    continue;

                minY = ld.yValue < minY ? ld.yValue : minY;
                maxY = ld.yValue > maxY ? ld.yValue : maxY;
            }

            this.graphPanelMacd.MinX = this.graphPanelCandle.MinX;
            this.graphPanelMacd.MaxX = this.graphPanelCandle.MaxX;
            this.graphPanelMacd.MinY = minY;
            this.graphPanelMacd.MaxY = maxY;
        }

        private void updateAllGraphMinMaxXY()
        {
            this.updateGraphCandleMinMaxXY();
            this.updateGraphOverviewMinMaxXY();
            this.updateGraphDistributionMinMaxXY();
            this.updateGraphRsiMinMaxXY();
            this.updateGraphMacdMinMaxXY();
        }

        public void updateAllGraphValues()
        {
            ModelCandleGraph modelCandleGraph = null;
            if ( !DbCandleGraph.getInstance().get( this.viewCandleGraph.CurrentGraphName, out modelCandleGraph ) )
                return;

            List<ModelCandleGraph.CandleData> candleDataList = modelCandleGraph.getCopyListCandleData();
            this.updateRsiGraph( candleDataList );
            this.updateAllEmaGraph( candleDataList );
            this.updateMacdGraph( candleDataList );
        }

        private void timerGraph_Tick( object sender, EventArgs e )
        {            
            this.graphPanelCandle.Refresh();
            this.graphPanelOverview.Refresh();
            this.graphPanelDistribution.Refresh();
            this.graphPanelRsi.Refresh();
            this.graphPanelMacd.Refresh();
        }
        #endregion

        #region GraphPanelCandle notification
        private void graphPanelCandle_Paint( object sender, PaintEventArgs e )
        {
            foreach ( BaseView graphView in this.graphCandleViewList )
                graphView.draw( this.graphPanelCandle, e.Graphics );
        }

        private void graphPanelCandle_Resize( object sender, EventArgs e )
        {
            this.updateGraphCandleMinMaxXY();            
        }

        private void graphPanelCandle_PreviewKeyDown( object sender, PreviewKeyDownEventArgs e )
        {
            DateTime currentDateTime = this.viewInfoCandle.getCurrentDateTime();
            double currentPrice = this.viewInfoCandle.getCurrentPrice();

            if ( e.KeyCode == Keys.F1 ) // horizontal marker
            {
                ModelMarker modelMarker = new ModelMarker( currentPrice, "", true, true );
                DbMarker.getInstance().update( currentPrice.ToString(), modelMarker );
            }
            else if ( e.KeyCode == Keys.F2 ) // horizontal marker
            {
                ModelMarker modelMarker = new ModelMarker( currentPrice, "", false, true );
                DbMarker.getInstance().update( currentPrice.ToString(), modelMarker );
            }
            else if ( e.KeyCode == Keys.F3 ) // vertial marker
            {
                ModelMarker modelMarker = new ModelMarker( currentDateTime.Ticks, "", false, false );
                double castedKey = (double)currentDateTime.Ticks; // because key is being shared by price (double) and ticks (long)
                DbMarker.getInstance().update( castedKey.ToString(), modelMarker );
            }

            this.updateGraphOverviewMinMaxXY();            
        }        

        private void toolStripMenuItemMarkerDelete_Click( object sender, EventArgs e )
        {
            double tagValue = (double)this.toolStripMenuItemMarkerDelete.Tag;
            ModelMarker model = null;
            DbMarker.getInstance().delete( tagValue.ToString(), out model );
            this.updateGraphOverviewMinMaxXY();
        }

        private void toolStripMenuItemMarkerRemarks_Click( object sender, EventArgs e )
        {
            double markerPrice = (double)this.toolStripMenuItemMarkerRemarks.Tag;
            ModelMarker model = null;
            if ( DbMarker.getInstance().get( markerPrice.ToString(), out model ) )
            {
                using ( FormRemarks formRemarks = new FormRemarks() )
                {                    
                    if ( formRemarks.ShowDialog() == DialogResult.OK )
                    {
                        model.Remarks = formRemarks.Remarks;
                    }
                }
            }
        }    

        private void graphPanelCandle_MouseWheel( object sender, MouseEventArgs e )
        {
            ModelUserInterfaceParam modelUserInterfaceParam = null;
            DbUserInterfaceParam.getInstance().get( GlobalConstant.INTERNAL_NAME_USER_INTERFACE_PARAM, out modelUserInterfaceParam );

            modelUserInterfaceParam.MainCandleMinX = this.graphPanelCandle.MinX;
            modelUserInterfaceParam.MainCandleMaxX = this.graphPanelCandle.MaxX;
            DbUserInterfaceParam.getInstance().update( ModelUserInterfaceParam.DB_ACTION_UPDATE_MAIN_CANDLE_MIN_MAX_X, GlobalConstant.INTERNAL_NAME_USER_INTERFACE_PARAM, modelUserInterfaceParam );
        }

        private void graphPanelCandle_MouseDown( object sender, MouseEventArgs e )
        {
            if ( e.Button == MouseButtons.Right )
            {
                bool foundMarker = false;
                List<ModelMarker> allMarker = DbMarker.getInstance().getCopyListValues();
                for ( int i = 0; i < allMarker.Count; ++i )
                {
                    ModelMarker model = allMarker[i];
                    if ( model.IsHorizontal )
                    {
                        Point p = this.graphPanelCandle.logicalToPixel( 0, model.Value );
                        if ( ( e.Location.Y - 2 ) <= p.Y && p.Y <= ( e.Location.Y + 2 ) )
                        {
                            this.toolStripMenuItemMarkerDelete.Tag = model.Value;
                            this.toolStripMenuItemMarkerRemarks.Tag = model.Value;
                            this.contextMenuStripMarker.Show( Cursor.Position );
                            foundMarker = true;
                        }
                    }
                    else
                    {
                        int candleIndex = DbCandleGraph.getInstance().getCandleIndexNearestMatch( this.viewCandleGraph.CurrentGraphName, (long)model.Value );
                        if ( candleIndex < 0 )
                            continue;

                        Point p = this.graphPanelCandle.logicalToPixel( candleIndex, 0 );
                        if ( ( e.Location.X - 2 ) <= p.X && p.X <= ( e.Location.X + 2 ) )
                        {
                            this.toolStripMenuItemMarkerDelete.Tag = model.Value;
                            this.toolStripMenuItemMarkerRemarks.Tag = model.Value;
                            this.contextMenuStripMarker.Show( Cursor.Position );
                            foundMarker = true;
                        }
                    }
                }

                if ( !foundMarker )
                {
                    this.contextMenuStripGraphType.Show( Cursor.Position );
                }
            }
            else if ( e.Button == MouseButtons.Middle )
            {
                this.viewInfoCandle.StartMiddleButtonMousePoint = e.Location;
            }
        }

        private void graphPanelCandle_MouseUp( object sender, MouseEventArgs e )
        {
            this.viewInfoCandle.resetMiddleButtonMousePoint();
        }

        private void graphPanelCandle_MouseMove( object sender, MouseEventArgs e )
        {
            this.viewInfoCandle.CurrentMousePoint = e.Location;
            this.viewRsiGraph.CurrentMousePoint = e.Location;
            this.viewMacdGraph.CurrentMousePoint = e.Location;

            ModelUserInterfaceParam modelUserInterfaceParam = null;
            DbUserInterfaceParam.getInstance().get( GlobalConstant.INTERNAL_NAME_USER_INTERFACE_PARAM, out modelUserInterfaceParam );

            modelUserInterfaceParam.MainCandleMinX = this.graphPanelCandle.MinX;
            modelUserInterfaceParam.MainCandleMaxX = this.graphPanelCandle.MaxX;
            DbUserInterfaceParam.getInstance().update( ModelUserInterfaceParam.DB_ACTION_UPDATE_MAIN_CANDLE_MIN_MAX_X, GlobalConstant.INTERNAL_NAME_USER_INTERFACE_PARAM, modelUserInterfaceParam );
        }
        #endregion

        #region GraphPanelOverview notification
        private void graphPanelOverview_Paint( object sender, PaintEventArgs e )
        {
            foreach ( BaseView graphView in this.graphOverviewViewList )
                graphView.draw( this.graphPanelOverview, e.Graphics );
        }

        private void graphPanelOverview_MouseClick( object sender, MouseEventArgs e )
        {
            if ( e.Button == MouseButtons.Left )
            {
                double logicalWindowWidth = this.viewOverviewRegion.RightX - this.viewOverviewRegion.LeftX;
                PointD newLogicalMidPoint = this.graphPanelOverview.pixelToLogical( e.Location.X, e.Location.Y );
                this.viewOverviewRegion.LeftX = newLogicalMidPoint.X - ( logicalWindowWidth / 2 );
                this.viewOverviewRegion.RightX = newLogicalMidPoint.X + ( logicalWindowWidth / 2 );

                ModelUserInterfaceParam modelUserInterfaceParam = null;
                DbUserInterfaceParam.getInstance().get( GlobalConstant.INTERNAL_NAME_USER_INTERFACE_PARAM, out modelUserInterfaceParam );                    

                modelUserInterfaceParam.OverviewCandleMinX = this.viewOverviewRegion.LeftX;
                modelUserInterfaceParam.OverviewCandleMaxX = this.viewOverviewRegion.RightX;                
                DbUserInterfaceParam.getInstance().update( ModelUserInterfaceParam.DB_ACTION_UPDATE_OVERVIEW_CANDLE_VISUAL_X, GlobalConstant.INTERNAL_NAME_USER_INTERFACE_PARAM, modelUserInterfaceParam );
            }
        }  
        #endregion

        #region GraphPanelDistribution notification
        private void graphPanelDistribution_Paint( object sender, PaintEventArgs e )
        {
            foreach ( BaseView graphView in this.graphDistributionViewList )
                graphView.draw( this.graphPanelDistribution, e.Graphics );
        }

        private void graphPanelDistribution_MouseMove( object sender, MouseEventArgs e )
        {
            this.viewInfoHistogram.CurrentMousePoint = e.Location;
        }
        #endregion

        #region GraphPanelRsi notification
        private void graphPanelRsi_Paint( object sender, PaintEventArgs e )
        {
            foreach ( BaseView graphView in this.graphRsiViewList )
                graphView.draw( this.graphPanelRsi, e.Graphics );
        }
        #endregion

        #region GraphPanelMacd notification
        private void graphPanelMacd_Paint( object sender, PaintEventArgs e )
        {
            foreach ( BaseView graphView in this.graphMacdViewList )
                graphView.draw( this.graphPanelMacd, e.Graphics );
        }   
        #endregion

        #region RSI EMA MACD functions
        private void updateRsiGraph( List<ModelCandleGraph.CandleData> candleDataList )
        {
            ModelLineGraph modelLineGraph = null;
            string rsiGraphName = GlobalConstant.GRAPH_NAME_LINE_RSI;
            if ( !DbLineGraph.getInstance().get( rsiGraphName, out modelLineGraph ) )
            {
                modelLineGraph = new ModelLineGraph( ModelLineGraph.EnumLineGraphType.RSI );
            }
            modelLineGraph.clear();

            AlgoRsi algoRsi = new AlgoRsi( GlobalConstant.DEFAULT_RSI_PERIOD );

            foreach ( ModelCandleGraph.CandleData cd in candleDataList )
            {
                double dataPoint = cd.closeMid;
                double weightedPoint = 0;
                algoRsi.addDataPoint( dataPoint, out weightedPoint );

                ModelLineGraph.LineData ld = new ModelLineGraph.LineData();
                ld.xValue = cd.xValue;
                ld.yValue = weightedPoint;                
                modelLineGraph.add( ld );
            }
            DbLineGraph.getInstance().update( rsiGraphName, modelLineGraph );
        }

        private void updateAllEmaGraph( List<ModelCandleGraph.CandleData> candleDataList )
        {
            this.updateEmaGraph( candleDataList, GlobalConstant.GRAPH_NAME_LINE_EMA_T1, GlobalConstant.DEFAULT_EMA_T1 );
            this.updateEmaGraph( candleDataList, GlobalConstant.GRAPH_NAME_LINE_EMA_T2, GlobalConstant.DEFAULT_EMA_T2 );
            this.updateEmaGraph( candleDataList, GlobalConstant.GRAPH_NAME_LINE_EMA_T3, GlobalConstant.DEFAULT_EMA_T3 );
        }

        private void updateEmaGraph( List<ModelCandleGraph.CandleData> candleDataList, string emaGraphName, int period )
        {
            ModelLineGraph modelLineGraph = null;
            if ( !DbLineGraph.getInstance().get( emaGraphName, out modelLineGraph ) )
            {
                modelLineGraph = new ModelLineGraph( ModelLineGraph.EnumLineGraphType.EMA );
            }
            modelLineGraph.clear();

            AlgoEma algoEma = new AlgoEma( period );

            foreach ( ModelCandleGraph.CandleData cd in candleDataList )
            {
                double dataPoint = cd.closeMid;
                double weightedPoint = 0;
                algoEma.addDataPoint( dataPoint, out weightedPoint );

                ModelLineGraph.LineData ld = new ModelLineGraph.LineData();
                ld.xValue = cd.xValue;
                ld.yValue = weightedPoint;
                modelLineGraph.add( ld );
            }
            DbLineGraph.getInstance().update( emaGraphName, modelLineGraph );
        }

        private void updateMacdGraph( List<ModelCandleGraph.CandleData> candleDataList )
        {
            ModelLineGraph modelLineMacd = null;
            string macdGraphName = GlobalConstant.GRAPH_NAME_LINE_MACD;
            if ( !DbLineGraph.getInstance().get( macdGraphName, out modelLineMacd ) )
            {
                modelLineMacd = new ModelLineGraph( ModelLineGraph.EnumLineGraphType.MACD );
            }
            modelLineMacd.clear();

            ModelLineGraph modelLineMacdSignal = null;
            string macdSignalGraphName = GlobalConstant.GRAPH_NAME_LINE_MACD_SIGNAL;
            if ( !DbLineGraph.getInstance().get( macdSignalGraphName, out modelLineMacdSignal ) )
            {
                modelLineMacdSignal = new ModelLineGraph( ModelLineGraph.EnumLineGraphType.MACD );
            }
            modelLineMacdSignal.clear();

            AlgoMacd algoMacd = new AlgoMacd( GlobalConstant.DEFAULT_MACD_FAST, GlobalConstant.DEFAULT_MACD_SLOW, GlobalConstant.DEFAULT_MACD_SIGNAL );

            foreach ( ModelCandleGraph.CandleData cd in candleDataList )
            {
                double dataPoint = cd.closeMid;
                double macdPoint = 0;
                double signalPoint = 0;
                algoMacd.addDataPoint( dataPoint, out macdPoint, out signalPoint );

                ModelLineGraph.LineData macdLd = new ModelLineGraph.LineData();
                macdLd.xValue = cd.xValue;
                macdLd.yValue = macdPoint;
                modelLineMacd.add( macdLd );

                ModelLineGraph.LineData signalLd = new ModelLineGraph.LineData();
                signalLd.xValue = cd.xValue;
                signalLd.yValue = signalPoint;
                modelLineMacdSignal.add( signalLd );
            }
            DbLineGraph.getInstance().update( macdGraphName, modelLineMacd );
            DbLineGraph.getInstance().update( macdSignalGraphName, modelLineMacdSignal );
        }
        #endregion

        private void FormMain_FormClosed( object sender, FormClosedEventArgs e )
        {
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }

        private void FormMain_Load( object sender, EventArgs e )
        {
            if ( Settings.Default.WindowState == (int)FormWindowState.Normal )
            {
                if ( Settings.Default.WindowSize != null )
                {
                    this.Size = Settings.Default.WindowSize;
                }
            }
            else if ( Settings.Default.WindowState == (int)FormWindowState.Maximized )
            {
                this.WindowState = FormWindowState.Maximized;
            }

            this.splitContainer1.SplitterDistance = Settings.Default.SplitContainer1SplitterDistance;
            this.splitContainer2.SplitterDistance = Settings.Default.SplitContainer2SplitterDistance;
            this.splitContainer3.SplitterDistance = Settings.Default.SplitContainer3SplitterDistance;
            this.splitContainer4.SplitterDistance = Settings.Default.SplitContainer4SplitterDistance;
            this.splitContainer5.SplitterDistance = Settings.Default.SplitContainer5SplitterDistance;
            this.splitContainer6.SplitterDistance = Settings.Default.SplitContainer6SplitterDistance;
        }

        private void FormMain_FormClosing( object sender, FormClosingEventArgs e )
        {
            Settings.Default.WindowState = (int)this.WindowState;

            if ( this.WindowState == FormWindowState.Normal )
            {
                Settings.Default.WindowSize = this.Size;
            }
            else
            {
                Settings.Default.WindowSize = this.RestoreBounds.Size;
            }

            Settings.Default.SplitContainer1SplitterDistance = this.splitContainer1.SplitterDistance;
            Settings.Default.SplitContainer2SplitterDistance = this.splitContainer2.SplitterDistance;
            Settings.Default.SplitContainer3SplitterDistance = this.splitContainer3.SplitterDistance;
            Settings.Default.SplitContainer4SplitterDistance = this.splitContainer4.SplitterDistance;
            Settings.Default.SplitContainer5SplitterDistance = this.splitContainer5.SplitterDistance;
            Settings.Default.SplitContainer6SplitterDistance = this.splitContainer6.SplitterDistance;

            Settings.Default.Save();
        }     

        private void candleScaleChange( GraphPanel.EnumViewScale enumViewScale )
        {
            this.graphPanelCandle.ViewScale = enumViewScale;
            this.graphPanelOverview.ViewScale = enumViewScale;

            int scaleIndex = (int)enumViewScale;

            foreach ( BaseView view in this.graphCandleViewList )
                view.CurrentGraphName = this.candleGraphNameList[scaleIndex];

            foreach ( BaseView view in this.graphOverviewViewList )
                view.CurrentGraphName = this.candleGraphNameList[scaleIndex];

            foreach ( BaseView view in this.graphDistributionViewList )
                view.CurrentGraphName = this.distributionGraphNameList[scaleIndex];

            this.updateAllGraphValues();
            this.updateAllGraphMinMaxXY();
        }
        
        private void minuteToolStripMenuItem_Click( object sender, EventArgs e )
        {
            minuteToolStripMenuItem.Checked = true;
            hourToolStripMenuItem.Checked = false;
            dayToolStripMenuItem.Checked = false;
            this.candleScaleChange( GraphPanel.EnumViewScale.MINUTE );
        }

        private void hourToolStripMenuItem_Click( object sender, EventArgs e )
        {
            minuteToolStripMenuItem.Checked = false;
            hourToolStripMenuItem.Checked = true;
            dayToolStripMenuItem.Checked = false;
            this.candleScaleChange( GraphPanel.EnumViewScale.HOUR );
        }

        private void dayToolStripMenuItem_Click( object sender, EventArgs e )
        {
            minuteToolStripMenuItem.Checked = false;
            hourToolStripMenuItem.Checked = false;
            dayToolStripMenuItem.Checked = true;
            this.candleScaleChange( GraphPanel.EnumViewScale.DAY );
        }

        private void candleToolStripMenuItem_CheckedChanged( object sender, EventArgs e )
        {
            this.viewCandleGraph.Visible = candleToolStripMenuItem.Checked;
        }

        private void eMAToolStripMenuItem_CheckedChanged( object sender, EventArgs e )
        {
            this.viewEmaGraph.Visible = eMAToolStripMenuItem.Checked;
        }

        private void updateSimulationResult( ModelTransaction model )
        {
            List<ModelTransaction.TransactionData> transactionList = model.getCopyListTransactionData();

            int totalUnresolveTrade = 0;
            int totalWinTrade = 0;
            int totalLoseTrade = 0;
            double totalWinPip = 0;
            double totalLosePip = 0;

            foreach ( ModelTransaction.TransactionData td in transactionList )
            {
                if ( td.transactOutcome == ModelTransaction.EnumTransactOutcome.PROFIT )
                {
                    ++totalWinTrade;
                    totalWinPip += ( Math.Abs( td.exitPrice - td.entryPrice ) / GlobalConstant.ONE_PIP_PRICE );
                }
                else if ( td.transactOutcome == ModelTransaction.EnumTransactOutcome.LOSS )
                {
                    ++totalLoseTrade;
                    totalLosePip += ( Math.Abs( td.exitPrice - td.entryPrice ) / GlobalConstant.ONE_PIP_PRICE );
                }
                else
                {
                    ++totalUnresolveTrade;
                }
            }
            
            double nettPip = totalWinPip - totalLosePip;
            ListViewItem lvt = new ListViewItem( ( this.listViewResult.Items.Count + 1 ).ToString() );
            lvt.UseItemStyleForSubItems = false;
            lvt.SubItems.Add( transactionList.Count + " (" + totalUnresolveTrade + ")" );            
            lvt.SubItems.Add( totalWinPip.ToString( "0.0" ) );
            lvt.SubItems.Add( totalLosePip.ToString( "0.0" ) );
            lvt.SubItems.Add( nettPip.ToString( "0.0" ) );
            if ( nettPip >= 0 )
                lvt.SubItems[4].BackColor = Color.PaleGreen;
            else
                lvt.SubItems[4].BackColor = Color.LightPink;
            lvt.SubItems.Add( model.Remarks );

            this.listViewResult.Items.Add( lvt );
            this.listViewResult.EnsureVisible( this.listViewResult.Items.Count - 1 );
        }

        private void buttonTestEmaStrategy_Click( object sender, EventArgs e )
        {
            double pipTakeProfit = double.Parse( this.textBoxTakeProfit.Text );
            double pipCutLoss = double.Parse( this.textBoxStopLoss.Text );
            double emaThresholdFastMedium = double.Parse( this.textBoxEmaThresholdFastMedium.Text );
            double emaThresholdMediumSlow = double.Parse( this.textBoxEmaThresholdMediumSlow.Text );

            StrategyEma st = new StrategyEma( false, this.incomingQueue, pipTakeProfit, pipCutLoss, emaThresholdFastMedium, emaThresholdMediumSlow );            
            ModelCandleGraph modelCandleGraph = null;
            if ( this.graphPanelCandle.ViewScale == GraphPanel.EnumViewScale.DAY )
                DbCandleGraph.getInstance().get( GlobalConstant.GRAPH_NAME_CANDLE_DAY, out modelCandleGraph );
            else
                DbCandleGraph.getInstance().get( GlobalConstant.GRAPH_NAME_CANDLE_HOUR, out modelCandleGraph );

            List<ModelCandleGraph.CandleData> copyList = modelCandleGraph.getCopyListCandleData();
            foreach ( ModelCandleGraph.CandleData cd in copyList )
            {
                st.incomingCandle( cd );
            }
            string transactionName = DbTransaction.getInstance().count() + "^" + "TP:" + this.textBoxTakeProfit.Text + " SL:" + this.textBoxStopLoss.Text + " " + this.textBoxEmaThresholdFastMedium.Text + " " + this.textBoxEmaThresholdMediumSlow.Text;
            ModelTransaction modelTransaction = st.getCompletedTransaction();
            DbTransaction.getInstance().update( transactionName, modelTransaction );
            this.updateSimulationResult( modelTransaction );            
        }

        private void buttonTestMacdStrategy_Click( object sender, EventArgs e )
        {
            double pipTakeProfit = 40;
            double pipCutLoss = 20;
            double macdThreshold = 0.0005;

            StrategyMacd st = new StrategyMacd( false, this.incomingQueue, pipTakeProfit, pipCutLoss, macdThreshold );
            ModelCandleGraph modelCandleGraph = null;
            if ( this.graphPanelCandle.ViewScale == GraphPanel.EnumViewScale.DAY )
                DbCandleGraph.getInstance().get( GlobalConstant.GRAPH_NAME_CANDLE_DAY, out modelCandleGraph );
            else
                DbCandleGraph.getInstance().get( GlobalConstant.GRAPH_NAME_CANDLE_HOUR, out modelCandleGraph );
            
            List<ModelCandleGraph.CandleData> copyList = modelCandleGraph.getCopyListCandleData();
            foreach ( ModelCandleGraph.CandleData cd in copyList )
            {
                st.incomingCandle( cd );
            }
            string transactionName = DbTransaction.getInstance().count() + "^" + "TP:" + this.textBoxTakeProfit.Text + " SL:" + this.textBoxStopLoss.Text + " " + this.textBoxEmaThresholdFastMedium.Text + " " + this.textBoxEmaThresholdMediumSlow.Text;
            ModelTransaction modelTransaction = st.getCompletedTransaction();
            DbTransaction.getInstance().update( transactionName, modelTransaction );
            this.updateSimulationResult( modelTransaction );   
        }

        private void buttonTestEmaMacdStrategy_Click( object sender, EventArgs e )
        {
            double pipTakeProfit = 40;
            double pipCutLoss = 20;
            double emaThresholdFastMedium = 0.0005;
            double emaThresholdMediumSlow = 0.0005;
            double macdThreshold = 0.0005;

            StrategyEmaMacd st = new StrategyEmaMacd( false, this.incomingQueue, pipTakeProfit, pipCutLoss, emaThresholdFastMedium, emaThresholdMediumSlow, macdThreshold );
            ModelCandleGraph modelCandleGraph = null;
            if ( this.graphPanelCandle.ViewScale == GraphPanel.EnumViewScale.DAY )
                DbCandleGraph.getInstance().get( GlobalConstant.GRAPH_NAME_CANDLE_DAY, out modelCandleGraph );
            else
                DbCandleGraph.getInstance().get( GlobalConstant.GRAPH_NAME_CANDLE_HOUR, out modelCandleGraph );
            
            List<ModelCandleGraph.CandleData> copyList = modelCandleGraph.getCopyListCandleData();
            foreach ( ModelCandleGraph.CandleData cd in copyList )
            {
                st.incomingCandle( cd );
            }
            string transactionName = DbTransaction.getInstance().count() + "^" + "TP:" + this.textBoxTakeProfit.Text + " SL:" + this.textBoxStopLoss.Text + " " + this.textBoxEmaThresholdFastMedium.Text + " " + this.textBoxEmaThresholdMediumSlow.Text;
            ModelTransaction modelSimulatedTransaction = st.getCompletedTransaction();
            DbTransaction.getInstance().update( transactionName, modelSimulatedTransaction );
            this.updateSimulationResult( modelSimulatedTransaction );   
        }

        private void buttonLiveEmaStrategy_Click( object sender, EventArgs e )
        {
            this.buttonLiveEmaStrategy.BackColor = Color.LightGray;
            this.buttonLiveEmaStrategy.Enabled = false;

            double pipTakeProfit = double.Parse( this.textBoxTakeProfit.Text );
            double pipStopLoss = double.Parse( this.textBoxStopLoss.Text );
            double emaThresholdFastMedium = double.Parse( this.textBoxEmaThresholdFastMedium.Text );
            double emaThresholdMediumSlow = double.Parse( this.textBoxEmaThresholdMediumSlow.Text );

            this.liveStrategy = new StrategyEma( true, this.incomingQueue, pipTakeProfit, pipStopLoss, emaThresholdFastMedium, emaThresholdMediumSlow );
            ManagerOanda.getInstance().setLiveStrategy( this.liveStrategy );
        }

        private void toolStripButton1_Click( object sender, EventArgs e )
        {
            ManagerOanda.getInstance().sendCreateNewOrder( 1, "buy", 0 );
        }

        private void toggleStrategyButton( bool isLiveStatus )
        {
            if ( isLiveStatus )
            {
                this.buttonTestEmaStrategy.BackColor = Color.LightGray;
                this.buttonTestEmaStrategy.Enabled = false;
                this.buttonLiveEmaStrategy.BackColor = Color.LightGreen;
                this.buttonLiveEmaStrategy.Enabled = true;
            }
            else
            {
                this.buttonTestEmaStrategy.BackColor = Color.LightGreen;
                this.buttonTestEmaStrategy.Enabled = true;
                this.buttonLiveEmaStrategy.BackColor = Color.LightGray;
                this.buttonLiveEmaStrategy.Enabled = false;
            }
        }
    }
}
