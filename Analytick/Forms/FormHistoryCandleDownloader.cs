﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Script.Serialization;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;

using AkDatabase;
using AkCommon;
using AkMessages;

namespace Analytick.Forms
{
    public partial class FormHistoryCandleDownloader : Form
    {
        private enum EnumDownloadingCandleType
        {
            MINUTE = 0,
            HOUR = 1,
            DAY = 2
        }

        private BackgroundWorker bwStaticRequest = new BackgroundWorker();
        private EnumDownloadingCandleType currentDownloadingCandleType = EnumDownloadingCandleType.DAY;

        public FormHistoryCandleDownloader()
        {
            InitializeComponent();

            this.updateCandleMaxValues();
            this.currentDownloadingCandleType = EnumDownloadingCandleType.DAY;
            this.bwStaticRequest.WorkerReportsProgress = true;
            this.bwStaticRequest.WorkerSupportsCancellation = true;
            this.bwStaticRequest.DoWork += new DoWorkEventHandler( bwStaticRequest_DoWork );
            this.bwStaticRequest.ProgressChanged += new ProgressChangedEventHandler( bwStaticRequest_ProgressChanged );
            this.bwStaticRequest.RunWorkerCompleted += new RunWorkerCompletedEventHandler( bwStaticRequest_RunWorkerCompleted );
        }

        
        private string getRequestString()
        {
            int secondsSince1970 = -1;
            if ( this.currentDownloadingCandleType == EnumDownloadingCandleType.DAY )
                secondsSince1970 = DbOanda.getInstance().getLatestDayCandleTime();
            else if ( this.currentDownloadingCandleType == EnumDownloadingCandleType.HOUR )
                secondsSince1970 = DbOanda.getInstance().getLatestHourCandleTime();
            else if ( this.currentDownloadingCandleType == EnumDownloadingCandleType.MINUTE )
                secondsSince1970 = DbOanda.getInstance().getLatestMinuteCandleTime();

            DateTime startTime = DateTime.Now;
            if ( secondsSince1970 == -1 )
            {
                startTime = new DateTime( 2005, 1, 1, 0, 0, 0 );
            }
            else
            {
                startTime = AkUtil.getDateTime( secondsSince1970 );

                if ( this.currentDownloadingCandleType == EnumDownloadingCandleType.DAY )
                {
                    startTime = startTime.AddHours( -8 );
                    startTime = startTime.AddDays( 1 );
                }
                else if ( this.currentDownloadingCandleType == EnumDownloadingCandleType.HOUR )
                {
                    startTime = startTime.AddHours( -8 );
                    startTime = startTime.AddHours( 1 );
                }
                else if ( this.currentDownloadingCandleType == EnumDownloadingCandleType.MINUTE )
                {
                    startTime = startTime.AddHours( -8 );
                    startTime = startTime.AddMinutes( 1 );
                }
            }

            string granularity = "";

            if ( this.currentDownloadingCandleType == EnumDownloadingCandleType.DAY )
                granularity = "D";
            else if ( this.currentDownloadingCandleType == EnumDownloadingCandleType.HOUR )
                granularity = "H1";
            else if ( this.currentDownloadingCandleType == EnumDownloadingCandleType.MINUTE )
                granularity = "M1";
            
            string requestString = string.Format( "{0}v1/candles?instrument={1}&count=500&start={2}-{3}-{4}T{5}%3A{6}%3A{7}Z&candleFormat=midpoint&granularity={8}&dailyAlignment=0&alignmentTimezone=Singapore",
                GlobalConstant.API_SERVER,
                GlobalConstant.INSTRUMENT,
                startTime.ToString( "yyyy" ),
                startTime.ToString( "MM" ),
                startTime.ToString( "dd" ),
                startTime.ToString( "HH" ),
                startTime.ToString( "mm" ),
                startTime.ToString( "ss" ),
                granularity );

            return requestString;
        }        

        void bwStaticRequest_RunWorkerCompleted( object sender, RunWorkerCompletedEventArgs e )
        {
            this.buttonStart.Enabled = true;
        }

        void bwStaticRequest_DoWork( object sender, DoWorkEventArgs e )
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            bool continueDownloading = true;

            while ( continueDownloading )
            {
                if ( ( worker.CancellationPending == true ) )
                {
                    e.Cancel = true;
                    continueDownloading = false;
                    return;
                }

                string requestString = this.getRequestString();
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create( requestString );
                request.KeepAlive = true;

                // for non-sandbox requests
                request.Headers.Add( "Authorization", "Bearer " + GlobalConstant.ACCESS_TOKEN );
                request.Method = "GET";

                try
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    using ( var response = request.GetResponse() )
                    {
                        using ( var reader = new StreamReader( response.GetResponseStream() ) )
                        {
                            string responseString = reader.ReadToEnd().Trim();
                            MessageStaticCandleList messageStaticCandleList = serializer.Deserialize<MessageStaticCandleList>( responseString );
                            //ResponseCandles responseCandles = serializer.Deserialize<ResponseCandles>( responseString );
                            if ( responseString == "" )
                            {
                                if ( !this.changeState() )
                                {
                                    worker.ReportProgress( 0, "All candles updated" );
                                    return;
                                }
                                else
                                    continue;
                            }
                            List<DataCandle> candleList = new List<DataCandle>();
                            candleList.AddRange( messageStaticCandleList.candles );

                            foreach ( DataCandle dc in candleList )
                            {
                                // stop downloading once encounter false completion candle
                                if ( dc.complete == false )
                                {
                                    if ( !this.changeState() )
                                    {
                                        worker.ReportProgress( 0, "All candles updated" );
                                        continueDownloading = false;
                                        return;
                                    }
                                    else
                                        continue;
                                }

                                if ( this.currentDownloadingCandleType == EnumDownloadingCandleType.DAY )
                                    DbOanda.getInstance().insertDayCandle( (int)AkUtil.getSecondsSince1970( dc.time ), dc.openMid, dc.highMid, dc.lowMid, dc.closeMid );
                                else if ( this.currentDownloadingCandleType == EnumDownloadingCandleType.HOUR )
                                    DbOanda.getInstance().insertHourCandle( (int)AkUtil.getSecondsSince1970( dc.time ), dc.openMid, dc.highMid, dc.lowMid, dc.closeMid );
                                else if ( this.currentDownloadingCandleType == EnumDownloadingCandleType.MINUTE )
                                    DbOanda.getInstance().insertMinuteCandle( (int)AkUtil.getSecondsSince1970( dc.time ), dc.openMid, dc.highMid, dc.lowMid, dc.closeMid );
                            }
                        }
                    }
                    worker.ReportProgress( 0, requestString );
                }
                catch ( Exception ex )
                {
                    worker.ReportProgress( -1, ex.Message );
                    continueDownloading = false;
                }
            }
        }

        void bwStaticRequest_ProgressChanged( object sender, ProgressChangedEventArgs e )
        {
            this.updateCandleMaxValues();
            this.toolStripStatusLabel1.Text = (string)e.UserState;
        }

        private bool changeState()
        {
            if ( this.currentDownloadingCandleType == EnumDownloadingCandleType.DAY )
            {
                this.currentDownloadingCandleType = EnumDownloadingCandleType.HOUR;
                return true;
            }
            else if ( this.currentDownloadingCandleType == EnumDownloadingCandleType.HOUR )
            {
                this.currentDownloadingCandleType = EnumDownloadingCandleType.MINUTE;
                return true;
            }
            else if ( this.currentDownloadingCandleType == EnumDownloadingCandleType.MINUTE )
            {
                this.currentDownloadingCandleType = EnumDownloadingCandleType.DAY;
                return false;
            }
            this.currentDownloadingCandleType = EnumDownloadingCandleType.DAY;
            return false;
        }

        private void updateCandleMaxValues()
        {
            int secondsSince1970 = -1;

            secondsSince1970 = DbOanda.getInstance().getLatestDayCandleTime();
            if ( secondsSince1970 > 0 )
                this.textBoxDayCandleMax.Text = AkUtil.getDateTime( secondsSince1970 ).ToString( "yyyy-MM-dd  HH:mm:ss" );

            secondsSince1970 = DbOanda.getInstance().getLatestHourCandleTime();
            if ( secondsSince1970 > 0 )
                this.textBoxHourCandleMax.Text = AkUtil.getDateTime( secondsSince1970 ).ToString( "yyyy-MM-dd  HH:mm:ss" );

            secondsSince1970 = DbOanda.getInstance().getLatestMinuteCandleTime();
            if ( secondsSince1970 > 0 )
                this.textBoxMinuteCandleMax.Text = AkUtil.getDateTime( secondsSince1970 ).ToString( "yyyy-MM-dd  HH:mm:ss" );
        }

        private void buttonStart_Click( object sender, EventArgs e )
        {
            if ( this.bwStaticRequest.IsBusy != true )
            {
                this.buttonStart.Enabled = false;
                this.toolStripStatusLabel1.Text = "Downloading...";
                this.currentDownloadingCandleType = EnumDownloadingCandleType.DAY;
                this.bwStaticRequest.RunWorkerAsync();
            }
        }

        private void FormHistoryCandleDownloader_FormClosing( object sender, FormClosingEventArgs e )
        {
            if ( this.bwStaticRequest.IsBusy )
                this.bwStaticRequest.CancelAsync();
        }        
    }
}
