﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using AkCommon;

namespace Analytick.Forms
{
    public partial class FormCalculator : Form
    {
        private enum ConversionModeTime
        {
            NONE = 0,
            SECONDS_SINCE_1970,
            DATE_TIME_STRING
        }

        private DateTime dateTimeOrigin = new DateTime( 1970, 1, 1, 0, 0, 0 );
        
        public FormCalculator()
        {
            InitializeComponent();
            this.comboBoxCurrencyPair.SelectedIndex = 0;
            this.textBoxProfit.BackColor = Color.LightGreen;
            this.textBoxProfit.ForeColor = Color.Black;
            this.textBoxUnits.Text = "10,000";
        }

        #region Time calculation
        private void textBoxSecondsSince1970_TextChanged( object sender, EventArgs e )
        {
            if ( this.textBoxSecondsSince1970.Focused == true )
                this.recalculateDateTime( ConversionModeTime.SECONDS_SINCE_1970 );
        }

        private void textBoxDateTimeString_TextChanged( object sender, EventArgs e )
        {
            if ( this.textBoxDateTimeString.Focused == true )
                this.recalculateDateTime( ConversionModeTime.DATE_TIME_STRING );
        }

        private void recalculateDateTime( ConversionModeTime mode )
        {
            if ( mode == ConversionModeTime.SECONDS_SINCE_1970 )
            {
                try
                {
                    DateTime dt = dateTimeOrigin.AddSeconds( int.Parse( this.textBoxSecondsSince1970.Text ) );
                    this.textBoxDateTimeString.Text = dt.ToString( "yyyy-MM-dd  HH:mm:ss" );
                }
                catch ( Exception )
                {
                    this.textBoxDateTimeString.Text = "NA";
                }
            }
            else if ( mode == ConversionModeTime.DATE_TIME_STRING )
            {
                try
                {
                    DateTime dt = Convert.ToDateTime( this.textBoxDateTimeString.Text );
                    this.textBoxSecondsSince1970.Text = dt.Subtract( dateTimeOrigin ).TotalSeconds.ToString();
                }
                catch ( Exception )
                {
                    this.textBoxSecondsSince1970.Text = "NA";
                }
            }
        }
        #endregion

        private void comboBoxCurrencyPair_SelectedIndexChanged( object sender, EventArgs e )
        {
            string[] strArr = this.comboBoxCurrencyPair.Text.Split( '/' );
            this.labelConversionQuote.Text = strArr[1] + "/" + strArr[1];
        }

        private void textBoxConversionQuote_TextChanged( object sender, EventArgs e )
        {
            this.recalculateProfit();
        }

        private void radioButtonBuy_Click( object sender, EventArgs e )
        {
            this.recalculateProfit();
        }

        private void radioButtonSell_Click( object sender, EventArgs e )
        {
            this.recalculateProfit();
        }

        private void textBoxEntryPrice_TextChanged( object sender, EventArgs e )
        {
            this.recalculateProfit();
        }

        private void textBoxExitPrice_TextChanged( object sender, EventArgs e )
        {
            this.recalculateProfit();
        }

        private void textBoxUnits_TextChanged( object sender, EventArgs e )
        {            
            this.recalculateProfit();
        }

        private void textBoxUnits_Leave( object sender, EventArgs e )
        {
            try
            {
                double value = double.Parse( this.textBoxUnits.Text );
                this.textBoxUnits.Text = string.Format( "{0:n0}", value );
            }
            catch ( Exception )
            {
            }
        }

        private void recalculateProfit()
        {
            try
            {
                double exitPrice = double.Parse( this.textBoxExitPrice.Text );
                double entryPrice = double.Parse( this.textBoxEntryPrice.Text );
                double conversionQuote = double.Parse( this.textBoxConversionQuote.Text );
                double units = double.Parse( this.textBoxUnits.Text );
                
                double priceDiff = exitPrice - entryPrice;
                if ( this.radioButtonSell.Checked )
                {
                    priceDiff *= -1;
                }

                double profit = priceDiff * conversionQuote * units;
                this.textBoxProfit.Text = profit.ToString( "N" );

                if ( profit >= 0 )
                    this.textBoxProfit.BackColor = Color.LightGreen;
                else
                    this.textBoxProfit.BackColor = Color.LightPink;


                double capital = units / 50; // margin is 50;
                double pips = Math.Abs( priceDiff ) / GlobalConstant.ONE_PIP_PRICE;
                double roiPercent = profit / capital * 100;
                this.updateListViewInvestment( capital, pips, profit, roiPercent );
            }
            catch ( Exception )
            {
                this.textBoxProfit.Text = "NA";
            }
        }     
   
        private void updateListViewInvestment( double capital, double pips, double profit, double roiPercent )
        {
            this.listViewInvestment.Items[1].SubItems[1].Text = string.Format( "{0:N}", capital );
            this.listViewInvestment.Items[2].SubItems[1].Text = string.Format( "{0:0.0}", pips );
            this.listViewInvestment.Items[3].SubItems[1].Text = string.Format( "{0:N}", profit );
            this.listViewInvestment.Items[4].SubItems[1].Text = string.Format( "{0:0.00} %", roiPercent );


            if ( roiPercent >= 0 )
                this.listViewInvestment.Items[4].SubItems[1].BackColor = Color.LightGreen;
            else
                this.listViewInvestment.Items[4].SubItems[1].BackColor = Color.LightPink;
        }
    }
}
