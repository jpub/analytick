﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Analytick.Forms
{
    public partial class FormCrash : Form
    {
        public FormCrash()
        {
            InitializeComponent();
        }

        public void setExceptionMessage( string message )
        {
            this.exceptionMessageTextBox.Text = message;
        }

        public void setExceptionStackTrace( string stackTrace )
        {
            this.exceptionStackTraceTextBox.Text = stackTrace;
        }
    }

}
