﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Analytick.Forms
{
    public partial class FormDateSelection : Form
    {
        public DateTime SelectedDateTimeStart { get; private set; }
        public DateTime SelectedDateTimeEnd { get; private set; }

        public FormDateSelection( DateTime startDt )
        {
            InitializeComponent();            
            this.dateTimePickerStart.Value = startDt;
            this.dateTimePickerEnd.Value = new DateTime( 2099, 1, 1 );
        }

        private void buttonCancel_Click( object sender, EventArgs e )
        {
            this.DialogResult = DialogResult.Cancel;
            this.Hide();
        }

        private void buttonLoad_Click( object sender, EventArgs e )
        {
            this.DialogResult = DialogResult.OK;
            this.SelectedDateTimeStart = this.dateTimePickerStart.Value;
            this.SelectedDateTimeEnd = this.dateTimePickerEnd.Value;
            this.Hide();
        }
    }
}
