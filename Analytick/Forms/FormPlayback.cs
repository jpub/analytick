﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Web.Script.Serialization;
using System.Net;
using System.Net.Sockets;
using System.Threading;

using AkBase;
using AkModel;
using AkMessages;
using AkCommon;
using Analytick.Manager;

namespace Analytick.Forms
{
    public partial class FormPlayback : Form
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger( System.Reflection.MethodBase.GetCurrentMethod().DeclaringType );

        private JavaScriptSerializer serializer = new JavaScriptSerializer();
        private BackgroundWorker bwSender = new BackgroundWorker();        
        private BackgroundWorker bwFileLoader = new BackgroundWorker();
        private MSG_DEBUG_LIST modelSendMessageList = new MSG_DEBUG_LIST();
        private FactoryMessage factoryMessage = new FactoryMessage();
        private MessageQueue<BaseMessage> incomingQueue = null;                        

        private int autoMilliDelayPerMsg = 20;

        public FormPlayback( MessageQueue<BaseMessage> incomingQueue )
        {
            InitializeComponent();

            this.incomingQueue = incomingQueue;

            this.openFileDialog.FileName = GlobalConstant.DEFAULT_PLAYBACK_FILENAME;

            this.bwSender.WorkerReportsProgress = true;
            this.bwSender.WorkerSupportsCancellation = true;
            this.bwSender.DoWork += new DoWorkEventHandler( bwSender_DoWork );
            this.bwSender.ProgressChanged += new ProgressChangedEventHandler( bwSender_ProgressChanged );
            this.bwSender.RunWorkerCompleted += new RunWorkerCompletedEventHandler( bwSender_RunWorkerCompleted );

            this.bwFileLoader.WorkerReportsProgress = true;
            this.bwFileLoader.WorkerSupportsCancellation = true;
            this.bwFileLoader.DoWork += new DoWorkEventHandler( bwFileLoader_DoWork );
            this.bwFileLoader.ProgressChanged += new ProgressChangedEventHandler( bwFileLoader_ProgressChanged );
            this.bwFileLoader.RunWorkerCompleted += new RunWorkerCompletedEventHandler( bwFileLoader_RunWorkerCompleted );
            
            this.toolStripComboBoxRate.SelectedIndex = 0;
            this.toolStripStatusLabel.Text = "Ready";
            
            if ( GlobalConstant.IS_LOAD_DEFAULT_PLAYBACK_FILENAME )
            {
                if ( this.bwFileLoader.IsBusy != true )
                {
                    this.openFileDialog.FileName = GlobalConstant.DEFAULT_PLAYBACK_FILENAME;
                    this.toolStripStatusLabel.Text = "Loading";
                    this.toolStripButtonLoadFolder.Enabled = false;
                    this.toolStripButtonStop.Enabled = false;
                    this.toolStripButtonPlay.Enabled = false;
                    this.bwFileLoader.RunWorkerAsync( new object[] { GlobalConstant.DEFAULT_PLAYBACK_FILENAME } );
                }
            }
        }

        private void listViewSend_RetrieveVirtualItem( object sender, RetrieveVirtualItemEventArgs e )
        {
            int index = e.ItemIndex;
            MSG_DEBUG msg = this.modelSendMessageList.get( index );
            ListViewItem lvItem = new ListViewItem( ( index + 1 ).ToString() );
            lvItem.SubItems.Add( msg.Name );            
            lvItem.SubItems.Add( msg.MessageTime.ToString( "MMMM dd HH:mm:ss" ) );
            e.Item = lvItem;
        }

        private void listViewSend_DoubleClick( object sender, EventArgs e )
        {
            if ( this.listViewSend.SelectedIndices.Count != 1 )
                return;

            int index = this.listViewSend.SelectedIndices[0];
            MSG_DEBUG msg = this.modelSendMessageList.get( index );
            FormJsonEdit formJsonEdit = new FormJsonEdit( this.incomingQueue, this.factoryMessage, msg );
            formJsonEdit.Show();
        }

        private void listViewSend_KeyDown( object sender, KeyEventArgs e )
        {
            if ( e.KeyCode == Keys.Space )
                this.manualPlayback();
        }

        private void toolStripButtonLoadFolder_Click( object sender, EventArgs e )
        {            
            if ( this.openFileDialog.ShowDialog() != DialogResult.OK )
                return;

            this.modelSendMessageList.clear();
            this.listViewSend.VirtualListSize = 0;

            if ( this.bwFileLoader.IsBusy != true )
            {
                this.toolStripStatusLabel.Text = "Loading";
                this.toolStripButtonLoadFolder.Enabled = false;                
                this.toolStripButtonStop.Enabled = false;
                this.toolStripButtonPlay.Enabled = false;
                this.bwFileLoader.RunWorkerAsync( new object[] { this.openFileDialog.FileName } );
            }
        }

        private void toolStripButtonStop_Click( object sender, EventArgs e )
        {
            if ( bwSender.WorkerSupportsCancellation == true )
            {
                bwSender.CancelAsync();
            }
        }

        private void toolStripButtonPlay_Click( object sender, EventArgs e )
        {
            if ( this.modelSendMessageList.count() == 0 )
                return;

            this.startBwSenderWorker();
        }

        private void startBwSenderWorker()
        {
            if ( this.bwSender.IsBusy != true )
            {
                this.listViewSend.Enabled = false;
                this.toolStripButtonPlay.Enabled = false;
                this.toolStripComboBoxRate.Enabled = false;

                int startIndex = 0;
                if ( this.listViewSend.SelectedIndices.Count == 1 )
                    startIndex = this.listViewSend.SelectedIndices[0];

                string[] strArr = this.toolStripComboBoxRate.Text.Split( ' ' );
                int rate = int.Parse( strArr[2] );                

                this.bwSender.RunWorkerAsync( new object[] { startIndex, this.listViewSend.VirtualListSize - 1, rate } );
            }
        }

        private void manualPlayback()
        {
            if ( this.listViewSend.SelectedIndices.Count != 1 )
                return;

            int index = this.listViewSend.SelectedIndices[0];
            MSG_DEBUG msgDebug = this.modelSendMessageList.get( index );

            if ( index < this.listViewSend.VirtualListSize - 1 )
            {
                this.listViewSend.Items[index + 1].Selected = true;
                this.listViewSend.Items[index + 1].Focused = true;
                this.listViewSend.EnsureVisible( index + 1 );
            }

            this.putToQueue( msgDebug );
        }

        private bool putToQueue( MSG_DEBUG msgDebug )
        {
            BaseMessage message = null;
            string messageType = "";

            Dictionary<string, object> jsonData = msgDebug.toJsonGraphDataOnly();
            string jsonString = this.serializer.Serialize( jsonData );

            if ( !jsonData.ContainsKey( "MessageType" ) )
            {
                log.Error( "Unknown message type in json string:" + jsonString );
                return false;
            }

            messageType = (string)jsonData["MessageType"];
            message = this.factoryMessage.getMessage( messageType, jsonString );
            if ( message == null )
            {
                log.Error( "skipping null message" );
                return false;
            }
            this.incomingQueue.add( message );
            return true;
        }

        #region Sender Thread
        void bwSender_ProgressChanged( object sender, ProgressChangedEventArgs e )
        {
            if ( e.ProgressPercentage != 0 )
            {
                string errMsg = (string)e.UserState;
                this.toolStripStatusLabel.Text = errMsg;
                return;
            }
            object[] args = (object[])e.UserState;
            int stopIndex = (int)args[0];
            string simTime = (string)args[1];

            int lastSendIndex = stopIndex - 1 < 0 ? 0 : stopIndex - 1;
            this.listViewSend.Items[lastSendIndex].Selected = true;
            this.listViewSend.Items[lastSendIndex].Focused = true;
            this.listViewSend.EnsureVisible( lastSendIndex );

            this.toolStripStatusLabel.Text = simTime;
        }

        void bwSender_DoWork( object sender, DoWorkEventArgs e )
        {
            try
            {
                BackgroundWorker worker = sender as BackgroundWorker;
                object[] args = (object[])e.Argument;
                int startIndex = (int)args[0];
                int stopIndex = startIndex;
                int endIndex = (int)args[1];
                int rate = (int)args[2];                

                DateTime tillTime = this.modelSendMessageList.get( startIndex ).MessageTime;
                DateTime lastSendTime = tillTime;

                Stopwatch stopwatch = new Stopwatch();
                while ( true )
                {
                    if ( ( worker.CancellationPending == true ) )
                    {
                        e.Cancel = true;
                        break;
                    }

                    stopwatch.Reset();
                    stopwatch.Start();
                    tillTime = tillTime.AddMilliseconds( 1000 * rate );
                    List<MSG_DEBUG> intervalList = this.modelSendMessageList.getTillTime( startIndex, tillTime, out stopIndex );
                    foreach ( MSG_DEBUG msgDebug in intervalList )
                    {
                        if ( msgDebug.MessageTime.Second != lastSendTime.Second ||
                             msgDebug.MessageTime.Minute != lastSendTime.Minute ||
                             msgDebug.MessageTime.Hour != lastSendTime.Hour )
                        {
                            lastSendTime = msgDebug.MessageTime;
                        }

                        this.putToQueue( msgDebug );
                    }
                    startIndex = stopIndex;

                    worker.ReportProgress( 0, new object[] { stopIndex, tillTime.ToString( "MMMM dd HH:mm:ss" ) } );
                    if ( startIndex == endIndex )
                    {
                        // sim ended
                        if ( this.modelSendMessageList.get( startIndex ).MessageTime <= tillTime )
                        {
                            return;
                        }
                    }

                    stopwatch.Stop();
                    if ( stopwatch.ElapsedMilliseconds < 1000 )
                    {
                        long sleepTimeMilli = 1000 - stopwatch.ElapsedMilliseconds;
                        System.Threading.Thread.Sleep( (int)sleepTimeMilli );
                    }
                }
            }
            catch ( Exception ex )
            {
                log.Error( ex.Message );
            }
        }

        void bwSender_RunWorkerCompleted( object sender, RunWorkerCompletedEventArgs e )
        {
            if ( ( e.Cancelled == true ) )
            {
                this.toolStripStatusLabel.Text = "Sim Stopped";
            }
            else if ( !( e.Error == null ) )
            {
                this.toolStripStatusLabel.Text = ( "Error: " + e.Error.Message );
            }
            else
            {
                this.toolStripStatusLabel.Text = "Sim Ended";
            }

            this.listViewSend.Enabled = true;
            this.toolStripButtonLoadFolder.Enabled = true;
            this.toolStripButtonPlay.Enabled = true;
            this.toolStripComboBoxRate.Enabled = true;
        }
        #endregion

        #region File Loading Thread
        void bwFileLoader_RunWorkerCompleted( object sender, RunWorkerCompletedEventArgs e )
        {
            if ( !e.Cancelled )
            {
                this.toolStripStatusLabel.Text = "Ready";
                this.toolStripProgressBar.Value = 0;

                this.listViewSend.VirtualListSize = this.modelSendMessageList.count();

                if ( File.Exists( this.openFileDialog.FileName ) )
                {
                    this.Text = this.openFileDialog.FileName;
                }
            }

            this.toolStripButtonLoadFolder.Enabled = true;
            this.toolStripButtonStop.Enabled = true;
            this.toolStripButtonPlay.Enabled = true;
        }

        void bwFileLoader_ProgressChanged( object sender, ProgressChangedEventArgs e )
        {
            if ( e.ProgressPercentage != 0 )
            {
                this.toolStripStatusLabel.Text = (string)e.UserState;
                return;
            }

            object[] args = (object[])e.UserState;
            int currProgress = (int)args[0];
            int maxProgress = (int)args[1];

            this.toolStripProgressBar.Maximum = maxProgress;
            this.toolStripProgressBar.Value = currProgress;
            this.toolStripStatusLabel.Text = currProgress + "/" + maxProgress;
        }

        void bwFileLoader_DoWork( object sender, DoWorkEventArgs e )
        {
            //isDir, this.openFileDialog.FileName, dirPath 
            BackgroundWorker worker = sender as BackgroundWorker;
            object[] args = (object[])e.Argument;
            string filename = (string)args[0];            

            try
            {
                this.loadJsonFileIntoModel( filename );
                this.modelSendMessageList.sort();
            }
            catch ( Exception ex )
            {
                worker.ReportProgress( -1, ex.Message );
                e.Cancel = true;
            }
        }

        private bool loadJsonFileIntoModel( string filename )
        {
            try
            {
                DateTime defaultDateTime = new DateTime( 2016, 1, 1 );
                StreamReader sr = File.OpenText( filename );
                string lineStr = "";
                while ( ( lineStr = sr.ReadLine() ) != null )
                {
                    Dictionary<string, object> jsonGraph = this.serializer.Deserialize<Dictionary<string, object>>( lineStr );
                    MSG_DEBUG msg = new MSG_DEBUG( jsonGraph );
                    if ( msg.MessageTime.Equals( defaultDateTime ) )
                    {
                        int currentTotal = this.modelSendMessageList.count();
                        msg.MessageTime = defaultDateTime.AddMilliseconds( currentTotal * this.autoMilliDelayPerMsg );
                    }
                    if ( msg.Name == null )
                        log.Warn( "sending message with null message type: " + lineStr );
                    this.modelSendMessageList.add( msg );
                }
                sr.Close();
                return true;
            }
            catch ( Exception ex )
            {
                log.Fatal( ex.Message );
                return false;
            }
        }
        #endregion

        private void toolStripTextBoxSearch_KeyDown( object sender, KeyEventArgs e )
        {
            if ( this.listViewSend.Items.Count == 0 )
                return;

            string searchString = this.toolStripTextBoxSearch.Text;
            if ( searchString.Length == 0 )
                return;
            
            if ( e.KeyCode == Keys.Enter && e.Shift )
            {
                int totalItems = this.listViewSend.Items.Count;
                if ( this.listViewSend.SelectedIndices.Count == 0 )
                {
                    this.listViewSend.Items[totalItems - 1].Selected = true;
                    this.listViewSend.Items[totalItems - 1].Focused = true;
                }

                this.searchBackward( searchString );
            }
            else if ( e.KeyCode == Keys.Enter )
            {
                if ( this.listViewSend.SelectedIndices.Count == 0 )
                {
                    this.listViewSend.Items[0].Selected = true;
                    this.listViewSend.Items[0].Focused = true;
                }

                this.searchForward( searchString );
            }
        }

        private void searchForward( string searchString )
        {
            int selectedIndex = this.listViewSend.SelectedIndices[0];
            int totalMessages = this.modelSendMessageList.count();

            for ( int current = selectedIndex + 1; current < totalMessages; ++current )
            {
                MSG_DEBUG msg = this.modelSendMessageList.get( current );
                if ( msg.Name.Contains( searchString ) ||
                     msg.MessageTime.ToString( "MMMM dd HH:mm:ss" ).Contains( searchString ) )
                {
                    this.listViewSend.EnsureVisible( current );
                    this.listViewSend.Items[current].Selected = true;
                    this.listViewSend.Items[current].Focused = true;
                    break;
                }
            }
        }

        private void searchBackward( string searchString )
        {
            int selectedIndex = this.listViewSend.SelectedIndices[0];
            int totalMessages = this.modelSendMessageList.count();

            for ( int current = selectedIndex - 1; current >= 0; --current )
            {
                MSG_DEBUG msg = this.modelSendMessageList.get( current );
                if ( msg.Name.Contains( searchString ) ||
                     msg.MessageTime.ToString( "MMMM dd HH:mm:ss" ).Contains( searchString ) )
                {
                    this.listViewSend.EnsureVisible( current );
                    this.listViewSend.Items[current].Selected = true;
                    this.listViewSend.Items[current].Focused = true;
                    break;
                }
            }
        }
    }
}
