﻿namespace Analytick.Forms
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem(new string[] {
            "Balance",
            "---"}, -1, System.Drawing.Color.Empty, System.Drawing.Color.WhiteSmoke, null);
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem(new string[] {
            "Unrealized PL",
            "---"}, -1);
            System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem(new System.Windows.Forms.ListViewItem.ListViewSubItem[] {
            new System.Windows.Forms.ListViewItem.ListViewSubItem(null, "Realized PL", System.Drawing.SystemColors.WindowText, System.Drawing.Color.WhiteSmoke, new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)))),
            new System.Windows.Forms.ListViewItem.ListViewSubItem(null, "---", System.Drawing.SystemColors.WindowText, System.Drawing.Color.WhiteSmoke, new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0))))}, -1);
            System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem(new string[] {
            "Open Trades",
            "---"}, -1, System.Drawing.Color.Empty, System.Drawing.SystemColors.Window, null);
            System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem(new string[] {
            "Open Orders",
            "---"}, -1, System.Drawing.Color.Empty, System.Drawing.Color.WhiteSmoke, null);
            System.Windows.Forms.ListViewItem listViewItem6 = new System.Windows.Forms.ListViewItem(new string[] {
            "Margin Used",
            "---"}, -1);
            System.Windows.Forms.ListViewItem listViewItem7 = new System.Windows.Forms.ListViewItem(new string[] {
            "Margin Left",
            "---"}, -1, System.Drawing.Color.Empty, System.Drawing.Color.WhiteSmoke, null);
            System.Windows.Forms.ListViewItem listViewItem8 = new System.Windows.Forms.ListViewItem(new string[] {
            "Pair",
            "---"}, -1, System.Drawing.Color.Empty, System.Drawing.Color.WhiteSmoke, null);
            System.Windows.Forms.ListViewItem listViewItem9 = new System.Windows.Forms.ListViewItem(new string[] {
            "Side",
            "---"}, -1);
            System.Windows.Forms.ListViewItem listViewItem10 = new System.Windows.Forms.ListViewItem(new string[] {
            "Price",
            "---"}, -1, System.Drawing.Color.Empty, System.Drawing.Color.WhiteSmoke, null);
            System.Windows.Forms.ListViewItem listViewItem11 = new System.Windows.Forms.ListViewItem(new string[] {
            "Units",
            "---"}, -1);
            System.Windows.Forms.ListViewItem listViewItem12 = new System.Windows.Forms.ListViewItem(new string[] {
            "",
            ""}, -1, System.Drawing.Color.Empty, System.Drawing.Color.WhiteSmoke, null);
            System.Windows.Forms.ListViewItem listViewItem13 = new System.Windows.Forms.ListViewItem(new string[] {
            "TP",
            "---"}, -1);
            System.Windows.Forms.ListViewItem listViewItem14 = new System.Windows.Forms.ListViewItem(new string[] {
            "SL",
            "---"}, -1, System.Drawing.Color.Empty, System.Drawing.Color.WhiteSmoke, null);
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonLoadDatabase = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonSaveState = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonLoadState = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonClearSimulationRun = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonCalculator = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonInformation = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonZoomIn = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonZoomOut = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonDownloadHistoryCandles = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonStartLiveStreaming = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonCloseAllPosition = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelEventHeartbeat = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel4 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelRateHeartbeat = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.timerGraph = new System.Windows.Forms.Timer(this.components);
            this.contextMenuStripMarker = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemMarkerDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemMarkerRemarks = new System.Windows.Forms.ToolStripMenuItem();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.splitContainer6 = new System.Windows.Forms.SplitContainer();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.listViewAccount = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.listViewOpenTrade = new System.Windows.Forms.ListView();
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.listViewResult = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.buttonTestEmaStrategy = new System.Windows.Forms.Button();
            this.textBoxStopLoss = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxTakeProfit = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxEmaThresholdMediumSlow = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxEmaThresholdFastMedium = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.buttonTestMacdStrategy = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.buttonTestEmaMacdStrategy = new System.Windows.Forms.Button();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.splitContainer5 = new System.Windows.Forms.SplitContainer();
            this.contextMenuStripGraphType = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.scaleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hourToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.minuteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.graphToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.candleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eMAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buttonLiveEmaStrategy = new System.Windows.Forms.Button();
            this.graphPanelDistribution = new AkWidget.GraphPanel();
            this.graphPanelOverview = new AkWidget.GraphPanel();
            this.graphPanelCandle = new AkWidget.GraphPanel();
            this.labelBidAsk = new System.Windows.Forms.Label();
            this.graphPanelRsi = new AkWidget.GraphPanel();
            this.graphPanelMacd = new AkWidget.GraphPanel();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.contextMenuStripMarker.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer6)).BeginInit();
            this.splitContainer6.Panel1.SuspendLayout();
            this.splitContainer6.Panel2.SuspendLayout();
            this.splitContainer6.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer5)).BeginInit();
            this.splitContainer5.Panel1.SuspendLayout();
            this.splitContainer5.Panel2.SuspendLayout();
            this.splitContainer5.SuspendLayout();
            this.contextMenuStripGraphType.SuspendLayout();
            this.graphPanelCandle.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonLoadDatabase,
            this.toolStripButtonSaveState,
            this.toolStripButtonLoadState,
            this.toolStripButtonClearSimulationRun,
            this.toolStripButtonCalculator,
            this.toolStripButtonInformation,
            this.toolStripButtonZoomIn,
            this.toolStripButtonZoomOut,
            this.toolStripButtonDownloadHistoryCandles,
            this.toolStripButtonStartLiveStreaming,
            this.toolStripButtonCloseAllPosition,
            this.toolStripButton1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1062, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButtonLoadDatabase
            // 
            this.toolStripButtonLoadDatabase.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonLoadDatabase.Image = global::Analytick.Properties.Resources.openFolder;
            this.toolStripButtonLoadDatabase.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonLoadDatabase.Name = "toolStripButtonLoadDatabase";
            this.toolStripButtonLoadDatabase.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonLoadDatabase.Text = "Load database";
            this.toolStripButtonLoadDatabase.Click += new System.EventHandler(this.toolStripButtonLoadDatabase_Click);
            // 
            // toolStripButtonSaveState
            // 
            this.toolStripButtonSaveState.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonSaveState.Image = global::Analytick.Properties.Resources.saveDatabase;
            this.toolStripButtonSaveState.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonSaveState.Name = "toolStripButtonSaveState";
            this.toolStripButtonSaveState.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonSaveState.Text = "Save state";
            this.toolStripButtonSaveState.Click += new System.EventHandler(this.toolStripButtonSaveState_Click);
            // 
            // toolStripButtonLoadState
            // 
            this.toolStripButtonLoadState.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonLoadState.Image = global::Analytick.Properties.Resources.loadDatabase;
            this.toolStripButtonLoadState.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonLoadState.Name = "toolStripButtonLoadState";
            this.toolStripButtonLoadState.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonLoadState.Text = "Load state";
            this.toolStripButtonLoadState.Click += new System.EventHandler(this.toolStripButtonLoadState_Click);
            // 
            // toolStripButtonClearSimulationRun
            // 
            this.toolStripButtonClearSimulationRun.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonClearSimulationRun.Image = global::Analytick.Properties.Resources.clearSimulationRun;
            this.toolStripButtonClearSimulationRun.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonClearSimulationRun.Name = "toolStripButtonClearSimulationRun";
            this.toolStripButtonClearSimulationRun.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonClearSimulationRun.Text = "Clear simulation run";
            this.toolStripButtonClearSimulationRun.Click += new System.EventHandler(this.toolStripButtonClearSimulationRun_Click);
            // 
            // toolStripButtonCalculator
            // 
            this.toolStripButtonCalculator.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonCalculator.Image = global::Analytick.Properties.Resources.calculator;
            this.toolStripButtonCalculator.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonCalculator.Name = "toolStripButtonCalculator";
            this.toolStripButtonCalculator.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonCalculator.Text = "toolStripButton2";
            this.toolStripButtonCalculator.Click += new System.EventHandler(this.toolStripButtonCalculator_Click);
            // 
            // toolStripButtonInformation
            // 
            this.toolStripButtonInformation.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonInformation.Image = global::Analytick.Properties.Resources.information;
            this.toolStripButtonInformation.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonInformation.Name = "toolStripButtonInformation";
            this.toolStripButtonInformation.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonInformation.Text = "Information";
            this.toolStripButtonInformation.Click += new System.EventHandler(this.toolStripButtonInformation_Click);
            // 
            // toolStripButtonZoomIn
            // 
            this.toolStripButtonZoomIn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonZoomIn.Image = global::Analytick.Properties.Resources.plusBarWidth;
            this.toolStripButtonZoomIn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonZoomIn.Name = "toolStripButtonZoomIn";
            this.toolStripButtonZoomIn.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonZoomIn.Text = "Zoom in";
            this.toolStripButtonZoomIn.Click += new System.EventHandler(this.toolStripButtonZoomIn_Click);
            // 
            // toolStripButtonZoomOut
            // 
            this.toolStripButtonZoomOut.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonZoomOut.Image = global::Analytick.Properties.Resources.minusBarWidth;
            this.toolStripButtonZoomOut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonZoomOut.Name = "toolStripButtonZoomOut";
            this.toolStripButtonZoomOut.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonZoomOut.Text = "Zoom out";
            this.toolStripButtonZoomOut.Click += new System.EventHandler(this.toolStripButtonZoomOut_Click);
            // 
            // toolStripButtonDownloadHistoryCandles
            // 
            this.toolStripButtonDownloadHistoryCandles.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonDownloadHistoryCandles.Image = global::Analytick.Properties.Resources.download;
            this.toolStripButtonDownloadHistoryCandles.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonDownloadHistoryCandles.Name = "toolStripButtonDownloadHistoryCandles";
            this.toolStripButtonDownloadHistoryCandles.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonDownloadHistoryCandles.Text = "Download history candles";
            this.toolStripButtonDownloadHistoryCandles.Click += new System.EventHandler(this.toolStripButtonDownloadHistoryCandles_Click);
            // 
            // toolStripButtonStartLiveStreaming
            // 
            this.toolStripButtonStartLiveStreaming.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonStartLiveStreaming.Image = global::Analytick.Properties.Resources.run;
            this.toolStripButtonStartLiveStreaming.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonStartLiveStreaming.Name = "toolStripButtonStartLiveStreaming";
            this.toolStripButtonStartLiveStreaming.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonStartLiveStreaming.Text = "Start live streaming";
            this.toolStripButtonStartLiveStreaming.Click += new System.EventHandler(this.toolStripButtonStartLiveStreaming_Click);
            // 
            // toolStripButtonCloseAllPosition
            // 
            this.toolStripButtonCloseAllPosition.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonCloseAllPosition.Image = global::Analytick.Properties.Resources.closeAllPosition;
            this.toolStripButtonCloseAllPosition.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonCloseAllPosition.Name = "toolStripButtonCloseAllPosition";
            this.toolStripButtonCloseAllPosition.Size = new System.Drawing.Size(23, 22);
            this.toolStripButtonCloseAllPosition.Text = "Close all position";
            this.toolStripButtonCloseAllPosition.Click += new System.EventHandler(this.toolStripButtonCloseAllPosition_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "toolStripButton1";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel,
            this.toolStripProgressBar,
            this.toolStripStatusLabel1,
            this.toolStripStatusLabelEventHeartbeat,
            this.toolStripStatusLabel4,
            this.toolStripStatusLabelRateHeartbeat,
            this.toolStripStatusLabel3});
            this.statusStrip1.Location = new System.Drawing.Point(0, 784);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1062, 24);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(39, 19);
            this.toolStripStatusLabel.Text = "Ready";
            // 
            // toolStripProgressBar
            // 
            this.toolStripProgressBar.Name = "toolStripProgressBar";
            this.toolStripProgressBar.Size = new System.Drawing.Size(100, 18);
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(794, 19);
            this.toolStripStatusLabel1.Spring = true;
            // 
            // toolStripStatusLabelEventHeartbeat
            // 
            this.toolStripStatusLabelEventHeartbeat.BackColor = System.Drawing.Color.Red;
            this.toolStripStatusLabelEventHeartbeat.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.toolStripStatusLabelEventHeartbeat.Name = "toolStripStatusLabelEventHeartbeat";
            this.toolStripStatusLabelEventHeartbeat.Size = new System.Drawing.Size(23, 19);
            this.toolStripStatusLabelEventHeartbeat.Text = "    ";
            // 
            // toolStripStatusLabel4
            // 
            this.toolStripStatusLabel4.Name = "toolStripStatusLabel4";
            this.toolStripStatusLabel4.Size = new System.Drawing.Size(36, 19);
            this.toolStripStatusLabel4.Text = "Event";
            // 
            // toolStripStatusLabelRateHeartbeat
            // 
            this.toolStripStatusLabelRateHeartbeat.BackColor = System.Drawing.Color.Red;
            this.toolStripStatusLabelRateHeartbeat.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.toolStripStatusLabelRateHeartbeat.Name = "toolStripStatusLabelRateHeartbeat";
            this.toolStripStatusLabelRateHeartbeat.Size = new System.Drawing.Size(23, 19);
            this.toolStripStatusLabelRateHeartbeat.Text = "    ";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(30, 19);
            this.toolStripStatusLabel3.Text = "Rate";
            // 
            // timerGraph
            // 
            this.timerGraph.Enabled = true;
            this.timerGraph.Interval = 50;
            this.timerGraph.Tick += new System.EventHandler(this.timerGraph_Tick);
            // 
            // contextMenuStripMarker
            // 
            this.contextMenuStripMarker.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemMarkerDelete,
            this.toolStripMenuItemMarkerRemarks});
            this.contextMenuStripMarker.Name = "contextMenuStrip";
            this.contextMenuStripMarker.Size = new System.Drawing.Size(120, 48);
            // 
            // toolStripMenuItemMarkerDelete
            // 
            this.toolStripMenuItemMarkerDelete.Name = "toolStripMenuItemMarkerDelete";
            this.toolStripMenuItemMarkerDelete.Size = new System.Drawing.Size(119, 22);
            this.toolStripMenuItemMarkerDelete.Text = "Delete";
            this.toolStripMenuItemMarkerDelete.Click += new System.EventHandler(this.toolStripMenuItemMarkerDelete_Click);
            // 
            // toolStripMenuItemMarkerRemarks
            // 
            this.toolStripMenuItemMarkerRemarks.Name = "toolStripMenuItemMarkerRemarks";
            this.toolStripMenuItemMarkerRemarks.Size = new System.Drawing.Size(119, 22);
            this.toolStripMenuItemMarkerRemarks.Text = "Remarks";
            this.toolStripMenuItemMarkerRemarks.Click += new System.EventHandler(this.toolStripMenuItemMarkerRemarks_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 25);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer4);
            this.splitContainer1.Size = new System.Drawing.Size(1062, 759);
            this.splitContainer1.SplitterDistance = 423;
            this.splitContainer1.TabIndex = 10;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.splitContainer6);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer2.Size = new System.Drawing.Size(423, 759);
            this.splitContainer2.SplitterDistance = 396;
            this.splitContainer2.TabIndex = 0;
            // 
            // splitContainer6
            // 
            this.splitContainer6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer6.Location = new System.Drawing.Point(0, 0);
            this.splitContainer6.Name = "splitContainer6";
            this.splitContainer6.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer6.Panel1
            // 
            this.splitContainer6.Panel1.Controls.Add(this.tabControl2);
            // 
            // splitContainer6.Panel2
            // 
            this.splitContainer6.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer6.Panel2.Controls.Add(this.tabControl1);
            this.splitContainer6.Size = new System.Drawing.Size(423, 396);
            this.splitContainer6.SplitterDistance = 197;
            this.splitContainer6.TabIndex = 1;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage5);
            this.tabControl2.Controls.Add(this.tabPage4);
            this.tabControl2.Controls.Add(this.tabPage7);
            this.tabControl2.Controls.Add(this.tabPage6);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl2.Location = new System.Drawing.Point(0, 0);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(423, 197);
            this.tabControl2.TabIndex = 0;
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.Color.DimGray;
            this.tabPage5.Controls.Add(this.listViewAccount);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(415, 171);
            this.tabPage5.TabIndex = 1;
            this.tabPage5.Text = "Account";
            // 
            // listViewAccount
            // 
            this.listViewAccount.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.listViewAccount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewAccount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listViewAccount.FullRowSelect = true;
            this.listViewAccount.GridLines = true;
            this.listViewAccount.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            listViewItem2.UseItemStyleForSubItems = false;
            listViewItem3.UseItemStyleForSubItems = false;
            this.listViewAccount.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1,
            listViewItem2,
            listViewItem3,
            listViewItem4,
            listViewItem5,
            listViewItem6,
            listViewItem7});
            this.listViewAccount.Location = new System.Drawing.Point(3, 3);
            this.listViewAccount.Name = "listViewAccount";
            this.listViewAccount.Size = new System.Drawing.Size(409, 165);
            this.listViewAccount.TabIndex = 0;
            this.listViewAccount.UseCompatibleStateImageBehavior = false;
            this.listViewAccount.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Width = 100;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Width = 120;
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.Color.DimGray;
            this.tabPage4.Controls.Add(this.listViewOpenTrade);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(415, 171);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "ActiveTrade";
            // 
            // listViewOpenTrade
            // 
            this.listViewOpenTrade.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader9,
            this.columnHeader10});
            this.listViewOpenTrade.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewOpenTrade.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listViewOpenTrade.FullRowSelect = true;
            this.listViewOpenTrade.GridLines = true;
            this.listViewOpenTrade.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.listViewOpenTrade.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem8,
            listViewItem9,
            listViewItem10,
            listViewItem11,
            listViewItem12,
            listViewItem13,
            listViewItem14});
            this.listViewOpenTrade.Location = new System.Drawing.Point(3, 3);
            this.listViewOpenTrade.Name = "listViewOpenTrade";
            this.listViewOpenTrade.Size = new System.Drawing.Size(409, 165);
            this.listViewOpenTrade.TabIndex = 1;
            this.listViewOpenTrade.UseCompatibleStateImageBehavior = false;
            this.listViewOpenTrade.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Width = 80;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Width = 120;
            // 
            // tabPage7
            // 
            this.tabPage7.BackColor = System.Drawing.Color.DimGray;
            this.tabPage7.Controls.Add(this.listView1);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(415, 171);
            this.tabPage7.TabIndex = 4;
            this.tabPage7.Text = "Result";
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader11,
            this.columnHeader12,
            this.columnHeader13,
            this.columnHeader14,
            this.columnHeader15,
            this.columnHeader16});
            this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView1.GridLines = true;
            this.listView1.Location = new System.Drawing.Point(3, 3);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(409, 165);
            this.listView1.TabIndex = 1;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "S/N";
            this.columnHeader11.Width = 40;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Trades";
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Win";
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "Lose";
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "Nett";
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "Remarks";
            this.columnHeader16.Width = 150;
            // 
            // tabPage6
            // 
            this.tabPage6.BackColor = System.Drawing.Color.DimGray;
            this.tabPage6.Controls.Add(this.listViewResult);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(415, 171);
            this.tabPage6.TabIndex = 2;
            this.tabPage6.Text = "Simulation";
            // 
            // listViewResult
            // 
            this.listViewResult.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8});
            this.listViewResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewResult.GridLines = true;
            this.listViewResult.Location = new System.Drawing.Point(3, 3);
            this.listViewResult.Name = "listViewResult";
            this.listViewResult.Size = new System.Drawing.Size(409, 165);
            this.listViewResult.TabIndex = 0;
            this.listViewResult.UseCompatibleStateImageBehavior = false;
            this.listViewResult.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "S/N";
            this.columnHeader3.Width = 40;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Trades";
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Win";
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Lose";
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Nett";
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Remarks";
            this.columnHeader8.Width = 150;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(423, 195);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Black;
            this.tabPage1.Controls.Add(this.buttonLiveEmaStrategy);
            this.tabPage1.Controls.Add(this.buttonTestEmaStrategy);
            this.tabPage1.Controls.Add(this.textBoxStopLoss);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.textBoxTakeProfit);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.textBoxEmaThresholdMediumSlow);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.textBoxEmaThresholdFastMedium);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(415, 169);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "EMA";
            // 
            // buttonTestEmaStrategy
            // 
            this.buttonTestEmaStrategy.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.buttonTestEmaStrategy.Location = new System.Drawing.Point(145, 17);
            this.buttonTestEmaStrategy.Name = "buttonTestEmaStrategy";
            this.buttonTestEmaStrategy.Size = new System.Drawing.Size(71, 42);
            this.buttonTestEmaStrategy.TabIndex = 15;
            this.buttonTestEmaStrategy.Text = "BackTest";
            this.buttonTestEmaStrategy.UseVisualStyleBackColor = false;
            this.buttonTestEmaStrategy.Click += new System.EventHandler(this.buttonTestEmaStrategy_Click);
            // 
            // textBoxStopLoss
            // 
            this.textBoxStopLoss.Location = new System.Drawing.Point(67, 95);
            this.textBoxStopLoss.Name = "textBoxStopLoss";
            this.textBoxStopLoss.Size = new System.Drawing.Size(60, 20);
            this.textBoxStopLoss.TabIndex = 14;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Yellow;
            this.label7.Location = new System.Drawing.Point(6, 98);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Stop Loss";
            // 
            // textBoxTakeProfit
            // 
            this.textBoxTakeProfit.Location = new System.Drawing.Point(67, 69);
            this.textBoxTakeProfit.Name = "textBoxTakeProfit";
            this.textBoxTakeProfit.Size = new System.Drawing.Size(60, 20);
            this.textBoxTakeProfit.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Yellow;
            this.label6.Location = new System.Drawing.Point(6, 72);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Take Profit";
            // 
            // textBoxEmaThresholdMediumSlow
            // 
            this.textBoxEmaThresholdMediumSlow.Location = new System.Drawing.Point(67, 43);
            this.textBoxEmaThresholdMediumSlow.Name = "textBoxEmaThresholdMediumSlow";
            this.textBoxEmaThresholdMediumSlow.Size = new System.Drawing.Size(60, 20);
            this.textBoxEmaThresholdMediumSlow.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Yellow;
            this.label5.Location = new System.Drawing.Point(6, 46);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "T2 ^ T3";
            // 
            // textBoxEmaThresholdFastMedium
            // 
            this.textBoxEmaThresholdFastMedium.Location = new System.Drawing.Point(67, 17);
            this.textBoxEmaThresholdFastMedium.Name = "textBoxEmaThresholdFastMedium";
            this.textBoxEmaThresholdFastMedium.Size = new System.Drawing.Size(60, 20);
            this.textBoxEmaThresholdFastMedium.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Yellow;
            this.label4.Location = new System.Drawing.Point(6, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "T1 ^ T2";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.buttonTestMacdStrategy);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(415, 169);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "MACD";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // buttonTestMacdStrategy
            // 
            this.buttonTestMacdStrategy.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.buttonTestMacdStrategy.Location = new System.Drawing.Point(89, 72);
            this.buttonTestMacdStrategy.Name = "buttonTestMacdStrategy";
            this.buttonTestMacdStrategy.Size = new System.Drawing.Size(115, 53);
            this.buttonTestMacdStrategy.TabIndex = 16;
            this.buttonTestMacdStrategy.Text = "Test Strategy";
            this.buttonTestMacdStrategy.UseVisualStyleBackColor = false;
            this.buttonTestMacdStrategy.Click += new System.EventHandler(this.buttonTestMacdStrategy_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.buttonTestEmaMacdStrategy);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(415, 169);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "E+M";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // buttonTestEmaMacdStrategy
            // 
            this.buttonTestEmaMacdStrategy.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.buttonTestEmaMacdStrategy.Location = new System.Drawing.Point(89, 72);
            this.buttonTestEmaMacdStrategy.Name = "buttonTestEmaMacdStrategy";
            this.buttonTestEmaMacdStrategy.Size = new System.Drawing.Size(115, 53);
            this.buttonTestEmaMacdStrategy.TabIndex = 17;
            this.buttonTestEmaMacdStrategy.Text = "Test Strategy";
            this.buttonTestEmaMacdStrategy.UseVisualStyleBackColor = false;
            this.buttonTestEmaMacdStrategy.Click += new System.EventHandler(this.buttonTestEmaMacdStrategy_Click);
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.graphPanelDistribution);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.graphPanelOverview);
            this.splitContainer3.Size = new System.Drawing.Size(423, 359);
            this.splitContainer3.SplitterDistance = 230;
            this.splitContainer3.TabIndex = 0;
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            this.splitContainer4.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.graphPanelCandle);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.splitContainer5);
            this.splitContainer4.Size = new System.Drawing.Size(635, 759);
            this.splitContainer4.SplitterDistance = 258;
            this.splitContainer4.TabIndex = 0;
            // 
            // splitContainer5
            // 
            this.splitContainer5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer5.Location = new System.Drawing.Point(0, 0);
            this.splitContainer5.Name = "splitContainer5";
            this.splitContainer5.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer5.Panel1
            // 
            this.splitContainer5.Panel1.Controls.Add(this.graphPanelRsi);
            // 
            // splitContainer5.Panel2
            // 
            this.splitContainer5.Panel2.Controls.Add(this.graphPanelMacd);
            this.splitContainer5.Size = new System.Drawing.Size(635, 497);
            this.splitContainer5.SplitterDistance = 267;
            this.splitContainer5.TabIndex = 0;
            // 
            // contextMenuStripGraphType
            // 
            this.contextMenuStripGraphType.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.scaleToolStripMenuItem,
            this.graphToolStripMenuItem});
            this.contextMenuStripGraphType.Name = "contextMenuStripGraphType";
            this.contextMenuStripGraphType.Size = new System.Drawing.Size(107, 48);
            // 
            // scaleToolStripMenuItem
            // 
            this.scaleToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dayToolStripMenuItem,
            this.hourToolStripMenuItem,
            this.minuteToolStripMenuItem});
            this.scaleToolStripMenuItem.Name = "scaleToolStripMenuItem";
            this.scaleToolStripMenuItem.Size = new System.Drawing.Size(106, 22);
            this.scaleToolStripMenuItem.Text = "Scale";
            // 
            // dayToolStripMenuItem
            // 
            this.dayToolStripMenuItem.Name = "dayToolStripMenuItem";
            this.dayToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.dayToolStripMenuItem.Tag = "";
            this.dayToolStripMenuItem.Text = "Day";
            this.dayToolStripMenuItem.Click += new System.EventHandler(this.dayToolStripMenuItem_Click);
            // 
            // hourToolStripMenuItem
            // 
            this.hourToolStripMenuItem.Name = "hourToolStripMenuItem";
            this.hourToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.hourToolStripMenuItem.Tag = "";
            this.hourToolStripMenuItem.Text = "Hour";
            this.hourToolStripMenuItem.Click += new System.EventHandler(this.hourToolStripMenuItem_Click);
            // 
            // minuteToolStripMenuItem
            // 
            this.minuteToolStripMenuItem.Enabled = false;
            this.minuteToolStripMenuItem.Name = "minuteToolStripMenuItem";
            this.minuteToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.minuteToolStripMenuItem.Tag = "";
            this.minuteToolStripMenuItem.Text = "Minute";
            this.minuteToolStripMenuItem.Click += new System.EventHandler(this.minuteToolStripMenuItem_Click);
            // 
            // graphToolStripMenuItem
            // 
            this.graphToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.candleToolStripMenuItem,
            this.eMAToolStripMenuItem});
            this.graphToolStripMenuItem.Name = "graphToolStripMenuItem";
            this.graphToolStripMenuItem.Size = new System.Drawing.Size(106, 22);
            this.graphToolStripMenuItem.Text = "Graph";
            // 
            // candleToolStripMenuItem
            // 
            this.candleToolStripMenuItem.Checked = true;
            this.candleToolStripMenuItem.CheckOnClick = true;
            this.candleToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.candleToolStripMenuItem.Name = "candleToolStripMenuItem";
            this.candleToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
            this.candleToolStripMenuItem.Text = "Candle";
            this.candleToolStripMenuItem.CheckedChanged += new System.EventHandler(this.candleToolStripMenuItem_CheckedChanged);
            // 
            // eMAToolStripMenuItem
            // 
            this.eMAToolStripMenuItem.Checked = true;
            this.eMAToolStripMenuItem.CheckOnClick = true;
            this.eMAToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.eMAToolStripMenuItem.Name = "eMAToolStripMenuItem";
            this.eMAToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
            this.eMAToolStripMenuItem.Text = "EMA";
            this.eMAToolStripMenuItem.CheckedChanged += new System.EventHandler(this.eMAToolStripMenuItem_CheckedChanged);
            // 
            // buttonLiveEmaStrategy
            // 
            this.buttonLiveEmaStrategy.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.buttonLiveEmaStrategy.Location = new System.Drawing.Point(145, 65);
            this.buttonLiveEmaStrategy.Name = "buttonLiveEmaStrategy";
            this.buttonLiveEmaStrategy.Size = new System.Drawing.Size(71, 42);
            this.buttonLiveEmaStrategy.TabIndex = 16;
            this.buttonLiveEmaStrategy.Text = "Live";
            this.buttonLiveEmaStrategy.UseVisualStyleBackColor = false;
            this.buttonLiveEmaStrategy.Click += new System.EventHandler(this.buttonLiveEmaStrategy_Click);
            // 
            // graphPanelDistribution
            // 
            this.graphPanelDistribution.BackColor = System.Drawing.Color.Black;
            this.graphPanelDistribution.Dock = System.Windows.Forms.DockStyle.Fill;
            this.graphPanelDistribution.EnablePanning = true;
            this.graphPanelDistribution.EnableZooming = true;
            this.graphPanelDistribution.Location = new System.Drawing.Point(0, 0);
            this.graphPanelDistribution.MaxX = 0D;
            this.graphPanelDistribution.MaxY = 0D;
            this.graphPanelDistribution.MinX = 0D;
            this.graphPanelDistribution.MinY = 0D;
            this.graphPanelDistribution.Name = "graphPanelDistribution";
            this.graphPanelDistribution.ShowXaxisLabel = true;
            this.graphPanelDistribution.ShowXGrid = true;
            this.graphPanelDistribution.ShowYaxisLabel = true;
            this.graphPanelDistribution.ShowYGrid = true;
            this.graphPanelDistribution.Size = new System.Drawing.Size(423, 230);
            this.graphPanelDistribution.TabIndex = 1;
            this.graphPanelDistribution.ViewScale = AkWidget.GraphPanel.EnumViewScale.HOUR;
            this.graphPanelDistribution.Paint += new System.Windows.Forms.PaintEventHandler(this.graphPanelDistribution_Paint);
            this.graphPanelDistribution.MouseMove += new System.Windows.Forms.MouseEventHandler(this.graphPanelDistribution_MouseMove);
            // 
            // graphPanelOverview
            // 
            this.graphPanelOverview.BackColor = System.Drawing.Color.Black;
            this.graphPanelOverview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.graphPanelOverview.EnablePanning = false;
            this.graphPanelOverview.EnableZooming = false;
            this.graphPanelOverview.Location = new System.Drawing.Point(0, 0);
            this.graphPanelOverview.MaxX = 0D;
            this.graphPanelOverview.MaxY = 0D;
            this.graphPanelOverview.MinX = 0D;
            this.graphPanelOverview.MinY = 0D;
            this.graphPanelOverview.Name = "graphPanelOverview";
            this.graphPanelOverview.ShowXaxisLabel = true;
            this.graphPanelOverview.ShowXGrid = true;
            this.graphPanelOverview.ShowYaxisLabel = true;
            this.graphPanelOverview.ShowYGrid = true;
            this.graphPanelOverview.Size = new System.Drawing.Size(423, 125);
            this.graphPanelOverview.TabIndex = 0;
            this.graphPanelOverview.ViewScale = AkWidget.GraphPanel.EnumViewScale.HOUR;
            this.graphPanelOverview.Paint += new System.Windows.Forms.PaintEventHandler(this.graphPanelOverview_Paint);
            this.graphPanelOverview.MouseClick += new System.Windows.Forms.MouseEventHandler(this.graphPanelOverview_MouseClick);
            // 
            // graphPanelCandle
            // 
            this.graphPanelCandle.BackColor = System.Drawing.Color.Black;
            this.graphPanelCandle.Controls.Add(this.labelBidAsk);
            this.graphPanelCandle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.graphPanelCandle.EnablePanning = true;
            this.graphPanelCandle.EnableZooming = true;
            this.graphPanelCandle.Location = new System.Drawing.Point(0, 0);
            this.graphPanelCandle.MaxX = 0D;
            this.graphPanelCandle.MaxY = 0D;
            this.graphPanelCandle.MinX = 0D;
            this.graphPanelCandle.MinY = 0D;
            this.graphPanelCandle.Name = "graphPanelCandle";
            this.graphPanelCandle.ShowXaxisLabel = true;
            this.graphPanelCandle.ShowXGrid = true;
            this.graphPanelCandle.ShowYaxisLabel = true;
            this.graphPanelCandle.ShowYGrid = true;
            this.graphPanelCandle.Size = new System.Drawing.Size(635, 258);
            this.graphPanelCandle.TabIndex = 0;
            this.graphPanelCandle.ViewScale = AkWidget.GraphPanel.EnumViewScale.HOUR;
            this.graphPanelCandle.Paint += new System.Windows.Forms.PaintEventHandler(this.graphPanelCandle_Paint);
            this.graphPanelCandle.MouseDown += new System.Windows.Forms.MouseEventHandler(this.graphPanelCandle_MouseDown);
            this.graphPanelCandle.MouseMove += new System.Windows.Forms.MouseEventHandler(this.graphPanelCandle_MouseMove);
            this.graphPanelCandle.MouseUp += new System.Windows.Forms.MouseEventHandler(this.graphPanelCandle_MouseUp);
            this.graphPanelCandle.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.graphPanelCandle_PreviewKeyDown);
            this.graphPanelCandle.Resize += new System.EventHandler(this.graphPanelCandle_Resize);
            // 
            // labelBidAsk
            // 
            this.labelBidAsk.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelBidAsk.AutoSize = true;
            this.labelBidAsk.BackColor = System.Drawing.Color.Yellow;
            this.labelBidAsk.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBidAsk.Location = new System.Drawing.Point(263, 22);
            this.labelBidAsk.Name = "labelBidAsk";
            this.labelBidAsk.Size = new System.Drawing.Size(109, 29);
            this.labelBidAsk.TabIndex = 0;
            this.labelBidAsk.Text = "123 | 456";
            // 
            // graphPanelRsi
            // 
            this.graphPanelRsi.BackColor = System.Drawing.Color.Black;
            this.graphPanelRsi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.graphPanelRsi.EnablePanning = false;
            this.graphPanelRsi.EnableZooming = true;
            this.graphPanelRsi.Location = new System.Drawing.Point(0, 0);
            this.graphPanelRsi.MaxX = 0D;
            this.graphPanelRsi.MaxY = 0D;
            this.graphPanelRsi.MinX = 0D;
            this.graphPanelRsi.MinY = 0D;
            this.graphPanelRsi.Name = "graphPanelRsi";
            this.graphPanelRsi.ShowXaxisLabel = true;
            this.graphPanelRsi.ShowXGrid = true;
            this.graphPanelRsi.ShowYaxisLabel = true;
            this.graphPanelRsi.ShowYGrid = true;
            this.graphPanelRsi.Size = new System.Drawing.Size(635, 267);
            this.graphPanelRsi.TabIndex = 0;
            this.graphPanelRsi.ViewScale = AkWidget.GraphPanel.EnumViewScale.HOUR;
            this.graphPanelRsi.Paint += new System.Windows.Forms.PaintEventHandler(this.graphPanelRsi_Paint);
            // 
            // graphPanelMacd
            // 
            this.graphPanelMacd.BackColor = System.Drawing.Color.Black;
            this.graphPanelMacd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.graphPanelMacd.EnablePanning = false;
            this.graphPanelMacd.EnableZooming = true;
            this.graphPanelMacd.Location = new System.Drawing.Point(0, 0);
            this.graphPanelMacd.MaxX = 0D;
            this.graphPanelMacd.MaxY = 0D;
            this.graphPanelMacd.MinX = 0D;
            this.graphPanelMacd.MinY = 0D;
            this.graphPanelMacd.Name = "graphPanelMacd";
            this.graphPanelMacd.ShowXaxisLabel = true;
            this.graphPanelMacd.ShowXGrid = true;
            this.graphPanelMacd.ShowYaxisLabel = true;
            this.graphPanelMacd.ShowYGrid = true;
            this.graphPanelMacd.Size = new System.Drawing.Size(635, 226);
            this.graphPanelMacd.TabIndex = 0;
            this.graphPanelMacd.ViewScale = AkWidget.GraphPanel.EnumViewScale.HOUR;
            this.graphPanelMacd.Paint += new System.Windows.Forms.PaintEventHandler(this.graphPanelMacd_Paint);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1062, 808);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MinimumSize = new System.Drawing.Size(800, 600);
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "AnalyTick";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormMain_FormClosed);
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.contextMenuStripMarker.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer6.Panel1.ResumeLayout(false);
            this.splitContainer6.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer6)).EndInit();
            this.splitContainer6.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage7.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            this.splitContainer5.Panel1.ResumeLayout(false);
            this.splitContainer5.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer5)).EndInit();
            this.splitContainer5.ResumeLayout(false);
            this.contextMenuStripGraphType.ResumeLayout(false);
            this.graphPanelCandle.ResumeLayout(false);
            this.graphPanelCandle.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButtonLoadDatabase;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.Timer timerGraph;
        private AkWidget.GraphPanel graphPanelCandle;
        private AkWidget.GraphPanel graphPanelOverview;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripMarker;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemMarkerDelete;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemMarkerRemarks;
        private System.Windows.Forms.ToolStripButton toolStripButtonSaveState;
        private System.Windows.Forms.ToolStripButton toolStripButtonLoadState;
        private AkWidget.GraphPanel graphPanelDistribution;
        private System.Windows.Forms.ToolStripButton toolStripButtonInformation;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.SplitContainer splitContainer5;
        private AkWidget.GraphPanel graphPanelRsi;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripGraphType;
        private System.Windows.Forms.ToolStripMenuItem scaleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem minuteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hourToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dayToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem graphToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem candleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eMAToolStripMenuItem;
        private AkWidget.GraphPanel graphPanelMacd;
        private System.Windows.Forms.ToolStripButton toolStripButtonClearSimulationRun;
        private System.Windows.Forms.ToolStripButton toolStripButtonZoomIn;
        private System.Windows.Forms.ToolStripButton toolStripButtonZoomOut;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button buttonTestEmaStrategy;
        private System.Windows.Forms.TextBox textBoxStopLoss;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxTakeProfit;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxEmaThresholdMediumSlow;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxEmaThresholdFastMedium;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.SplitContainer splitContainer6;
        private System.Windows.Forms.Button buttonTestMacdStrategy;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button buttonTestEmaMacdStrategy;
        private System.Windows.Forms.ToolStripButton toolStripButtonDownloadHistoryCandles;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelRateHeartbeat;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.ListView listViewAccount;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ToolStripButton toolStripButtonStartLiveStreaming;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelEventHeartbeat;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel4;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.ListView listViewResult;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.ListView listViewOpenTrade;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ToolStripButton toolStripButtonCloseAllPosition;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButtonCalculator;
        private System.Windows.Forms.Label labelBidAsk;
        private System.Windows.Forms.Button buttonLiveEmaStrategy;  
    }
}

