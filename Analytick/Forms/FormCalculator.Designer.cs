﻿namespace Analytick.Forms
{
    partial class FormCalculator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem(new string[] {
            "Margin",
            "50"}, -1);
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem(new System.Windows.Forms.ListViewItem.ListViewSubItem[] {
            new System.Windows.Forms.ListViewItem.ListViewSubItem(null, "Capital"),
            new System.Windows.Forms.ListViewItem.ListViewSubItem(null, "0", System.Drawing.SystemColors.WindowText, System.Drawing.SystemColors.Window, new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0))))}, -1);
            System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem(new string[] {
            "Pips",
            "0"}, -1);
            System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem(new string[] {
            "Profit",
            "0"}, -1);
            System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem(new string[] {
            "ROI",
            "0"}, -1);
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.listViewInvestment = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.textBoxProfit = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.radioButtonSell = new System.Windows.Forms.RadioButton();
            this.radioButtonBuy = new System.Windows.Forms.RadioButton();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxUnits = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxExitPrice = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxEntryPrice = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxConversionQuote = new System.Windows.Forms.TextBox();
            this.labelConversionQuote = new System.Windows.Forms.Label();
            this.comboBoxCurrencyPair = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.textBoxDateTimeString = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxSecondsSince1970 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(345, 273);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.listViewInvestment);
            this.tabPage1.Controls.Add(this.textBoxProfit);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.radioButtonSell);
            this.tabPage1.Controls.Add(this.radioButtonBuy);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.textBoxUnits);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.textBoxExitPrice);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.textBoxEntryPrice);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.textBoxConversionQuote);
            this.tabPage1.Controls.Add(this.labelConversionQuote);
            this.tabPage1.Controls.Add(this.comboBoxCurrencyPair);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.textBox1);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(337, 247);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Pip";
            // 
            // listViewInvestment
            // 
            this.listViewInvestment.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.listViewInvestment.FullRowSelect = true;
            this.listViewInvestment.GridLines = true;
            this.listViewInvestment.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            listViewItem2.UseItemStyleForSubItems = false;
            listViewItem5.UseItemStyleForSubItems = false;
            this.listViewInvestment.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1,
            listViewItem2,
            listViewItem3,
            listViewItem4,
            listViewItem5});
            this.listViewInvestment.Location = new System.Drawing.Point(172, 22);
            this.listViewInvestment.Name = "listViewInvestment";
            this.listViewInvestment.Size = new System.Drawing.Size(148, 212);
            this.listViewInvestment.TabIndex = 18;
            this.listViewInvestment.UseCompatibleStateImageBehavior = false;
            this.listViewInvestment.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Width = 70;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Width = 70;
            // 
            // textBoxProfit
            // 
            this.textBoxProfit.BackColor = System.Drawing.SystemColors.Control;
            this.textBoxProfit.ForeColor = System.Drawing.Color.Black;
            this.textBoxProfit.Location = new System.Drawing.Point(75, 214);
            this.textBoxProfit.Name = "textBoxProfit";
            this.textBoxProfit.ReadOnly = true;
            this.textBoxProfit.Size = new System.Drawing.Size(85, 20);
            this.textBoxProfit.TabIndex = 17;
            this.textBoxProfit.Text = "USD";
            this.textBoxProfit.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(8, 217);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 13);
            this.label10.TabIndex = 16;
            this.label10.Text = "Profit (USD)";
            // 
            // radioButtonSell
            // 
            this.radioButtonSell.AutoSize = true;
            this.radioButtonSell.ForeColor = System.Drawing.Color.Black;
            this.radioButtonSell.Location = new System.Drawing.Point(124, 105);
            this.radioButtonSell.Name = "radioButtonSell";
            this.radioButtonSell.Size = new System.Drawing.Size(42, 17);
            this.radioButtonSell.TabIndex = 15;
            this.radioButtonSell.Text = "Sell";
            this.radioButtonSell.UseVisualStyleBackColor = true;
            this.radioButtonSell.Click += new System.EventHandler(this.radioButtonSell_Click);
            // 
            // radioButtonBuy
            // 
            this.radioButtonBuy.AutoSize = true;
            this.radioButtonBuy.Checked = true;
            this.radioButtonBuy.ForeColor = System.Drawing.Color.Black;
            this.radioButtonBuy.Location = new System.Drawing.Point(75, 105);
            this.radioButtonBuy.Name = "radioButtonBuy";
            this.radioButtonBuy.Size = new System.Drawing.Size(43, 17);
            this.radioButtonBuy.TabIndex = 14;
            this.radioButtonBuy.TabStop = true;
            this.radioButtonBuy.Text = "Buy";
            this.radioButtonBuy.UseVisualStyleBackColor = true;
            this.radioButtonBuy.Click += new System.EventHandler(this.radioButtonBuy_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(8, 108);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "Action";
            // 
            // textBoxUnits
            // 
            this.textBoxUnits.Location = new System.Drawing.Point(75, 186);
            this.textBoxUnits.Name = "textBoxUnits";
            this.textBoxUnits.Size = new System.Drawing.Size(85, 20);
            this.textBoxUnits.TabIndex = 12;
            this.textBoxUnits.TextChanged += new System.EventHandler(this.textBoxUnits_TextChanged);
            this.textBoxUnits.Leave += new System.EventHandler(this.textBoxUnits_Leave);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(8, 189);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Units";
            // 
            // textBoxExitPrice
            // 
            this.textBoxExitPrice.Location = new System.Drawing.Point(75, 158);
            this.textBoxExitPrice.Name = "textBoxExitPrice";
            this.textBoxExitPrice.Size = new System.Drawing.Size(85, 20);
            this.textBoxExitPrice.TabIndex = 10;
            this.textBoxExitPrice.TextChanged += new System.EventHandler(this.textBoxExitPrice_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(8, 161);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(24, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Exit";
            // 
            // textBoxEntryPrice
            // 
            this.textBoxEntryPrice.Location = new System.Drawing.Point(75, 131);
            this.textBoxEntryPrice.Name = "textBoxEntryPrice";
            this.textBoxEntryPrice.Size = new System.Drawing.Size(85, 20);
            this.textBoxEntryPrice.TabIndex = 8;
            this.textBoxEntryPrice.TextChanged += new System.EventHandler(this.textBoxEntryPrice_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(8, 134);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Entry";
            // 
            // textBoxConversionQuote
            // 
            this.textBoxConversionQuote.Location = new System.Drawing.Point(75, 78);
            this.textBoxConversionQuote.Name = "textBoxConversionQuote";
            this.textBoxConversionQuote.Size = new System.Drawing.Size(85, 20);
            this.textBoxConversionQuote.TabIndex = 6;
            this.textBoxConversionQuote.Text = "1";
            this.textBoxConversionQuote.TextChanged += new System.EventHandler(this.textBoxConversionQuote_TextChanged);
            // 
            // labelConversionQuote
            // 
            this.labelConversionQuote.AutoSize = true;
            this.labelConversionQuote.ForeColor = System.Drawing.Color.Black;
            this.labelConversionQuote.Location = new System.Drawing.Point(8, 81);
            this.labelConversionQuote.Name = "labelConversionQuote";
            this.labelConversionQuote.Size = new System.Drawing.Size(30, 13);
            this.labelConversionQuote.TabIndex = 5;
            this.labelConversionQuote.Text = "---/---";
            // 
            // comboBoxCurrencyPair
            // 
            this.comboBoxCurrencyPair.FormattingEnabled = true;
            this.comboBoxCurrencyPair.Items.AddRange(new object[] {
            "EUR/USD"});
            this.comboBoxCurrencyPair.Location = new System.Drawing.Point(75, 49);
            this.comboBoxCurrencyPair.Name = "comboBoxCurrencyPair";
            this.comboBoxCurrencyPair.Size = new System.Drawing.Size(85, 21);
            this.comboBoxCurrencyPair.TabIndex = 4;
            this.comboBoxCurrencyPair.SelectedIndexChanged += new System.EventHandler(this.comboBoxCurrencyPair_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(8, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Pair";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.Control;
            this.textBox1.ForeColor = System.Drawing.Color.Black;
            this.textBox1.Location = new System.Drawing.Point(75, 22);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(85, 20);
            this.textBox1.TabIndex = 2;
            this.textBox1.Text = "USD";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(8, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Account";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.textBoxDateTimeString);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.textBoxSecondsSince1970);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(337, 247);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Time";
            // 
            // textBoxDateTimeString
            // 
            this.textBoxDateTimeString.Location = new System.Drawing.Point(91, 72);
            this.textBoxDateTimeString.Name = "textBoxDateTimeString";
            this.textBoxDateTimeString.Size = new System.Drawing.Size(140, 20);
            this.textBoxDateTimeString.TabIndex = 3;
            this.textBoxDateTimeString.TextChanged += new System.EventHandler(this.textBoxDateTimeString_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(8, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "DateTime";
            // 
            // textBoxSecondsSince1970
            // 
            this.textBoxSecondsSince1970.Location = new System.Drawing.Point(91, 33);
            this.textBoxSecondsSince1970.Name = "textBoxSecondsSince1970";
            this.textBoxSecondsSince1970.Size = new System.Drawing.Size(140, 20);
            this.textBoxSecondsSince1970.TabIndex = 1;
            this.textBoxSecondsSince1970.TextChanged += new System.EventHandler(this.textBoxSecondsSince1970_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(8, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "SecSince1970";
            // 
            // FormCalculator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(345, 273);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FormCalculator";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Calculator";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox textBoxSecondsSince1970;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxDateTimeString;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxCurrencyPair;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton radioButtonSell;
        private System.Windows.Forms.RadioButton radioButtonBuy;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxUnits;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxExitPrice;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxEntryPrice;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxConversionQuote;
        private System.Windows.Forms.Label labelConversionQuote;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxProfit;
        private System.Windows.Forms.ListView listViewInvestment;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
    }
}