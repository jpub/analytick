﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Web.Script.Serialization;

using AkBase;
using AkMessages;

namespace Analytick.Forms
{
    public partial class FormJsonEdit : Form
    {
        private JavaScriptSerializer serializer = new JavaScriptSerializer();
        private MessageQueue<BaseMessage> incomingQueue = null;
        private FactoryMessage factoryMessage = null;
        private MSG_DEBUG msgDebug = null;

        public FormJsonEdit( MessageQueue<BaseMessage> incomingQueue, FactoryMessage factoryMessage, MSG_DEBUG msg )
        {
            InitializeComponent();

            this.treeListView.CanExpandGetter = delegate( Object x )
            {
                if ( x is MSG_DEBUG )
                    return true;

                if ( x is Record )
                {
                    if ( ( (Record)x ).RecordList.Count > 0 )
                        return true;
                }

                return false;
            };

            this.treeListView.ChildrenGetter = delegate( Object x )
            {
                if ( x is MSG_DEBUG )
                    return ( (MSG_DEBUG)x ).RecordList;

                if ( x is Record )
                {
                    if ( ( (Record)x ).RecordList.Count > 0 )
                        return ( (Record)x ).RecordList;
                }
                throw new ArgumentException( "MSG_DEBUG or Record" );
            };

            this.incomingQueue = incomingQueue;
            this.factoryMessage = factoryMessage;
            this.msgDebug = msg;

            List<MSG_DEBUG> msgList = new List<MSG_DEBUG>();
            msgList.Add( msg );
            this.treeListView.SetObjects( msgList );
            this.treeListView.ExpandAll();
        }

        private void treeListView_CellEditFinishing( object sender, BrightIdeasSoftware.CellEditEventArgs e )
        {
            Record rec = (Record)e.RowObject;
            rec.Value = (string)e.NewValue;
            if ( this.msgDebug != null )
            {
                string messageType = "";
                Dictionary<string, object> jsonData = this.msgDebug.toJsonGraphDataOnly();

                if ( !jsonData.ContainsKey( "MessageType" ) )
                {
                    return;
                }
                messageType = (string)jsonData["MessageType"];

                string jsonString = this.serializer.Serialize( jsonData );
                BaseMessage message = this.factoryMessage.getMessage( messageType, jsonString );

                if ( message != null )
                {
                    this.incomingQueue.add( message );
                }
            }
        }

        private void treeListView_CellEditStarting( object sender, BrightIdeasSoftware.CellEditEventArgs e )
        {
            if ( e.RowObject is MSG_DEBUG )
            {
                e.Cancel = true;
            }

            if ( e.RowObject is Record )
            {
                Record rec = (Record)e.RowObject;
                if ( rec.RecordList.Count > 0 )
                {
                    e.Cancel = true;
                }
            }
        }
    }
}
