﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Analytick.Forms
{
    public partial class FormRemarks : Form
    {
        public string Remarks { get; set; }

        public FormRemarks()
        {
            InitializeComponent();
            this.Remarks = "";
        }

        private void textBoxRemarks_KeyDown( object sender, KeyEventArgs e )
        {
            if ( e.KeyCode == Keys.Enter )
            {
                this.Remarks = this.textBoxRemarks.Text;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else if ( e.KeyCode == Keys.Escape )
            {
                this.Remarks = "";
                this.DialogResult = DialogResult.Cancel;
                this.Close();
            }
        }
    }
}
