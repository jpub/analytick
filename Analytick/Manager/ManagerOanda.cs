﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using AkBase;
using AkCommon;
using AkOanda;
using AkMessages;
using AkDatabase;
using AkModel;
using AkAlgo;

using Analytick.Forms;

namespace Analytick.Manager
{
    class ManagerOanda
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger( System.Reflection.MethodBase.GetCurrentMethod().DeclaringType );        

        private static ManagerOanda msInstance = null;
        private OandaStaticApi oandaStaticApi = null;
        private OandaStreamApi oandaStreamApi = null;
        private MessageQueue<BaseMessage> incomingQueue = null;
        private FormMain formMain = null;

        private bool hasUpdateHistoryFirstRun = false;
        private DateTime lastHistoryUpdatedDt = DateTime.MinValue;

        private Strategy liveStrategy = null;

        private ManagerOanda()
        {
        }

        static public ManagerOanda getInstance()
        {
            if ( msInstance == null )
            {
                msInstance = new ManagerOanda();
                return msInstance;
            }
            return msInstance;
        }

        public void init( MessageQueue<BaseMessage> incomingQueue, FormMain formMain )
        {
            this.incomingQueue = incomingQueue;
            this.formMain = formMain;
            this.oandaStaticApi = new OandaStaticApi( this.incomingQueue, GlobalConstant.API_SERVER, GlobalConstant.ACCOUNT_ID, GlobalConstant.ACCESS_TOKEN );
            this.oandaStreamApi = new OandaStreamApi( this.incomingQueue, GlobalConstant.API_SERVER, GlobalConstant.ACCOUNT_ID, GlobalConstant.ACCESS_TOKEN );
        }

        public void abortAPI()
        {
            this.oandaStaticApi.abortAPI();
            this.oandaStreamApi.abortAPI();
        }

        public void setLiveStrategy( Strategy inStrategy )
        {
            this.liveStrategy = inStrategy;
            // check the status of the last candle and get ready to enter the market
            ModelCandleGraph model = null;
            if ( !DbCandleGraph.getInstance().get( GlobalConstant.GRAPH_NAME_CANDLE_HOUR, out model ) )
                return;

            ModelCandleGraph.CandleData lastCd = model.getRefListCandleData().Last();
            this.liveStrategy.incomingCandle( lastCd );
        }

        public void updateDisconnectedDetails( MessageStreamDisconnectedTick message )
        {
            string displayString = message.disconnect.code + Environment.NewLine + message.disconnect.message + Environment.NewLine + message.disconnect.moreInfo;
            this.formMain.toggleLabelRateHeartbeatColor( false );

            MessageBox.Show( displayString, "Ticks Disconnected", MessageBoxButtons.OK, MessageBoxIcon.Exclamation );
        }

        public void updateDisconnectedDetails( MessageStreamDisconnectedEvent message )
        {
            string displayString = message.disconnect.code + Environment.NewLine + message.disconnect.message + Environment.NewLine + message.disconnect.moreInfo;
            this.formMain.toggleLabelEventHeartbeatColor( false );

            MessageBox.Show( displayString, "Events Disconnected", MessageBoxButtons.OK, MessageBoxIcon.Exclamation );
        }

        public void updateProgressFeedback( MessageProgressFeedback message )
        {
            this.formMain.updateProgressFeedback( message );
        }

        public void sendAccountDetails()
        {
            this.oandaStaticApi.getAccountDetails();
        }

        public void updateAccountDetails( MessageStaticAccountDetails message )
        {
            this.formMain.updateAccountDetails( message );
            GlobalConstant.ALLOW_TO_TRADE = message.openTrades == 0 ? true : false;
        }

        public void sendCreateNewOrder( uint tradeUnits, string side, double price )
        {
            if ( !GlobalConstant.ALLOW_TO_TRADE )
            {
                log.Error( "Not allow to trade!!!" );
                return;
            }

            if ( GlobalConstant.ALLOW_TO_TRADE )
                GlobalConstant.ALLOW_TO_TRADE = false;

            double stopLossPrice = 0;
            double takeProfitPrice = 0;
            if ( price != 0 )
            {
                if ( side == "buy" )
                {
                    stopLossPrice = price - GlobalConstant.DEFAULT_TP_SL_THRESHOLD * GlobalConstant.ONE_PIP_PRICE;
                    takeProfitPrice = price + GlobalConstant.DEFAULT_TP_SL_THRESHOLD * GlobalConstant.ONE_PIP_PRICE;
                }
                else
                {
                    stopLossPrice = price + GlobalConstant.DEFAULT_TP_SL_THRESHOLD * GlobalConstant.ONE_PIP_PRICE;
                    takeProfitPrice = price - GlobalConstant.DEFAULT_TP_SL_THRESHOLD * GlobalConstant.ONE_PIP_PRICE;
                }
            }

            this.oandaStaticApi.createNewOrder( GlobalConstant.INSTRUMENT, tradeUnits, side, stopLossPrice, takeProfitPrice, price );
        }

        public void updateCreateNewOrder( MessageStaticCreateNewOrder message )
        {            
            this.formMain.updateOpenTrade( message );

            string tradeSide = message.tradeOpened.side;
            double marketPrice = message.price;
            double stopLossPrice = 0;
            double takeProfitPrice = 0;

            if ( tradeSide == "buy" )
            {
                stopLossPrice = marketPrice - GlobalConstant.STRATEGY_STOP_LOSS * GlobalConstant.ONE_PIP_PRICE;
                takeProfitPrice = marketPrice + GlobalConstant.STRATEGY_TAKE_PROFIT * GlobalConstant.ONE_PIP_PRICE;
            }
            else
            {
                stopLossPrice = marketPrice + GlobalConstant.STRATEGY_STOP_LOSS * GlobalConstant.ONE_PIP_PRICE;
                takeProfitPrice = marketPrice - GlobalConstant.STRATEGY_TAKE_PROFIT * GlobalConstant.ONE_PIP_PRICE;
            }

            if ( this.liveStrategy != null )
                this.liveStrategy.incomingTradeUpdate( message );

            this.sendModifyExistingTrade( message.tradeOpened.id, stopLossPrice, takeProfitPrice );
        }

        public void sendModifyExistingTrade( long tradeId, double stopLossPrice, double takeProfitPrice )
        {
            this.oandaStaticApi.modifyAnExistingTrade( tradeId, stopLossPrice, takeProfitPrice );
        }

        public void updateModifyExistingTrade( MessageStaticModifyExistingTrade message )
        {
            this.formMain.updateOpenTrade( message );
            this.updateModelActiveTradeParams( message.side, message.units, message.price, message.takeProfit, message.stopLoss );

            if ( this.liveStrategy != null )
                this.liveStrategy.incomingTradeUpdate( message );
        }

        public void sendOpenTradeList()
        {
            this.oandaStaticApi.getListOfOpenTrades();
        }

        public void updateOpenTradeList( MessageStaticOpenTradeList message )
        {
            if ( message.trades.Count >=2 )
            {
                MessageBox.Show( "Unable to handle 2 or more trades!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error );
                return;
            }

            if ( message.trades.Count == 1 )
            {
                DataTrade dataTrade = message.trades[0];
                this.formMain.updateOpenTrade( dataTrade );
                this.updateModelActiveTradeParams( dataTrade.side, dataTrade.units, dataTrade.price, dataTrade.takeProfit, dataTrade.stopLoss );
            }
        }

        public void sendClosePosition()
        {
            this.oandaStaticApi.closeAnExistingPosition( GlobalConstant.INSTRUMENT );
        }

        public void updateClosePosition( MessageStaticClosePosition message )
        {
            // let streaming event handle this
            //this.formMain.updateClosePosition();
            //this.oandaStaticApi.getAccountDetails();
            //this.updateModelActiveTradeParams( "", 0, 0, 0, 0 );            
        }

        public void startLiveStreaming()
        {
            this.oandaStreamApi.startStreamingRates( GlobalConstant.INSTRUMENT );
            this.oandaStreamApi.startStreamingEvents();            
        }

        public void toggleRateHeartbeat()
        {
            this.formMain.toggleLabelRateHeartbeatColor( true );
        }

        public void toggleEventHeartbeat()
        {
            this.formMain.toggleLabelEventHeartbeatColor( true );
        }

        public void updateTick( MessageStreamTick message )
        {
            if ( this.liveStrategy != null )
                this.liveStrategy.incomingTick( message );

            ModelActiveTrade modelCurrentTick = null;
            DbActiveTrade.getInstance().get( GlobalConstant.DB_NAME_ACTIVE_TRADE, out modelCurrentTick );                

            modelCurrentTick.TickTime = message.tick.time;
            modelCurrentTick.Bid = message.tick.bid;
            modelCurrentTick.Ask = message.tick.ask;

            DbActiveTrade.getInstance().update( GlobalConstant.DB_NAME_ACTIVE_TRADE, modelCurrentTick );
            if ( hasUpdateHistoryFirstRun )
            {
                if ( message.tick.time.Hour != this.lastHistoryUpdatedDt.Hour )
                {
                    this.lastHistoryUpdatedDt = DateTime.Now;
                    this.sendHistoryDayAndHourCandle();
                }
            }
            else
            {
                this.lastHistoryUpdatedDt = DateTime.Now;
                this.hasUpdateHistoryFirstRun = true;
                this.sendHistoryDayAndHourCandle();                
            }
            this.formMain.updateLabelBidAsk( message );
        }

        public void handleIncomingEvent( MessageStreamEvent message )
        {
            // possible events are:
            /* MARKET_ORDER_CREATE
             * STOP_ORDER_CREATE
             * LIMIT_ORDER_CREATE
             * MARKET_IF_TOUCHED_ORDER_CREATE
             * ORDER_UPDATE
             * ORDER_CANCEL
             * ORDER_FILLED
             * TRADE_UPDATE
             * TRADE_CLOSE
             * MIGRATE_TRADE_CLOSE
             * MIGRATE_TRADE_OPEN
             * TAKE_PROFIT_FILLED
             * STOP_LOSS_FILLED
             * TRAILING_STOP_FILLED
             * MARGIN_CALL_ENTER
             * MARGIN_CALL_EXIT
             * MARGIN_CLOSEOUT
             * SET_MARGIN_RATE
             * TRANSFER_FUNDS
             * DAILY_INTEREST
             * FEE
             */
            if ( message.transaction.type == "MARKET_ORDER_CREATE" )
            {
                this.oandaStaticApi.getAccountDetails();
            }

            if ( message.transaction.type == "TRADE_CLOSE" || message.transaction.type == "TAKE_PROFIT_FILLED" || message.transaction.type == "STOP_LOSS_FILLED" )
            {                
                if ( message.transaction.type == "TRADE_CLOSE" )
                {
                    if ( this.liveStrategy != null )
                        this.liveStrategy.incomingTradeClose( message );
                }
                else if ( message.transaction.type == "TAKE_PROFIT_FILLED" )
                {
                    if ( this.liveStrategy != null )
                        this.liveStrategy.incomingTakeProfit( message );
                }
                else if ( message.transaction.type == "STOP_LOSS_FILLED" )
                {
                    if ( this.liveStrategy != null )
                        this.liveStrategy.incomingStopLoss( message );
                }

                this.formMain.updateClosePosition();
                this.oandaStaticApi.getAccountDetails();
                this.updateModelActiveTradeParams( "", 0, 0, 0, 0 );
            }
        }

        public void updateCandleList( MessageStaticCandleList message )
        {
            List<DataCandle> candleList = new List<DataCandle>();
            candleList.AddRange( message.candles );

            if ( message.granularity == "H1" )
            {
                ModelCandleGraph model = null;
                if ( !DbCandleGraph.getInstance().get( GlobalConstant.GRAPH_NAME_CANDLE_HOUR, out model ) )
                    return;

                foreach ( DataCandle dc in candleList )
                {
                    if ( dc.complete == false )
                        break;

                    if ( GlobalConstant.IS_SAVE_INCOMING_HISTORY_CANDLE_TO_SQLITE )
                        DbOanda.getInstance().insertHourCandle( (int)AkUtil.getSecondsSince1970( dc.time ), dc.openMid, dc.highMid, dc.lowMid, dc.closeMid );

                    ModelCandleGraph.CandleData cd = new ModelCandleGraph.CandleData();
                    cd.xValue = dc.time.Ticks;
                    cd.highMid = dc.highMid;
                    cd.lowMid = dc.lowMid;
                    cd.openMid = dc.openMid;
                    cd.closeMid = dc.closeMid;
                    model.add( cd );

                    if ( this.liveStrategy != null )
                        this.liveStrategy.incomingCandle( cd );
                }
                DbCandleGraph.getInstance().update( GlobalConstant.GRAPH_NAME_CANDLE_HOUR, model );
            }
            else if ( message.granularity == "D" )
            {
                ModelCandleGraph model = null;
                if ( !DbCandleGraph.getInstance().get( GlobalConstant.GRAPH_NAME_CANDLE_DAY, out model ) )
                    return;

                foreach ( DataCandle dc in candleList )
                {
                    if ( dc.complete == false )
                        break;

                    if ( GlobalConstant.IS_SAVE_INCOMING_HISTORY_CANDLE_TO_SQLITE )
                        DbOanda.getInstance().insertDayCandle( (int)AkUtil.getSecondsSince1970( dc.time ), dc.openMid, dc.highMid, dc.lowMid, dc.closeMid );

                    ModelCandleGraph.CandleData cd = new ModelCandleGraph.CandleData();
                    cd.xValue = dc.time.Ticks;
                    cd.highMid = dc.highMid;
                    cd.lowMid = dc.lowMid;
                    cd.openMid = dc.openMid;
                    cd.closeMid = dc.closeMid;
                    model.add( cd );
                }
                DbCandleGraph.getInstance().update( GlobalConstant.GRAPH_NAME_CANDLE_DAY, model );
            }

            this.formMain.updateAllGraphValues();            
        }

        private void sendHistoryDayAndHourCandle()
        {
            // update hour candle
            int secondsSince1970 = DbOanda.getInstance().getLatestHourCandleTime();
            DateTime startTimeHour = AkUtil.getDateTime( secondsSince1970 );
            startTimeHour = startTimeHour.AddHours( -8 );
            startTimeHour = startTimeHour.AddHours( 1 );
            this.oandaStaticApi.getHistoricalCandles( GlobalConstant.INSTRUMENT, 500, startTimeHour, "H1" );

            // update day candle
            secondsSince1970 = DbOanda.getInstance().getLatestHourCandleTime();
            DateTime startTimeHourDay = AkUtil.getDateTime( secondsSince1970 );
            startTimeHourDay = startTimeHourDay.AddHours( -8 );
            startTimeHourDay = startTimeHourDay.AddDays( 1 );
            this.oandaStaticApi.getHistoricalCandles( GlobalConstant.INSTRUMENT, 500, startTimeHourDay, "D" );
        }

        private void updateModelActiveTradeParams( string side, int units, double entryPrice, double takeProfit, double stopLoss )
        {
            log.Debug( "updating active trade" );
            ModelActiveTrade model = null;
            DbActiveTrade.getInstance().get( GlobalConstant.DB_NAME_ACTIVE_TRADE, out model );

            model.Side = side;
            model.Units = units;
            model.EntryPrice = entryPrice;
            model.TakeProfit = takeProfit;
            model.StopLoss = stopLoss;
            DbActiveTrade.getInstance().update( GlobalConstant.DB_NAME_ACTIVE_TRADE, model );
        }
    }
}
