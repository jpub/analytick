﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AkBase;
using AkModel;
using AkView;
using AkWidget;
using AkCommon;
using AkDatabase;
using AkAlgo;

namespace Analytick.Observer
{
    class ObserverGraphPanelCandle : IBaseObserver
    {
        private GraphPanel graphPanel = null;

        public ObserverGraphPanelCandle( GraphPanel graphPanel )
        {
            this.graphPanel = graphPanel;            
        }

        public void update( string action, object subject )
        {
            if ( action == ModelUserInterfaceParam.DB_ACTION_UPDATE_OVERVIEW_CANDLE_VISUAL_X )
            {
                ModelUserInterfaceParam model = (ModelUserInterfaceParam)subject;
                this.graphPanel.MinX = model.OverviewCandleMinX;
                this.graphPanel.MaxX = model.OverviewCandleMaxX;
                this.updateMinMaxY();
            }            
        }

        private void updateMinMaxY()
        {
            if ( DbCandleGraph.getInstance().count() == 0 )
                return;

            ModelCandleGraph model = null;
            List<ModelCandleGraph.CandleData> listCandleData = null;

            int totalBar = (int)( this.graphPanel.MaxX - this.graphPanel.MinX );

            if ( this.graphPanel.ViewScale == GraphPanel.EnumViewScale.MINUTE )
            {
                DbCandleGraph.getInstance().get( GlobalConstant.GRAPH_NAME_CANDLE_MINUTE, out model );
            }
            else if ( this.graphPanel.ViewScale == GraphPanel.EnumViewScale.HOUR )
            {
                DbCandleGraph.getInstance().get( GlobalConstant.GRAPH_NAME_CANDLE_HOUR, out model );
            }
            else if ( this.graphPanel.ViewScale == GraphPanel.EnumViewScale.DAY )
            {
                DbCandleGraph.getInstance().get( GlobalConstant.GRAPH_NAME_CANDLE_DAY, out model );
            }

            listCandleData = model.getRefListCandleData();
            double minY = double.MaxValue;
            double maxY = double.MinValue;
            int candleIndex = -1;
            foreach ( ModelCandleGraph.CandleData candleData in listCandleData )
            {
                ++candleIndex;
                if ( candleIndex < this.graphPanel.MinX || candleIndex > this.graphPanel.MaxX )
                    continue;

                minY = candleData.lowMid < minY ? candleData.lowMid : minY;
                maxY = candleData.highMid > maxY ? candleData.highMid : maxY;
            }
            double paddingY = ( maxY - minY ) * 0.2;
            this.graphPanel.MinY = minY - paddingY;
            this.graphPanel.MaxY = maxY + paddingY;
        }
    }
}