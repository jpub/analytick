﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AkBase;
using AkModel;
using AkView;
using AkWidget;

namespace Analytick.Observer
{
    class ObserverGraphPanelRsi : IBaseObserver
    {
        private GraphPanel graphPanel = null;

        public ObserverGraphPanelRsi( GraphPanel graphPanel )
        {
            this.graphPanel = graphPanel;            
        }

        public void update( string action, object subject )
        {
            if ( action == ModelUserInterfaceParam.DB_ACTION_UPDATE_MAIN_CANDLE_MIN_MAX_X )
            {
                ModelUserInterfaceParam model = (ModelUserInterfaceParam)subject;
                this.graphPanel.MinX = model.MainCandleMinX;
                this.graphPanel.MaxX = model.MainCandleMaxX;
            }
            else if ( action == ModelUserInterfaceParam.DB_ACTION_UPDATE_OVERVIEW_CANDLE_VISUAL_X )
            {
                ModelUserInterfaceParam model = (ModelUserInterfaceParam)subject;
                this.graphPanel.MinX = model.OverviewCandleMinX;
                this.graphPanel.MaxX = model.OverviewCandleMaxX;
            }   
        }
    }
}