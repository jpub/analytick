﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AkBase;
using AkModel;
using AkView;
using AkWidget;
using AkCommon;
using AkDatabase;
using AkAlgo;

namespace Analytick.Observer
{
    class ObserverGraphPanelMacd : IBaseObserver
    {
        private GraphPanel graphPanel = null;

        public ObserverGraphPanelMacd( GraphPanel graphPanel )
        {
            this.graphPanel = graphPanel;            
        }

        public void update( string action, object subject )
        {
            if ( action == ModelUserInterfaceParam.DB_ACTION_UPDATE_MAIN_CANDLE_MIN_MAX_X )
            {
                ModelUserInterfaceParam model = (ModelUserInterfaceParam)subject;
                this.graphPanel.MinX = model.MainCandleMinX;
                this.graphPanel.MaxX = model.MainCandleMaxX;
                this.updateMinMaxY();
            }
            else if ( action == ModelUserInterfaceParam.DB_ACTION_UPDATE_OVERVIEW_CANDLE_VISUAL_X )
            {
                ModelUserInterfaceParam model = (ModelUserInterfaceParam)subject;
                this.graphPanel.MinX = model.OverviewCandleMinX;
                this.graphPanel.MaxX = model.OverviewCandleMaxX;
                this.updateMinMaxY();
            }   
        }

        private void updateMinMaxY()
        {
            ModelLineGraph model = null;
            if ( !DbLineGraph.getInstance().get( GlobalConstant.GRAPH_NAME_LINE_MACD, out model ) )
                return;

            List<ModelLineGraph.LineData> listLineData = model.getCopyListLineData();

            double minY = double.MaxValue;
            double maxY = double.MinValue;
            int candleIndex = -1;
            foreach ( ModelLineGraph.LineData ld in listLineData )
            {
                if ( ld.yValue == AlgoMacd.UNDEFINE_VALUE )
                    continue;

                ++candleIndex;
                if ( candleIndex < this.graphPanel.MinX || candleIndex > this.graphPanel.MaxX )
                    continue;

                minY = ld.yValue < minY ? ld.yValue : minY;
                maxY = ld.yValue > maxY ? ld.yValue : maxY;
            }

            this.graphPanel.MinY = minY;
            this.graphPanel.MaxY = maxY;
        }
    }
}