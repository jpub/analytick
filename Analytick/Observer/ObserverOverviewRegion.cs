﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AkBase;
using AkModel;
using AkView;

namespace Analytick.Observer
{
    class ObserverOverviewRegion : IBaseObserver
    {
        private ViewOverviewRegion viewOverviewRegion = null;

        public ObserverOverviewRegion( ViewOverviewRegion viewOverviewRegion )
        {
            this.viewOverviewRegion = viewOverviewRegion;            
        }

        public void update( string action, object subject )
        {
            if ( action == ModelUserInterfaceParam.DB_ACTION_UPDATE_MAIN_CANDLE_MIN_MAX_X )
            {
                ModelUserInterfaceParam model = (ModelUserInterfaceParam)subject;
                this.viewOverviewRegion.LeftX = model.MainCandleMinX;
                this.viewOverviewRegion.RightX = model.MainCandleMaxX;
            }            
        }
    }
}