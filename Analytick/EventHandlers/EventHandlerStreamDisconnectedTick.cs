﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AkBase;
using AkMessages;

using Analytick.Manager;

namespace Analytick.EventHandlers
{
    class EventHandlerStreamDisconnectedTick : BaseEventHandler
    {
        override public void handle( BaseMessage baseMessage )
        {
            MessageStreamDisconnectedTick message = (MessageStreamDisconnectedTick)baseMessage;
            ManagerOanda.getInstance().updateDisconnectedDetails( message );
        }
    }
}
