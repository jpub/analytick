﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AkBase;
using AkMessages;

using Analytick.Manager;

namespace Analytick.EventHandlers
{
    class EventHandlerStaticCandleList : BaseEventHandler
    {
        override public void handle( BaseMessage baseMessage )
        {
            MessageStaticCandleList message = (MessageStaticCandleList)baseMessage;
            ManagerOanda.getInstance().updateCandleList( message );
        }
    }
}
