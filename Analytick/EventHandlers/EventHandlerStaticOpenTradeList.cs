﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AkBase;
using AkMessages;

using Analytick.Manager;

namespace Analytick.EventHandlers
{
    class EventHandlerStaticOpenTradeList : BaseEventHandler
    {
        override public void handle( BaseMessage baseMessage )
        {
            MessageStaticOpenTradeList message = (MessageStaticOpenTradeList)baseMessage;
            ManagerOanda.getInstance().updateOpenTradeList( message );
        }
    }
}
