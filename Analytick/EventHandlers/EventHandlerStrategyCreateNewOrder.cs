﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AkBase;
using AkMessages;

using Analytick.Manager;

namespace Analytick.EventHandlers
{
    class EventHandlerStrategyCreateNewOrder : BaseEventHandler
    {
        override public void handle( BaseMessage baseMessage )
        {
            MessageStrategyCreateNewOrder message = (MessageStrategyCreateNewOrder)baseMessage;
            ManagerOanda.getInstance().sendCreateNewOrder( message.units, message.side, message.price );
        }
    }
}
