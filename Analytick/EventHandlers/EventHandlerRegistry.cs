﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AkMessages;
using AkBase;

namespace Analytick.EventHandlers
{
    class EventHandlerRegistry : BaseEventHandlerRegistry
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger( System.Reflection.MethodBase.GetCurrentMethod().DeclaringType );        

        public EventHandlerRegistry()
        {
            this.registryMap = new Dictionary<string, BaseEventHandler>();

            this.registryMap.Add( BaseMessage.ID_PROGRESS_FEEDBACK, new EventHandlerProgressFeedback() );
            this.registryMap.Add( BaseMessage.ID_STRATEGY_CREATE_NEW_ORDER, new EventHandlerStrategyCreateNewOrder() );

            this.registryMap.Add( BaseMessage.ID_DISCONNECTED_TICK, new EventHandlerStreamDisconnectedTick() );
            this.registryMap.Add( BaseMessage.ID_DISCONNECTED_EVENT, new EventHandlerStreamDisconnectedEvent() );            
            this.registryMap.Add( BaseMessage.ID_HEARTBEAT_TICK, new EventHandlerStreamHeartbeatTick() );
            this.registryMap.Add( BaseMessage.ID_HEARTBEAT_EVENT, new EventHandlerStreamHeartbeatEvent() );
            this.registryMap.Add( BaseMessage.ID_TICK, new EventHandlerStreamTick() );
            this.registryMap.Add( BaseMessage.ID_EVENT, new EventHandlerStreamEvent() );

            this.registryMap.Add( BaseMessage.ID_ACCOUNT_DETAILS, new EventHandlerStaticAccountDetails() );
            this.registryMap.Add( BaseMessage.ID_CANDLE_LIST, new EventHandlerStaticCandleList() );
            this.registryMap.Add( BaseMessage.ID_CREATE_NEW_ORDER, new EventHandlerStaticCreateNewOrder() );
            this.registryMap.Add( BaseMessage.ID_MODIFY_EXISTING_TRADE, new EventHandlerStaticModifyExistingTrade() );
            this.registryMap.Add( BaseMessage.ID_OPEN_TRADE_LIST, new EventHandlerStaticOpenTradeList() );
            this.registryMap.Add( BaseMessage.ID_CLOSE_POSITION, new EventHandlerStaticClosePosition() );            
        }

        override public bool handle( BaseMessage message )
        {
            if ( !this.registryMap.ContainsKey( message.MessageType ) )
            {
                log.Warn( "No event handler for:" + message.MessageType );
                return false;
            }

            this.registryMap[message.MessageType].handle( message );
            return true;
        }
    }
}
