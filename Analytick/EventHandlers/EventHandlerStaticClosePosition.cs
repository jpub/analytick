﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AkBase;
using AkMessages;

using Analytick.Manager;

namespace Analytick.EventHandlers
{
    class EventHandlerStaticClosePosition : BaseEventHandler
    {
        override public void handle( BaseMessage baseMessage )
        {
            MessageStaticClosePosition message = (MessageStaticClosePosition)baseMessage;
            ManagerOanda.getInstance().updateClosePosition( message );
        }
    }
}
