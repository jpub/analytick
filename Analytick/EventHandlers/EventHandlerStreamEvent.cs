﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AkBase;
using AkMessages;

using Analytick.Manager;

namespace Analytick.EventHandlers
{
    class EventHandlerStreamEvent : BaseEventHandler
    {
        override public void handle( BaseMessage baseMessage )
        {
            MessageStreamEvent message = (MessageStreamEvent)baseMessage;
            ManagerOanda.getInstance().handleIncomingEvent( message );
        }
    }
}
