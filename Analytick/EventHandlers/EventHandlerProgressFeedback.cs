﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AkBase;
using AkMessages;

using Analytick.Manager;

namespace Analytick.EventHandlers
{
    class EventHandlerProgressFeedback : BaseEventHandler
    {
        override public void handle( BaseMessage baseMessage )
        {
            MessageProgressFeedback message = (MessageProgressFeedback)baseMessage;
            ManagerOanda.getInstance().updateProgressFeedback( message );
        }
    }
}
